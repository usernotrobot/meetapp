const elixir = require('laravel-elixir');
require('laravel-elixir-vue-2');

var shell = require('gulp-shell');
var Task = elixir.Task;
elixir.extend('langjs', function(path) {
  new Task('langjs', function() {
    gulp.src('').pipe(shell('php artisan lang:js ' + path));
  });
});

/*elixir.extend('lastversion', function(path) {
  new Task('lastversion', function () {
    gulp.task('lastversion', shell.task("git describe --tags > " + path));
  });
});*/

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(mix => {
  mix
    .sass([
      '../../../node_modules/font-awesome/scss/font-awesome.scss',
      '../../../node_modules/bootstrap/scss/bootstrap.scss',
      'mdb.scss',
      'app.scss'
    ])
    .webpack([
        'app.js',
      ],
      'public/js/app.js')
    .langjs('public/js/messages.js')
    .scripts([
        'mdb.min.js',
        'mdb/*',
        'jquery/jquery.dataTables.min.js',
        'jquery/dataTables.bootstrap4.min.js',
        'jquery/validate/jquery.validate.min.js',
        'jquery/validate/additional-methods.js',
        '../../../public/js/messages.js',
        'custom.js'
      ],
      'public/js/scripts.js')
    .copy('resources/assets/img', 'public/img')
    .copy('resources/assets/icons', 'public/icons')
    .copy('resources/assets/font', 'public/build/font')
    .copy('node_modules/font-awesome/fonts', 'public/build/fonts')
    // .lastversion('public/version.js')
    .version(['version.js', 'js/app.js', 'js/scripts.js', 'css/app.css']);
});
