#!/bin/bash

php artisan down
php composer.phar install
php artisan migrate

npm run dev

php artisan config:clear
php artisan route:clear
php artisan view:clear
php artisan cache:clear

php artisan up
