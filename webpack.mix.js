const { mix } = require('laravel-mix');
const webpack = require('webpack');
const WebpackShellPlugin = require('webpack-shell-plugin');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
  .webpackConfig({
    plugins: [
      new WebpackShellPlugin({
        onBuildStart: ['php artisan lang:js resources/assets/js/messages.js --quiet'],
      }),
    ]
  })
  .sass('resources/assets/sass/app.scss', 'public/css')
  .sass('resources/assets/sass/now.scss', 'public/css')
  .sass('resources/assets/sass/moderation.scss', 'public/css')
  .sass('resources/assets/sass/admin.scss', 'public/css')
  .sass('resources/assets/sass/delegate.scss', 'public/css')
  .js('resources/assets/js/app.js', 'public/js')
  .js('resources/assets/js/min.js', 'public/js')
  .js('resources/assets/js/now.js', 'public/js')
  .js('resources/assets/js/moderation.js', 'public/js')
  .js('resources/assets/js/headline-topics.js', 'public/js')
  .js('resources/assets/js/headline-edit.js', 'public/js')
  .copy('resources/assets/icons', 'public/icons')
  .copy('resources/assets/img/!*.png', 'public/images')
  .version();

