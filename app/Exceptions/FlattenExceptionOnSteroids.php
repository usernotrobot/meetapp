<?php

namespace App\Exceptions;

use \Symfony\Component\Debug\Exception\FlattenException;

class FlattenExceptionOnSteroids extends FlattenException {

  public function getClassName() {
    return array_slice(explode('\\', $this->getClass()), -1, 1)[0];
  }

  public function getTraceAsArray() {
    $retArr = [];

    foreach ($this->toArray()[0]['trace'] as $id => $trace) {
      $tracestring = [];

      if (isset($trace['file']) && isset($trace['line'])) {
        $tracestring['file'] = $this->escapeHtml($trace['file']);
        $tracestring['line'] = $trace['line'];
      }

      if ($trace['function']) {
        $tracestring['function'] = sprintf('%s%s%s(%s)', $trace['class'], $trace['type'],
                                           $trace['function'], $this->formatArgs($trace['args']));
      }

      $retArr[$id] = $tracestring;
    }

    return $retArr;
  }

  private function formatClass($class) {
    $parts = explode('\\', $class);

    return sprintf('<abbr title="%s">%s</abbr>', $class, array_pop($parts));
  }

  /**
   * Formats an array as a string.
   *
   * @param array $args The argument array
   *
   * @return string
   */
  private function formatArgs(array $args) {
    $result = array();
    foreach ($args as $key => $item) {
      if ('object' === $item[0]) {
        $formattedValue = sprintf('<em>object</em>(%s)', $this->formatClass($item[1]));
      } elseif ('array' === $item[0]) {
        $formattedValue = sprintf('<em>array</em>(%s)', is_array($item[1]) ? $this->formatArgs($item[1]) : $item[1]);
      } elseif ('string' === $item[0]) {
        $formattedValue = sprintf("'%s'", $this->escapeHtml($item[1]));
      } elseif ('null' === $item[0]) {
        $formattedValue = '<em>null</em>';
      } elseif ('boolean' === $item[0]) {
        $formattedValue = '<em>' . strtolower(var_export($item[1], true)) . '</em>';
      } elseif ('resource' === $item[0]) {
        $formattedValue = '<em>resource</em>';
      } else {
        $formattedValue = str_replace("\n", '', var_export($this->escapeHtml((string) $item[1]), true));
      }

      $result[] = is_int($key) ? $formattedValue : sprintf("'%s' => %s", $key, $formattedValue);
    }

    return implode(', ', $result);
  }

  /**
   * HTML-encodes a string.
   */
  private function escapeHtml($str) {
    return htmlspecialchars($str, ENT_QUOTES | ENT_SUBSTITUTE, ini_get('default_charset') ?: 'UTF-8');
  }
}
