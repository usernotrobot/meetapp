<?php

namespace App\Policies;

use App\User;
use App\Models\Suggestion;

class SuggestionPolicy extends BasePolicy {
  /**
   * Determine whether the user can manage(edit/delete) suggestion
   *
   * @param User $user
   * @param Suggestion $suggestion
   * @return bool
   */
  public function manage(User $user, Suggestion $suggestion) {
    return $user->can('edit-suggestions', $suggestion->topic)
      && ($suggestion->user_id == $user->id
        || $user->can('manage-suggestions', $suggestion->topic->headline->event));
  }

  /**
   * Determine whether the user can manage(edit/delete) suggestion
   * if he is suggestion owner
   *
   * @param User $user
   * @param Suggestion $suggestion
   * @return bool
   */
  public function manageOwn(User $user, Suggestion $suggestion) {
    return $user->can('edit-suggestions', $suggestion->topic)
      && ($suggestion->user_id == $user->id);
  }
}
