<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class BasePolicy {
  use HandlesAuthorization;

  /**
   * Before any other check
   *
   * @param User $user
   * @param $ability
   * @return bool
   */
  public function before(User $user, $ability) {
    /* FIXME: This is a dirty hack. Please-please-please, fix me better! */
    if ($user->isGlobalAdmin() && ($ability !== 'delete')) {
      return true;
    }
  }
}
