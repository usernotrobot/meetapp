<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ClientPolicy extends BasePolicy {

  /**
   * Determine if the user can list clients
   *
   * @param User $user
   * @return bool
   */
  public function index(User $user) {
    return ($user->isGlobalAdmin());
  }

  /**
   * Determine if the user can manage clients (crud)
   *
   * @param User $user
   * @return bool
   */
  public function manage(User $user) {
    return ($user->isGlobalAdmin());
  }
}
