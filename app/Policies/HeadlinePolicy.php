<?php

namespace App\Policies;

use App\User;
use App\Models\Headline;

class HeadlinePolicy extends BasePolicy {

  /**
   * Determine whether the user can delete the headline
   *
   * @param User $user
   * @param Headline $headline
   * @return bool
   */
  public function delete(User $user, Headline $headline) {
    $client = $headline->event->client;
    return $headline->topics->isEmpty()
      && ($user->isGlobalAdmin()
          || $user->isClientAdmin($client));
  }
}
