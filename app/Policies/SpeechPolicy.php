<?php

namespace App\Policies;

use App\User;
use App\Models\Speech;
use App\Models\Delegate;
use App\Models\Event;

class SpeechPolicy extends BasePolicy {
  /**
   * Determine whether the user can manage(edit/delete) suggestion
   *
   * @param User $user
   * @param Speech $speech
   * @return bool
   */
  public function delete(User $user, Speech $speech) {
    /**
     * @var Event $event
     * @var Delegate $delegate
     */
    if ($user->isGlobalAdmin()) {
      return true;
    }

    $event = $speech->topic->headline->event;
    $delegate = $user->delegate($event);

    return $user->can('view', $event)
      && $speech->waiting()
      && ($speech->delegate_id == $delegate->id
        || $user->can('manage-speechlist', $event));
  }

  /**
   * Determine whether the user can manage(edit/delete) suggestion
   * if he is suggestion owner
   *
   * @param User $user
   * @param Speech $speech
   * @return bool
   */
  public function manageOwn(User $user, Speech $speech) {
    /**
     * @var Event $event
     * @var Delegate $delegate
     */
    $event = $speech->topic->headline->event;
    $delegate = $user->delegate($event);

    return $user->can('view', $event)
      && $speech->waiting()
      && $speech->delegate_id == $delegate->id;
  }
}
