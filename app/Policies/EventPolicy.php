<?php

namespace App\Policies;

use App\User;
use App\Models\Event;

class EventPolicy extends BasePolicy {

  /**
   * Determine whether the user can list events.
   *
   * @param  User $user
   * @return bool
   */
  public function index(User $user) {
    return (!$user->client->trashed());
  }

  /**
   * Determine whether the user can view the event
   *
   * @param User $user
   * @param Event $event
   * @return bool
   */
  public function view(User $user, Event $event) {
    return $this->index($user) && ($user->eventRole($event));
  }

  /**
   * Determine whether the user can manage events (crud).
   *
   * @param  User $user
   * @param  Event $event
   * @return bool
   */
  public function manage(User $user, Event $event = null) {
    return ($event)
      ? ($event->client_id == $user->client_id && $user->isAdmin())
      : ($this->index($user) && $user->isAdmin());
  }

  /**
   * Determine whether the user can manage event files.
   *
   * @param  User $user
   * @param  Event $event
   * @return bool
   */
  public function files(User $user, Event $event) {
    return $this->view($user, $event) && $user->getRole(User::ROLE_MODERATOR, $event);
  }

  /**
   * Determine whether the user can view event delegates.
   *
   * @param User $user
   * @param Event $event
   * @return bool
   */
  public function viewDelegates(User $user, Event $event) {
    return $this->view($user, $event);
  }


  /**
   * Determine whether the user can manage event delegates.
   *
   * @param User $user
   * @param Event $event
   * @return bool
   */
  public function manageDelegates(User $user, Event $event) {
    return $this->view($user, $event) && $user->getRole(User::ROLE_MODERATOR, $event);
  }

  /**
   * Determine whether the user can view event delegates.
   *
   * @param User $user
   * @param Event $event
   * @return bool
   */
  public function viewTopics(User $user, Event $event) {
    return $this->view($user, $event);
  }

  /**
   * Determine whether the user can manage event delegates.
   *
   * @param User $user
   * @param Event $event
   * @return bool
   */
  public function manageTopics(User $user, Event $event) {
    return $this->view($user, $event) && $user->getRole(User::ROLE_MODERATOR, $event);
  }

  /**
   * Determine whether the user can view event suggestions.
   *
   * @param User $user
   * @param Event $event
   * @return bool
   */
  public function viewSuggestions(User $user, Event $event) {
    return $this->view($user, $event);
  }

  /**
   * Determine whether the user has right to add suggestions
   *
   * @param User $user
   * @param Event $event
   * @return mixed
   */
  public function addSuggestions(User $user, Event $event) {
    return $user->can('view', $event) && $user->delegate($event)->right_submit;
  }

  /**
   * Determine whether the user can manage event suggestions.
   *
   * @param User $user
   * @param Event $event
   * @return bool
   */
  public function manageSuggestions(User $user, Event $event) {
    return $user->can('secretary', $event);
  }

  /**
   * Determine whether the user has right to add himself to speechlist
   *
   * @param User $user
   * @param Event $event
   * @return mixed
   */
  public function addSpeechlist(User $user, Event $event) {
    return $user->can('view', $event) && $user->delegate($event)->right_speak;
  }

  /**
   * Determine whether the user is able to manage speechlist
   *  -- delete not-own items
   *  -- reordering
   *  -- etc
   *
   * @param User $user
   * @param Event $event
   * @return bool
   */
  public function manageSpeechlist(User $user, Event $event) {
    return $user->can('moderate', $event);
  }

  /**
   * Determine whether the user is able to start voting
   *
   * @param User $user
   * @param Event $event
   * @return bool
   */
  public function manageVoting(User $user, Event $event) {
    return $user->can('moderate', $event);
  }

  /**
   * Determine whether the user is able to vote
   *
   * @param User $user
   * @param Event $event
   * @return bool
   */
  public function vote(User $user, Event $event) {
    return $this->view($user, $event) && $user->delegate($event)->right_vote;
  }

  /**
   * Determine whether the user has right to moderate stuff
   *
   * @param User $user
   * @param Event $event
   * @return mixed
   */
  public function moderate(User $user, Event $event) {
    return $user->can('view', $event) && $user->delegate($event)->moderator();
  }

  /**
   * Determine whether the user has right to be a secretary
   *
   * @param User $user
   * @param Event $event
   * @return mixed
   */
  public function secretary(User $user, Event $event) {
    return $user->can('view', $event) && $user->delegate($event)->secretary();
  }
}
