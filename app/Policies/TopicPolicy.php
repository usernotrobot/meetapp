<?php

namespace App\Policies;

use App\User;
use App\Models\Topic;

class TopicPolicy extends BasePolicy {

  /**
   * Determine whether the user can edit the suggestion
   *
   * @param User $user
   * @param Topic $topic
   * @return bool
   */
  public function editSuggestions(User $user, Topic $topic) {
    return $topic->openedForSuggestion()
      && $user->can('view', $topic->headline->event);
  }

  /**
   * Determine whether the user can add suggestions.
   *
   * @param User $user
   * @param Topic $topic
   * @return bool
   */
  public function addSuggestions(User $user, Topic $topic) {
    return $this->editSuggestions($user, $topic)
      && $user->can('add-suggestions', $topic->headline->event);
  }

  /**
   * Determine whether the user can add to speechlist
   *
   * @param User $user
   * @param Topic $topic
   * @return bool
   */
  public function addSpeechlist(User $user, Topic $topic) {
    return $topic->allowedForNewSpeeches()
      && ($user->can('add-speechlist', $topic->headline->event)
        || $user->can('moderate', $topic->headline->event));
  }
}
