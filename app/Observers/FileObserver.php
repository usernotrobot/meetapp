<?php

namespace App\Observers;

use App\Models\File;
use Illuminate\Support\Facades\Storage;
use App\Repositories\FileRepository;

class FileObserver{
  private $repository;

  public function __construct() {
    $this->repository = resolve('App\Repositories\FileRepository');
  }


  public function creating(File $file) {

  }

  /**
   * Listen to the File deleting event.
   *
   * @param File $file
   * @param FileRepository $repository
   * @return void
   */
  public function deleting(File $file) {
    if (!$this->repository->countOthers($file)) {
      Storage::delete($file->path);
    }
  }

}
