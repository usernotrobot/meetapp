<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Models\Topic;
use App\Repositories\TopicRepository;

use Carbon\Carbon;

use Illuminate\Support\Facades\Log;

class FinishVoting implements ShouldQueue {
  use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

  /** @var Topic */
  protected $topic;

  /**
   * Create a new job instance.
   *
   * @param Topic $topic
   * @return FinishVoting
   */
  public function __construct(Topic $topic) {
    $this->topic = $topic;
  }

  /**
   * Execute the job.
   * @param TopicRepository $repository
   * @return void
   */
  public function handle(TopicRepository $repository) {
    $topic = $this->topic;
    $start = $topic->voting_start->addSeconds($topic->voting_length);

    if ($topic->status == Topic::STATUS_VOTING
      && empty($topic->voting_end)
      && $start->lte(Carbon::now())) {

      $repository->finishVoting($this->topic);
    }
  }
}
