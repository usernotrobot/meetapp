<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use App\Models\Client;
use App\Policies\ClientPolicy;
use App\Models\Event;
use App\Policies\EventPolicy;
use App\Models\Headline;
use App\Policies\HeadlinePolicy;
use App\Models\Topic;
use App\Policies\TopicPolicy;
use App\Models\Suggestion;
use App\Policies\SuggestionPolicy;
use App\Models\Speech;
use App\Policies\SpeechPolicy;

class AuthServiceProvider extends ServiceProvider {
  /**
   * The policy mappings for the application.
   *
   * @var array
   */
  protected $policies = [
    Client::class => ClientPolicy::class,
    Event::class => EventPolicy::class,
    Headline::class => HeadlinePolicy::class,
    Topic::class => TopicPolicy::class,
    Suggestion::class => SuggestionPolicy::class,
    Speech::class => SpeechPolicy::class,
  ];

  /**
   * Register any authentication / authorization services.
   *
   * @return void
   */
  public function boot() {
    $this->registerPolicies();

    //
  }
}
