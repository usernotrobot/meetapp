<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\Contracts\DelegateRouter as DelegateRouterContract;
use App\Services\DelegateRouter;

class DelegateRouterServiceProvider extends ServiceProvider {

  /**
   * Indicates if loading of the provider is deferred.
   *
   * @var bool
   */
  protected $defer = true;

  /**
   * Bootstrap the application services.
   *
   * @return void
   */
  public function boot() {
    //
  }

  /**
   * Register the application services.
   *
   * @return void
   */
  public function register() {
    $this->app->bind(DelegateRouterContract::class, function() {
      return new DelegateRouter();
    });
  }

  /**
   * Get the services provided by the provider.
   *
   * @return array
   */
  public function provides() {
    return [DelegateRouterContract::class];
  }
}
