<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\DB;
use App\Observers\FileObserver;
use App\Models\File;

class AppServiceProvider extends ServiceProvider {
  /**
   * Bootstrap any application services.
   *
   * @return void
   */
  public function boot() {
    File::observe(FileObserver::class);

    if (env('APP_DEBUG_SQL') == true) {
      DB::listen(function ($query) {
        echo "<pre>" . ($query->sql) . " (Time: " . $query->time . ") <br/>Bindings: " . implode(', ', $query->bindings) . "</pre>";
      });
    }

  }

  /**
   * Register any application services.
   *
   * @return void
   */
  public function register() {
    $this->app->resolving(function($object, $app)
    {
      // Called when container resolves object of any type...
    });
  }
}
