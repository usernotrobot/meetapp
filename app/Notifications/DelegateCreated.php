<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

use App\Models\Event;

class DelegateCreated extends Notification implements ShouldQueue {
  use Queueable;

  protected $event;

  /**
   * Create a new notification instance.
   *
   * @param Event $event
   * @return DelegateCreated
   */
  public function __construct(Event $event) {
    $this->event = $event;
  }

  /**
   * Get the notification's delivery channels.
   *
   * @param  mixed $notifiable
   * @return array
   */
  public function via($notifiable) {
    return ['mail'];
  }

  /**
   * Get the mail representation of the notification.
   *
   * @param  mixed $notifiable
   * @return \Illuminate\Notifications\Messages\MailMessage
   */
  public function toMail($notifiable) {
    return (new MailMessage)
      ->subject(trans('mail.delegate-created.subject'))
      ->greeting(trans('mail.delegate-created.header', ['name' => $this->event->name]))
      ->line(trans('mail.delegate-created.body'))
      ->action(trans('mail.delegate-created.action'), route('admin::events.index'))
      ->line(trans('mail.delegate-created.footer'));
  }

  /**
   * Get the array representation of the notification.
   *
   * @param  mixed $notifiable
   * @return array
   */
  public function toArray($notifiable) {
    return [
      //
    ];
  }
}
