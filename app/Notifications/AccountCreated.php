<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class AccountCreated extends Notification implements ShouldQueue {
  use Queueable;

  protected $password;

  /**
   * Create a new notification instance.
   *
   * @param string $password
   * @return AccountCreated
   */
  public function __construct($password) {
    $this->password = $password;
  }

  /**
   * Get the notification's delivery channels.
   *
   * @param  mixed $notifiable
   * @return array
   */
  public function via($notifiable) {
    return ['mail'];
  }

  /**
   * Get the mail representation of the notification.
   *
   * @param  mixed $notifiable
   * @return \Illuminate\Notifications\Messages\MailMessage
   */
  public function toMail($notifiable) {
    return (new MailMessage)
      ->subject(trans('mail.account-created.subject'))
      ->greeting(trans('mail.account-created.header'))
      ->line(trans('mail.account-created.body', ['password' => $this->password]))
      ->action(trans('mail.account-created.action'), config('app.url'))
      ->line(trans('mail.account-created.footer'));
  }

  /**
   * Get the array representation of the notification.
   *
   * @param  mixed $notifiable
   * @return array
   */
  public function toArray($notifiable) {
    return [
      //
    ];
  }
}
