<?php
namespace App\Repositories;

use Illuminate\Http\UploadedFile;
use Maatwebsite\Excel\Facades\Excel;

use App\Models\Delegate;
use App\Models\Event;
use App\User;

use Illuminate\Support\Facades\DB;
use Prettus\Repository\Events\RepositoryEntityDeleted;

use App\Notifications\DelegateCreated;

use App\Criteria\SpeakerUserNameCriteria;

class DelegateRepository extends BaseRepository {

  const STATUS_CREATED = 'created';
  const STATUS_UPDATED = 'updated';
  const STATUS_ERROR = 'error';
  const STATUS_EXISTS = 'exists';

  /**
   * Specify Model class name
   *
   * @return string
   */
  function model() {
    return Delegate::class;
  }

  public function getByEvent(Event $event) {
    return $this
      ->with(['user'])
      ->orderBy('participant_id')
      ->findWhere(['event_id' => $event->id]);
  }

  public function processFile(UploadedFile $file, Event $event, User $owner) {
    /* TODO: Move to parsing service */
    $retArr = [self::STATUS_CREATED => [], self::STATUS_UPDATED => [],
               self::STATUS_ERROR => [], self::STATUS_EXISTS => []];
    Excel::load($file, function ($reader) use ($event, $owner, &$retArr) {
      $results = $reader->get();

      foreach ($results as $obj) {
        try {
          /* TODO: Validate!!! */
          $user_values = [
            'first_name' => $obj->first_name,
            'last_name' => $obj->last_name,
            'union' => $obj->unioncompany,
            'union_sort_key' => $obj->union_sort_key,
            'branch' => $obj->branchlocal,
            'branch_sort_key' => $obj->branch_sort_key,
            'delegate_id' => $obj->delegate_id,
          ];

          $user_attributes['email'] = $obj->email;

          $delegate_values = [
            'participant_id' => $obj->participant_id,
            'role' => strtolower($obj->participant_type),
            'right_submit' => boolval($obj->right_to_submit_proposals),
            'right_speak' => boolval($obj->right_to_speak),
            'right_vote' => boolval($obj->voting_rights),
            'created_by' => $owner->id,
          ];
          list($status, $user) = $this->updateOrCreateForEvent($event,
                                                               $user_attributes, $user_values,
                                                               $delegate_values,
                                                               true);
          $retArr[$status][] = $user->email;
        }
        catch (\Exception $e) {
          $retArr[self::STATUS_ERROR][] = trans('delegates.msg.user-parse-fail', ['email' => $obj->email,
                                                                                  'error' => $e->getMessage()]);
          continue;
        }
      }
    });

    return $retArr;
  }

  public function updateOrCreateForEvent(Event $event, array $user_attributes, array $user_values,
                                         array $delegate_values, bool $notify = false) {
    DB::beginTransaction();

    /* @var UserRepository $userRepository*/
    $userRepository = app(UserRepository::class);

    /* Processing user */
    $user = $userRepository->findWhere($user_attributes)->first();

    /* There is no user with such email */
    if (empty($user)) {
      $values = array_merge($user_attributes, $user_values);
      $user = $userRepository->createForClient($event->client, $values,
                                               $delegate_values['role'], $notify);

      $status = self::STATUS_CREATED;
    }
    /* User exists, but other client is owner - error */
    elseif ($user->client_id != $event->client_id) {
      $status = self::STATUS_EXISTS;
      DB::rollBack();

      return [$status, $user];
    }
    /* User is ok, updating it's details */
    else {
      if (!empty($user_values['password'])) {
        $user_values['password'] = bcrypt($user_values['password']);
      }
      $userRepository->update($user_values, $user->id);

      $status = self::STATUS_UPDATED;
    }

    /* Processing delegate */
    $delegate_attributes = [
      'event_id' => $event->id,
      'user_id' => $user->id,
    ];

    /* Processing delegate */
    $delegate = $this->findWhere($delegate_attributes)->first();

    if (empty($delegate)) {
      $delegate = $this->create(array_merge($delegate_attributes, $delegate_values));
      if ($notify && $status == self::STATUS_UPDATED) {
        $user->notify(new DelegateCreated($event));
      }
    }
    else {
      $delegate = $this->updateDelegate($delegate_values, $delegate->id);
    }

    DB::commit();

    return [$status, $user, $delegate];
  }

  public function updateForEvent(Delegate $delegate, Event $event, array $user_attributes, array $delegate_attributes) {
    DB::beginTransaction();

    /* @var UserRepository $userRepository*/
    $userRepository = app(UserRepository::class);

    /* Processing user */
    $user = $delegate->user;

    /* There is no user with such email */
    if (!empty($user_attributes['password'])) {
      $user_attributes['password'] = bcrypt($user_attributes['password']);
    }

    $user_attributes['api_token'] = (isset($user_attributes['user_token'])) ? $user_attributes['user_token'] : null;

    $userRepository->update($user_attributes, $user->id);

    /* Processing delegate */
    $delegate = $this->updateDelegate($delegate_attributes, $delegate->id);

    DB::commit();

    return $delegate;
  }

  public function updateDelegate($delegate_attributes, $id) {
    $delegate_attributes['right_submit'] = isset($delegate_attributes['right_submit']) ?: false;
    $delegate_attributes['right_speak'] = isset($delegate_attributes['right_speak']) ?: false;
    $delegate_attributes['right_vote'] = isset($delegate_attributes['right_vote']) ?: false;

    return $this->update($delegate_attributes, $id);
  }

  public function getEvents(User $user) {
    return $this->with('event')
      ->findWhere(['user_id' => $user->id]);
  }

  public function getAllowedToSpeak(Event $event) {
    return $this
      ->with(['user'])
      ->findWhere(['event_id' => $event->id, 'right_speak' => true])
      ->sortBy(function($item) {
        return $item->user->last_name . $item->user->first_name;
      });
  }

  public function getByUser(User $user, Event $event) {
    return $this->findWhere(
      ['user_id' => $user->id,
       'event_id' => $event->id
      ])->first();
  }

  public function bulkDelete(Event $event) {
    $delegates = DB::select("DELETE FROM delegates WHERE event_id = :event_id RETURNING *",
                        [':event_id' => $event->id]);
    $retArr = [];
    if ($delegates) {
      $retArr = array_map(function ($item) {
        return (new Delegate(get_object_vars($item)));
      }, $delegates);
    }

    if (!empty($retArr[0])) event(new RepositoryEntityDeleted($this, $retArr[0]));

    return $retArr;
  }

  public function getAutocomplete(Event $event, $name) {
    return $this->getByCriteria(new SpeakerUserNameCriteria($name, $event));
  }

  public function getAllowedToVote(Event $event) {
    return $this
      ->with(['user'])
      ->findWhere(['event_id' => $event->id, 'right_vote' => true])
      ->sortBy(function($item) {
        return $item->user->last_name . $item->user->first_name;
      });
  }

}
