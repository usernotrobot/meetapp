<?php

namespace App\Repositories;

use Prettus\Repository\Events\RepositoryEntityUpdated;

/**
 * Class SoftDeleteRepository
 * Some stuff for soft-deleted items
 */
trait SoftDeleteRepository {

  public function withTrashed() {
    $this->model = $this->model->withTrashed();

    return $this;
  }

  public function restore($id) {
    $this->applyCriteria();
    $this->applyScope();

    $temporarySkipPresenter = $this->skipPresenter;
    $this->skipPresenter(true);

    $model = $this->find($id);
    $originalModel = clone $model;

    $this->skipPresenter($temporarySkipPresenter);
    $this->resetModel();

    $restored = $model->restore();

    if (!empty($this->cached)) {
      $this->forget($this->cached);
    }

    event(new RepositoryEntityUpdated($this, $originalModel));

    return $restored;
  }
}
