<?php

namespace App\Repositories;

use App\Models\Topic;
use App\Models\Speech;
use App\Models\Event;
use App\Models\Delegate;
use Prettus\Repository\Events\RepositoryEntityCreated;
use Prettus\Repository\Events\RepositoryEntityUpdated;
use Prettus\Repository\Events\RepositoryEntityDeleted;

use Carbon\Carbon;

use Illuminate\Support\Facades\DB;

class SpeechRepository extends BaseRepository {
  /**
   * Specify Model class name
   *
   * @return string
   */
  public function model() {
    return Speech::class;
  }

  public function getByTopic(Topic $topic) {
    return $this->findWhereOrder(['topic_id' => $topic->id], ['order_key' => 'asc']);
  }

  public function getByDelegate(Delegate $delegate, Event $event) {
    /* @var TopicRepository $topicRepository */
    //$topicRepository = app(TopicRepository::class);

    $topicIds = $event->topics->map(function($item) {
      return $item->id;
    });

    return $this->findWhereOrder(
      ['delegate_id' => $delegate->id, ['topic_id', 'in', $topicIds]],
      ['topic_id' => 'asc', 'order_key' => 'asc']);
  }

  public function countSpeechTimes(Delegate $delegate, Topic $topic) {
    return $this->findWhere(['topic_id' => $topic->id, 'delegate_id' => $delegate->id])->count();
  }

  protected function getLastOrder(Topic $topic) {
    $speech = $this->findWhereOrder(['topic_id' => $topic->id], ['order_key' => 'desc'])->first();
    return empty($speech) ? 1 : $speech->order_key + 1;
  }

  public function addWithOrderCheck(Delegate $delegate, Topic $topic) {
    $errors = 0;
    do {
      try {
        DB::beginTransaction();

        $count = $this->countSpeechTimes($delegate, $topic);
        if ($count >= $topic->speech_times) {
          DB::rollback();
          $errors = 3;
          throw new \UnexpectedValueException(trans('speechlist.msg.speech-times-exceed', ['max' => $topic->speech_times]));
        }

        $model = Speech::create(['topic_id' => $topic->id,
                                 'delegate_id' => $delegate->id,
                                 'order_key' => $this->getLastOrder($topic),
                                 'length' => $topic->speech_length[$count + 1]]);
        DB::commit();

        event(new RepositoryEntityCreated($this, $model));
        event(new RepositoryEntityUpdated(app(DelegateRepository::class), $delegate));

        return $this->parserResult($model);

      } catch (\Exception $e) {
        DB::rollback();
        $errors++;
      }
    }
    while($errors < 3);

    throw $e;
  }

  public function markLast(Speech $speech) {
    DB::transaction(function() use ($speech) {

      /* Deny adding new speeches */
      app(TopicRepository::class)->denyNewSpeeches($speech->topic);

      /* Delete all speech list items after selected */
      $this->findWhere(['topic_id' => $speech->topic_id, ['order_key', '>', $speech->order_key]])
        ->each(function ($item) {
          $this->delete($item->id);
        });

      event(new RepositoryEntityUpdated($this, $speech));
    });
  }

  public function getNextSpeakers(Topic $topic, $active = true) {
    $status = $active ? [Speech::STATUS_ACTIVE, Speech::STATUS_WAITING] : [Speech::STATUS_WAITING];

    return $this->findWhereOrder(['topic_id' => $topic->id,
                                  ['status', 'in', $status]],
                                 ['order_key' => 'asc']);
  }


  public function updateLength(Speech $speech, $length) {
    $speech->length = $length;

    return $this->update(['length' => $length * 60], $speech->id);
  }

  public function updatePriority(Speech $speech, $priority) {
    $success = false;

    DB::transaction(function () use ($speech, $priority, &$success) {
      /* Alert: pg-specific stuff!*/
      $list = $this->getNextSpeakers($speech->topic, false);

      $current = $speech->order_key;
      $desired = $speech->order_key + $priority;

      if ($desired > $list->last()->order_key) $desired = $list->last()->order_key;
      elseif ($desired < $list->first()->order_key) $desired = $list->first()->order_key;

      $result = null;

      if ($current < $desired) {
        $result = DB::select("
          UPDATE speeches 
          SET order_key = order_key - 1 
          WHERE order_key > :current AND order_key <= :desired
          RETURNING *", [':current' => $current, ':desired' => $desired]);
      } elseif ($current > $desired) {
        $result = DB::select("
          UPDATE speeches 
          SET order_key = order_key + 1 
          WHERE order_key >= :desired AND order_key < :current
          RETURNING *", [':current' => $current, ':desired' => $desired]);
      }

      DB::update("UPDATE speeches SET order_key = :order_key WHERE id = :id",
                 ['order_key' => $desired, 'id' => $speech->id]);

      if ($result) {
        array_walk($result, function ($item) {
          event(new RepositoryEntityUpdated($this, new Speech(get_object_vars($item))));
        });
      }

      event(new RepositoryEntityUpdated($this, $speech));

      $success = true;
    }, 3);

    return $success;
  }

  public function start(Speech $speech) {
    DB::transaction(function () use (&$speech) {
      $speechlist = $this->getNextSpeakers($speech->topic);

      if (!empty($speechlist) && $speechlist[0]->active()) {
        throw new \RuntimeException();
      }

      $speech = $this->update(
        ['status' => Speech::STATUS_ACTIVE,
         'start' => Carbon::now(),
        ], $speech->id);

    });

    return $speech;
  }

  public function stop(Speech $speech) {
    return $this->update(
      ['status' => Speech::STATUS_FINISHED,
       'end' => Carbon::now(),
      ], $speech->id);
  }
}
