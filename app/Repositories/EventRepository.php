<?php
namespace App\Repositories;

use Carbon\Carbon;
use App\Models\Event;
use App\User;

class EventRepository extends BaseRepository {
  use SoftDeleteRepository;

  /**
   * Specify Model class name
   *
   * @return string
   */
  function model() {
    return Event::class;
  }

  public function getAllForAdmin(User $user) {
    $result = empty($user->client_id)
      ? $this->model->whereHas('client', function ($query) {
        return $query->where(['deleted_at' => null]);
      })
      : $this->model->where(['client_id' => $user->client_id]);

    return $result->orderBy('start')->paginate();
  }

  public function create(array $attributes = []) {
    $event = parent::create($attributes);

    $status = Event::STATUS_PLANNED;
    $now = Carbon::now();
    if ($event->end->lt($now)) {
      $status = Event::STATUS_COMPLETED;
    } elseif ($event->start->lte($now) && $event->end->gte($now)) {
      $status = Event::STATUS_ACTIVE;
    }

    return $this->update(['status' => $status], $event->id);
  }

  public function getDelegateEvents(User $user, $status = null) {
    $status = collect($status)->filter(function ($item, $key) {
      return in_array($item, Event::allStatuses());
    });

    $result = $this
      ->whereHas('delegates', function ($query) use ($user) {
        return $query->where(['user_id' => $user->id]);
      });

    if (!empty($status->toArray())) $result = $result->model->whereIn('status', $status->toArray());

    return $result;
  }
}

