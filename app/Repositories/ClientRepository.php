<?php
namespace App\Repositories;

use App\Models\Client;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Repositories\EventRepository;
use App\Models\Event;

class ClientRepository extends BaseRepository {
  use SoftDeleteRepository;

  protected $cached = [EventRepository::class];

  /**
   * Specify Model class name
   *
   * @return string
   */
  function model() {
    return Client::class;
  }

  public function getOrderedTrashedList() {
    // PG-specific sorting
    if (env('DB_CONNECTION') == 'pgsql') {
      $this->model = $this->model->orderByRaw('deleted_at NULLS FIRST');
    }

    return $this->orderBy('name', 'asc')->all();
  }

  public function getAvailableClients() {
    return $this->orderBy('name')->all();
  }

  public function delete($id) {
    $result = parent::delete($id);

    if (!empty($this->cached)) {
      $this->forget($this->cached);
    }

    return $result;
  }

}
