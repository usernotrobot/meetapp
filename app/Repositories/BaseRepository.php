<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository as Repository;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Traits\CacheableRepository;

use Prettus\Repository\Helpers\CacheKeys;
use Illuminate\Support\Facades\Log;


abstract class BaseRepository extends Repository implements CacheableInterface {
  use CacheableRepository;

  protected function processWith($with) {
    $with = is_array($with) ? $with : [$with];
    $with = (!empty($this->with) && is_array($this->with)) ? array_merge($this->with, $with) : $with;

    return empty($with) ? $this : $this->with($with);
  }

  public function get($id, $with = []) {
    return $this->processWith($with)->find($id);
  }


  public function update(array $attributes, $id) {
    $this->applyCriteria();

    return parent::update($attributes, $id);
  }

  public function updateOrCreate(array $attributes, array $values = []) {
    $this->applyCriteria();

    return parent::updateOrCreate($attributes, $values);
  }

  public function delete($id) {
    $this->applyCriteria();

    $result = parent::delete($id);

    if (!empty($this->cacheOnRestore)) {
      $this->forget($this->cacheOnRestore);
    }

    return $result;
  }

  public function deleteWhere(array $where) {
    $this->applyCriteria();

    return parent::deleteWhere($where);
  }

  public function forget($list) {
    foreach ($list as $repository) {
      try {
        $cleanEnabled = config("repository.cache.clean.enabled", true);
        $action = 'update';

        if ($cleanEnabled) {
          if (config("repository.cache.clean.on.{$action}", true)) {
            $cacheKeys = CacheKeys::getKeys($repository);

            if (is_array($cacheKeys)) {
              foreach ($cacheKeys as $key) {
                app(config('repository.cache.repository', 'cache'))->forget($key);
              }
            }
          }
        }
      } catch (\Exception $e) {
        Log::error($e->getMessage());
      }
    }
  }

  /**
   * Applies the given where conditions to the model.
   *
   * @param array $where
   * @return void
   */
  protected function applyConditions(array $where) {
    foreach ($where as $field => $value) {
      if (is_array($value)) {
        list($field, $condition, $val) = $value;

        if ($condition == 'in') {
          $this->model = $this->model->whereIn($field, $val);
        } else {
          $this->model = $this->model->where($field, $condition, $val);
        }
      } else {
        $this->model = $this->model->where($field, '=', $value);
      }
    }
  }


  /**
   * Find data by multiple fields with ordering
   *
   * @param array $where
   * @param array $order
   * @param array $columns
   *
   * @return mixed
   */
  public function findWhereOrder(array $where, array $order = null, $columns = ['*']) {
    if (!$this->allowedCache('findWhereOrder') || $this->isSkippedCache()) {
      return parent::findWhere($where, $columns);
    }

    $key = $this->getCacheKey('findWhereOrder', func_get_args());
    $minutes = $this->getCacheMinutes();
    $value = $this->getCacheRepository()->remember($key, $minutes, function () use ($where, $order, $columns) {
      $this->applyCriteria();
      $this->applyScope();

      $this->applyConditions($where);

      if (!empty($order)) {
        foreach ($order as $fld => $how) {
          $this->model = $this->model->orderBy($fld, $how);
        }
      }

      $model = $this->model->get($columns);
      $this->resetModel();

      return $this->parserResult($model);
    });

    return $value;
  }
}

