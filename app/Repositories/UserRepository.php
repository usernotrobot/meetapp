<?php
namespace App\Repositories;

use App\User;
use App\Models\Client;
use App\Notifications\AccountCreated;

class UserRepository extends BaseRepository {
  use SoftDeleteRepository;

  /**
   * Specify Model class name
   *
   * @return string
   */
  function model() {
    return User::class;
  }

  public function createForClient(Client $client, $data, $role = User::ROLE_DELEGATE, $notify = false) {
    $attributes = $data;

    $password = !empty($data['password']) ? $data['password'] : str_random(10);
    $attributes['password'] = bcrypt($password);

    $attributes['client_id'] = $client->id;
    $attributes['role'] = $role;

    if ( $role == User::ROLE_PROJECTOR ){
      $attributes['api_token'] = str_random(32);
    }

    $user = $this->create($attributes);

    if ($notify) {
      $user->notify(new AccountCreated($password));
    }

    return $user;
  }

  public function getAdmin(Client $client) {
    $a = $this
      ->findByField('client_id', $client->id)
      ->where('role', User::ROLE_ADMIN)
      ->first();

    return $a;
  }

}
