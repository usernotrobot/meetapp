<?php
namespace App\Repositories;

use App\Models\File;
use App\User;
use Illuminate\Http\UploadedFile;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class FileRepository extends BaseRepository {
  /**
   * Specify Model class name
   *
   * @return string
   */
  public function model() {
    return File::class;
  }

  public function countOthers($file) {
    return $this->findWhere(
      ['path' => $file->path,
       ['id', '<>', $file->id]
      ])->count();
  }

  protected function generatePath(Model $entity = null) {
    return date('y.m.d');
  }

  public function upload(UploadedFile $file, Model $entity, User $user, $original_name, $name = null) {
    $path = $this->generatePath($entity);
    $filename = Storage::putFile($path, $file, 'private');

    return $this->create(
      ['name' => empty($name) ? $original_name : $name,
       'original_name' => $original_name,
       'path' => $filename,
       'owner_id' => $user->id,
       'entity_id' => $entity->id,
       'entity_type' => get_class($entity),
      ]);
  }
}

