<?php
namespace App\Repositories;

use App\Models\Vote;
use App\Models\Topic;

use Illuminate\Support\Facades\DB;

class VoteRepository extends BaseRepository {

  /**
   * Specify Model class name
   *
   * @return string
   */
  function model() {
    return Vote::class;
  }

  public function calc(Topic $topic) {
    $results = DB::table('votes')
      ->select('vote', DB::raw('COUNT(*) as votes'))
      ->groupBy('vote')
      ->where('topic_id', $topic->id)
      ->get();

    $retArr = [];
    foreach ($results as $result) {
      $retArr[$result->vote] = $result->votes;
    }

    return $retArr;
  }

  public function clear(Topic $topic)  {
    DB::table('votes')
      ->where('topic_id', $topic->id)
      ->delete();
  }
}
