<?php

namespace App\Repositories;

use App\Models\Topic;
use App\Models\Suggestion;
use App\Events\SuggestionCreated;

class SuggestionRepository extends BaseRepository {
  use SoftDeleteRepository;

  protected $with = ['topic'];

  /**
   * Specify Model class name
   *
   * @return string
   */
  public function model() {
    return Suggestion::class;
  }

  public function getAllByTopic(Topic $topic) {
    $this->scopeQuery(function($query) use ($topic) {
      return $query
        ->where('topic_id', $topic->id)
        ->orderBy('created_at', 'asc');
    });

    return $this;
  }

	public function getMySuggestions($user_id, $with = []) {
		return $this->processWith($with)
			->scopeQuery(function($query) use ($user_id) {
				return $query
					->where('user_id', $user_id)
					->orderBy('id');
			});
	}
}
