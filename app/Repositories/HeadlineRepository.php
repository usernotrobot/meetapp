<?php

namespace App\Repositories;

use App\Models\Headline;
use App\Models\Event;

class HeadlineRepository extends BaseRepository {
  use SoftDeleteRepository;

  protected $with = ['event'];

  /**
   * Specify Model class name
   *
   * @return string
   */
  public function model() {
    return Headline::class;
  }

  public function getAllByEvent(Event $event, $with = []) {
    return $this
      ->processWith($with)
      ->findWhereOrder(['event_id' => $event->id,
                        'deleted_at' => null
      ], ['position' => 'asc']);
  }

  public function getByEvent(Event $event, $headline_id, $with = []) {
    $this->processWith($with);

    $query = ['event_id' => $event->id, 'id' => $headline_id];

    return $this->findWhere($query)->first();
  }


  public function createFromEvent(array $attributes, Event $event) {
    $attributes['event_id'] = $event->id;

    return parent::create($attributes);
  }

  public function updateSettings($attributes, Headline $headline) {
    $attributes['description'] = isset($attributes['description']) ? $attributes['description'] : "";

    return $this->update($attributes, $headline->id);
  }
}
