<?php

namespace App\Repositories;

use App\Models\Topic;
use App\Models\Headline;
use App\Models\Event;
use Carbon\Carbon;
use App\Repositories\VoteRepository;

class TopicRepository extends BaseRepository {
  use SoftDeleteRepository;

  protected $with = ['headline', 'event'];

  /**
   * Specify Model class name
   *
   * @return string
   */
  public function model() {
    return Topic::class;
  }

  public function getAllByEvent(Event $event, $with = []) {
    $this->processWith($with);
  	$this->with(['headline', 'event'])->orderBy('id');

    return $this;
  }

  public function getByEvent(Event $event, $topic_id, $with = []) {
    $this->processWith($with);

	  $topic = $event->topics()->where('topics.id', $topic_id)->with('headline')->first();

    return $topic;
  }

  public function createFromHeadline(array $attributes, Headline $headline) {
    $attributes['headline_id'] = $headline->id;

    $attributes['speech_opened'] = isset($attributes['speech_opened']) ? true : false;
    $attributes['speech_length'] = array_map(function($item) {
      return $item * 60;
    }, $attributes['speech_length']);

    return parent::create($attributes);
  }

  public function createFromEvent(array $attributes, Event $event) {
    $attributes['event_id'] = $event->id;

    $attributes['speech_opened'] = isset($attributes['speech_opened']) ? true : false;
    $attributes['speech_length'] = array_map(function($item) {
      return $item * 60;
    }, $attributes['speech_length']);

    return parent::create($attributes);
  }

  public function denyNewSpeeches(Topic $topic) {
    $topic->speech_opened = false;

    return parent::update(['speech_opened' => false], $topic->id);
  }

  public function updateSettings($attributes, Topic $topic) {
    $attributes['description'] = isset($attributes['description']) ? $attributes['description'] : "";
    $attributes['speech_opened'] = (isset($attributes['speech_opened']) && $attributes['speech_opened']) ? true : false;
    $attributes['speech_length'] = array_map(function($item) {
      return $item * 60;
    }, $attributes['speech_length']);

    return $this->update($attributes, $topic->id);
  }

  public function getActiveTopic(Event $event) {
    /* TODO: Change order field after topic Order field addition! */

	  $topic = $event->topics()->whereIn('status',[Topic::STATUS_DISCUSSION, Topic::STATUS_VOTING])->with('headline')->orderBy('id', 'asc')->first();

	  if (empty($topic))
		  $topic = $event->topics()->whereIn('status',[Topic::STATUS_OPEN])->with('headline')->orderBy('id', 'asc')->first();

    return $topic;
  }

  public function startVoting(Topic $topic, $settings) {
    $settings['length'] *= 60;

    $attributes = [
      'status' => Topic::STATUS_VOTING,
      'voting_start' => Carbon::now(),
      'voting_settings' => $settings,
      'voting_end' => null,
      'voting_results' => [],
    ];

    app(VoteRepository::class)->clear($topic);

    return $this->update($attributes, $topic->id);
  }

  public function finishVoting(Topic $topic) {
    $topic = $this->update(['voting_end' => Carbon::now()], $topic->id);

    if ($topic->voting_type == Topic::VOTING_TYPE_AUTO) {
      $votes = app(VoteRepository::class)->calc($topic);
      $delegatesCount = app(DelegateRepository::class)->getAllowedToVote($topic->event)->count();
      $votesCount = array_sum($votes);

      $topic = $this->update(
        ['voting_results' =>
          ['votes' => $votes,
           'delegatesCount' => $delegatesCount,
           'votesCount' => $votesCount,
          ]
        ],
        $topic->id);
    }

    return $topic;
  }

  public function manualVoting(Topic $topic, $votes) {
    $delegatesCount = app(DelegateRepository::class)->getAllowedToVote($topic->event)->count();
    $votesCount = array_sum($votes);

    $results = [
      'votes' => $votes,
      'delegatesCount' => $delegatesCount,
      'votesCount' => $votesCount,
    ];

    return $this->update(['voting_end' => Carbon::now(), 'voting_results' => $results],
                         $topic->id);
  }

  public function cancelVoting(Topic $topic) {
    $topic = $this->update(
      ['voting_settings' => Topic::DEFAULT_VOTING_SETTINGS,
       'voting_start' => null,
       'voting_end' => null,
       'voting_results' => [],
       'status' => Topic::STATUS_OPEN
      ], $topic->id);

    app(VoteRepository::class)->clear($topic);

    return $topic;
  }

	public function updateHeadlines($attr)
	{
		$topic = [];

		foreach ($attr as $key => $value)
			$topic[] = $this->update(['headline_id' => $value], $key);

		return $topic;
	}
}
