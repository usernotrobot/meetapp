<?php
namespace App\Repositories\Files;

use App\Repositories\FileRepository;
use Illuminate\Database\Eloquent\Model;

class TopicFileRepository extends FileRepository {
  protected function generatePath(Model $entity = null) {
    return 'events/' . $entity->event->id . '/topics/' . $entity->id;
  }
}

