<?php
namespace App\Repositories\Files;

use App\Repositories\FileRepository;
use Illuminate\Database\Eloquent\Model;

class EventFileRepository extends FileRepository {
  protected function generatePath(Model $entity = null) {
    return 'events/' . $entity->id;
  }
}

