<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SuggestionStoreRequest extends FormRequest {
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize() {
    return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules() {
    return [
      'summary' => 'required|string|max:255',
      'details' => 'required|string|max:10000',
    ];
  }

  public function attributes() {
    $items = ['details', 'summary'];

    return array_combine($items, array_map(function($item) {
      return '"' . trans('suggestions.lbl.' . $item) . '"';
    }, $items));
  }
}
