<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Speech;

class SpeechStoreRequest extends FormRequest {
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize() {
    return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules() {
    return [
      'delegate_id' => 'required|exists:delegates,id',
    ];
  }

  public function attributes() {
    $items = ['delegate_id'];

    return array_combine($items, array_map(function($item) {
      return '"' . trans('speeches.lbl.' . $item) . '"';
    }, $items));
  }
}
