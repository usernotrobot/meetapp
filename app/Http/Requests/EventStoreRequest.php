<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EventStoreRequest extends FormRequest {
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize() {
    return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules() {
    return [
      'name' => 'required|max:255',
      'union' => 'required|max:255',
      'description' => 'present|max:255',
      'start' => 'required|date',
      'end' => 'required|date|after:start',
      'client_id' => 'required|exists:clients,id',
    ];
  }

  public function attributes() {
    $items = ['name', 'union', 'description', 'start', 'end', 'client_id'];

    return array_combine($items, array_map(function($item) {
      return '"' . trans('events.lbl.' . $item) . '"';
    }, $items));
  }
}
