<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Client;
use Illuminate\Validation\Rule;

class ClientUpdateRequest extends FormRequest {
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize() {
    return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules() {
    return [
      /* Client validation */
      'name' => 'required|max:255',
      'description' => 'present|max:255',

      /* Client admin validation */
      'first_name' => 'required|max:255',
      'last_name' => 'required|max:255',
      'email' => [
        'required',
        'email',
        'max:255',
        Rule::unique('users')->ignore($this->route('client'), 'client_id'),
      ],
      'password' => 'present|min:6|confirmed',
    ];
  }

  public function attributes() {
    $user_items = ['email', 'password', 'password-confirmation', 'first_name', 'last_name'];
    $client_items = ['name', 'description'];

    return array_merge(
      array_combine($user_items, array_map(function($item) {
        return '"' . trans('users.lbl.' . str_replace('_', '-', $item)) . '"';
      }, $user_items)),
      array_combine($client_items, array_map(function($item) {
        return '"' . trans('delegates.lbl.' . str_replace('_', '-', $item)) . '"';
      }, $client_items))
    );
  }
}
