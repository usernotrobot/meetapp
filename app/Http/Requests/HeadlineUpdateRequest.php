<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class HeadlineUpdateRequest extends FormRequest {
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize() {
    return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules() {
    return [
      'name' => 'required|max:255',
      'description' => 'present|max:255',
      'subtopic.*' => 'sometimes|numeric',
    ];
  }

  public function attributes() {
    $items = ['name', 'description'];

    return array_combine($items, array_map(function($item) {
        return '"' . trans('headlines.lbl.' . $item) . '"';
      }, $items));
  }
}
