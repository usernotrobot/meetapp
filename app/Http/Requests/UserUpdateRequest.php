<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

use Illuminate\Support\Facades\Auth;

class UserUpdateRequest extends FormRequest {
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize() {
    return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules() {
    $user = Auth::user();
    if ($user->isGlobalAdmin()) {
      return [
        'first_name' => 'required|max:255',
        'last_name' => 'present|max:255',
        'email' => [
          'required', 'email', 'max:255',
          Rule::unique('users')->where(function ($query) {
            if ($this->input('client_id')) {
              $query->where('client_id', '<>', $this->input('client_id'))
                ->orWhere(function ($query) {
                  $query->whereNull('client_id');
                });
            } else {
              $query->whereNull('client_id')
                ->where('id', '<>', Auth::user()->id);
            }
          })
        ],
        'password' => 'sometimes|min:6|confirmed',

        /* Union/Company */
        'union' => 'present|max:255',
        'branch' => 'present|max:255',
        'delegate_id' => 'present|max:255',
      ];
    }

    return [
      /* User validation */
      'first_name' => 'required|max:255',
      'last_name' => 'required|max:255',
      'email' => [
        'required', 'email', 'max:255',
        Rule::unique('users')->where(function ($query) {
          if ($this->input('client_id')) {
            $query->where('client_id', '<>', $this->input('client_id'))
              ->orWhere(function ($query) {
                $query->whereNull('client_id');
              });
          } else {
            $query->whereNull('client_id')
              ->where('id', '<>', Auth::user()->id);
          }
        })
      ],
      'password' => 'sometimes|min:6|confirmed',

      /* Union/Company */
      'union' => 'required|max:255',
      'branch' => 'present|max:255',
      'delegate_id' => 'present|max:255',
    ];
  }

  public function attributes() {
    $user_items = ['first_name', 'last_name', 'email', 'password',
                   'union', 'union_sort_key', 'branch', 'branch_sort_key', 'delegate_id'];
    return
      array_combine($user_items, array_map(function($item) {
        return '"' . trans('users.lbl.' . $item) . '"';
      }, $user_items));
  }

  public function messages() {
    return [
      'email.unique' => trans('users.msg.user-exists'),
    ];
  }
}
