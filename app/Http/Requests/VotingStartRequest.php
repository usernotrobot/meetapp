<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use App\Models\Topic;

class VotingStartRequest extends FormRequest {
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize() {
    return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules() {
    return [
      'length' => 'required|int|min:3|max:120',
      'answers' => 'required|array|min:2',
      'type' => ['required', Rule::in(Topic::allVotingTypes())],
      'privacy' => ['required', Rule::in(Topic::allVotingPrivacy())],
    ];
  }

  public function attributes() {
    $items = ['length', 'answers', 'type', 'privacy'];

    return array_combine($items, array_map(function($item) {
      return '"' . trans('voting.lbl.' . $item) . '"';
    }, $items));
  }
}
