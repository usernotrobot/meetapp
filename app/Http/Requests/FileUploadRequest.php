<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Kamaln7\Toastr\Facades\Toastr;

class FileUploadRequest extends FormRequest {
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize() {
    return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules() {
    return [
      'file' => 'required|file|mimes:pdf,xls,xlsx,doc,docx|max:50000',
    ];
  }

  protected function formatErrors(Validator $validator) {
    if (!$this->ajax()) {
      foreach ($validator->errors()->all() as $error) {
        Toastr::error($error);
      }
    }

    return $validator->errors()->all();
  }
}
