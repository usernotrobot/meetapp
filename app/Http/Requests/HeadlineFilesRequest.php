<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class HeadlineFilesRequest extends FormRequest {
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize() {
    return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules() {
	  return [
		  'file.*' => 'required|file|mimes:pdf,xls,xlsx,doc,docx|max:50000',
	  ];
  }

  public function attributes() {
    $items = ['file'];

    return array_combine($items, array_map(function($item) {
        return '"' . trans('headlines.lbl.' . $item) . '"';
      }, $items));
  }
}
