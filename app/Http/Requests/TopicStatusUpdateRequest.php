<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Topic;

class TopicStatusUpdateRequest extends FormRequest {
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize() {
    return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules() {
    return [
      'status' => 'required|in:' . implode(',', Topic::availableStatuses())
    ];
  }

  public function attributes() {
    $items = ['status'];

    return array_combine($items, array_map(function($item) {
      return '"' . trans('topics.lbl.' . $item) . '"';
    }, $items));
  }
}
