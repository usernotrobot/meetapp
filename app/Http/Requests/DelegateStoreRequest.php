<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

use App\User;
use App\Repositories\EventRepository;

class DelegateStoreRequest extends FormRequest {
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize() {
/*    $event = app(EventRepository::class)->get($this->route('event'));
    return $event && $this->user()->can('manage-delegates', $event);*/
    return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules() {
    $event_id = $this->route('event');

    return [
      /* User validation */
      'first_name' => 'required|max:255',
      'last_name' => 'required|max:255',
      'email' => [
        'required', 'email', 'max:255',
        Rule::unique('users')->where(function ($query) {
          $query->where('client_id', '<>', $this->input('client_id'))
            ->orWhere(function ($query) {
              $query->whereNull('client_id');
            });
        })
      ],
      'password' => 'required|min:6|confirmed',

      /* Union/Company */
      'union' => 'required|max:255',
      'union_sort_key' => 'present|max:255',
      'branch' => 'present|max:255',
      'branch_sort_key' => 'present|max:255',
      'delegate_id' => 'present|required_if:role,' . User::ROLE_DELEGATE . '|max:255',

      /* Event rights & stuff */
      'participant_id' => [
        'present',
        Rule::unique('delegates')->where(function ($query) use ($event_id) {
          $query->where('event_id', $event_id);
        }),
        'integer',
        'max:2147483647'
      ],
      'role' => 'required|in:' . implode(',', User::allRoles()),

      'right_vote' => 'sometimes|bool',
      'right_submit' => 'sometimes|bool',
      'right_speak' => 'sometimes|bool',

      'notify' => 'sometimes|bool',
    ];
  }

  public function attributes() {
    $user_items = ['first_name', 'last_name', 'email', 'password',
              'union', 'union_sort_key', 'branch', 'branch_sort_key', 'delegate_id'];
    $delegate_items = ['participant_id', 'role', 'right_vote', 'right_speak', 'right_submit', 'notify'];
    return
    array_merge(
      array_combine($user_items, array_map(function($item) {
        return '"' . trans('users.lbl.' . str_replace('_', '-', $item)) . '"';
      }, $user_items)),
      array_combine($delegate_items, array_map(function($item) {
        return '"' . trans('delegates.lbl.' . str_replace('_', '-', $item)) . '"';
      }, $delegate_items))
    );
  }

  public function messages() {
    return [
      'email.unique' => trans('users.msg.user-exists'),
    ];
  }
}
