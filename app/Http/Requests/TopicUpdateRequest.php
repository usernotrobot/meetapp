<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Topic;

class TopicUpdateRequest extends FormRequest {
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize() {
    return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules() {
    return [
      'name' => 'required|max:255',
      'description' => 'present|max:255',
      'status' => 'required|in:' . implode(',', Topic::availableStatuses()),

      /* topic settings */
      'speech_times' => 'required|integer|min:' . Topic::SPEECH_COUNT_MIN . '|max:' . Topic::SPEECH_COUNT_MAX,
      'speech_length.1' => 'required|integer|min:1|max:60',
      'speech_length.2' => 'required_if:speech_times,2|integer|min:1|max:60',
      'speech_length.3' => 'required_if:speech_times,3|integer|min:1|max:60',
      'speech_opened' => 'sometimes|bool',
    ];
  }

  public function attributes() {
    $items = ['name', 'description', 'status', 'speech_times', 'speech_opened'];

    return array_merge(
      array_combine($items, array_map(function($item) {
        return '"' . trans('topics.lbl.' . $item) . '"';
      }, $items)),
      ['speech_length.*' => '"' . trans('topics.lbl.speech_length_item' ) . '"']);
  }
}
