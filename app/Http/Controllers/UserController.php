<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;

use App\Repositories\UserRepository;
use App\Http\Requests\UserUpdateRequest;

use Kamaln7\Toastr\Facades\Toastr;

class UserController extends Controller {
  /**
   * @var UserRepository
   */
  protected $repository;

  public function __construct(UserRepository $repository) {
    $this->middleware('auth');

    $this->repository = $repository;
  }

  /**
   * Redirects to right location according to user's role
   * GET /
   *
   * @return \Illuminate\Http\RedirectResponse
   */
  public function redirect() {
    if (Auth::user()->isAdmin()) {
      return redirect()->route('admin::events.index');
    }

    $event = Auth::user()->current_event;
    if ($event) {
      if ($event->active()) {
        return redirect()->route('user::now', $event);
      }

      return redirect()->route('user::agenda', $event);
    }

    return redirect()->route('profile');
  }

  /**
   * Show the form for editing the specified resource.
   * GET /profile
   *
   * @return \Illuminate\Http\Response
   */
  public function edit() {
    $user = Auth::user();

    return view('profile.edit', compact('user'));
  }


  /**
   * Update the specified resource in storage.
   * PUT /profile
   *
   * @param  UserUpdateRequest $request
   * @return \Illuminate\Http\Response
   */
  public function update(UserUpdateRequest $request) {
    $user_attributes = $request->only(['first_name', 'last_name', 'union', 'branch', 'delegate_id', 'email']);

    /* TODO: Move password crypting to the same lvl */
    if ($request->get('password')) {
      $user_attributes['password'] = bcrypt($request->get('password'));
    }

    $user = $this->repository->update($user_attributes, Auth::user()->id);

    Toastr::success(trans('users.msg.profile-update-success'));

    return redirect()->route('profile');
  }

}
