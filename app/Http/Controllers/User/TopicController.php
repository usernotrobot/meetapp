<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller as BaseController;

use App\Models\Topic;

use App\Repositories\HeadlineRepository;
use App\Repositories\TopicRepository;
use App\Repositories\EventRepository;
use App\Repositories\SpeechRepository;
use App\Repositories\VoteRepository;
use App\Repositories\Files\TopicFileRepository;

use App\Services\Contracts\DelegateRouter;

use App\Http\Requests\TopicSettingsUpdateRequest;
use App\Http\Requests\TopicStatusUpdateRequest;
use App\Http\Requests\TopicModerationCreateRequest;
use App\Http\Requests\FileUploadRequest;
use App\Http\Requests\VotingStartRequest;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Kamaln7\Toastr\Facades\Toastr;

use App\Jobs\FinishVoting;

use Carbon\Carbon;

use App\Repositories\SuggestionRepository;
use App\Repositories\FileRepository;

class TopicController extends BaseController {
  use CurrentEventTrait;

  /**
   * @var TopicRepository
   */
  protected $repository;

  /**
   * @var TopicRepository
   */
  protected $topicRepository;

  /**
   * @var EventRepository
   */
  protected $eventRepository;

  /**
   * @var DelegateRouter
   */
  protected $delegateRouter;


  protected $suggestionRepository;
  protected $fileRepository;

  public function __construct(
  	HeadlineRepository $headline_repository,
  	TopicRepository $repository, EventRepository $eventRepository,
                              DelegateRouter $delegateRouter, SuggestionRepository $suggestionRepository, FileRepository $fileRepository ) {
    $this->middleware('auth');

    $this->topicRepository = $this->repository = $repository;
    $this->eventRepository = $eventRepository;
    $this->delegateRouter = $delegateRouter;
    $this->headline_repository = $headline_repository;

    $this->suggestionRepository = $suggestionRepository;
    $this->fileRepository = $fileRepository;
  }

  /**
   * Display a listing of the resource.
   * GET /events/{event}/topics
   * GET /events/current/topics
   *
   * @param int $id
   * @return \Illuminate\Http\Response
   */
  public function index($id = null) {
    $user = Auth::user();

    $event = $this->routeEvent($user, $id);
    $this->authorize('view-topics', $event);

    $headlines = $this->headline_repository->getAllByEvent($event)->paginate();

    return view('user.topics.index', compact('event', 'headlines', 'user'));
  }

  /**
   * Display the specified resource.
   * GET /events/{event}/topics/{topic}
   *
   * @param int $event_id
   * @param int $topic_id
   * @return \Illuminate\Http\Response
   */
  public function show($event_id, $topic_id) {
    $user = Auth::user();

    $event = $this->routeEvent($user, $event_id);
    $topic = $this->routeTopic($event, $topic_id);
    $this->authorize('view-topics', $event);

    $suggestions = $topic->suggestions->sortByDesc(function ($item) {
      return $item->created_at;
    })->take(5);

    list($suggestionsCount, $suggestionsDelegatesCount) = $topic->suggestionsSummary();

    $speechlist = app(SpeechRepository::class)->getNextSpeakers($topic);

    return view('user.topics.show',
                compact('topic', 'event', 'speechlist', 'user',
                        'suggestions', 'suggestionsCount', 'suggestionsDelegatesCount'));
  }

  /**
   * Show topic based on current topic.
   * GET /events/{event}/topics/{topic}
   *
   * @param int $event_id
   * @param int $topic_id
   * @return \Illuminate\Http\Response
   */
  public function createNewBasedOnCurrent($event_id, $topic_id = null) {
      $user = Auth::user();

      if (empty($topic_id)) {
          $topic_id = $event_id;
          $event_id = null;
      }

      $event = $this->routeEvent($user, $event_id);
      $topic = $this->routeTopic($event, $topic_id);
      $this->authorize('secretary', $event);

      $suggestions = $topic->suggestions->sortByDesc(function ($item) {
          return $item->created_at;
      })->values()->map->shortForm();

      list($suggestionsCount, $suggestionsDelegatesCount) = $topic->suggestionsSummary();

      $routes_json = json_encode(
      ['suggestion' => [
          'index' => route('user::topics.suggestions', [$event, $topic]),
          'get' => route('user::suggestions.show', ['event' => $event, 'topic' => $topic, 'suggestion' => '_id_']),
          'update' => route('user::suggestions.update', ['event' => $event, 'topic' => $topic, 'suggestion' => '_id_']),
          'store' => route('user::suggestions.store', ['event' => $event, 'topic' => $topic]),
          'delete' => route('user::suggestions.destroy', ['event' => $event, 'topic' => $topic, 'suggestion' => '_id_']),
          'moderation' => route('user::suggestions.moderation.save', ['event' => $event, 'topic' => $topic])
      ]],
      JSON_UNESCAPED_SLASHES | JSON_FORCE_OBJECT | JSON_UNESCAPED_UNICODE | JSON_PARTIAL_OUTPUT_ON_ERROR | JSON_HEX_APOS);

      $updateUrl = route('user::now-json', [$event]);

      $speechlist = app(SpeechRepository::class)->getNextSpeakers($topic);

      return view('user.topics.moderation',
          compact('topic', 'event', 'speechlist', 'user', 'routes_json', 'updateUrl',
              'suggestions', 'suggestionsCount', 'suggestionsDelegatesCount'));
  }

  public function moderationSave(TopicModerationCreateRequest $request, $event_id, $topic_id)
  {
    $user = Auth::user();

    $event = $this->routeEvent($user, $event_id);
    $topic = $this->routeTopic($event, $topic_id);

    $this->authorize('secretary', $event);

    $data = $request->intersect(['name', 'pickup_pdf', 'preserve_list', 'suggestions_id']);

    $new_data = [
      "headline_id" => $topic->headline->id,
      "name" => $data['name'],
      "description" => $topic->description,
      "status" => "open",
      "speech_times" => $topic->speech_times,
      "speech_length_default" => $topic->speech_length_default,
      "speech_opened" => $topic->speech_opened,
      "speech_length" => $topic->speech_length,
      "voting_settings" => $topic->voting_settings,
      "voting_start" => $topic->voting_start,
      "voting_end" => $topic->voting_end,
      "voting_results" => $topic->voting_results
    ];

    //Create new topic from other one
    $result = app(TopicRepository::class)->create($new_data, $event);

    if (key_exists('suggestions_id', $data)) {
      $suggestions = $this->suggestionRepository->findWhereIn('id', $data['suggestions_id']);

      foreach ($suggestions as $suggestion) {
        $this->suggestionRepository->update(
          ['topic_id' => $result->id],
          $suggestion->id
        );
      }
    }

    if (key_exists('pickup_pdf', $data)) {
      $pickup_pdf = filter_var($data['pickup_pdf'], FILTER_VALIDATE_BOOLEAN);

      if ($pickup_pdf) {
        $files = $topic->files->all();

        foreach ($files as $file) {
          $this->fileRepository->update(
            ['entity_id' => $result->id, 'entity_type' => get_class($result)],
            $file->id
          );
        }
      }
    }

    if (key_exists('preserve_list', $data)) {
      $preserve_list = filter_var($data['preserve_list'], FILTER_VALIDATE_BOOLEAN);

      if ($preserve_list) {
        $speechlist = app(SpeechRepository::class)->getNextSpeakers($topic);

        foreach ($speechlist as $speech) {
          app(SpeechRepository::class)->update(['topic_id' => $result->id], $speech->id);
        }
      }
    }

    app(TopicRepository::class)->update(['status' => 'closed'], $topic->id);

    return ['status' => 'success', 'url' => route('user::topics.show', [$event, $result])];
  }

  /**
   * Gets topic file list
   * GET /events/{event}/topics/{topic}/files
   *
   * @param int $event_id
   * @param int $topic_id
   * @return \Illuminate\Http\Response
   */
  public function files($event_id, $topic_id) {
    $user = Auth::user();

    $event = $this->routeEvent($user, $event_id);
    $topic = $this->routeTopic($event, $topic_id);

    $this->authorize('view-topics', $event);

    $files = $topic->files->all();
    $pageItems = config('repository.pagination.limit');

    return view('user.topics.files', compact('topic', 'files', 'event', 'pageItems', 'user'));
  }

  /**
   * Gets topic settings page
   * GET /events/{event}/topics/{topic}/settings
   *
   * @param $event_id
   * @param $topic_id
   * @return \Illuminate\Http\Response
   */
  public function settings($event_id, $topic_id) {
    $user = Auth::user();

    $event = $this->routeEvent($user, $event_id);
    $topic = $this->routeTopic($event, $topic_id);

    $this->authorize('manage-speechlist', $event);

    $speech_min = Topic::SPEECH_COUNT_MIN;
    $speech_max = Topic::SPEECH_COUNT_MAX;
    $speech_default = Topic::SPEECH_DEFAULT_LENGTH / 60;

    return view('user.topics.settings', compact('topic', 'event', 'user', 'speech_min', 'speech_max', 'speech_default'));
  }

  /**
   * Update topic settings
   * PUT /events/{event}/topics/{topic}/settings
   *
   * @param TopicSettingsUpdateRequest $request
   * @param $event_id
   * @param $topic_id
   * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
   */
  public function settingsUpdate(TopicSettingsUpdateRequest $request, $event_id, $topic_id) {
    $user = Auth::user();

    $event = $this->routeEvent($user, $event_id);
    $topic = $this->routeTopic($event, $topic_id);

    $this->authorize('manage-speechlist', $event);

    $topic = $this->repository->updateSettings(
      $request->intersect(['speech_length', 'speech_times', 'speech_opened']),
      $topic);

    if ($request->ajax()) {
      return response()->json($topic);
    }

    Toastr::success(trans('topics.msg.settings-update-success'));

    return redirect()->route('user::topics.show', [$event, $topic]);
  }

  /***
   * Changes Topic status
   * POST /events/{event}/topics/{topic}/status
   *
   * @param TopicStatusUpdateRequest $request
   * @param $event_id
   * @param $topic_id
   * @return \Illuminate\Http\JsonResponse
   */
  public function status(TopicStatusUpdateRequest $request, $event_id, $topic_id) {
    $user = Auth::user();

    $event = $this->routeEvent($user, $event_id);
    $topic = $this->routeTopic($event, $topic_id);

    $this->authorize('manage-topics', $event);

    $status = $request->get('status');

    if ($status == Topic::STATUS_VOTING) {
      return response()->json(['error' => trans('topics.lbl')], 403);
    }

    $topic->status = $status;
    $this->repository->update(['status' => $status], $topic_id);

    return response()->json($topic);
  }

  /**
   * Uploads file to topic
   * POST /events/{event}/topics/{topic}/upload
   *
   * @param FileUploadRequest $request
   * @param TopicFileRepository $fileRepository
   * @param $event_id
   * @param $topic_id
   * @return \Illuminate\Http\Response
   */
  public function upload(FileUploadRequest $request, TopicFileRepository $fileRepository,
                         $event_id, $topic_id) {
    $user = Auth::user();

    $event = $this->routeEvent($user, $event_id);
    $topic = $this->routeTopic($event, $topic_id);

    $this->authorize('manage-topics', $event);

    if ($request->file('file')->isValid()) {
      $fileRepository->upload($request->file('file'), $topic, Auth::user(),
                              $request->file('file')->getClientOriginalName(), $request->get('name'));

      Toastr::success(trans('app.msg.file-upload-success'));
    } else {
      Toastr::error(trans('app.msg.file-upload-fail'));
    }

    return redirect()->route('user::topics.files', [$event, $topic]);
  }

  /**
   * Starts topic voting
   * POST /events/{event}/topics/{topic}/voting/start
   *
   * @param VotingStartRequest $request
   * @param $event_id
   * @param $topic_id
   * @return \Illuminate\Http\JsonResponse
   */
  public function startVoting(VotingStartRequest $request, $event_id, $topic_id) {
    $user = Auth::user();
    $event = $this->routeEvent($user, $event_id);
    $topic = $this->routeTopic($event, $topic_id);

    $this->authorize('manage-voting', $event);

    if ($topic->closed() || $topic->voting()) {
      return response()->json(['error' => trans('voting.msg.unable-to-start')], 400);
    }

    $topic = $this->repository->startVoting($topic, $request->intersect(['length','type','answers','privacy']));

    if($topic->voting_type == Topic::VOTING_TYPE_AUTO) {
      dispatch((new FinishVoting($topic))
                 ->delay(Carbon::now()->addSeconds($topic->voting_length)));
    }

    return response()->json($topic);
  }

  /**
   * Saves delegate's vote
   * POST /events/{event}/topics/{topic}/voting
   *
   * @param VoteRepository $voteRepository
   * @param Request $request
   * @param $event_id
   * @param $topic_id
   * @return mixed
   */
  public function vote(VoteRepository $voteRepository,
                       Request $request, $event_id, $topic_id) {
    $user = Auth::user();
    $event = $this->routeEvent($user, $event_id);
    $topic = $this->routeTopic($event, $topic_id);

    $this->authorize('vote', $event);

    $delegate = $user->delegate($event);

    if (!$topic->allowedToVote()) {
      return response()->json(['error' => trans('voting.msg.vote-finished')], 400);
    }

    $vote = $voteRepository->updateOrCreate(
      ['topic_id' => $topic->id,
       'delegate_id' => $delegate->id],
      ['vote' => $request->get('vote')]);

    return response()->json($vote);
  }

  /**
   * Posts manual vote results from moderator and finishes voting
   * POST /events/{event}/topics/{topic}/voting/manual
   *
   * @param Request $request
   * @param $event_id
   * @param $topic_id
   * @return \Illuminate\Http\JsonResponse
   */
  public function manualVoting(Request $request, $event_id, $topic_id) {
    $user = Auth::user();
    $event = $this->routeEvent($user, $event_id);
    $topic = $this->routeTopic($event, $topic_id);

    $this->authorize('manage-voting', $event);

    $topic = $this->repository->manualVoting($topic, $request->get('answers'));

    return response()->json($topic);
  }

  /**
   * Recalcs auto voting results and finishes voting
   * POST /events/{event}/topics/{topic}/voting/auto
   *
   * @param $event_id
   * @param $topic_id
   * @return \Illuminate\Http\JsonResponse
   */
  public function autoVoting($event_id, $topic_id) {
    $user = Auth::user();
    $event = $this->routeEvent($user, $event_id);
    $topic = $this->routeTopic($event, $topic_id);

    $this->authorize('manage-voting', $event);

    $topic = $this->repository->finishVoting($topic);

    return response()->json($topic);
  }

  /**
   * Cancels voting and sets topic to opened. Deletes all previous voting results
   * POST /events/{event}/topics/{topic}/voting/cancel
   *
   * @param $event_id
   * @param $topic_id
   * @return \Illuminate\Http\JsonResponse
   */
  public function cancelVoting($event_id, $topic_id) {
    $user = Auth::user();
    $event = $this->routeEvent($user, $event_id);
    $topic = $this->routeTopic($event, $topic_id);

    $this->authorize('manage-voting', $event);

    $topic = $this->repository->cancelVoting($topic);

    return response()->json($topic);
  }
}
