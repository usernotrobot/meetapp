<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller as BaseController;
use Illuminate\Http\Request;

use App\Repositories\TopicRepository;
use App\Repositories\EventRepository;
use App\Repositories\SuggestionRepository;

use App\Services\Contracts\DelegateRouter;

use Illuminate\Support\Facades\Auth;

use App\Http\Requests\SuggestionStoreRequest;
use App\Http\Requests\SuggestionUpdateRequest;

use Kamaln7\Toastr\Facades\Toastr;

class SuggestionController extends BaseController {
  use CurrentEventTrait;

  /**
   * @var SuggestionRepository
   */
  protected $repository;

  /**
   * @var EventRepository
   */
  protected $eventRepository;

  /**
   * @var TopicRepository
   */
  protected $topicRepository;

  /**
   * @var DelegateRouter
   */
  protected $delegateRouter;

  public function __construct(SuggestionRepository $repository,
                              TopicRepository $topicRepository,
                              EventRepository $eventRepository,
                              DelegateRouter $delegateRouter) {
    $this->middleware('auth');

    $this->repository = $repository;
    $this->eventRepository = $eventRepository;
    $this->topicRepository = $topicRepository;
    $this->delegateRouter = $delegateRouter;
  }

  /**
   * Gets all topics to the event with suggestions count and last suggestion info
   *
   * GET /events/{event}/suggestions
   *
   * @param $event_id
   * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
   */
  public function event($event_id) {
    $user = Auth::user();
    $event = $this->routeEvent($user, $event_id);

    $topics = $event->topics;

    $pageItems = config('repository.pagination.limit');

    return view('user.suggestions.event', compact('user', 'event', 'topics', 'pageItems'));
  }

  /**
   * Gets all topic suggestions
   *
   * GET /events/{event}/topics/{topic}/suggestions
   *
   * @param Request $request
   * @param $event_id
   * @param $topic_id
   * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
   */
  public function index(Request $request, $event_id, $topic_id) {
    $user = Auth::user();

    $event = $this->routeEvent($user, $event_id);
    $topic = $this->routeTopic($event, $topic_id);

    $suggestions = $this->repository->getAllByTopic($topic)->all();
    $pageItems = config('repository.pagination.limit');

    return ($request->ajax())
      ? response()->json(
        ['suggestions' => $suggestions->sortByDesc(function($item) { return $item->created_at; })->values()->map->shortForm()])
      : view('user.suggestions.index', compact('event', 'topic', 'user', 'suggestions', 'pageItems'));
  }

  /**
   * Create suggestion view
   * GET /events/{event}/topics/{topic}/suggestions/create
   *
   * @param $event_id
   * @param $topic_id
   * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
   */
  public function create($event_id, $topic_id) {
    $user = Auth::user();

    $event = $this->routeEvent($user, $event_id);
    $topic = $this->routeTopic($event, $topic_id);

    $this->authorize('add-suggestions', $topic);

    return view('user.suggestions.create', compact('user', 'event', 'topic'));
  }

  /**
   * Saves new suggestion
   * POST /events/{event}/topics/{topic}/suggestions
   *
   * @param SuggestionStoreRequest $request
   * @param $event_id
   * @param $topic_id

   * @return \Illuminate\Http\RedirectResponse
   */
  public function store(SuggestionStoreRequest $request, $event_id, $topic_id) {
    $user = Auth::user();

    $event = $this->routeEvent($user, $event_id);
    $topic = $this->routeTopic($event, $topic_id);

    $this->authorize('add-suggestions', $topic);

    $suggestion = $this->repository->create(
      array_merge(
        $request->intersect(['summary', 'details']),
        ['user_id' => $user->id, 'topic_id' => $topic->id]
      ));

    $request->ajax() ?: Toastr::success(trans('suggestions.msg.create-success'));

    return $request->ajax()
      ? response()->json($suggestion)
      : redirect()->route('user::topics.suggestions', [$event, $topic]);
  }

  /**
   * Shows specific suggestion
   * GET /events/{event}/topics/{topic}/suggestions/{suggestion}
   *
   * @param Request $request
   * @param $event_id
   * @param $topic_id
   * @param $suggestion_id
   * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
   */
  public function show(Request $request, $event_id, $topic_id, $suggestion_id) {
    $user = Auth::user();

    $event = $this->routeEvent($user, $event_id);
    $topic = $this->routeTopic($event, $topic_id);

    $suggestion = $this->repository->get($suggestion_id);

    return $request->ajax()
      ? response()->json($suggestion)
      : view('user.suggestions.show', compact('user', 'event', 'topic', 'suggestion'));
  }

  /**
   * Page edit the suggestion
   * GET /events/{event}/topics/{topic}/suggestions/{suggestion}/edit
   *
   * @param $event_id
   * @param $topic_id
   * @param $suggestion_id
   * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
   */
  public function edit($event_id, $topic_id, $suggestion_id) {
    $user = Auth::user();

    $event = $this->routeEvent($user, $event_id);
    $topic = $this->routeTopic($event, $topic_id);

    $suggestion = $this->repository->get($suggestion_id);

    $this->authorize('manage', $suggestion);

    return view('user.suggestions.edit', compact('user', 'event', 'topic', 'suggestion'));
  }

  /**
   * Updates suggestion fields
   * PUT /events/{event}/topics/{topic}/suggestions/{suggestion}
   *
   * @param SuggestionUpdateRequest $request
   * @param $event_id
   * @param $topic_id
   * @param null $suggestion_id
   * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
   */
  public function update(SuggestionUpdateRequest $request, $event_id, $topic_id, $suggestion_id) {
    $user = Auth::user();

    $event = $this->routeEvent($user, $event_id);
    $topic = $this->routeTopic($event, $topic_id);

    $suggestion = $this->repository->get($suggestion_id);
    $this->authorize('manage', $suggestion);

    $this->repository->update($request->intersect(['summary', 'details']), $suggestion->id);

    $request->ajax() ?: Toastr::success(trans('suggestions.msg.update-success'));

    return $request->ajax()
      ? response()->json($suggestion)
      : redirect()->route('user::topics.suggestions', [$event, $topic]);
  }

  /**
   * Deletes specified suggestion
   * DELETE /events/{event}/topics/{topic}/suggestions/{suggestion}
   *
   * @param Request $request
   * @param $event_id
   * @param $topic_id
   * @param $suggestion_id
   * @return \Illuminate\Http\JsonResponse
   */
  public function destroy(Request $request, $event_id, $topic_id, $suggestion_id) {
    $user = Auth::user();

    $event = $this->routeEvent($user, $event_id);
    $topic = $this->routeTopic($event, $topic_id);

    $suggestion = $this->repository->get($suggestion_id);
    $this->authorize('manage', $suggestion);

    $state = $this->repository->delete($suggestion->id);

    if ($request->get('showMessage')) {
      Toastr::success(trans('suggestions.msg.delete-success'));
    }

    return response()->json(['status' => trans('suggestions.msg.delete-success')]);
  }

  /**
   * Gets authenticated user's suggestion list
   * GET /events/{event}/my-suggestions
   *
   * @param $event_id
   * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
   */
  public function my($event_id) {
    $user = Auth::user();

    $event = $this->routeEvent($user, $event_id);

    $pageItems = config('repository.pagination.limit');

	  $suggestions = $this->repository->getMySuggestions(Auth::user()->id)->all();

    return view('user.suggestions.my', compact('user', 'event', 'suggestions', 'pageItems'));
  }

}
