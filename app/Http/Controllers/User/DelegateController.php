<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller as BaseController;

use App\Repositories\EventRepository;
use App\Repositories\DelegateRepository;

use Illuminate\Support\Facades\Auth;
use App\Services\Contracts\DelegateRouter;

use Illuminate\Http\Request;

class DelegateController extends BaseController {
  /**
   * The events repository instance.
   * @var DelegateRepository
   */
  protected $repository;

  /**
   * EventController constructor.
   * Create a new controller instance.
   *
   * @param DelegateRepository $repository
   */
  public function __construct(DelegateRepository $repository) {
    $this->repository = $repository;
  }

}
