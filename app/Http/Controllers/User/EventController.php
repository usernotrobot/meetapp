<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller as BaseController;

use App\Repositories\EventRepository;
use App\Repositories\TopicRepository;
use App\Repositories\SpeechRepository;
use App\Repositories\SuggestionRepository;

use Illuminate\Support\Facades\Auth;
use App\Services\Contracts\DelegateRouter;

use App\Models\Event;

class EventController extends BaseController {
  /**
   * The events repository instance.
   * @var EventRepository
   */
  protected $repository;

  /**
   * EventController constructor.
   * Create a new controller instance.
   *
   * @param EventRepository $repository
   */
  public function __construct(EventRepository $repository) {
    $this->repository = $repository;
  }

  /**
   * Gets event file list
   * GET /event/{event}/files
   *
   * @param DelegateRouter $delegateRouter
   * @param int $id
   * @return \Illuminate\Http\Response
   */
  public function files(DelegateRouter $delegateRouter, $id = null) {
    $user = Auth::user();

    $event = $delegateRouter->get($id, $user);
    $this->authorize('view', $event);

    $files = $event->files->all();
    $pageItems = config('repository.pagination.limit');

    return view('user.events.files', compact('event', 'files', 'pageItems', 'user'));
  }

  /**
   * Gets events list
   * GET /event/all/
   * GET /event/all/upcoming
   * GET /event/all/past
   *
   * @param null $status
   * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
   */
  public function other($status = null) {
    $user = Auth::user();

    $this->authorize('index', Event::class);

    $eventStatus = null;
    if ($status) {
      if ($status == 'upcoming') $eventStatus = [Event::STATUS_ACTIVE, Event::STATUS_PLANNED];
      elseif($status == 'past') $eventStatus = [Event::STATUS_COMPLETED];
    }

    $events = $this->repository->getDelegateEvents($user, $eventStatus)->paginate();

    return view('user.events.index', compact('events', 'status', 'user'));
  }

  /**
   * NOW page
   * GET /events/{event}/now
   *
   * @param TopicRepository $topicRepository
   * @param SpeechRepository $speechRepository
   * @param DelegateRouter $delegateRouter
   * @param null $event_id
   * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
   */
  public function now(
                      TopicRepository $topicRepository,
                      SpeechRepository $speechRepository,
                      DelegateRouter $delegateRouter,
                      $event_id) {
    $user = Auth::user();

    $event = $delegateRouter->get($event_id, $user);
    $this->authorize('view', $event);

	  $topic = $topicRepository->getActiveTopic($event);

    $delegate = $user->delegate($event);

    $speechlist = []; $topic_json = '{}';
    if ($topic) {
      $speechlist = $speechRepository->getNextSpeakers($topic);
      $topic_json = json_encode($topic->transform(),
                                JSON_UNESCAPED_SLASHES | JSON_FORCE_OBJECT | JSON_UNESCAPED_UNICODE | JSON_PARTIAL_OUTPUT_ON_ERROR | JSON_HEX_APOS);

    }

    $delegate_json = json_encode($delegate->transform(),
                                 JSON_UNESCAPED_SLASHES | JSON_FORCE_OBJECT | JSON_UNESCAPED_UNICODE | JSON_PARTIAL_OUTPUT_ON_ERROR | JSON_HEX_APOS);

    $routes_json = json_encode($this->mapNowRoutesToJson($event),
      JSON_UNESCAPED_SLASHES | JSON_FORCE_OBJECT | JSON_UNESCAPED_UNICODE | JSON_PARTIAL_OUTPUT_ON_ERROR | JSON_HEX_APOS);

    $speechlist_json = json_encode($speechlist,
                               JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE | JSON_PARTIAL_OUTPUT_ON_ERROR | JSON_HEX_APOS);

    $updateUrl = route('user::now-json', [$event]);

    return view('user.events.now', compact('event','delegate_json', 'topic_json', 'routes_json',
                                           'speechlist_json', 'updateUrl'));
  }

  public function event($id) {
    return redirect()->route('user::agenda', $id);
  }

  public function current(DelegateRouter $dRouter) {
    $user = Auth::user();
    $delegate = $dRouter->current($user);

    if (!$delegate) {
      abort(404);
    }

    return redirect()->route('user::agenda', $delegate->event);
  }

  public function nowJson(TopicRepository $topicRepository,
                          SpeechRepository $speechRepository,
                          DelegateRouter $delegateRouter,
                          $event_id = null) {
    $user = Auth::user();

    $event = $delegateRouter->get($event_id, $user);
    $this->authorize('view', $event);

    $topic = $topicRepository->getActiveTopic($event);
    $delegate = $user->delegate($event);

    $delegate = $delegate->transform();
    $topic = $topic->transform();

    $routes = $this->mapNowRoutesToJson($event);

    $speechlist = $speechRepository->getNextSpeakers($topic);

    return response()->json(compact('delegate', 'topic', 'routes', 'speechlist'));
  }


  /** Helpers */
  public function mapNowRoutesToJson(Event $event) {
    return
      ['suggestion' => [
        'index' => route('user::topics.suggestions', [$event, '_topic_']),
        'get' => route('user::suggestions.show', ['event' => $event, 'topic' => '_topic_', 'suggestion' => '_id_']),
        'update' => route('user::suggestions.update', ['event' => $event, 'topic' => '_topic_', 'suggestion' => '_id_']),
        'store' => route('user::suggestions.store', ['event' => $event, 'topic' => '_topic_']),
        'delete' => route('user::suggestions.destroy', ['event' => $event, 'topic' => '_topic_', 'suggestion' => '_id_']),
      ],
       'speechlist' => [
         'index' => route('user::topics.speechlist', ['event' => $event, 'topic' => '_topic_']),
         'update' => route('user::speechlist.update', ['event' => $event, 'topic' => '_topic_', 'speech' => '_id_']),
         'store' => route('user::speechlist.store', ['event' => $event, 'topic' => '_topic_']),
         'delete' => route('user::speechlist.destroy', ['event' => $event, 'topic' => '_topic_', 'speech' => '_id_']),
         'next' => route('user::speechlist.next', ['event' => $event, 'topic' => '_topic_']),
         'edit-length' => route('user::speechlist.edit-length', ['event' => $event, 'topic' => '_topic_', 'speech' => '_id_']),
         'mark-last' => route('user::speechlist.mark-last', ['event' => $event, 'topic' => '_topic_', 'speech' => '_id_']),
         'update-priority' => route('user::speechlist.update-priority', ['event' => $event, 'topic' => '_topic_', 'speech' => '_id_']),
         'delegates' => route('user::speechlist.delegates', ['event' => $event, 'topic' => '_topic_']),
         'start' => route('user::speechlist.start', ['event' => $event, 'topic' => '_topic_', 'speech' => '_id_']),
         'stop' => route('user::speechlist.stop', ['event' => $event, 'topic' => '_topic_', 'speech' => '_id_']),
       ],
       'topic' => [
         'get' => route('user::topics.show', [$event, '_topic_']),
         'update' => route('user::topics.settings-update', [$event, '_topic_']),
         'voting' => route('user::topics.voting', [$event, '_topic_']),
         'vote' => route('user::topics.vote', [$event, '_topic_']),
         'status' => route('user::topics.status', [$event, '_topic_']),
         'files' => route('user::topics.files', [$event, '_topic_']),
         'voting-cancel' => route('user::topics.voting-cancel', [$event, '_topic_']),
         'voting-auto' => route('user::topics.voting-auto', [$event, '_topic_']),
         'voting-manual' => route('user::topics.voting-manual', [$event, '_topic_']),
       ],
       'user' => [
         'logout' => route('logout'),
       ],
       'event' => [
         'index' => route('user::events.all'),
       ]
      ];
  }
}
