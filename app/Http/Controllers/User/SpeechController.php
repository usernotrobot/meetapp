<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller as BaseController;

use App\Repositories\TopicRepository;
use App\Repositories\EventRepository;
use App\Repositories\SpeechRepository;
use App\Repositories\DelegateRepository;

use App\Services\Contracts\DelegateRouter;

use Illuminate\Support\Facades\Auth;

use App\Http\Requests\SpeechStoreRequest;
use App\Http\Requests\SpeechStatusUpdateRequest;
use App\Http\Requests\SpeechUpdateLengthRequest;
use App\Http\Requests\SpeechUpdatePriorityRequest;

use Illuminate\Http\Request;

use Kamaln7\Toastr\Facades\Toastr;

class SpeechController extends BaseController {
  use CurrentEventTrait;

  /**
   * @var SpeechRepository
   */
  protected $repository;

  /**
   * @var EventRepository
   */
  protected $eventRepository;

  /**
   * @var TopicRepository
   */
  protected $topicRepository;

  /**
   * @var DelegateRouter
   */
  protected $delegateRouter;

  public function __construct(SpeechRepository $repository,
                              TopicRepository $topicRepository,
                              EventRepository $eventRepository,
                              DelegateRouter $delegateRouter) {
    $this->middleware('auth');

    $this->repository = $repository;
    $this->eventRepository = $eventRepository;
    $this->topicRepository = $topicRepository;
    $this->delegateRouter = $delegateRouter;
  }

  /**
   * Gets topic speechlist
   * GET /events/{event}/topics/{topic}/speechlist
   *
   * @param DelegateRepository $delegateRepository
   * @param $event_id
   * @param $topic_id
   * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
   */
  public function index(DelegateRepository $delegateRepository, $event_id, $topic_id) {
    $user = Auth::user();

    $event = $this->routeEvent($user, $event_id);
    $topic = $this->routeTopic($event, $topic_id);
    $delegate = $user->delegate($event);

    $speechlist = $this->repository->getByTopic($topic);
    $pageItems = config('repository.pagination.limit');

    $speech_delegates = $delegateRepository->getAllowedToSpeak($event);

    return view('user.speechlist.index',
                compact('event', 'topic', 'user', 'speechlist', 'pageItems', 'delegate', 'speech_delegates'));
  }

  /**
   * Gets authenticated user's speech list
   * GET /events/{event}/my-speechlist
   *
   * @param null $event_id
   * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
   */
  public function my($event_id) {
    $user = Auth::user();

    $event = $this->routeEvent($user, $event_id);

    $speechlist = $this->repository->getByDelegate($user->delegate($event), $event);

    $pageItems = config('repository.pagination.limit');

    return view('user.speechlist.my', compact('user', 'event', 'speechlist', 'pageItems'));
  }

  /**
   * Creates new speechlist entry
   * POST /events/{event}/topics/{topic}/speechlist
   *
   * @param SpeechStoreRequest $request
   * @param DelegateRepository $delegateRepository
   * @param $event_id
   * @param $topic_id
   * @return \Illuminate\Http\JsonResponse
   */
  public function store(SpeechStoreRequest $request, DelegateRepository $delegateRepository,
                        $event_id, $topic_id) {
    $user = Auth::user();

    $event = $this->routeEvent($user, $event_id);
    $topic = $this->routeTopic($event, $topic_id);

    $this->authorize('add-speechlist', $topic);

    $delegate = $delegateRepository->get($request->get('delegate_id'));
    $runningDelegate = $user->delegate($event);

    if ($delegate->id != $runningDelegate->id && !$runningDelegate->moderator()) {
      if ($request->ajax()){
        return response()->json(false, 401);
      }

      abort(401);
    }

    if ($this->repository->countSpeechTimes($delegate, $topic) >= $topic->speech_times) {
      $error = trans('speechlist.msg.speech-times-exceed', ['max' => $topic->speech_times]);

      if ($request->ajax()) {
        return response()->json(['error' => $error], 403);
      }

      Toastr::error($error);
      return back();
    }

    if (!$topic->allowedForNewSpeeches()) {
      $error = trans('speechlist.msg.topic-discussion-closed', ['max' => $topic->speech_times]);

      if ($request->ajax()) {
        return response()->json(['error' => $error], 403);
      }

      Toastr::error($error);
      return back();
    }

    $speech = $this->repository->addWithOrderCheck($delegate, $topic);

    $request->ajax() ?: Toastr::success(trans('speechlist.msg.create-success'));

    if ($request->ajax()) {
      return response()->json(['success' => trans('speechlist.msg.create-success'), 'speech' => $speech]);
    }

    return back();
  }

  /**
   * Deletes a speechlist entry
   * DELETE /events/{event}/topics/{topic}/speechlist/{speech}
   *
   * @param Request $request
   * @param $event_id
   * @param $topic_id
   * @param $speech_id
   * @return \Illuminate\Http\JsonResponse
   */
  public function destroy(Request $request, $event_id, $topic_id, $speech_id) {
    $user = Auth::user();

    $event = $this->routeEvent($user, $event_id);
    $topic = $this->routeTopic($event, $topic_id);

    $speech = $this->repository->get($speech_id);

    $this->authorize('delete', $speech);

    $state = $this->repository->delete($speech->id);

    $request->ajax() ?: Toastr::success(trans('speechlist.msg.delete-success'));

    return response()->json(['success' => trans('speechlist.msg.delete-success')]);
  }

  /***
   * Marks speech as last for this topic
   * POST /events/{event}/topics/{topic}/speechlist/{speech}/last
   *
   * @param Request $request
   * @param $event_id
   * @param $topic_id
   * @param $speech_id
   * @return \Illuminate\Http\RedirectResponse
   */
  public function markLast(Request $request, $event_id, $topic_id, $speech_id) {
    $user = Auth::user();

    $event = $this->routeEvent($user, $event_id);
    $topic = $this->routeTopic($event, $topic_id);
    $this->authorize('manage-speechlist', $event);

    $speech = $this->repository->get($speech_id);

    $state = $this->repository->markLast($speech);
    $request->ajax() ?: Toastr::success(trans('speechlist.msg.mark-last-success',
                                              ['name' => $speech->delegate->user->name]));

    return response()->json(['success' => trans('speechlist.msg.mark-last-success',
                                                ['name' => $speech->delegate->user->name])]);
  }

  /***
   * Edit Length for specific speech
   * POST /events/{event}/topics/{topic}/speechlist/{speech}/length
   *
   * @param SpeechUpdateLengthRequest $request
   * @param $event_id
   * @param $topic_id
   * @param $speech_id
   * @return \Illuminate\Http\RedirectResponse
   */
  public function editLength(SpeechUpdateLengthRequest $request, $event_id, $topic_id, $speech_id) {
    $user = Auth::user();

    $event = $this->routeEvent($user, $event_id);
    $topic = $this->routeTopic($event, $topic_id);
    $this->authorize('manage-speechlist', $event);

    $speech = $this->repository->get($speech_id);

    if ($state = $this->repository->updateLength($speech, $request->get('length'))) {
      $request->ajax() ?: Toastr::success(trans('speechlist.msg.update-length-success',
                                                ['name' => $speech->delegate->user->name]));
      return response()->json(['success' => trans('speechlist.msg.update-length-success',
                                                  ['name' => $speech->delegate->user->name])]);
    }

    $request->ajax() ?: Toastr::error(trans('speechlist.msg.update-length-error'));
    return response()->json(['error' => trans('speechlist.msg.update-length-error')]);
  }


  /***
   * Update priority for specific speech
   * POST /events/{event}/topics/{topic}/speechlist/{speech}/priority
   *
   * @param SpeechUpdatePriorityRequest $request
   * @param $event_id
   * @param $topic_id
   * @param $speech_id
   * @return \Illuminate\Http\RedirectResponse
   */
  public function updatePriority(SpeechUpdatePriorityRequest $request, $event_id, $topic_id, $speech_id) {
    $user = Auth::user();

    $event = $this->routeEvent($user, $event_id);
    $topic = $this->routeTopic($event, $topic_id);
    $this->authorize('manage-speechlist', $event);

    $speech = $this->repository->get($speech_id);

    if ($state = $this->repository->updatePriority($speech, $request->get('priority'))) {
      $request->ajax() ?: Toastr::success(trans('speechlist.msg.update-priority-success',
                                                ['name' => $speech->delegate->user->name]));
      return response()->json(['success' => trans('speechlist.msg.update-priority-success',
                                                  ['name' => $speech->delegate->user->name])]);
    }

    $request->ajax() ?: Toastr::error(trans('speechlist.msg.update-priority-fail'));
    return response()->json(['error' => trans('speechlist.msg.update-priority-fail')]);
  }

  /**
   * Gets event delegates list with speaker permission
   * GET /event/{event}/delegates
   *
   * @param Request $request
   * @param DelegateRouter $delegateRouter
   * @param DelegateRepository $delegateRepository
   * @param int $id
   * @return \Illuminate\Http\Response
   */
  public function delegates(Request $request, DelegateRouter $delegateRouter,
                            DelegateRepository $delegateRepository, $id = null) {
    $user = Auth::user();

    $event = $delegateRouter->get($id, $user);
    $this->authorize('view', $event);

    $delegates = $delegateRepository->getAutocomplete($event, $request->get('q'));

    return response()->json($delegates);
  }

  /**
   * Gets speakers that are in waiting status
   * GET /events/{event}/topics/{topic}/next
   *
   * @param $event_id
   * @param $topic_id
   * @return \Illuminate\Http\JsonResponse
   */
  public function next($event_id, $topic_id) {
    $user = Auth::user();

    if (empty($topic_id)) {
      $topic_id = $event_id;
      $event_id = null;
    }

    $event = $this->routeEvent($user, $event_id);
    $topic = $this->routeTopic($event, $topic_id);

    $speechlist = $this->repository->getNextSpeakers($topic, false);

    return response()->json($speechlist);
  }

  /**
   * Starts speech
   * POST events/{event}/topics/{topic}/speechlist/{speech}/start
   *
   * @param $event_id
   * @param $topic_id
   * @param $speech_id
   * @return \Illuminate\Http\Response
   */

  public function start($event_id, $topic_id, $speech_id) {
    $user = Auth::user();

    $event = $this->routeEvent($user, $event_id);
    $topic = $this->routeTopic($event, $topic_id);
    $this->authorize('manage-speechlist', $event);

    $speech = $this->repository->get($speech_id);

    try {
      $speech = $this->repository->start($speech);
    }
    catch (\RuntimeException $e) {
      return response()->json(false, 401);
    }

    return response()->json($speech);
  }

  /**
   * Stops speech
   * POST events/{event}/topics/{topic}/speechlist/{speech}/stop
   *
   * @param $event_id
   * @param $topic_id
   * @param $speech_id
   * @return \Illuminate\Http\Response
   */

  public function stop($event_id, $topic_id, $speech_id) {
    $user = Auth::user();

    $event = $this->routeEvent($user, $event_id);
    $topic = $this->routeTopic($event, $topic_id);
    $this->authorize('manage-speechlist', $event);

    $speech = $this->repository->get($speech_id);
    if (!$speech->active()) {
      return response()->json(false, 401);
    }

    $speech = $this->repository->stop($speech);

    return response()->json($speech);
  }
}
