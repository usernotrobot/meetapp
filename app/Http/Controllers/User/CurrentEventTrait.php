<?php
namespace App\Http\Controllers\User;

use App\Models\Event;
use App\User;

trait CurrentEventTrait {
  /**
   * Helper function to identify event from route
   * with view-access check
   *
   * @param User $user
   * @param $event_id
   * @return Event $event
   */
  protected function routeEvent(User $user, $event_id) {
    $event = (empty($event_id))
      ? $user->current_event
      : $this->eventRepository->get($event_id);

    if (empty($event)) abort(404);

    $this->authorize('view', $event);

    return $event;
  }

  /**
   * Helper function to identify topic from route
   *
   * @param Event $event
   * @param $topic_id
   * @return mixed
   */
  protected function routeTopic(Event $event, $topic_id) {
    $topic = $this->topicRepository->getByEvent($event, $topic_id);
    if (empty($topic)) abort(404);

    return $topic;
  }

}
