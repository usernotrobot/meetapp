<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller as BaseController;

use App\Models\Topic;
use App\Http\Requests\TopicStoreRequest;
use App\Http\Requests\TopicUpdateRequest;
use App\Http\Requests\TopicStatusUpdateRequest;
use App\Http\Requests\FileUploadRequest;

use App\Repositories\TopicRepository;
use App\Repositories\EventRepository;
use App\Repositories\HeadlineRepository;
use App\Repositories\Files\TopicFileRepository;

use App\Criteria\WithTrashedCriteria;
use Kamaln7\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\Auth;

class TopicController extends BaseController {

  /**
   * @var TopicRepository
   */
  protected $repository;

  /**
   * @var EventRepository
   */
  protected $eventRepository;

  /**
   * @var HeadlineRepository
   */
  protected $headlineRepository;

  /**
   * TopicController constructor.
   * @param TopicRepository $repository
   * @param EventRepository $eventRepository
   * @param HeadlineRepository $headlineRepository
   */
  public function __construct(TopicRepository $repository, EventRepository $eventRepository, HeadlineRepository $headlineRepository) {
    $this->middleware('auth');

    $this->repository = $repository;
    $this->eventRepository = $eventRepository;
    $this->headlineRepository = $headlineRepository;

    /* Here should be check if current user is admin */
    $this->repository->pushCriteria(app(WithTrashedCriteria::class));
    $this->eventRepository->pushCriteria(app(WithTrashedCriteria::class));
  }

  /**
   * Display a listing of the resource.
   * GET /events/{event}/topics
   *
   * @param int $id
   * @return \Illuminate\Http\Response
   */
  public function index($id) {
    $event = $this->eventRepository->get($id);

    $this->authorize('view-topics', $event);

    $topics = $this->repository->getAllByEvent($event)->paginate();

    return view('admin.topics.index', compact('event', 'topics'));
  }

  /**
   * Display a listing of the resource.
   * GET /headlines/{headline}/topics
   *
   * @param int $id
   * @return \Illuminate\Http\Response
   */
  public function indexHeadline($id) {
    throw new \Exception('Not implemented yet!');
  }

  /**
   * Show the form for creating a new resource.
   * GET /headlines/{headline}/topics/create
   *
   * @param int $id
   * @return \Illuminate\Http\Response
   */
  public function create($id) {
    $headline = $this->headlineRepository->get($id);

    $event = $headline->event;
    $this->authorize('manage-topics', $event);

    $speech_min = Topic::SPEECH_COUNT_MIN;
    $speech_max = Topic::SPEECH_COUNT_MAX;
    $speech_default = Topic::SPEECH_DEFAULT_LENGTH / 60;

    return view('admin.topics.create', compact('headline', 'event', 'speech_min', 'speech_max', 'speech_default'));
  }


  /**
   * Store a newly created resource in storage.
   * POST /headlines/{headline}/topics
   *
   * @param int $id
   * @param TopicStoreRequest $request
   * @return \Illuminate\Http\Response
   */
  public function store(TopicStoreRequest $request, $id) {
    $headline = $this->headlineRepository->get($id);
    $event = $headline->event;

    $this->authorize('manage-topics', $event);

    $topic = $this->repository->createFromHeadline($request->intersect(['name', 'description', 'speech_length', 'speech_times', 'speech_opened']), $headline);

    $request->ajax() ?:  Toastr::success(trans('topics.msg.create-success'));

    return ($request->ajax())
      ? response()->json([
        'success' => trans('topics.msg.create-success'),
        'topic' => $topic,
        // TODO: manage by parameter whether to include this or not
        'headline' => $topic->headline->transform(Auth::user())
      ])
      : redirect()->route('admin::topics.index', $event);
  }


  /**
   * Display the specified resource.
   * GET /topics/{topic}
   *
   * @param int $id
   * @return \Illuminate\Http\Response
   */
  public function show($id) {
    /*$topic = $this->repository->get($id);

    return view('admin.topics.show', compact('topic'));*/
    return redirect()->route('admin::topics.edit', $id);
  }


  /**
   * Show the form for editing the specified resource.
   * GET /topic/{topic}/edit
   *
   * @param int $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id) {
    $topic = $this->repository->get($id);
    $event = $topic->headline->event;

    $this->authorize('manage-topics', $event);

    $speech_min = Topic::SPEECH_COUNT_MIN;
    $speech_max = Topic::SPEECH_COUNT_MAX;
    $speech_default = Topic::SPEECH_DEFAULT_LENGTH / 60;

    return view('admin.topics.edit', compact('topic', 'event', 'speech_min', 'speech_max', 'speech_default'));
  }


  /**
   * Update the specified resource in storage.
   * PUT /topics/{topic}
   *
   * @param  TopicUpdateRequest $request
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function update(TopicUpdateRequest $request, $id) {
    $topic = $this->repository->get($id);
    $event = $topic->headline->event;

    $this->authorize('manage-topics', $event);

    $topic = $this->repository->updateSettings(
      $request->intersect(['speech_length', 'speech_times', 'speech_opened', 'name', 'description', 'status']),
      $topic);

    Toastr::success(trans('topics.msg.update-success'));

    return redirect()->route('admin::topics.edit', $topic);
  }


  /**
   * Soft-deletes the specified resource.
   * DELETE /topics/{topic}
   *
   * @param int $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id) {
    $topic = $this->repository->get($id);
    $event = $topic->headline->event;

    $this->authorize('manage-topics', $event);

    $state = $this->repository->delete($id);
    Toastr::success(trans('events.msg.delete-success'));

    return response()->json($state);
  }

  /**
   * Restore the the specified resource.
   * POST /topics/{topic}/restore
   *
   * @param $id
   * @return \Illuminate\Http\JsonResponse
   */
  public function restore($id) {
    $topic = $this->repository->get($id);
    $event = $topic->headline->event;

    $this->authorize('manage-topics', $event);

    $state = $this->repository->restore($id);

    Toastr::success(trans('events.msg.restore-success'));

    return response()->json($state);
  }

  /**
   * Changes topic status
   * POST /topics/{topic}/status
   *
   * @param int $id
   * @param TopicStatusUpdateRequest $request
   * @return \Illuminate\Http\JsonResponse
   */
  public function status(TopicStatusUpdateRequest $request, $id) {
    $topic = $this->repository->get($id);
    $event = $topic->headline->event;

    $this->authorize('manage-topics', $event);

    $topic = $this->repository->update(['status' => $request->get('status')], $id);

    Toastr::success(trans('events.msg.status-change-success'));

    return response()->json($topic);
  }

  /**
   * Gets topic file list
   * GET /topics/{topic}/files
   *
   * @param int $id
   * @return \Illuminate\Http\Response
   */
  public function files($id) {
    $topic = $this->repository->get($id);
    $event = $topic->headline->event;

    $this->authorize('view-topics', $event);

    $files = $topic->files->all();
    $pageItems = config('repository.pagination.limit');

    return view('admin.topics.files', compact('topic', 'files', 'event', 'pageItems'));
  }

  /**
   * Uploads file to topic
   * POST /topics/{topic}/upload
   *
   * @param FileUploadRequest $request
   * @param $id
   * @param TopicFileRepository $fileRepository
   * @return \Illuminate\Http\Response
   */
  public function upload(FileUploadRequest $request, $id, TopicFileRepository $fileRepository) {
    $topic = $this->repository->get($id);
    $event = $topic->headline->event;
    $this->authorize('manage-topics', $event);

    if ($request->file('file')->isValid()) {
      $fileRepository->upload($request->file('file'), $topic, Auth::user(),
                              $request->file('file')->getClientOriginalName(), $request->get('name'));

      Toastr::success(trans('app.msg.file-upload-success'));
    } else {
      Toastr::error(trans('app.msg.file-upload-fail'));
    }

    return response()->redirectToroute('admin::topics.files', $id);
  }

}
