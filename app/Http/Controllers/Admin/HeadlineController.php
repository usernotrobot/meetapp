<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller as BaseController;

use App\Models\Headline;
use App\Http\Requests\HeadlineStoreRequest;
use App\Http\Requests\HeadlineUpdateRequest;
use App\Http\Requests\FileUploadRequest;
use App\Http\Requests\HeadlineFilesRequest;

use App\Repositories\HeadlineRepository;
use App\Repositories\EventRepository;
use App\Repositories\Files\HeadlineFileRepository;
use App\Repositories\TopicRepository;

use App\Criteria\WithTrashedCriteria;
use Illuminate\Http\Request;
use Kamaln7\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\Auth;

class HeadlineController extends BaseController {

	/**
	 * @var HeadlineRepository
	 */
	protected $repository;

	/**
	 * @var EventRepository
	 */
	protected $eventRepository;

	/**
	 * @var TopicRepository
	 */
	protected $topicRepository;

	public function __construct(HeadlineRepository $repository, EventRepository $eventRepository, TopicRepository $topicRepository)
	{
		$this->middleware('auth');

		$this->repository = $repository;
		$this->eventRepository = $eventRepository;
		$this->topicRepository = $topicRepository;

		/* Here should be check if current user is admin */
		$this->repository->pushCriteria(app(WithTrashedCriteria::class));
		$this->eventRepository->pushCriteria(app(WithTrashedCriteria::class));
	}

	/**
	 * Display a listing of the resource.
	 * GET /events/{event}/headlines
	 *
	 * @param int $id
	 * @return \Illuminate\Http\Response
	 */
	public function index($id)
	{
		$event = $this->eventRepository->get($id);
		$this->authorize('view-topics', $event);

        $headlines = $this->repository->getAllByEvent($event)->values()->map->transform(Auth::user());

		return view('admin.headlines.index', compact('event', 'headlines'));
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /events/{event}/headlines/create
	 *
	 * @param int $id
	 * @return \Illuminate\Http\Response
	 */
	public function create($id)
	{
		$event = $this->eventRepository->get($id);
		$this->authorize('manage-topics', $event);

		return view('admin.headlines.create', compact('event'));
	}


	/**
	 * Store a newly created resource in storage.
	 * POST /events/{event}/headlines
	 *
	 * @param int $id
	 * @param HeadlineStoreRequest $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(HeadlineStoreRequest $request, $id)
	{
		$event = $this->eventRepository->get($id);
		$this->authorize('manage-topics', $event);

		$headline = $this->repository->createFromEvent(array_merge($request->intersect(['name', 'description']), [ 'last_updated_user_id' => Auth::id() ]), $event);

		Toastr::success(trans('headlines.msg.create-success'));

		return ($request->ajax())
          ? response()->json(['headline' => $headline ])
          : redirect()->route('admin::headlines.index', $event);
	}


	/**
	 * Display the specified resource.
	 * GET /headlines/{headline}
	 *
	 * @param int $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		return redirect()->route('admin::headline.edit', $id);
	}


	/**
	 * Show the form for editing the specified resource.
	 * GET /headlines/{headline}/edit
	 *
	 * @param int $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		$headline = $this->repository->get($id);
		$event = $headline->event;

		$this->authorize('manage-topics', $event);

		return view('admin.headlines.edit', compact('headline', 'event'));
	}


	/**
	 * Update the specified resource in storage.
	 * PUT /headlines/{headline}
	 *
	 * @param  HeadlineUpdateRequest $request
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(HeadlineUpdateRequest $request, $id)
	{
		$headline = $this->repository->get($id);
		$event = $headline->event;

		$this->authorize('manage-topics', $event);

		$this->topicRepository->updateHeadlines($request->get('subtopic', []));

		$headline = $this->repository->updateSettings($request->intersect(['name', 'description']), $headline);

		Toastr::success(trans('headlines.msg.update-success'));

		return ($request->ajax())
			? response()->json(['headline' => $headline ])
			: redirect()->route('admin::headlines.edit', $headline);

	}

	/**
	 * Soft-deletes the specified resource.
	 * DELETE /headlines/{headline}
	 *
	 * @param int $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		$headline = $this->repository->get($id);
		$event = $headline->event;

		$this->authorize('delete', $headline);

		$this->repository->delete($id);

		return response()->json(['success' => trans('headlines.msg.delete-success', [ 'name' => $headline->name ]) ]);
	}

	/**
	 * Restore the the specified resource.
	 * POST /headlines/{headline}/restore
	 *
	 * @param $id
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function restore($id)
	{
		$headline = $this->repository->get($id);
		$event = $headline->event;

		$this->authorize('manage-topics', $event);

		$state = $this->repository->restore($id);

		Toastr::success(trans('headlines.msg.restore-success'));

		return response()->json($state);
	}

	/**
	 * Gets headlines file list
	 * GET /headlines/{headline}/files
	 *
	 * @param int $id
	 * @return \Illuminate\Http\Response
	 */
	public function files($id)
	{
		$headline = $this->repository->get($id);
		$event = $headline->event;

		$this->authorize('view-topics', $event);

		$files = $headline->files->all();
		$pageItems = config('repository.pagination.limit');

		return view('admin.headlines.files', compact('headline', 'files', 'event', 'pageItems'));
	}

	/**
	 * Uploads file to headline
	 * POST /headlines/{headline}/upload
	 *
	 * @param FileUploadRequest $request
	 * @param $id
	 * @param HeadlineFileRepository $fileRepository
	 * @return \Illuminate\Http\Response
	 */
	public function upload(FileUploadRequest $request, $id, HeadlineFileRepository $fileRepository)
	{
		$headline = $this->repository->get($id);
		$event = $headline->event;
		$this->authorize('manage-topics', $event);

		if ($request->file('file')->isValid())
		{
			$fileRepository->upload($request->file('file'), $headline, Auth::user(),
				$request->file('file')->getClientOriginalName(), $request->get('name'));

			Toastr::success(trans('app.msg.file-upload-success'));
		} else
		{
			Toastr::error(trans('app.msg.file-upload-fail'));
		}

		return response()->redirectToroute('admin::headlines.files', $id);
	}

	/**
	 * Upload files to headline
	 * POST /headlines/{headline}/massupload
	 *
	 * @param FileUploadRequest $request
	 * @param $id
	 * @param HeadlineFileRepository $fileRepository
	 * @return \Illuminate\Http\Response
	 */
	public function massUpload(HeadlineFilesRequest $request, $id, HeadlineFileRepository $fileRepository)
	{
		$headline = $this->repository->get($id);
		$event = $headline->event;

		$this->authorize('manage-topics', $event);

		foreach ($request->file('file') as $file)
			if ($file->isValid())
				$files[] = $fileRepository->upload($file, $headline, Auth::user(), $file->getClientOriginalName());

		return response()->json($files);
	}

  /**
   * Change headline position
   * POST /events/{event}/headlinesPosition
   *
   * @param Request $request
   * @param $id
   * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
   */
	public function updatePosition(Request $request, $id){
      $event = $this->eventRepository->get($id);

      $this->authorize('manage-topics', $event);

      $positionInfo = request()->json()->all();

      foreach($positionInfo as $info) {
        $headline = $this->repository->get($info['id']);
        $this->repository->updateSettings(['position' => $info['position']], $headline);
      }

      return response()->json(['success' => true ]);
  }

}
