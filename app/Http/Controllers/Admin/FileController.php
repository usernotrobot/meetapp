<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller as BaseController;

use App\Repositories\FileRepository;
use Illuminate\Support\Facades\Storage;
use Kamaln7\Toastr\Facades\Toastr;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class FileController extends BaseController {

  /**
   * @var FileRepository
   */
  protected $repository;

  /**
   * FileController constructor.
   * @param FileRepository $repository
   */
  public function __construct(FileRepository $repository) {
    $this->middleware('auth');
    $this->repository = $repository;
  }

  /**
   * Downloads file
   * GET /file/{file}
   *
   * @param $id
   * @return bool|\Symfony\Component\HttpFoundation\BinaryFileResponse
   */
  public function download($id) {
    $file = $this->repository->get($id);

    if (Storage::exists($file->path)) {
      $path = storage_path('app/public/' . $file->path);

      return response()->download($path, $file->original_name, [
        'Content-Length: ' . Storage::size($file->path)
      ]);
    }

    throw new NotFoundHttpException();
  }

  /**
   * Deletes file
   * DELETE /file/{file}
   *
   * @param $id
   * @return \Illuminate\Http\JsonResponse
   */
  public function destroy($id) {
    $state = $this->repository->delete($id);

    Toastr::success(trans('files.msg.delete-success'));

    return response()->json($state);
  }
}
