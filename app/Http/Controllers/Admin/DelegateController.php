<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller as BaseController;

use App\Repositories\DelegateRepository;
use App\Repositories\EventRepository;

use App\Http\Requests\DelegateStoreRequest;
use App\Http\Requests\DelegateUpdateRequest;
use App\Http\Requests\DelegateUploadRequest;

use App\Notifications\DelegateDeleted;

use Kamaln7\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\Auth;


class DelegateController extends BaseController {
  /**
   * @var DelegateRepository
   */
  protected $repository;

  /**
   * @var EventRepository
   */
  protected $eventRepository;


  /**
   * EventController constructor.
   * Create a new controller instance.
   *
   * @param DelegateRepository $repository
   * @param EventRepository $eventRepository
   * @return DelegateController
   */
  public function __construct(DelegateRepository $repository, EventRepository $eventRepository) {
    $this->repository = $repository;
    $this->eventRepository = $eventRepository;
  }

  /**
   * Display a listing of the resource.
   * GET /events/{event}/delegates
   *
   * @param int $id
   * @return \Illuminate\Http\Response
   */
  public function index($id) {
    $event = $this->eventRepository->get($id);
    $this->authorize('view-delegates', $event);

    $delegates = $this->repository->getByEvent($event);

    $pageItems = config('repository.pagination.limit');

    return view('admin.delegates.index', compact('event', 'delegates', 'pageItems'));
  }

  /**
   * Show the form for creating a new resource.
   * GET /events/{event}/delegates/create
   *
   * @param int $id
   * @return \Illuminate\Http\Response
   */
  public function create($id) {
    $event = $this->eventRepository->get($id);
    $this->authorize('manage-delegates', $event);

    return view('admin.delegates.create', compact('event'));
  }


  /**
   * Store a newly created resource in storage.
   * POST /events/{event}/delegates
   *
   * @param int $id
   * @param DelegateStoreRequest $request
   * @return \Illuminate\Http\Response
   */
  public function store(DelegateStoreRequest $request, $id) {
    $event = $this->eventRepository->get($id);
    $this->authorize('manage-delegates', $event);

    $user_attributes = $request->intersect(['email']);
    $user_values = $request->intersect(['first_name', 'last_name', 'password', 'union', 'union_sort_key',
                                        'branch', 'branch_sort_key', 'delegate_id']);
    $delegate_attributes = $request->intersect(['participant_id', 'role', 'right_submit',
                                                'right_speak', 'right_vote']);
    $delegate_attributes['created_by'] = Auth::user()->id;

    $notify = boolval($request->get('notify'));

    list($result, $user, $delegate) =
      $this->repository->updateOrCreateForEvent($event, $user_attributes, $user_values, $delegate_attributes, $notify);

    if ($result !== DelegateRepository::STATUS_ERROR) {
      Toastr::success(trans('delegates.msg.create-success'));
    } else {
      Toastr::error(trans('delegates.msg.create-fail'));
    }

    return redirect()->route('admin::delegates.index', $event);
  }


  /**
   * Display the specified resource.
   * GET /delegates/{delegate}
   *
   * @param int $id
   * @return \Illuminate\Http\Response
   */
  public function show($id) {
    $delegate = $this->repository->get($id, ['event']);

    $event = $delegate->event;
    $this->authorize('view-delegates', $event);

    return view('admin.delegates.show', compact('delegate', 'event'));
  }


  /**
   * Show the form for editing the specified resource.
   * GET /delegates/{delegate}/edit
   *
   * @param int $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id) {
    $delegate = $this->repository->get($id, ['event']);
    $event = $delegate->event;

    $this->authorize('manage-delegates', $event);

    return view('admin.delegates.edit', compact('delegate', 'event'));
  }


  /**
   * Update the specified resource in storage.
   * PUT /delegates/{delegate}
   *
   * @param  DelegateUpdateRequest $request
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function update(DelegateUpdateRequest $request, $id) {
    $delegate = $this->repository->get($id, ['event']);

    $event = $delegate->event;
    $this->authorize('manage-delegates', $event);

    $user_attributes = $request->intersect(['first_name', 'last_name', 'password', 'union', 'union_sort_key',
                                        'branch', 'branch_sort_key', 'delegate_id', 'email', 'user_token']);
    $delegate_attributes = $request->intersect(['participant_id', 'role', 'right_submit',
                                                'right_speak', 'right_vote']);
    $delegate_attributes['created_by'] = Auth::user()->id;

    $delegate = $this->repository->updateForEvent($delegate, $event, $user_attributes, $delegate_attributes);

    Toastr::success(trans('delegates.msg.update-success'));

    return redirect()->route('admin::delegates.edit', $delegate);
  }


  /**
   * Deletes the specified resource.
   * DELETE /delegates/{delegate}
   *
   * @param int $id
   * @param Request $request
   * @return \Illuminate\Http\Response
   */
  public function destroy($id, Request $request) {
    $delegate = $this->repository->get($id);

    $event = $delegate->event;
    $this->authorize('manage-delegates', $event);

    $state = $this->repository->delete($id);

    if ($request->get('notify')) {
      $delegate->user->notify(new DelegateDeleted($event));
    }

    Toastr::success(trans('delegates.msg.delete-success', [
      'name' => $delegate->user->name,
      'email' => $delegate->user->email]));

    return response()->json($state);
  }

  /**
   * Uploads delegates list to event
   * POST /events/{event}/delegates/upload
   *
   * @param DelegateUploadRequest $request
   * @param $id
   * @return \Illuminate\Http\Response
   */
  public function upload(DelegateUploadRequest $request, $id) {
    $event = $this->eventRepository->get($id);
    $this->authorize('manage-delegates', $event);

    $result = ['updated' => [], 'created' => [], 'error' => [], 'exists' => []];
    if ($request->file('file')->isValid()) {
      $result = $this->repository->processFile($request->file('file'), $event, Auth::user());
    } else {
      $result['error'][] = trans('delegates.msg.file-upload-fail-no-entries');
    }

    return view('admin.delegates.upload', array_merge($result, compact('event')));
  }

  public function bulkDelete($id, Request $request) {
    $event = $this->eventRepository->get($id);
    $this->authorize('manage-delegates', $event);

    $result = $this->repository->bulkDelete($event);
    if ($request->get('notify')) {
      foreach ($result as $delegate) {
        $delegate->user->notify(new DelegateDeleted($event));
      }
    }

    Toastr::success(trans('delegates.msg.bulk-delete-success'));

    return response()->json(true);
  }

}
