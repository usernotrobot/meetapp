<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller as BaseController;

use App\Repositories\ClientRepository;
use App\Repositories\UserRepository;

use App\Http\Requests\ClientStoreRequest;
use App\Http\Requests\ClientUpdateRequest;

use App\Criteria\WithTrashedCriteria;
use Kamaln7\Toastr\Facades\Toastr;

use App\Models\Client;
use App\User;

class ClientController extends BaseController {
  /**
   * The clients repository instance.
   * @var ClientRepository
   */
  protected $repository;

  /**
   * ClientController constructor.
   * Create a new controller instance.
   *
   * @param ClientRepository $repository
   * @return ClientController
   */
  public function __construct(ClientRepository $repository) {
    $this->middleware('can:manage,' . Client::class);

    $this->repository = $repository;
    $this->repository->pushCriteria(app(WithTrashedCriteria::class));
  }

  /**
   * Display a listing of the resource.
   * GET /clients
   *
   * @return \Illuminate\Http\Response
   */
  public function index() {
    $clients = $this->repository->getOrderedTrashedList();
    $pageItems = config('repository.pagination.limit');

    return view('admin.clients.index', compact('clients', 'pageItems'));
  }

  /**
   * Show the form for creating a new resource.
   * GET /clients/create
   *
   * @return \Illuminate\Http\Response
   */
  public function create() {
    return view('admin.clients.create');
  }

  /**
   * Store a newly created resource in storage.
   * POST /clients
   *
   * @param  ClientStoreRequest $request
   * @param  UserRepository $userRepository
   * @return \Illuminate\Http\RedirectResponse
   */
  public function store(ClientStoreRequest $request, UserRepository $userRepository) {
    try {
      $client = $this->repository->create($request->intersect(['name', 'description']));
      $admin = $userRepository->createForClient(
        $client,
        $request->intersect('first_name', 'last_name', 'email', 'password'),
        User::ROLE_ADMIN);

      Toastr::success(trans('clients.msg.create-success'));

      return redirect()->route('admin::clients.index');

    } catch (\Exception $e) {
      if (empty($admin) && !empty($client)) $client->forceDelete();

      Toastr::error(trans('clients.msg.create-fail'));

      return back()->withInput();
    }
  }

  /**
   * Display the specified resource.
   * GET /clients/{client}
   *
   * @param int $id
   * @return \Illuminate\Http\Response
   */
  public function show($id) {
    return redirect()->route('admin::clients.edit', $id);
  }

  /**
   * Show the form for editing the specified resource.
   * GET /clients/{client}/edit
   *
   * @param int $id
   * @param UserRepository $userRepository
   * @return \Illuminate\Http\Response
   */
  public function edit($id, UserRepository $userRepository) {
    $client = $this->repository->get($id);
    $admin = $userRepository->getAdmin($client);

    return view('admin.clients.edit', compact('client', 'admin'));
  }

  /**
   * Update the specified resource in storage.
   * PUT /clients/{client}
   *
   * @param int $id
   * @param ClientUpdateRequest $request
   * @return \Illuminate\Http\Response
   */
  public function update(ClientUpdateRequest $request, $id, UserRepository $userRepository) {
    $client = $this->repository->update($request->intersect(
      ['name', 'description']
    ), $id);

    $user = $userRepository->getAdmin($client);
    $user_attributes = $request->only(['first_name', 'last_name', 'email']);

    if (!empty($request->get('password'))) {
      $user_attributes['password'] = bcrypt($request->get('password'));
    }

    $user = $userRepository->update($user_attributes, $user->id);

    Toastr::success(trans('clients.msg.update-success'));

    return redirect()->route('admin::clients.edit', $client);
  }

  /**
   * Soft-delete the specified resource.
   * DELETE /clients/{client}
   *
   * @param int $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id) {
    $state = $this->repository->delete($id);

    Toastr::success(trans('clients.msg.delete-success'));

    return response()->json($state);
  }


  /**
   * Restore the the specified resource.
   * POST /clients/{client}/restore
   *
   * @param $id
   * @return \Illuminate\Http\JsonResponse
   */
  public function restore($id) {
    /* @var \App\Models\Client $client*/
    $state = $this->repository->restore($id);

    Toastr::success(trans('clients.msg.restore-success'));

    return response()->json($state);
  }
}
