<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller as BaseController;

use App\Repositories\TopicRepository;
use App\Repositories\EventRepository;
use App\Repositories\SuggestionRepository;

use App\Criteria\WithTrashedCriteria;

class SuggestionController extends BaseController {

  /**
   * @var SuggestionRepository
   */
  protected $repository;

  /**
   * @var TopicRepository
   */
  protected $topicRepository;

  /**
   * @var EventRepository
   */
  protected $eventRepository;

  public function __construct(TopicRepository $topicRepository, EventRepository $eventRepository,
                              SuggestionRepository $suggestionRepository) {
    $this->middleware('auth');

    $this->repository = $suggestionRepository;
    $this->topicRepository = $topicRepository;
    $this->eventRepository = $eventRepository;

    $this->topicRepository->pushCriteria(app(WithTrashedCriteria::class));
    $this->eventRepository->pushCriteria(app(WithTrashedCriteria::class));
  }

  /**
   * Shows all topic suggestions
   * GET /topic/{topic}/suggestions
   *
   * @param $topic_id
   * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
   */
  public function index($topic_id) {
    $topic = $this->topicRepository->get($topic_id);
    $event = $topic->event;
    $this->authorize('view-topics', $event);

    $suggestions = $this->repository->getAllByTopic($topic)->all();
    $pageItems = config('repository.pagination.limit');

    return view('admin.suggestions.index', compact('event', 'topic', 'suggestions', 'pageItems'));
  }

  /**
   * Shows suggestion details
   * GET /topic/{topic}/suggestions/{suggestion}
   *
   * @param $topic_id
   * @param $suggestion_id
   * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
   */
  public function show($topic_id, $suggestion_id) {
    $topic = $this->topicRepository->get($topic_id);
    $event = $topic->event;
    $this->authorize('view-topics', $event);

    $suggestion = $this->repository->get($suggestion_id);

    return view('admin.suggestions.show', compact('event', 'topic', 'suggestion'));

  }
}
