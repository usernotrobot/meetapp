<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller as BaseController;

use App\Repositories\EventRepository;
use App\Repositories\ClientRepository;
use App\Repositories\Files\EventFileRepository;

use App\Http\Requests\EventStoreRequest;
use App\Http\Requests\EventUpdateRequest;
use App\Http\Requests\EventStatusUpdateRequest;
use App\Http\Requests\FileUploadRequest;

use App\Criteria\WithTrashedCriteria;
use Kamaln7\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\Auth;

use App\Models\Event;

class EventController extends BaseController {
  /**
   * The events repository instance.
   * @var EventRepository
   */
  protected $repository;

  /**
   * EventController constructor.
   * Create a new controller instance.
   *
   * @param EventRepository $repository
   * @return EventController
   */
  public function __construct(EventRepository $repository) {
    $this->repository = $repository;

    /* Here should be check if current user is admin */
    $this->repository->pushCriteria(app(WithTrashedCriteria::class));
  }

  /**
   * Display a listing of the resource.
   * GET /events
   *
   * @return \Illuminate\Http\Response
   */
  public function index() {
    $this->authorize('index', Event::class);

    $events = $this->repository->getAllForAdmin(Auth::user());

    return view('admin.events.index', compact('events'));
  }

  /**
   * Show the form for creating a new resource.
   * GET /events/create
   *
   * @param ClientRepository $clientRepository
   * @return \Illuminate\Http\Response
   */
  public function create(ClientRepository $clientRepository, \Illuminate\Http\Request $request) {
    $this->authorize('manage', Event::class);

    $clients = $clientRepository->getAvailableClients();
    $selectedClientId = $request->get('client', null);

    return view('admin.events.create', compact('clients', 'selectedClientId'));
  }

  /**
   * Store a newly created resource in storage.
   * POST /events
   *
   * @param  EventStoreRequest $request
   * @return \Illuminate\Http\Response
   */
  public function store(EventStoreRequest $request) {
    $this->authorize('manage', Event::class);

    $event = $this->repository->create($request->intersect(
      [
        'name', 'union', 'description', 'end', 'start', 'client_id'
      ]));

    Toastr::success(trans('events.msg.create-success'));

    return redirect()->route('admin::events.index');
  }

  /**
   * Display the specified resource.
   * GET /events/{event}
   *
   * @param int $id
   * @return \Illuminate\Http\Response
   */
  public function show($id) {
    $this->authorize('manage', $this->repository->get($id));

    /* TODO: Make view page for event */
    return redirect()->route('admin::events.edit', $id);
  }

  /**
   * Show the form for editing the specified resource.
   * GET /events/{event}/edit
   *
   * @param int $id
   * @param ClientRepository $clientRepository
   * @return \Illuminate\Http\Response
   */
  public function edit($id, ClientRepository $clientRepository) {
    $event = $this->repository->get($id);

    $this->authorize('manage', $event);

    $clients = $clientRepository->getAvailableClients();

    return view('admin.events.edit', compact('event', 'clients'));
  }

  /**
   * Update the specified resource in storage.
   * PUT /events/{event}
   *
   * @param int $id
   * @param EventUpdateRequest $request
   * @return \Illuminate\Http\Response
   */
  public function update(EventUpdateRequest $request, $id) {
    $this->authorize('manage', $this->repository->get($id));

    $event = $this->repository->update($request->intersect(
      ['name', 'description', 'union', 'start', 'end', 'status', 'client_id']
    ), $id);

    Toastr::success(trans('events.msg.update-success'));

    return redirect()->route('admin::events.edit', $event);
  }

  /**
   * Soft-delete the specified resource.
   * DELETE /events/{event}
   *
   * @param int $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id) {
    $this->authorize('manage', $this->repository->get($id));

    $state = $this->repository->delete($id);

    Toastr::success(trans('events.msg.delete-success'));

    return response()->json($state);
  }


  /**
   * Restore the the specified resource.
   * POST /events/{event}/restore
   *
   * @param $id
   * @return \Illuminate\Http\JsonResponse
   */
  public function restore($id) {
    $this->authorize('manage', $this->repository->get($id));

    $state = $this->repository->restore($id);

    Toastr::success(trans('events.msg.restore-success'));

    return response()->json($state);
  }

  /**
   * Changes event status
   * POST /events/{event}/status
   *
   * @param int $id
   * @param EventStatusUpdateRequest $request
   * @return \Illuminate\Http\JsonResponse
   */
  public function status(EventStatusUpdateRequest $request, $id) {
    $this->authorize('manage', $this->repository->get($id));

    $event = $this->repository->update(['status' => $request->get('status')], $id);

    Toastr::success(trans('events.msg.status-change-success'));

    return response()->json($event);
  }

  /**
   * Gets event file list
   * GET /event/{event}/files
   *
   * @param int $id
   * @return \Illuminate\Http\Response
   */
  public function files($id) {
    $event = $this->repository->get($id);

    $this->authorize('manage', $event);
    $files = $event->files->all();

    $pageItems = config('repository.pagination.limit');

    return view('admin.events.files', compact('event', 'files', 'pageItems'));
  }

  /**
   * Uploads file to event
   * POST /event/{event}/upload
   *
   * @param FileUploadRequest $request
   * @param $id
   * @param EventFileRepository $fileRepository
   * @return \Illuminate\Http\Response
   */
  public function upload(FileUploadRequest $request, $id, EventFileRepository $fileRepository) {
    $this->authorize('files', $this->repository->get($id));

    $event = $this->repository->get($id);

    if ($request->file('file')->isValid()) {
      $fileRepository->upload($request->file('file'), $event, Auth::user(),
                              $request->file('file')->getClientOriginalName(), $request->get('name'));

      Toastr::success(trans('app.msg.file-upload-success'));
    } else {
      Toastr::error(trans('app.msg.file-upload-fail'));
    }

    return response()->redirectToroute('admin::events.files', $id);
  }
}
