<?php

namespace App\Http\Middleware;

use Illuminate\Contracts\Auth\Guard;
use Closure;
use App\User;

class CheckRole {

  public function __construct(Guard $auth) {
    $this->auth = $auth;
  }

  public function handle($request, Closure $next, $role) {
    $user = $this->auth->user();

    if ($role == User::ROLE_ADMIN && !$user->isAdmin()) {
      abort(404);
    }
    else if ($role == User::ROLE_DELEGATE && $user->isAdmin()) {
      abort(404);
    };

    return $next($request);
  }
}
