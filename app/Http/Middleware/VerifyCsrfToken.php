<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;
use App\Exceptions\MaxUploadSizeException;
use Kamaln7\Toastr\Facades\Toastr;

class VerifyCsrfToken extends BaseVerifier {
  /**
   * The URIs that should be excluded from CSRF verification.
   *
   * @var array
   */
  protected $except = [
    // '*/upload'
  ];

  protected $exceptIfSizeLimits = [
    '*/upload'
  ];

  public function handle($request, \Closure $next) {
    $user = $request->user('api');

    if ( $user ) {
      \Auth::loginUsingId($user->id);
      return $next($request);
    }

    try {
      $this->filterUploadLimit($request);
    } catch (MaxUploadSizeException $e) {
      Toastr::error(trans('app.msg.post-fail', ['size' => ini_get('post_max_size')]));

      $this->except = array_merge($this->except, $this->exceptIfSizeLimits);
    }

    return parent::handle($request, $next);
  }

  /**
   * Check if a user uploaded a file larger than the max size limit.
   * This filter is used when we also use a CSRF filter and don't want
   * to get a TokenMismatchException due to $_POST and $_GET being cleared.
   *
   * @param $request
   * @throws MaxUploadSizeException
   */
  protected function filterUploadLimit($request) {
    // Check if upload has exceeded max size limit
    if (!($request->isMethod('POST') || $request->isMethod('PUT'))) {
      return;
    }
    // Get the max upload size (in Mb, so convert it to bytes)
    $maxUploadSize = 1024 * 1024 * ini_get('post_max_size');
    $contentSize = 0;
    if (isset($_SERVER['HTTP_CONTENT_LENGTH'])) {
      $contentSize = $_SERVER['HTTP_CONTENT_LENGTH'];
    } elseif (isset($_SERVER['CONTENT_LENGTH'])) {
      $contentSize = $_SERVER['CONTENT_LENGTH'];
    }
    // If content exceeds max size, throw an exception
    if ($contentSize > $maxUploadSize) {
      throw new MaxUploadSizeException;
    }
  }
}
