<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

use App\Models\Client;
use App\Models\File;
use App\Models\Event;
use App\Models\Delegate;

use App\Repositories\DelegateRepository;
use App\Repositories\ClientRepository;
use App\Repositories\EventRepository;

use App\Services\Contracts\DelegateRouter;
use Illuminate\Support\Facades\Storage;

use App\Notifications\ResetPasswordNotification;

class User extends Authenticatable {
  use Notifiable;

  protected $delegate_event = [];

  const ROLE_ADMIN = 'admin';
  const ROLE_MODERATOR = 'moderator';
  const ROLE_SECRETARY = 'secretary';
  const ROLE_DELEGATE = 'delegate';
  const ROLE_OFFICIAL = 'official';
  const ROLE_PROJECTOR = 'projector';
  const ROLE_GUEST = 'guest';

  const ROLES_PRIORITY = [
    self::ROLE_ADMIN => 0,
    self::ROLE_MODERATOR => 10,
    self::ROLE_SECRETARY => 20,
    self::ROLE_DELEGATE => 30,
    self::ROLE_OFFICIAL => 30,
    self::ROLE_GUEST => 30,
    self::ROLE_PROJECTOR => 30,
  ];

  static function allRoles() {
    return array_keys(self::ROLES_PRIORITY);
  }

  static function allEventRoles() {
    return array_keys(array_where(self::ROLES_PRIORITY, function($index, $value) {
      return $index != self::ROLE_ADMIN;
    }));
  }


  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'name', 'email', 'password',
    'role', 'client_id',
    'first_name', 'last_name', 'image',
    'union', 'union_sort_key', 'branch', 'branch_sort_key', 'delegate_id',
    'api_token'
  ];

  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */
  protected $hidden = [
    'password', 'remember_token',
  ];

  /**
   * Attribute accessors
   */

  public function getImagePathAttribute($value) {
    return Storage::url($value);
  }

  public function getNameAttribute() {
    return $this->first_name . ' ' . $this->last_name;
  }

  public function getCurrentEventAttribute() {
    if (!isset($this->attributes['current_event'])) {
      $delegate = app(DelegateRouter::class)->current($this);

      if ($delegate) {
        $this->attributes['current_event'] = $delegate->event;
      } else {
        $this->attributes['current_event'] = null;
      }

    }

    return $this->attributes['current_event'];
  }

  public function getClientAttribute() {
    if (empty($this->attributes['client'])) {
      $this->attributes['client'] = app(ClientRepository::class)->get($this->client_id);
    }
    return $this->attributes['client'];
  }

  /**
   * Attribute Mutators
   */
  public function setRoleAttribute($value) {
    if (!in_array($value, User::allRoles())) throw new \InvalidArgumentException();
    $this->attributes['role'] = strtolower($value);

    return $value;
  }

  /**
   * Relations
   */
  public function files() {
    return $this->hasMany(File::class);
  }

  public function events() {
    return $this->belongsToMany(Event::class, with(new Delegate)->getTable(), 'user_id', 'event_id')->withPivot('role');
  }

  public function delegates() {
    return $this->hasMany(Delegate::class, 'user_id', 'id');
  }

  /** Roles */

  /* Explicit checks */
  public function isGlobalAdmin() {
    if ($this->role == self::ROLE_ADMIN
      && $this->client_id == null) {
      return true;
    }

    return false;
  }

  public function isAdmin() {
    return ($this->role == User::ROLE_ADMIN);
  }

  public function isClientAdmin(Client $client) {
    return (($this->role == self::ROLE_ADMIN)
            && ($this->client_id == $client->id));
  }

  /* Event specific checks */
  public function delegate(Event $event) {
    if (empty($this->delegate_event[$event->id])) {
      $this->delegate_event[$event->id] = app(DelegateRepository::class)->getbyUser($this, $event);
    }

    return $this->delegate_event[$event->id];
  }

  public function eventRole(Event $event) {
    if (in_array($this->role, User::allEventRoles())) {
      $delegate = $this->delegate($event);
      return ($delegate) ? $delegate->role : $delegate;
    }

    return $this->role;
  }

  public function getRole($role, Event $event = null) {
    if (empty(User::ROLES_PRIORITY[$role])) return false;

    $userRole = $event ? $this->eventRole($event) : $this->role;
    return (User::ROLES_PRIORITY[$userRole] <= User::ROLES_PRIORITY[$role]);
  }

  public function eqRole($role, Event $event = null) {
    return $this->eventRole($event) == $role;
  }


  /** Notifications */

  /**
   * Send the password reset notification.
   *
   * @param  string  $token
   * @return void
   */
  public function sendPasswordResetNotification($token) {
    $this->notify(new ResetPasswordNotification($token));
  }

  /** Helpers */

  /***
   * @param null $status
   * @return mixed
   */
  public function listEvents($status = null) {
    return app(EventRepository::class)->getDelegateEvents($this, $status)->get();
  }
}

