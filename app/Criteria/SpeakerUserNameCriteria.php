<?php
namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class SpeakerUserNameCriteria
 * @package namespace App\Criteria;
 */
class SpeakerUserNameCriteria implements CriteriaInterface {

  protected $name;
  protected $event;

  public function __construct($name, $event) {
    $this->name = $name;
    $this->event = $event;
  }

  public function apply($model, RepositoryInterface $repository) {
    $name = $this->name;
    $model = $model
      ->with('user')
      ->where('event_id', $this->event->id)
      ->where('right_speak', true)
      ->whereHas('user', function ($query) use ($name) {
        $query->where("first_name", 'ilike', '%' . $name . '%')
          ->orWhere("last_name", 'ilike', '%' . $name . '%');
      });

    return $model;
  }
}
