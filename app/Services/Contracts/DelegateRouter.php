<?php
namespace App\Services\Contracts;

use App\User;
use App\Models\Event;

interface DelegateRouter {

  public function current(User $user);
  public function get($id, User $user);
  public function getRoute(User $user, Event $event, $name);
  public function checkRoute(User $user, Event $event, $name);
}
