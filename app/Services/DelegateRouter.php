<?php

namespace App\Services;

use App\Services\Contracts\DelegateRouter as Contract;

use App\User;
use App\Models\Event;

use App\Repositories\DelegateRepository;
use App\Repositories\EventRepository;
use Carbon\Carbon;

use Illuminate\Support\Facades\Auth;

class DelegateRouter implements Contract {

  public function current(User $user) {
    /* @var $repository DelegateRepository */
    $repository = app(DelegateRepository::class);

    /* Get all the user events (cache) */
    /* TODO: use cached queries instead of script side sorting/filtering */
    $collection = $repository->getEvents($user);

    /* Find any active event */
    $actives = $collection
      ->filter(function ($item, $key) {
        if ($item->event->active()) {
          return $item;
        }
      })
      ->sort(function ($left, $right) {
        return $left->event->start == $right->event->start
          ? $left->event->id <=> $right->event->id
          : $left->event->start <=> $right->event->start;
      })
      ->first();

    if (!empty($actives)) return $actives;

    /* No actives, find first upcoming */
    $upcoming = $collection->filter(function ($item, $key) {
      if ($item->event->planned()
        && $item->event->start->gte(Carbon::now())
      ) {
        return $item;
      }
    })->sort(function ($left, $right) {
      return $left->event->start == $right->event->start
        ? $left->event->id <=> $right->event->id
        : $left->event->start <=> $right->event->start;
    })->first();

    return $upcoming ? $upcoming : null;
  }

  public function get($id, User $user) {
    if (empty($id)) {
      return Auth::user()->current_event;
    }

    return app(EventRepository::class)->get($id);
  }

  public function check(User $user, Event $event) {
    return $user->current_event && $user->current_event->id == $event->id;
  }

  public function getRoute(User $user, Event $event, $name, $args = []) {
    if ($this->check($user, $event)) {
      return route('user::current.' . $name, $args);
    }

    return route('user::' . $name, array_merge([$event], $args));
  }

  /* TODO: remove me? */
  public function checkRoute(User $user, Event $event, $name) {
    return $user->current_event && $user->current_event->event->id == $event->id;
  }
}
