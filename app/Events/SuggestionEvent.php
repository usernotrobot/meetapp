<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use App\Models\Suggestion;
use App\Models\Topic;

class SuggestionEvent implements ShouldBroadcast {
  use Dispatchable, InteractsWithSockets, SerializesModels;

  /* @var Suggestion */
  public $suggestion;

  /* @var string */
  public $action;

  /* @var Topic */
  protected $topic;

  /**
   * Create a new event instance.
   *
   * @param Suggestion $suggestion
   * @return SuggestionEvent $this
   */
  public function __construct(Suggestion $suggestion, $action) {
    $this->suggestion = $suggestion->shortForm();
    $this->action = $action;

    $this->topic = $suggestion->topic;
  }

  /**
   * Get the channels the event should broadcast on.
   *
   * @return Channel|array
   */
  public function broadcastOn() {
    return new PrivateChannel('topics.' . $this->topic->id);
  }
}
