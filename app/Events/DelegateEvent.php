<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

use App\Models\Delegate;

class DelegateEvent implements ShouldBroadcast {
  use Dispatchable, InteractsWithSockets, SerializesModels;

  /* @var Delegate */
  public $delegate;

  /* @var string */
  public $action;

  /**
   * Create a new event instance.
   *
   * @param Delegate $delegate
   * @return DelegateEvent $this
   */
  public function __construct(Delegate $delegate, $action) {
    $this->delegate = $delegate->transform();
    $this->action = $action;
  }

  /**
   * Get the channels the event should broadcast on.
   *
   * @return Channel|array
   */
  public function broadcastOn() {
    return new PrivateChannel('events.' . $this->delegate['event_id']);
  }
}
