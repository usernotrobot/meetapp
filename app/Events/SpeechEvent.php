<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use App\Models\Speech;
use App\Models\Topic;
use App\Repositories\SpeechRepository;

class SpeechEvent implements ShouldBroadcast {
  use Dispatchable, InteractsWithSockets, SerializesModels;

  /* @var string */
  public $action;

  /* @var array */
  public $speechlist;

  /* @var Topic */
  protected $topic;


  /**
   * Create a new event instance.
   *
   * @param Speech $speech
   * @return SpeechEvent $this
   */
  public function __construct(Speech $speech, $action) {
    $this->action = $action;
    $this->topic = $speech->topic;

    $this->speechlist = app(SpeechRepository::class)->getNextSpeakers($this->topic);
  }

  /**
   * Get the channels the event should broadcast on.
   *
   * @return Channel|array
   */
  public function broadcastOn() {
    return new PrivateChannel('topics.' . $this->topic->id);
  }
}
