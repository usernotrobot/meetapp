<?php

namespace App\Listeners;

use Prettus\Repository\Events\RepositoryEntityCreated;
use Prettus\Repository\Events\RepositoryEntityUpdated;
use Prettus\Repository\Events\RepositoryEntityDeleted;

use App\Events\SuggestionEvent;
use App\Events\TopicEvent;
use App\Events\DelegateEvent;
use App\Events\SpeechEvent;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Illuminate\Events\Dispatcher;

use Illuminate\Support\Facades\Auth;

use App\Models\Suggestion;
use App\Models\Speech;
use App\Models\Delegate;
use App\Models\Topic;
use App\Models\File;

class EntitySubscriber {

  const EventSuggestionCreated = 'SuggestionCreated';
  const EventSuggestionUpdated = 'SuggestionUpdated';
  const EventSuggestionDeleted = 'SuggestionDeleted';

  const EventTopicUpdated = 'TopicUpdated';

  const EventVotingCreated = 'VotingCreated';
  const EventVotingUpdated = 'VotingUpdated';
  const EventVotingDeleted = 'VotingDeleted';

  const EventDelegateUpdated = 'DelegateUpdated';
  const EventDelegateDeleted = 'DelegateDeleted';

  const EventSpeechCreated = 'SpeechCreated';
  const EventSpeechUpdated = 'SpeechUpdated';
  const EventSpeechDeleted = 'SpeechDeleted';

  /* TODO: Rewrite these noodles */

  public function onCreated(RepositoryEntityCreated $event) {
    if (is_a($event->getModel(), Suggestion::class)) {
      broadcast(new SuggestionEvent($event->getModel(), self::EventSuggestionCreated));
    } elseif (is_a($event->getModel(), Speech::class)) {
      broadcast(new SpeechEvent($event->getModel(), self::EventSpeechCreated));
    } elseif (is_a($event->getModel(), File::class)
      && $event->getModel()->entity_type == Topic::class) {
      broadcast(new TopicEvent($event->getModel()->entity, self::EventTopicUpdated));
    }
  }

  public function onUpdated(RepositoryEntityUpdated $event) {
    if (is_a($event->getModel(), Suggestion::class)) {
      broadcast(new SuggestionEvent($event->getModel(), self::EventSuggestionUpdated));
    } elseif (is_a($event->getModel(), Topic::class)) {
      broadcast(new TopicEvent($event->getModel(), self::EventTopicUpdated));
    } elseif (is_a($event->getModel(), Delegate::class) && $event->getModel()->id == Auth::user()->id) {
      broadcast(new DelegateEvent($event->getModel(), self::EventDelegateUpdated));
    } elseif (is_a($event->getModel(), Speech::class)) {
      broadcast(new SpeechEvent($event->getModel(), self::EventSpeechUpdated));
    } elseif (is_a($event->getModel(), File::class)
      && $event->getModel()->entity_type == Topic::class) {
      broadcast(new TopicEvent($event->getModel()->entity, self::EventTopicUpdated));
    }
  }

  public function onDeleted(RepositoryEntityDeleted $event) {
    if (is_a($event->getModel(), Suggestion::class)) {
      broadcast(new SuggestionEvent($event->getModel(), self::EventSuggestionDeleted));
    } elseif (is_a($event->getModel(), Delegate::class) && $event->getModel()->id == Auth::user()->id) {
      broadcast(new DelegateEvent($event->getModel(), self::EventDelegateDeleted));
    } elseif (is_a($event->getModel(), Speech::class)) {
      broadcast(new SpeechEvent($event->getModel(), self::EventSpeechDeleted));
    } elseif (is_a($event->getModel(), File::class)
      && $event->getModel()->entity_type == Topic::class) {
      broadcast(new TopicEvent($event->getModel()->entity, self::EventTopicUpdated));
    }
  }

  /**
   * Register the listeners for the subscriber.
   *
   * @param Dispatcher $events
   */
  public function subscribe($events) {
    $events->listen(
      RepositoryEntityCreated::class,
      'App\Listeners\EntitySubscriber@onCreated'
    );

    $events->listen(
      RepositoryEntityUpdated::class,
      'App\Listeners\EntitySubscriber@onUpdated'
    );

    $events->listen(
      RepositoryEntityDeleted::class,
      'App\Listeners\EntitySubscriber@onDeleted'
    );
  }

}
