<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Repositories\SuggestionRepository;
use App\Repositories\SpeechRepository;

use Carbon\Carbon;

class Topic extends Model {
  use SoftDeletes;

  /**
   * Model fillable fields
   *
   * @var array
   */
  protected $fillable = [
    'headline_id', 'name', 'description', 'status',
    'speech_times', 'speech_length', 'speech_opened',
    'voting_settings', 'voting_start', 'voting_end', 'voting_results'
  ];

  /**
   * The attributes that should be casted to native types.
   *
   * @var array
   */
  protected $casts = [
    'speech_length' => 'array',
    'voting_settings' => 'array',
    'voting_results' => 'array',
  ];

  /**
   * The attributes that should be mutated to dates.
   *
   * @var array
   */
  protected $dates = [
    'voting_start',
    'voting_end',
    'created_at',
    'updated_at',
    'deleted_at'
  ];

  /** Topic statuses constants */
  const STATUS_OPEN = 'open';
  const STATUS_DISCUSSION = 'discussion';
  const STATUS_VOTING = 'voting';
  const STATUS_CLOSED = 'closed';

  /**
   * All available topic statuses
   *
   * @return array
   */
  static function allStatuses() {
    return [
      Topic::STATUS_OPEN,
      Topic::STATUS_DISCUSSION,
      Topic::STATUS_VOTING,
      Topic::STATUS_CLOSED,
    ];
  }

  static function availableStatuses() {
    return [
      Topic::STATUS_OPEN,
      Topic::STATUS_DISCUSSION,
      Topic::STATUS_VOTING,
      Topic::STATUS_CLOSED,
    ];
  }

  /** Speech options constants */
  const SPEECH_COUNT_MIN = 1;
  const SPEECH_COUNT_MAX = 3;
  const SPEECH_DEFAULT_LENGTH = 300;

  /** VOTING */
  const VOTING_TYPE_AUTO = 'auto';
  const VOTING_TYPE_MANUAL = 'manual';

  static function allVotingTypes() {
    return [
      self::VOTING_TYPE_AUTO,
      self::VOTING_TYPE_MANUAL,
    ];
  }

  const VOTING_PRIVACY_OPEN = 'open';
  const VOTING_PRIVACY_SECRET = 'secret';

  static function allVotingPrivacy() {
    return [
      self::VOTING_PRIVACY_OPEN,
      self::VOTING_PRIVACY_SECRET
    ];
  }

  /** Voting params*/
  const DEFAULT_VOTING_SETTINGS = [
    'type' => self::VOTING_TYPE_AUTO,
    'length' => 600,
    'privacy' => self::VOTING_PRIVACY_OPEN,
    'answers' => []
  ];

  /** Attributes and mutators */

  /**  Suggestions that belong to this topic */
  public function getSuggestionsAttribute() {
    return app(SuggestionRepository::class)->findWhere(['topic_id' => $this->id]);
  }

  /**  SpeechList delegates that belong to this topic */
  public function getSpeechlistAttribute() {
    return app(SpeechRepository::class)->findWhere(['topic_id' => $this->id]);
  }

  /** Speech Length attribute */
  public function getSpeechLengthAttribute() {
    $lengths = is_array($this->attributes['speech_length'])
      ? $this->attributes['speech_length']
      : json_decode($this->attributes['speech_length'], true);

    $count = count($lengths);

    if (empty($lengths) || !is_array($lengths)) {
      $lengths = array_fill_keys(range(Topic::SPEECH_COUNT_MIN, $this->speech_times), Topic::SPEECH_DEFAULT_LENGTH);
    }
    elseif ($count < $this->speech_times) {
      $lengths = array_replace(
        array_fill_keys(range(Topic::SPEECH_COUNT_MIN, $this->speech_times), Topic::SPEECH_DEFAULT_LENGTH),
        $lengths);
    }
    elseif ($count > $this->speech_times) {
      $lengths = array_slice($lengths, 0, $this->speech_times, true);
    }

    $this->attributes['speech_length'] = $lengths;

    return $lengths;
  }

  /** Voting settings */
  public function getVotingSettingsAttribute() {
    $settings = is_array($this->attributes['voting_settings'])
      ? $this->attributes['voting_settings']
      : json_decode($this->attributes['voting_settings'], true);

    $settings = (array) $settings;

    $this->attributes['voting_settings'] = array_merge(self::DEFAULT_VOTING_SETTINGS, $settings);

    return $settings;
  }

  public function getVotingLengthAttribute() {
    return $this->voting_settings['length'];
  }
  public function getVotingTypeAttribute() {
    return $this->voting_settings['type'];
  }

  public function allowedToVote() {
    return $this->voting() && empty($this->voting_end);
  }
  /** Status functions */

  /**
   * Is topic status OPEN
   *
   * @return bool
   */
  public function open() {
    if ($this->status == Topic::STATUS_OPEN) return true;

    return false;
  }

  /**
   * Is topic status DISCUSSION
   *
   * @return bool
   */
  public function discussion() {
    if ($this->status == Topic::STATUS_DISCUSSION) return true;

    return false;
  }

  /**
   * Is topic status VOTING
   *
   * @return bool
   */
  public function voting() {
    if ($this->status == Topic::STATUS_VOTING) return true;

    return false;
  }

  /**
   * Is topic status CLOSED
   *
   * @return bool
   */
  public function closed() {
    if ($this->status == Topic::STATUS_CLOSED) return true;

    return false;
  }

  /**
   * If it's possible to add new suggestions to this topic
   *
   * @return bool
   */
  public function openedForSuggestion() {
    return ($this->open() || $this->discussion());
  }

  /**
   * If it's possible to add new speeches to speechlist of this topic
   * @return bool
   */
  public function allowedForNewSpeeches() {
    return $this->speech_opened && ($this->open() || $this->discussion());
  }

  public function speechMinutes($i) {
    if ($i > $this->speech_times) {
      return null;
    }

    return $this->speech_length[$i] / 60;
  }

  /** Relations */

  /**
   * Get the event that owns the topic.
   *
   * @return \Illuminate\Database\Eloquent\Relations\belongsTo
   */
  public function event() {
    return $this->headline->event();
  }

  /**
   * Get the headline that owns the topic.
   *
   * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
   */
  public function headline() {
    return $this->belongsTo(Headline::class);
  }

	/**
	 * Get the suggestions that owns the topic.
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\hasMany
	 */
	public function suggestions() {
		return $this->hasMany(Suggestion::class);
	}

  /**
   * Files that belong to this event
   *
   * @return \Illuminate\Database\Eloquent\Relations\MorphMany
   */
  public function files() {
    return $this->morphMany(File::class, 'entity');
  }

  /**
   * Counts number if delegates written suggestions
   *
   * @return int
   */
  public function suggestionDelegatesCount() {
    return $this->suggestions->count() ?
      $this->suggestions->map(function ($item) {
        return $item->user_id;
      })->unique()->count()
      : 0;
  }

  /**
   * Counts number of suggestions and delegates
   *
   * @return array
   */
  public function suggestionsSummary() {
    return [$this->suggestions->count(), $this->suggestionDelegatesCount()];
  }

  /** Helpers */
  public function lastSuggestion() {
    return $this->suggestions ? $this->suggestions->last() : null;
  }

  public function nextStatus() {
    $i = array_search($this->status, Topic::availableStatuses()) + 1;

    return Topic::availableStatuses()[$i];
  }

  public function votingTimeLeft() {
    $nulls = '00:00';
    if ($this->voting_end) return $nulls;

    $now = Carbon::now();
    $start = $this->voting_start ? $this->voting_start : $now->copy();

    $start->addSeconds($this->voting_length);

    if ($start->lt($now)) return $nulls;

    $minutes = $start->diffInMinutes($now);
    $seconds = $start->diffInSeconds($now) - $start->diffInMinutes($now) * 60;

    return $minutes . ':' . ($seconds >= 10 ? $seconds : '0' . $seconds);
  }

  /** JSON */
  public function transform() {
    return
      array_merge(
        $this->toArray(),
        [
          'filesCount' => $this->files->count(),
          'voting_settings' => $this->voting_settings,
          'voting_results' => $this->voting_results,
          'voting_type' => $this->voting_settings['type'],
          'voting_time_left' => $this->votingTimeLeft(),
          'voting_available' => $this->allowedToVote(),

          'speechAvailable' => $this->allowedForNewSpeeches(),

          'default_speech_settings' => [
            'count_min' => Topic::SPEECH_COUNT_MIN,
            'count_max' => Topic::SPEECH_COUNT_MAX,
            'length' => Topic::SPEECH_DEFAULT_LENGTH / 60,
          ]
        ]
      );
  }

}
