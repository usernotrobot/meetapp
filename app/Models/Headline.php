<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Headline extends Model {
  use SoftDeletes;

  /**
   * Model fillable fields
   *
   * @var array
   */
  protected $fillable = [
    'event_id', 'name', 'description', 'last_updated_user_id', 'position'
  ];

    /**
   * The attributes that should be mutated to dates.
   *
   * @var array
   */
  protected $dates = [
    'created_at',
    'updated_at',
    'deleted_at'
  ];

  /** Relations */

  /**
   * Get the event that owns the topic.
   *
   * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
   */
  public function event() {
    return $this->belongsTo(Event::class);
  }

  /**
   * Get the topics that owns the headline.
   *
   * @return \Illuminate\Database\Eloquent\Relations\hasMany
   */
  public function topics() {
    return $this->hasMany(Topic::class);
  }

  /**
   * Files that belong to this event
   *
   * @return \Illuminate\Database\Eloquent\Relations\MorphMany
   */
  public function files() {
    return $this->morphMany(File::class, 'entity');
  }

  /**
   * User who created this suggestion
   *
   * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
   */
  public function user() {
    return $this->belongsTo(User::class, 'last_updated_user_id');
  }

  /**
   * @return array
   */
  public function transform(User $user) {
    return [
      'id' => (int) $this->id,
      'topics' => trans_choice('events.lbl.topics-count', $this->topics->count(), ['count' => $this->topics->count()]),
      'name' => $this->name,
      'updated_at' => ( !empty($this->updated_at) ) ? $this->updated_at->format('d M Y, H:i') : null,
      'description' => $this->description,
      'last_updated' => $this->user ? ($this->user->first_name . ' ' . $this->user->last_name) : '',
      'position' => $this->position,
      'canDelete' => $user->can('delete', $this)
    ];
  }


}
