<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Delegate extends Model {
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'event_id', 'user_id',
    'participant_id', 'role',
    'right_speak', 'right_vote', 'right_submit',
    'created_by'
  ];

  protected $with = ['user'];

  /**
   * The accessors to append to the model's array form.
   *
   * @var array
   */
  protected $appends = ['name'];

  /** Attribute Mutators */
  public function setRoleAttribute($value) {
    if (!in_array($value, User::allRoles())) throw new \InvalidArgumentException();
    $this->attributes['role'] = strtolower($value);

    return $value;
  }

  public function getNameAttribute() {
    return $this->user->name;
  }

  /** Relations */
  public function user() {
    return $this->belongsTo(User::class);
  }

  public function event() {
    return $this->belongsTo(Event::class);
  }

  /** Roles */
  public function moderator() {
    return $this->role == User::ROLE_MODERATOR;
  }

  public function secretary() {
    return $this->role == User::ROLE_SECRETARY;
  }

  public function guest() {
    return $this->role == User::ROLE_GUEST;
  }

  public function delegate() {
    return $this->role == User::ROLE_DELEGATE;
  }

	public function projector() {
		return $this->role == User::ROLE_PROJECTOR;
	}

  /** JSON */
  public function transform() {
    return [
      'id' => $this->id,
      'event_id' => $this->event_id,
      'user_id' => $this->user_id,
      'username' => $this->name,

      'canVote' => $this->right_vote,

      'canAddSuggestion' => $this->user->can('add-suggestions', $this->event),
      'canManageSuggestion' => $this->user->can('manage-suggestions', $this->event),

      'canAddSpeech' => $this->user->can('add-speechlist', $this->event),
      'canManageSpeechlist' => $this->user->can('manage-speechlist', $this->event),

      'canModerate' => $this->user->can('moderate', $this->event),
      'canSecretary' => $this->user->can('secretary', $this->event),
    ];
  }
}
