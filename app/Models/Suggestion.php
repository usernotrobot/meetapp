<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\User;
use Venturecraft\Revisionable\RevisionableTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Support\Str;

/* TODO: Use delegate instead of user! */

class Suggestion extends Model {
  use RevisionableTrait, SoftDeletes;

  protected $fillable = [
    'summary',
    'details',
    'topic_id',
    'user_id'
  ];

  /**
   * The accessors to append to the model's array form.
   *
   * @var array
   */
  protected $appends = ['username'];

  /** Attribute accessors, mutators and helpers */
  public function getShortDetailsAttribute() {
    return str_limit($this->details, 255);
  }

  public function getIsDetailsShortAttribute() {
    return mb_strwidth($this->details, 'UTF-8') <= 255;
  }

  public function getUsernameAttribute() {
    return $this->user->name;
  }

  /** Relations */

  /**
   * Topic that this suggestion belongs to
   *
   * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
   */
  public function topic() {
    return $this->belongsTo(Topic::class);
  }

  /**
   * User who created this suggestion
   *
   * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
   */
  public function user() {
    return $this->belongsTo(User::class);
  }

  /**
   * @return array
   */
  public function shortForm() {
    return [
      'id' => (int) $this->id,
      'summary' => ucfirst($this->summary),
      'details' => ucfirst($this->shortDetails),
      'username' => ucfirst($this->username),
      'isShort' => $this->isDetailsShort,
      'owner_id' => $this->user->id,
    ];
  }
}
