<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Repositories\DelegateRepository;
use App\Repositories\TopicRepository;

use Carbon\Carbon;

class Speech extends Model {

  protected $fillable = [
    'topic_id', 'delegate_id', 'status', 'order_key', 'length',
    'start', 'end',
  ];

  /**
   * The attributes that should be mutated to dates.
   *
   * @var array
   */
  protected $dates = [
    'start',
    'end',
    'created_at',
    'updated_at'
  ];

  const STATUS_WAITING = 'waiting';
  const STATUS_ACTIVE = 'active';
  const STATUS_FINISHED = 'finished';

  const DEFAULT_STATUS = self::STATUS_WAITING;

  static function allStatuses() {
    return [
      self::STATUS_WAITING,
      self::STATUS_ACTIVE,
      self::STATUS_FINISHED,
    ];
  }

  /** Mutators and accessors */
  public function getStatusAttribute($value) {
    return empty($value) ? self::DEFAULT_STATUS : $value;
  }
  public function setStatusAttribute($value) {
    $value = strtolower($value);
    $this->attributes['status'] = in_array($value, Speech::allStatuses()) ? $value : Speech::DEFAULT_STATUS;
  }

  public function getLengthMinutesAttribute() {
    return $this->length / 60;
  }

  /**
   * Delegate that this speechlist item belongs to
   *
   * @return Delegate
   */
  public function getDelegateAttribute() {
    return app(DelegateRepository::class)
      ->findWhere(['event_id' => $this->topic->headline->event->id,
                   'id' => $this->delegate_id])->first();
  }

  /**
   * Topic that this speechlist item belongs to
   *
   * @return Topic
   */
  public function getTopicAttribute() {
    return app(TopicRepository::class)
      ->get($this->topic_id);
  }

  /** Status functions */
  public function waiting() {
    return $this->status == self::STATUS_WAITING;
  }

  public function active() {
    return $this->status == self::STATUS_ACTIVE;
  }

  public function finished() {
    return $this->status == self::STATUS_FINISHED;
  }

  /** Helpers */
  public function timeLeft() {
    $nulls = '00:00';
    if ($this->end) return $nulls;

    $now = Carbon::now();
    $start = $this->start ? $this->start : $now->copy();

    $start->addSeconds($this->length);

    $minutes = $start->diffInMinutes($now);
    $seconds = $start->diffInSeconds($now) - $start->diffInMinutes($now) * 60;

    return (($start->lt($now)) ? '-' : '') . $minutes . ':' . ($seconds >= 10 ? $seconds : '0' . $seconds);
  }

  /* Some modifications for json serialization */
  public function toArray() {
    return array_merge(parent::toArray(), [
      'name' => $this->delegate->name,
      'active' => $this->active(),
      'time_left' => $this->timeLeft(),
    ]);
  }
}
