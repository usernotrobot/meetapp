<?php

namespace App\Models;

use App\User;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Client extends Model {
  use SoftDeletes;

  protected $fillable = [
    'name', 'description'
  ];

  /**
   * Relations
   */
  public function users() {
    return $this->hasMany(User::class);
  }

  public function events() {
    return $this->hasMany(Event::class);
  }
}
