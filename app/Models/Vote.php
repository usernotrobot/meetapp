<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Repositories\TopicRepository;
use App\Repositories\DelegateRepository;

class Vote extends Model {
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'topic_id', 'delegate_id', 'vote'
  ];

  /** Accessors and mutators */
  public function getTopicAttribute() {
    return app(TopicRepository::class)->get($this->attributes['topic_id']);
  }

  public function getDelegateAttribute() {
    return app(DelegateRepository::class)->get($this->attributes['delegate_id']);
  }

  public function setVoteAttribute($value) {
    if (!in_array($value, $this->topic->voting_settings['answers'])) throw new \InvalidArgumentException();

    $this->attributes['vote'] = $value;
  }
}
