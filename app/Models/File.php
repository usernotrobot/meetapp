<?php

namespace App\Models;

use App\User;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class File extends Model {

  protected $fillable = [
    'original_name','name', 'path', 'owner_id', 'entity_id', 'entity_type'
  ];

  /*protected $hidden = [
    'path'
  ];*/

  public function getPath() {
    return Storage::url($this->path);
  }

  /**
   * Relations
   */
  public function owner() {
    return $this->belongsTo(User::class);
  }

  public function entity() {
    return $this->morphTo();
  }

}
