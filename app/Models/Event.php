<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\User;
use App\Models\Suggestion;

class Event extends Model {
  use SoftDeletes;

  /**
   * The attributes that should be mutated to dates.
   *
   * @var array
   */
  protected $dates = ['deleted_at', 'start', 'end'];

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = ['name', 'union', 'description', 'start', 'end', 'status', 'client_id'];


  protected $with = ['headlines'];

  /* Constants */
  const STATUS_PLANNED = 'planned';
  const STATUS_ACTIVE = 'active';
  const STATUS_COMPLETED = 'completed';

  static function allStatuses() {
    return [
      self::STATUS_PLANNED,
      self::STATUS_ACTIVE,
      self::STATUS_COMPLETED,
    ];
  }

  /**
   * Status functions
   */
  public function active() {
    if ($this->status == Event::STATUS_ACTIVE) return true;

    return false;
  }

  public function planned() {
    if ($this->status == Event::STATUS_PLANNED) return true;

    return false;
  }

  public function completed() {
    if ($this->status == Event::STATUS_COMPLETED) return true;

    return false;
  }

  /**
  /* Relations
   */

  /**
   * Get all the headlines that belong to this event.
   */
  public function headlines() {
    return $this->hasMany(Headline::class)->orderBy('position');
  }

	/**
	 * Get all the topics that belong to this event.
	 */
	public function topics() {
		return $this->hasManyThrough(Topic::class, Headline::class);
	}

  /**
   * Files that belong to this event
   */
  public function files() {
    return $this->morphMany(File::class, 'entity');
  }

  /**
   * Client that this event belongs to
   */
  public function client() {
    return $this->belongsTo(Client::class);
  }

  /**
   * Event participants
   */
  public function participants() {
    return $this->hasManyThrough(User::class, Delegate::class, 'event_id', 'user_id', 'id')
      ->withPivot('role');
  }

  /**
   * Event Delegates
   */
  public function delegates() {
    return $this->hasMany(Delegate::class);
  }

	public function suggestions() {
		return $this->hasManyThrough(Suggestion::class, Topic::class);
	}
}
