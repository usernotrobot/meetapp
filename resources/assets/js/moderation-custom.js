$(function () {
    $('[data-toggle="tooltip"]').bstooltip();

    $("#moderationForm").submit(function() {
        return false;
    });

    $('#save_moderation').on('click submit', function() {
        ($('#sortable .suggestion').length > 0)? saveTopic() : $('#confirmSaveTopic').modal('show');
    });

});

function saveTopic() {
    var $inputs = $("#moderationForm input"),
        collections = {},
        suggestions_id = [];

    $inputs.each(function() {
        collections[this.name] = ($(this).is(':checkbox'))? $(this).is(':checked') : $(this).val();
    });

    $('#sortable .suggestion').each(function(){
        suggestions_id.push($(this).attr('data-id'));
    });

    collections['suggestions_id'] = suggestions_id;

    $.ajax({
        type: 'POST',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: window.moderation.routes.suggestion.moderation,
        data: collections,
        success: function (response) {
            if (response.status == 'success') {
                toastr.success(Lang.get('messages.moderation.msg.create-success'));
                window.location.replace(response.url);
            }
        },
        error: function (response) {
            toastr.error(response.responseJSON.name);
        }
    });
}

$('.card-header input').focus(function(){
    $(this).next('i').hide();
}).blur(function(){
    $(this).next('i').show();
});

// function changeLabels(obj) {
//     return obj
//         .replace('fa-share', 'fa-times')
//         .replace('to-new-suggestion', 'remove-from-list')
//         .replace(Lang.get('messages.moderation.topic.add-suggestion'), Lang.get('messages.moderation.topic.remove-suggestion'));
// }

// function toggleList(id, show) {
//     $('#draggable').find('.suggestion').each(function(){
//         if ($(this).attr('data-id') == id)(show)? $(this).addClass('hide-list').next().addClass('hide-list') : $(this).removeClass('hide-list').next().removeClass('hide-list');
//     });
// }

/*$('#sortable').on('click', '.fa-times', function () {
    var list_target = $(this).closest('.suggestion'),
        list_id = $(list_target).attr('data-id');
    toggleList(list_id, false);
    $(list_target).remove();
});

$('#draggable').on('click', '.fa-share', function () {
    var list_target = $(this).closest('.suggestion');
    $(list_target).clone().wrapAll('<div>').parent().html(function(){
        return changeLabels($(this).html());
    }).appendTo('#sortable');

    // $('#sortable').find('.card-more, .moderator-menu-btn').remove();
    $(list_target).addClass('hide-list').next().addClass('hide-list');
});*/

/*$( function() {
    $( "#draggable .suggestion" ).draggable({
        appendTo: '.topic-blocks',
        helper: 'clone',

        start: function( event, ui ) {
            ui.helper.addClass('on_drag');
        }
    });

    $("#drop-box").droppable({
        hoverClass: "hover_droppable",
        accept: ":not(.ui-sortable-helper)",

        drop: function( event, ui ) {
            var id = $(ui.helper).attr('data-id');
            var $cloned = $(ui.draggable).clone(true, true);
            $cloned.find('.card-more').on('click', function(evt){ showModal(evt) })

            $('#sortable').prepend(
                $cloned
                // '<div class="suggestion clearfix" data-id='+id+'>' +
                // ui.draggable.html()
                //     .replace('fa-share', 'fa-times')
                //     .replace('to-new-suggestion','remove-from-list')
                //     .replace(Lang.get('messages.moderation.topic.add-suggestion'))
                // +
                // '</div>'
            // ).find('.card-more, .moderator-menu-btn').remove();
            );

            toggleList(id, true);
        }
    });

    $( "#sortable" ).sortable({
        axis: 'y',
        revert: true,
        scroll: false
    }).disableSelection();
});*/
