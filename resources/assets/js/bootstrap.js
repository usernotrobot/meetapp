window._ = require('lodash');

/**
 * We'll load jQuery and the Bootstrap jQuery plugin which provides support
 * for JavaScript based Bootstrap features such as modals and tabs. This
 * code may be modified to fit the specific needs of your application.
 */
global.$ = global.jQuery = require('jquery');

/**
 * Required by the bootstrap4 js library
 */
global.Tether = require('tether');

/**
 * Vue is a modern JavaScript library for building interactive web interfaces
 * using reactive data binding and reusable components. Vue's API is clean
 * and simple, leaving you to focus on building your next great project.
 */
window.Vue = require('vue');

/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

window.axios = require('axios');
window.axios.defaults.headers.common = {
  'X-CSRF-TOKEN': window.Laravel.csrfToken,
  'X-Requested-With': 'XMLHttpRequest'
};

/**
 * Kinda the same for the jQuery ajax setup
 */
$.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': window.Laravel.csrfToken
  }
});

const io = window.io = require('socket.io-client');

import Echo from "laravel-echo";

var echo = window.Echo = new Echo({
  broadcaster: 'socket.io',
  host: window.location.hostname + ':6001',
  appId: "7ac454be4dfae7cd",
  key: "5bfedfac9fb1f6cdfcf88122daca0334"
});

