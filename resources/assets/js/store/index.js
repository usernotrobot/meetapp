/** Vuex state management machine */
import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

/** Modules */
import moduleTopic from './modules/topic';
import moduleDelegate from './modules/delegate';
import moduleRoute from './modules/route';
import moduleSpeechlist from './modules/speechlist';

/** New Vuex store */
export default new Vuex.Store({
  modules: {
    topic: moduleTopic,
    delegate: moduleDelegate,
    route: moduleRoute,
    speechlist: moduleSpeechlist,
  }
})
