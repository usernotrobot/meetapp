// initial state
const state = {
  topic: {}
};

// getters
const getters = {
  get: state => state.topic
};

// actions
const actions = {
  init ({ commit }, topic) {
    commit('init', topic);
  },
  update ({ commit }, { action, topic }) {
    if (action === 'TopicUpdated') commit('init', topic);
  }
};

// mutations
const mutations = {
  init (state, topic) {
    $.each(topic.speech_length, function(index, value) {
      topic.speech_length[index] = typeof(value) === 'number' ? (value / 60) : 0;;
    });

    state.topic = topic;
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
}
