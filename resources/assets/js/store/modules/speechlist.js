// initial state
const state = {
  current: {},
  next: {}
};

// getters
const getters = {
  current: state => state.current,
  list: state => state.next,
};

// actions
const actions = {
  init ({ commit }, speechlist) {
    commit('init', speechlist);
  },
  update ({ commit }, { action, speechlist }) {
    commit('init', speechlist);
  }
};

// mutations
const mutations = {
  init (state, speechlist) {
    speechlist.sort(function (l, r) {
      return l.order_key - r.order_key;
    });

    state.current = speechlist.shift();
    state.next = speechlist;
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
}
