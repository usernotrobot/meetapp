// initial state
const state = {
  delegate: {}
};

// getters
const getters = {
  get: (state) => state.delegate,
  isOwnSuggestion: (state) => ({owner_id}) => owner_id === state.delegate.user_id,
  rightManageOwnSuggestion: (state, getters) => (suggestion) => getters.isOwnSuggestion(suggestion) && state.delegate.canAddSuggestion,
  isOwnSpeech: (state) => (speech) => speech.delegate_id === state.delegate.id,
  rightManageOwnSpeech: (state, getters) => (speech) => getters.isOwnSpeech(speech) && speech.status === 'waiting',
};

// actions
const actions = {
  init ({ commit }, delegate) {
    commit('init', delegate);
  },

  update ({ commit, rootGetters, state }, { action, delegate }) {
    if (delegate.id !== state.delegate.id) return;

    if (action === 'DelegateUpdated') commit('init', delegate);
    else if (action === 'DelegateDeleted') {
      /* TODO: say hello to removed delegate */
      document.location.href = rootGetters["route/url"]('event', 'index');
    }
  }
};

// mutations
const mutations = {
  init (state, delegate) {
    state.delegate = delegate;
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
}
