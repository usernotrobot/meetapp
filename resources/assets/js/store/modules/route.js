// initial state
const state = {
  route: {}
};

// getters
const getters = {
  get: state => state.route,
  url: (state, getters, rootState) => (type, action, id) => {
    let url = state.route[type][action],
      topic = rootState.topic.topic;
    return id
      ? url.replace('_id_', id).replace('_topic_', topic.id)
      : url.replace('_topic_', topic.id);
  },
};

// actions
const actions = {
  init ({ commit }, route) {
    commit('init', route);
  },
};

// mutations
const mutations = {
  init (state, route) {
    state.route = route;
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
}
