window._ = require('lodash');

/*global.$ = global.jQuery = require('jquery');*/

// required for bootstrap 4
/*global.Tether = require('tether');*/

/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */
if (window.Laravel) {
  window.axios = require('axios');

  window.axios.defaults.headers.common = {
    'X-CSRF-TOKEN': window.Laravel.csrfToken,
    'X-Requested-With': 'XMLHttpRequest'
  };
}

/*require("script-loader!./mdb/bootstrap.js");
require("script-loader!./mdb/mdb.js");*/
