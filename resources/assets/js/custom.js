function objMarkAs(args) {
  const reload = (typeof args.reload == 'undefined' || args.reload == true);
  $.ajax({
    method: 'post',
    url: $('[data-obj=' + args.obj + ']').data('change-status-url'),
    cache: false,
    data: {status: args.status}
  }).done(function () {
    if (reload) location.reload(true);
  }).fail(function () {
    toastr.error(Lang.get('messages.status-change-fail'));
  });
}

function objTrash(args, notify) {
  const reload = (typeof args.reload == 'undefined' || args.reload == true);
  $.ajax({
    method: args.method,
    url: args.url,
    cache: false,
    data: { notify: !!notify | 0, showMessage: reload | 0}
  }).done(function (response) {
    if (reload) location.reload(true);
    if (response.status) toastr.success(response.status);
    if (response.success) toastr.success(response.success);
    if (response.error) toastr.error(response.error);
  }).fail(function () {
    toastr.error(Lang.get('messages.action-fail'));
  });
}

function objAction(args) {
  const reload = (typeof args.reload == 'undefined' || args.reload == true);
  $.ajax({
    method: args.method,
    url: args.url,
    cache: false,
    data: args.data || null
  }).done(function (response) {
    if (reload) location.reload(true);
    if (response.success) toastr.success(response.success);
    if (response.error) toastr.error(response.error);
  }).fail(function (result) {
    let msg = result.responseJSON
      ? result.responseJSON.error
      : (args.msg ? args.msg.fail : Lang.get('messages.action-fail'));

    toastr.error(msg);
  });
}

/**
 * DataTables default settings
 */
if (typeof ($.fn.dataTable) !== 'undefined') {

  $.extend($.fn.dataTable.defaults, {
    order: [[0, "asc"]],
    columnDefs: [{sortable: false, targets: [-1]}, {searchable: false, targets: [0, -1]}],
    dom: '<<"text-right"f>t<"row"<"col-6"i><"col-6 text-right mt-1"p>>>',
    pageLength: 8,
    responsive: true,
    autoWidth: false,
    language: {
      /* TODO: Add here other datatable lang vars if you wanna use translation */
      search: "",
      searchPlaceholder: Lang.get('messages.datatable.search-placeholder'),
      info: Lang.get('messages.datatable.info'),
      infoEmpty: Lang.get('messages.datatable.info-empty'),
      infoFiltered: Lang.get('messages.datatable.info-filtered'),
    }
  });

  $.fn.dataTable.ext.order['data-order'] = function (settings, col) {
    return this.api().column(col, {order: 'index'}).nodes().map(function (td, i) {
      return $(td).data('order');
    });
  };
}


$(document).ready(function() {
  $('.mdb-select').material_select();
});
