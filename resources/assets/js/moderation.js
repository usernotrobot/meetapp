/** Common stuff */
require('./bootstrap');

/** Localization lib */
require("script-loader!./messages.js");

/** Bootstrap & MDB libs */
require("script-loader!./mdb/bootstrap.js");
require("script-loader!./mdb/mdb.js");

/** Registering vue components */
Vue.component('modal', require('./components/Modal.vue'));
Vue.component('modalSuggestion', require('./components/ModalSuggestion.vue'));

import store from './store/index.js';
import { mapGetters, mapActions } from 'vuex';
import draggable from 'vuedraggable';

/** New vue app */
const vueapp = new Vue({
  el: '#app',
  store,

  created () {
    this.initRoute(window.moderation.routes);
  },

  components: {
    draggable,
  },

  computed: {
    ...mapGetters({
      route: 'route/get',
      url: 'route/url',
    }),
  },

  data: {
    modal: null,
    modalSuggestion: null,
    dragList: window.moderation.suggestions || [],
    dropList: []
  },

  watch: {
    dragList: function(val, oldVal){
      setTimeout( ()=> { $('[data-toggle="tooltip"]').bstooltip() }, 100);
    },
    dropList: function(val, oldVal){
      setTimeout( ()=> { $('[data-toggle="tooltip"]').bstooltip() }, 100);
    }
  },

  methods: {
    updateData: function() {
      let that = this;
      axios.get(window.moderation.url).then(function(response) {
        that.initRoute(response.data.routes);
      });
    },

    showModalChild: function(event) {
      this[event.modalTarget] = event;
    },

    showModal: function(event) {
      let btn = event.target.hasAttribute('data-modal-target')
        ? $(event.target)
        : $(event.target).parent('[data-modal-target]');

      this[btn.data('modal-target')] = btn.data();
    },

    shownModal: function(wut) {
      this[wut] = null;
    },

    addToList: function( suggestion, evt ) {
      $(evt.target).parent().bstooltip('hide');
      let i = this.dragList.map( item => item.id ).indexOf( suggestion.id );
      this.dragList.splice( i, 1 );
      this.dropList.push( suggestion );
    },

    removeFromList( suggestion, evt ) {
      $(evt.target).parent().bstooltip('hide');
      let i = this.dropList.map( item => item.id ).indexOf( suggestion.id );
      this.dropList.splice( i, 1 );
      this.dragList.push( suggestion );
    },

    ...mapActions
    ({
      initRoute: 'route/init',
    }),

  }
});


/** Loading other js scripts */

jQuery.fn.bstooltip = jQuery.fn.tooltip;
require("script-loader!./custom.js");
require("script-loader!./moderation-custom.js");
