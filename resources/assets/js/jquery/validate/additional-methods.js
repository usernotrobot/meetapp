/*!
 * jQuery Validation Plugin v1.15.0
 *
 * http://jqueryvalidation.org/
 *
 * Copyright (c) 2016 Jörn Zaefferer
 * Released under the MIT license
 */
(function (factory) {
  if (typeof define === "function" && define.amd) {
    define(["jquery", "./jquery.validate"], factory);
  } else if (typeof module === "object" && module.exports) {
    module.exports = factory(require("jquery"));
  } else {
    factory(jQuery);
  }
}(function ($) {

  // Accept a value from a file input based on a required mimetype
  $.validator.addMethod("accept", function (value, element, param) {

    // Split mime on commas in case we have multiple types we can accept
    var typeParam = typeof param === "string" ? param.replace(/\s/g, "") : "image/*",
      optionalValue = this.optional(element),
      i, file, regex;

    // Element is optional
    if (optionalValue) {
      return optionalValue;
    }

    if ($(element).attr("type") === "file") {

      // Check if the element has a FileList before checking each file
      if (element.files && element.files.length) {
        regex = new RegExp(typeParam.replace('*', '[^\\/,]+'), "i");
        for (i = 0; i < element.files.length; i++) {
          file = element.files[i];

          // Grab the mimetype from the loaded file, verify it matches
          if (file.type !== "" && !file.type.match(regex)) {
            return false;
          }
        }
      }
    }

    // Either return true because we've validated each file, or because the
    // browser does not support element.files and the FileList feature
    return true;
  }, $.validator.format("Please select a file with a valid mimetype."));

  $.validator.addMethod("filesize", function (value, element, param) {

    // Split mime on commas in case we have multiple types we can accept
    var typeParam = parseInt(param),
      optionalValue = this.optional(element),
      i, file;

    // Element is optional
    if (optionalValue) {
      return optionalValue;
    }

    if ($(element).attr("type") === "file") {

      // Check if the element has a FileList before checking each file
      if (element.files && element.files.length) {
        for (i = 0; i < element.files.length; i++) {
          file = element.files[i];

          // Grab the mimetype from the loaded file, verify it matches
          if (file.size > typeParam) {
            return false;
          }
        }
      }
    }

    // Either return true because we've validated each file, or because the
    // browser does not support element.files and the FileList feature
    return true;
  }, $.validator.format("Please select a file with a valid size."));


  $.validator.addMethod( "extension", function( value, element, param ) {
    param = typeof param === "string" ? param.replace( /,/g, "|" ) : "png|jpe?g|gif";
    return this.optional( element ) || value.match( new RegExp( "\\.(" + param + ")$", "i" ) );
  }, $.validator.format( "Please enter a value with a valid extension." ) );
}));

