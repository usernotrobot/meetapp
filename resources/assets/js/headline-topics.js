/** Common stuff */
require('./bootstrap');

/** Localization lib */
require("script-loader!./messages.js");

/** Bootstrap & MDB libs */
require("script-loader!./mdb/bootstrap.js");
require("script-loader!./mdb/mdb.js");
require("script-loader!./custom.js");
/** Registering vue components */
Vue.component('modal', require('./components/Modal.vue'));

import draggable from 'vuedraggable';

const vueapp = new Vue({
  el: '#app',
  data: {
    modal: null,
    moveModalOpened: false,
    headlineTopics: headlineTopics,

    deleteRoute: deleteRoute,
    moveRoute: moveRoute,
    deletingItem: null,
    deletingItemIndex: null,
    modalText: Lang.get('messages.headlines.txt-delete'),

    topicsShowRoute: topicsShowRoute,
    topicCreateRoute: topicCreateRoute,
    topicCreateInItem: null,
    topicCreateInIndex: null,
    topicName: '',
    topicDescription: '',
    topicAjaxProgress: false
  },
  components:{
    draggable: draggable
  },
  methods:{

    confirmDelete: function(){
      $.ajax({
        method: "delete",
        url: `${this.deleteRoute}/${this.deletingItem.id}`,
        cache: false,
        data: this.deletingItem || null
      }).done((response) => {
        $('#deleteModal').modal('hide');
        if (response.success) toastr.success(response.success);
        if (response.error) toastr.error(response.error);

        let index = this.deletingItemIndex,
            $deleteEl = $(`.h-topics_table-body .h-topics_table-row:nth(${index})`);

        if( $deleteEl.length > 0 ){
            $deleteEl.animate({
              opacity: .2
            }, 1300, () => {
                Vue.delete(this.headlineTopics, index);
                $deleteEl.css('opacity',1);
            });
        }else{
          Vue.delete(this.headlineTopics, index);
        }

      }).fail((result) => {
        let msg = result.responseJSON
          ? result.responseJSON.error
          : Lang.get('messages.action-fail');
        toastr.error(msg);
      });
    },

    getHref: function( url, id ){
      url = url.replace('#', id);
      return url;
    },

    navigateToTopics( item, event ){
      let hasButtons = $(event.target).hasClass('has-buttons'),
          isButton = $(event.target).parents('.has-buttons').length > 0;
      if( !hasButtons && !isButton){
        let url = this.topicsShowRoute;
        url = url.replace('#', item.id)
        window.location.assign(url);
      }
    },

    onMoveEnd: function(evt){

      let data = [];
      this.headlineTopics.forEach((el, i) => {
        el.position = i;
        data.push({
          id: el.id,
          position: i
        });
      });

      $.ajax({
        method: "POST",
        url: moveRoute,
        cache: false,
        contentType: 'application/json',
        data: JSON.stringify(data)
      }).done((response) => {
        if (response.success) toastr.success(response.success);
        if (response.error) toastr.error(response.error);
        toastr.success(Lang.get('messages.headlines.move-success'));
      }).fail((result) => {
        // let msg = result.responseJSON
        //   ? result.responseJSON.error
        //   : Lang.get('messages.action-fail');
        // toastr.error(msg);

        /*
          THIS NEED TO BE REPLACED WITH ERROR
          WHEN BACKEND WILL BE READY
                   v v v
        */
        toastr.success(Lang.get('messages.headlines.move-success'));

      });

    },

    modalBodyText: function(name){
      let str = this.modalText;
      str = str.replace(':Name', name);
      return str;
    },

    showDeleteModal: function(item, index) {
      this.deletingItem = item;
      this.deletingItemIndex = index;
      this.modalText = this.modalBodyText(item.name);
      $('#deleteModal').modal('show');
    },

    showNewTopicModal: function( item, index ) {
      this.topicCreateInItem = item.id;
      this.topicCreateInIndex = index;
      $('#modalTopicNew').modal('show');
    },

    submitNewTopic: function(event) {
      let route = this.topicCreateRoute;
      route = route.replace('#', `${this.topicCreateInItem}` );
      let fd = new FormData($('#newTopicForm').get(0));
      this.topicAjaxProgress = true;

      $.ajax({
        method: 'POST',
        url: route,
        data: fd,
        cache: false,
        contentType: false,
        processData: false,
        success: (response) => {
          if( response.topic ){
            this.updateHeadline( response.headline );
            toastr.success(Lang.get('messages.moderation.msg.create-success'))
          }else{
            if (response.error) toastr.error(response.error);
          }
        },
        error: (xhr, textStatus, errorThrown) => {
          console.warn(xhr, textStatus, errorThrown);
        },
        complete: () => {
          this.topicAjaxProgress = false;
          $('#modalTopicNew').modal('hide');
        }
      });

    },

    updateHeadline: function( updatedHeadLine ){
      this.headlineTopics[this.topicCreateInIndex] = updatedHeadLine;
    },


    shownModal: function(wut) {
      this[wut] = null;
    },
  }
})


