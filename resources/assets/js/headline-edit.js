/** Common stuff */
require('./bootstrap');

/** Localization lib */
require("script-loader!./messages.js");

/** Bootstrap & MDB libs */
require("script-loader!./mdb/bootstrap.js");
require("script-loader!./mdb/mdb.js");
require("script-loader!./jquery/validate/jquery.validate.min.js");

Vue.component('modal', require('./components/Modal.vue'));

const STATUS_INITIAL = 0, STATUS_SAVING = 1, STATUS_SUCCESS = 2, STATUS_FAILED = 3;

const headlineEdit = new Vue({
  el: '#app',

  data:{
    modal: null,
    modalTopicNew: null,
    topicsArr: topicsArr,
    filesArr: filesArr,
    deleteFileRoute: deleteFileRoute,
    deletingItem: null,
    modalText: null,
    currentStatus: null,
    uploadFieldName: 'file[]',

    uploadMethod: "POST",
    uploadRoute: uploadRoute,
    fileInputRules: filesExt,
    allowedFileTypes: null,

    topicCreateRoute: topicCreateRoute,
    topicName: '',
    topicDescription: '',
    topicAjaxProgress: false
  },

  computed: {
    isInitial() {
      return this.currentStatus === STATUS_INITIAL;
    },
    isSaving() {
      return this.currentStatus === STATUS_SAVING;
    }
  },

  created: function() {
    this.currentStatus = STATUS_INITIAL;
    this.allowedFileTypes = new RegExp(`\\.(${this.fileInputRules})$`, 'i');

  },

  methods:{

    filesChange: function( evt ){
      const formData = new FormData();
      const fileList = evt.target.files
      const fieldName = evt.target.name;

      // append the files to FormData
      Array.from(Array(fileList.length).keys()).map(x => {
        if( this.allowedFileTypes.test(fileList[x].name) ){
          formData.append(fieldName, fileList[x], fileList[x].name);
        }
      });


      if (!fileList.length) return;

      this.save(formData);
      evt.target.value = "";
    },

    save( formData ) {

      this.currentStatus = STATUS_SAVING;

      $.ajax({
        method: this.uploadMethod,
        url: this.uploadRoute,
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        success: (response) => {
          this.addToTheList( response );
        },
        error: (xhr, textStatus, errorThrown) => {
          toastr.error( Lang.get('messages.validation.type', {
            types: this.fileInputRules.replace( /\|/g, ', ' )
          }));
          console.warn(xhr, textStatus, errorThrown);
        },
        complete: () => {
          this.currentStatus = STATUS_INITIAL;
        }
      });
    },

    changeModalText: function( title ){
      let str = Lang.get('messages.mdl.files.txt-delete');
      str = str.replace(':name', title);
      return str;
    },

    addToTheList: function( uploadedFiles ){
      for (var i = 0; i < uploadedFiles.length; i++) {
        this.filesArr.push(uploadedFiles[i]);
      }
    },

    showRemoveModal: function( file, index ){
      this.modalText = this.changeModalText( file.name );
      this.deletingItem = file;
      this.deletingItem.index = index;
      $('#confirmDelete').modal('show')
    },

    removeFromList: function( index ){
      this.filesArr.splice( index, 1 );
    },

    okBtn: function(){

      $.ajax({
        method: "delete",
        url: `${this.deleteFileRoute}/${this.deletingItem.id}`,
        cache: false,
        data: this.deletingItem || null
      }).done((response) => {

        if (response.success) toastr.success(response.success);
        if (response.error) toastr.error(response.error);

        $('#confirmDelete').modal('hide');

        this.removeFromList( this.deletingItem.index );

      }).fail((result) => {
        let msg = result.responseJSON
          ? result.responseJSON.error
          : Lang.get('messages.action-fail');
        toastr.error(msg);
      });

    },

    showNewTopicModal: function(event) {
      $('#modalTopicNew').modal('show');
    },

    submitNewTopic: function(event) {

      let fd = new FormData($('#newTopicForm').get(0));

      this.topicAjaxProgress = true;

      $.ajax({
        method: this.uploadMethod,
        url: this.topicCreateRoute,
        data: fd,
        cache: false,
        contentType: false,
        processData: false,
        success: (response) => {
          this.addToTheList( response );
          $('#modalTopicNew').modal('hide');
          setTimeout(()=>{
            window.location.reload();
          },300);
        },
        error: (xhr, textStatus, errorThrown) => {
          // toastr.error( Lang.get('messages.validation.type', {
          //   types: this.fileInputRules.replace( /\|/g, ', ' )
          // }));
          console.warn(xhr, textStatus, errorThrown);
        },
        complete: () => {
          this.topicAjaxProgress = false;
        }
      });

    },


    shownModal: function(wut) {
      this[wut] = null;
    }
  },
});

/*

  jQuery js goes below

*/

(function(){
    /*
     filesArr defined in /resources/views/admin/headlines/edit.blade.php
     */
    let classes = {
        isMoving: 'is-moving',
        cancelMove: 'cancel-move',
        hidden: 'is-hidden',
        topicCard: 'js-topic-card',
        dragOver: 'is-dragover'
    }

    // Display objects
    let $hEdeitForm = $('.js-h-edit-form'),
        $moveModal = $('.js-move-modal'),
        $topicCards = $('.js-topic-card'),
        $checkboxes = $('.js-checkbox'),
        $headlineSelect = $('.js-headline-select'),
        $mmCancelBtn = $('.js-mm-cancel'),
        $mmSubmitBtn = $('.js-mm-submit'),
        $dropZone = $('.js-drop-zone'),
        $cancelMove = $(`<a class="${classes.cancelMove}" href="#"><i class="fa fa-times" aria-hidden="true"></i></a>`);

    // Events
    $checkboxes.on( 'change', checkboxChanged );
    $headlineSelect.on( 'change', selectChanged );
    $mmCancelBtn.on( 'click', cancelMoving );
    $mmSubmitBtn.on( 'click', moveTopics );
    $cancelMove.on( 'click', unMarkToStay );

    $(document).on({
      dragover: onDragOver,
      dragleave: onDragOut,
      drop: onDrop
    });

    function onDrop( e ){
      $dropZone.removeClass( classes.dragOver );
    }

    function getCheckboxes(){
        let checkboxes = [];
        $.each( $checkboxes, function(i,el){
            if( $(el).is( ':checked' ) ){
                checkboxes.push($(el).attr('name'))
            }
        })
        return checkboxes;
    }

    function uncheckCheckboxes(){
        $.each( $checkboxes, function(i,el){
            if( $( el ).is( ':checked' ) && $( el ).val() === 'null'){
                $( el ).trigger( 'click' );
            }
        });
    }

    function cancelMoving( e ){
        e.preventDefault();
        uncheckCheckboxes();
        hideMoveModal();
    }

    function showMoveModal(){
        $moveModal.removeClass( classes.hidden );
    }

    function hideMoveModal(){
        $moveModal.addClass( classes.hidden );
    }

    function moveTopics(){
        let checkboxes = getCheckboxes(),
            selectVal = $headlineSelect.val();
        if( checkboxes.length && selectVal !== '' ){
            setCheckBoxVal( selectVal );
        }
        hideMoveModal();
    }

    function unMarkToStay( e ){
        e.preventDefault();
        let $box = $(e.target).parents(`.${classes.topicCard}`);
        $box.find('input[type=checkbox]').val('null').trigger('click');
        $box.removeClass( classes.isMoving );
        $box.find( `.${classes.cancelMove}` ).remove();
        hideMoveModal();
    }

    function markToMove( $el ){
        let $box = $el.parents(`.${classes.topicCard}`),
            dest = $headlineSelect.find('option:selected').text();
        $box.addClass( classes.isMoving );
        $box.append( $cancelMove.clone(true, true) );
        $box.attr( 'data-dest', dest) ;
    }

    function setCheckBoxVal( newVal ){
        $.each( $checkboxes, function( i, el ){
            if( $( el ).is( ':checked' ) && $( el ).val() === 'null' ){
                $( el ).val( newVal );
                markToMove( $( el ) );
            }
        })
    }

    function toggleAccept( onOff ){
        $mmSubmitBtn.attr( 'disabled', onOff );
    }

    function selectChanged( e ){
        let val = $( this ).val();
        toggleAccept( ( val === '' ) ? true : false );
    }

    function checkboxChanged( e ){
        let checkboxes = getCheckboxes();
        if ( checkboxes.length ){
            showMoveModal();
        } else {
            hideMoveModal();
        }
    }

    function onDragOver( e ){
      e.preventDefault();
      $dropZone.addClass( classes.dragOver )
    }

    function onDragOut( e ){
      $dropZone.removeClass( classes.dragOver )
    }
})();