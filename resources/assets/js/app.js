/**
 * First we will load all of this project's JavaScript dependencies which
 * include Vue and Vue Resource. This gives a great starting point for
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

require("script-loader!./messages.js");

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the body of the page. From here, you may begin adding components to
 * the application, or feel free to tweak this setup for your needs.
 */

Vue.component('modal', require('./components/Modal.vue'));
Vue.component('modalUpload', require('./components/ModalUpload.vue'));
Vue.component('modalDelegatesUpload', require('./components/ModalDelegatesUpload.vue'));
Vue.component('modalSpeechlist', require('./components/ModalSpeechlist.vue'));

vueapp = new Vue({
  el: '#app',
  data: {
    modal: null,
    modalUpload: null,
    modalDelegatesUpload: null,
    modalSpeechlist: null,
  },
  methods: {
    showModal: function(event) {
      let btn = event.target.hasAttribute('data-modal-target')
        ? $(event.target)
        : $(event.target).parent('[data-modal-target]');

      this[btn.data('modal-target')] = btn.data();
    },
    shownModal: function(wut) {
      this[wut] = null;
    },
  }
});


require("script-loader!./mdb/bootstrap.js");
require("script-loader!./mdb/mdb.js");
require("script-loader!./jquery/jquery.dataTables.min.js");
require("script-loader!./jquery/dataTables.bootstrap4.min.js");
require("script-loader!./jquery/validate/jquery.validate.min.js");
require("script-loader!./jquery/validate/additional-methods.js");
require("script-loader!./custom.js");
