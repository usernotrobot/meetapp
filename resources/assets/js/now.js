/** Common stuff */
require('./bootstrap');

/** Localization lib */
require("script-loader!./messages.js");

/** Bootstrap & MDB libs */
require("script-loader!./mdb/bootstrap.js");
require("script-loader!./mdb/mdb.js");

/** Socket.IO client */
window.io = require('socket.io-client');

/** Registering vue components */
Vue.component('modal', require('./components/Modal.vue'));
Vue.component('modalSuggestion', require('./components/ModalSuggestion.vue'));
Vue.component('modalSpeakerPriority', require('./components/ModalSpeakerPriority.vue'));
Vue.component('modalSpeakerLength', require('./components/ModalSpeakerLength.vue'));
Vue.component('modalVotingStart', require('./components/ModalVotingStart.vue'));
Vue.component('modalTopicSettings', require('./components/ModalTopicSettings.vue'));
Vue.component('blockAddSpeaker', require('./components/BlockAddSpeaker.vue'));
Vue.component('blockSuggestion', require('./components/BlockSuggestion.vue'));
Vue.component('blockTopic', require('./components/BlockTopic.vue'));
Vue.component('blockSpeechlist', require('./components/BlockSpeechlist.vue'));
Vue.component('blockVote', require('./components/BlockVote.vue'));

Vue.filter('two_digits', function (value) {
  return value.toString().length <= 1 ? "0" + value.toString() :value.toString();
});

import store from './store/index.js';
import { mapGetters, mapActions } from 'vuex';

/** New vue app */
const vueapp = new Vue({
  el: '#app',
  store,

  created () {
    /* TODO: Think how to get data dynamically before loading app */
    //this.updateData();
    this.initTopic(window.now_page.topic);
    this.initDelegate(window.now_page.delegate);
    this.initRoute(window.now_page.routes);
    this.initSpeechlist(window.now_page.speechlist);
  },

  mounted() {
    this.listen();
    this.checkVoteStatus(this.topic.status);
  },

  computed: {
    ...mapGetters({
      topic: 'topic/get',
      delegate: 'delegate/get',
      route: 'route/get',
      url: 'route/url',
    }),
  },

  data: {
    modal: null,
    modalSuggestion: null,
    modalSpeakerPriority: null,
    modalSpeakerLength: null,
    modalVotingStart: null,
    modalTopicSettings: null,
    blockAddSpeaker: null,
    blockVote: null,
  },

  watch: {
    topic: function (val) {
      this.checkVoteStatus(val.status);
    },
  },

  methods: {
    updateData: function() {
      let that = this;
      axios.get(window.now_page.url).then(function(response) {
        that.initTopic(response.data.topic);
        that.initDelegate(response.data.delegate);
        that.initRoute(response.data.routes);
        that.initSpeechlist(response.data.speechlist);
      });
    },

    showModalChild: function(event) {
      this[event.modalTarget] = event;
    },

    showModal: function(event) {
      let btn = event.target.hasAttribute('data-modal-target')
        ? $(event.target)
        : $(event.target).parent('[data-modal-target]');

      this[btn.data('modal-target')] = btn.data();
    },

    shownModal: function(wut) {
      this[wut] = null;
    },

    hide: function (wut) {
      this[wut] = false;
    },

    listen: function() {
      Echo.private('topics.' + this.topic.id)
        .listen('SuggestionEvent', (e) => {
          this.$refs.blockSuggestion.$emit(e.action, e.suggestion);
        })
        .listen('TopicEvent', (e) => {
          this.updateTopic(e);
        })
        .listen('SpeechEvent', (e) => {
          this.updateSpeechlist(e);
        });

      /* TODO: Add management for changed active topic */
      Echo.private('events.' + this.delegate.event_id)
        .listen('DelegateEvent', (e) => {
          this.updateDelegate(e);
        })
    },

    ...mapActions
    ({
      initTopic: 'topic/init',
      initDelegate: 'delegate/init',
      initRoute: 'route/init',
      initSpeechlist: 'speechlist/init',
      updateTopic: 'topic/update',
      updateDelegate: 'delegate/update',
      updateSpeechlist: 'speechlist/update',
    }),

    checkVoteStatus: function (status) {
      if (status === 'voting') {
        this.blockVote = 'blockVote';
      } else {
        this.blockVote = null;
      }
    }
  }
});

/** Loading other js scripts */
require("script-loader!./jquery/validate/jquery.validate.min.js");
require("script-loader!./jquery/validate/additional-methods.js");
require("script-loader!./custom.js");
