<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>{{ config('app.name', 'myMeet') }}</title>

  <!-- Styles -->
  <link href="{{ mix('css/app.css') }}" rel="stylesheet">
  @stack('styles')

  {{-- Icons --}}
  @include('layouts.icons')

  <!-- Scripts -->
  <script>
    window.Laravel = {!! json_encode(['csrfToken' => csrf_token()]) !!};
  </script>
</head>
<body class="fixed-sn light-meet-skin">
<header>
  <!-- Sidebar navigation -->
  <div id="slide-out" class="side-nav fixed custom-scrollbar">
    <!-- Side navigation links -->

    <!-- <li class="logo"> -->
      <div class="logo">
        <a class="waves-light" href="/"><img src="/images/logo_white.png" alt="{{ config('app.name') }}"/></a>
      </div>
    <!-- </li> -->

    <!-- <li class="divider"></li> -->

    <!-- <li> -->
      <div class="accordion side-nav_accordion" id="accordion" role="tablist" aria-multiselectable="true">
        @if(Auth::user()->isAdmin())
          @include('admin.left-menu')
        @else
          @include('user.left-menu')
        @endif
      </div>
    <!-- </li> -->
    <!--/. Side navigation links -->

    @if (config('app.env') != 'production' && file_exists(public_path() . elixir('version.js')))
      <div style="position: absolute; bottom:0;width:100%;" class="text-size-sm blue-grey-text text-center"><small id="version">
          Version {{ file_get_contents(public_path() . elixir('version.js')) }}
        </small></div>
    @endif
  </div>
  <!--/. Sidebar navigation -->

  <nav class="navbar navbar-light orange lighten-1 navbar-toggleable-md double-nav">

    <!-- SideNav slide-out button -->
    <div class="float-xs-left hidden-xlg-up">
      <a href="#" data-activates="slide-out" class="button-collapse"><i class="fa fa-bars"></i></a>
    </div>

    <!-- Breadcrumb-->
    @section('breadcrumbs')
      <div class="breadcrumb-dn mr-auto d-flex justify-content-center">
        <p class="align-self-center black-text">
          <a href="/"><img src="/images/logo.png" alt="{{ config('app.name') }}" style="max-height: 24px" /></a>
        </p>
      </div>
    @show

    <!--Navbar icons-->
    <ul class="nav navbar-nav nav-flex-icons ml-auto">
      <li class="nav-item"><a class="nav-link text-nowrap" href="{{ route('profile') }}"><span class="hidden-sm-down">{{ Auth::user()->name }}</span> <i class="fa fa-user-circle fa-fw"></i></a></li>

      <li class="nav-item btn-group">
        <a class="nav-link" id="dropdownMenuProfile" data-toggle="dropdown"
           aria-haspopup="true" aria-expanded="false"><i class="fa fa-ellipsis-v"></i>
        </a>
        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuProfile">
          @if (!Auth::user()->isAdmin())
            <a href="#" class="dropdown-item">
              <i class="fa fa-check-circle fa-fw text-success" aria-hidden="true"></i> @lang('app.status.online')
            </a>
            <a href="#" class="dropdown-item">
              <i class="fa fa-circle fa-fw text-warning" aria-hidden="true"></i> @lang('app.status.away')
            </a>
          @endif

          <a href="{{ route('logout') }}" class="dropdown-item"
             onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
            <i class="fa fa-circle fa-fw text-danger" aria-hidden="true"></i> @lang('app.status.logout')
          </a>

          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>
        </div>
      </li>
    </ul>
  </nav>
</header>
<main class="@stack('main-class')">
  <div class="container-fluid">
    {{--@yield('breadcrumbs')--}}

    <div id="app">
      @yield('content')

      {{-- Vue Modal --}}
      <div is="modal" v-bind:data="modal" v-on:modal-shown="shownModal"></div>
    </div>
  </div>
</main>

<!-- Scripts -->
@if(Request::is('*now*'))
  @yield('data')
  <script src="{{ mix('js/now.js') }}"></script>
@else
  @section('vuejs')
    <script src="{{ mix('js/app.js') }}"></script>
  @show
@endif

<script>
  $(".button-collapse").sideNav();
  var el = document.querySelector('.custom-scrollbar');
  Ps.initialize(el);
</script>
{!! Toastr::render() !!}

@yield('scripts')

</body>
</html>
