<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <title>{{ config('app.name', 'myMeet') }}</title>

  {{-- Icons --}}
  @include('layouts.icons')

  <!-- Styles -->
  <link href="{{ mix('css/app.css') }}" rel="stylesheet">
</head>
<body class="light-meet-skin">
<nav class="navbar navbar-light orange lighten-1">
  <div class="container">
    <a class="navbar-brand" href="/"><img src="/images/logo.png" alt="{{ config('app.name') }}" style="max-height: 24px"/></a>
  </div>
</nav>

@yield('content')

@yield('scripts')

</body>
</html>
