<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
  
  <title>{{ config('app.name', 'myMeet') }}</title>
  
  <!-- Styles -->
  <link href="{{ mix('css/app.css') }}" rel="stylesheet">

  {{-- Icons --}}
  @include('layouts.icons')

  <!-- Scripts -->
  <script>
    window.Laravel = {!! json_encode(['csrfToken' => csrf_token()]) !!};
  </script>
</head>
<body>

<div class="view">
  <div class="full-bg-img flex-center">
    @yield('content')
  </div>
</div>

<!-- Scripts -->
<script src="{{ mix('js/min.js') }}"></script>

</body>
</html>
