@can('index', App\Models\Client::class)
  <a class="waves-effect {{ (Request::is('*clients*') ? 'active' : '') }}" href="{{ route('admin::clients.index') }}"><i class="fa fa-users"></i> @lang('app.nav.clients')</a>
@endcan

@can('index', App\Models\Event::class)
  <a class="waves-effect {{ (Request::is('*events*') || Request::is('*topics*') || Request::is('*delegates*')? 'active' : '') }}" href="{{ route('admin::events.index') }}"><i class="fa fa-calendar"></i> @lang('app.nav.events')</a>
@endcan

<div class="panel panel-default">
  <div class="panel-heading" role="tab" id="headingFour">
    <h5 class="panel-title"><a class="arrow-r" data-toggle="collapse" data-parent="#accordion" href="#collapseFour">@lang('app.mail.header')<i class="fa fa-angle-down rotate-icon"></i></a></h5>
  </div>
  <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
    <ul>
      <li>
        <a href="#" class="waves-effect">@lang('app.mail.inbox')</a>
      </li>
      <li>
        <a href="#" class="waves-effect">@lang('app.mail.compose')</a>
      </li>
    </ul>
  </div>
</div>

{{--
<li class="divider"></li>

<li>
  <a href="{{ route('profile') }}" class="collapsible-header waves-effect {{ (Request::is('profile*') ? 'active' : '') }}">@lang('app.nav.profile')</a>
</li>

<li class="divider"></li>

<li>
  <a href="{{ route('logout') }}" class="collapsible-header waves-effect"
     onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
    @lang('app.status.logout')
  </a>
</li>
--}}
