@extends('layouts.app')

@section('breadcrumbs')
  <ol class="breadcrumb transparent">

    <li class="breadcrumb-item active">@lang('clients.index')</li>
  </ol>
@endsection

@section('content')
  <div class="row">
    <div class="col-12">
      <table class="table table-hover" id="data-table" style="table-layout: fixed">
        <thead>
        <tr>
          <th style="width: 75px">@lang('clients.lbl.id')</th>
          <th style="width: 20%">@lang('clients.lbl.name')</th>
          <th>@lang('clients.lbl.description')</th>
          <th style="width: 10%" class="text-center">@lang('clients.lbl.events')</th>
          <th style="width: 10%"></th>
        </tr>
        </thead>
        <tbody>
        @foreach($clients as $client)
          <tr class="@if($client->trashed()) table-active @endif @if($loop->iteration >= $pageItems) datatable-hidden-tr @endif">
            <th scope="row">{{ $loop->iteration }}</th>
            <td>{{ $client->name }}</td>
            <td class="text-ellipsis" title="{{ $client->description}}">{{ $client->description }}</td>
            <td class="text-center">{{ $client->events->count() }}</td>
            <td class="dropnone text-center">
              <a href="#" id="dropdownMenuClient{{ $client->id }}"
                 data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-ellipsis-v"></i>
              </a>
              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuClient{{ $client->id }}"
                   data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">
                @if(!$client->trashed())
                  <a href="{{ route('admin::events.create')}}?client={{$client->id}}" class="dropdown-item" >@lang('clients.nav.add-event')</a>
                  <div class="dropdown-divider"></div>
                  <a href="{{ route('admin::clients.edit', $client) }}" class="dropdown-item">@lang('clients.nav.edit')</a>
                  <a href="#" class="dropdown-item" v-on:click="showModal" data-modal-target="modal"
                     data-modal-title="@lang('clients.modal.ttl-delete')"
                     data-modal-body="@lang('clients.modal.txt-delete', ['name' => $client->name])"
                     data-ok-func="objTrash"
                     data-ok-args='{"url": "{{ route('admin::clients.destroy', $client) }}", "method": "delete"}'>
                    @lang('clients.nav.delete')
                  </a>
                @else
                  <a href="#" class="dropdown-item" v-on:click="showModal" data-modal-target="modal"
                     data-modal-title="@lang('clients.modal.ttl-restore')"
                     data-modal-body="@lang('clients.modal.txt-restore', ['name' => $client->name])"
                     data-ok-func="objTrash"
                     data-ok-args='{"url": "{{ route('admin::clients.restore', $client) }}", "method": "post"}'>
                    @lang('clients.nav.restore')
                  </a>
                @endif
              </div>
            </td>
          </tr>
        @endforeach
        </tbody>
      </table>
    </div>
  </div>

  <a class="float-right mdl-button mdl-button--fab mdl-button--colored amber darken-4" href="{{ route('admin::clients.create') }}">
    <i class="fa fa-plus material-icons"></i>
  </a>
@endsection

@section('scripts')
  <script>
    $().ready(function () {
      $('#data-table').DataTable({
        paging: {{ (count($clients) >= $pageItems) ? 'true' : 'false' }},
      });
    });
  </script>
@endsection
