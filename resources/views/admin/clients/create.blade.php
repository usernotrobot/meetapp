@extends('layouts.app')

@section('breadcrumbs')
  <ol class="breadcrumb transparent">
    <li class="breadcrumb-item"><a href="{{ route('admin::clients.index') }}">@lang('clients.index')</a></li>
    <li class="breadcrumb-item active">@lang('clients.create')</li>
  </ol>
@endsection

@section('content')
  <form class="row" role="form" method="POST" action="{{ route('admin::clients.store') }}">

    {{ csrf_field() }}
    <div class="col-12 col-md-6 form-horizontal">
      <div class="mb-2"><h3>@lang('clients.client-details')</h3></div>

      <div class="md-form form-group{{ $errors->has('name') ? ' has-danger' : '' }} ">
        <input id="client-name" type="text"
               class="form-control{{ $errors->has('name') ? ' form-control-danger' : '' }}"
               name="name" value="{{ old('name') }}" required autofocus maxlength="255">
        <label for="client-name">@lang('clients.lbl.name')</label>
        @if ($errors->has('name'))
          <div class="form-control-feedback">{{ $errors->first('name') }}</div>
        @endif
      </div>

      <div class="md-form form-group{{ $errors->has('description') ? ' has-danger' : '' }}">
        <textarea class="md-textarea form-control{{ $errors->has('description') ? ' form-control-danger' : '' }}"
                  type="text" id="client-description" name="description" style="height: 9.55rem;"
                  maxlength="255" length="255">{{ old('description') }}</textarea>
        <label for="client-description">@lang('clients.lbl.description')</label>
        @if ($errors->has('description'))
          <div class="form-control-feedback">{{ $errors->first('description') }}</div>
        @endif
      </div>
    </div>

    {{-- Admin details --}}
    <div class="col-12 col-md-6 form-horizontal">
      <div class="mb-2"><h3>@lang('clients.admin-details')</h3></div>

      <div class="row">
        <div class="col-6">
          <div class="md-form form-group{{ $errors->has('first_name') ? ' has-danger' : '' }} ">
            <input id="client-first_name" type="text"
                   class="form-control{{ $errors->has('first_name') ? ' form-control-danger' : '' }}"
                   name="first_name" value="{{ old('first_name') }}" required maxlength="255">
            <label for="client-first_name">@lang('users.lbl.first-name')</label>
            @if ($errors->has('first_name'))
              <div class="form-control-feedback">{{ $errors->first('first_name') }}</div>
            @endif
          </div>
        </div>
        <div class="col-6">
          <div class="md-form form-group{{ $errors->has('last_name') ? ' has-danger' : '' }} ">
            <input id="client-last_name" type="text"
                   class="form-control{{ $errors->has('last_name') ? ' form-control-danger' : '' }}"
                   name="last_name" value="{{ old('last_name') }}" required maxlength="255">
            <label for="client-last_name">@lang('users.lbl.last-name')</label>
            @if ($errors->has('last_name'))
              <div class="form-control-feedback">{{ $errors->first('last_name') }}</div>
            @endif
          </div>
        </div>
      </div>

      <div class="md-form form-group{{ $errors->has('email') ? ' has-danger' : '' }} ">
        <input id="client-email" type="email"
               class="form-control{{ $errors->has('email') ? ' form-control-danger' : '' }}"
               name="email" value="{{ old('email') }}" required maxlength="255">
        <label for="client-email">@lang('users.lbl.email')</label>
        @if ($errors->has('email'))
          <div class="form-control-feedback">{{ $errors->first('email') }}</div>
        @endif
      </div>

      <div class="md-form form-group{{ $errors->has('password') ? ' has-danger' : '' }} ">
        <input id="client-password" type="password"
               class="form-control{{ $errors->has('password') ? ' form-control-danger' : '' }}"
               name="password" required maxlength="255">
        <label for="client-password">@lang('users.lbl.password')</label>
        @if ($errors->has('password'))
          <div class="form-control-feedback">{{ $errors->first('password') }}</div>
        @endif
      </div>

      <div class="md-form form-group{{ $errors->has('password_confirmation') ? ' has-danger' : '' }} ">
        <input id="client-password-confirmation" type="password"
               class="form-control{{ $errors->has('password_confirmation') ? ' form-control-danger' : '' }}"
               name="password_confirmation" required maxlength="255">
        <label for="client-password-confirmation">@lang('users.lbl.password-confirmation')</label>
        @if ($errors->has('password_confirmation'))
          <div class="form-control-feedback">{{ $errors->first('password_confirmation') }}</div>
        @endif
      </div>

    </div>
    <div class="col-12 mt-2">
      <div class="text-center">
        <button class="btn btn-default" type="reset">@lang('app.btn.reset')</button>
        <button class="btn btn-amber" type="submit">@lang('app.btn.save')</button>
      </div>
    </div>

  </form>
@endsection
