@extends('layouts.app')

@section('breadcrumbs')
  <ol class="breadcrumb transparent">

    <li class="breadcrumb-item"><a href="{{ route('admin::events.index') }}">@lang('events.index')</a></li>
    <li class="breadcrumb-item">{{ $event->name }} @if($event->trashed())(deleted)@endif</li>
    <li class="breadcrumb-item"><a href="{{ route('admin::delegates.index', $event) }}">@lang('delegates.index')</a></li>
    <li class="breadcrumb-item">{{ $delegate->user->name }}</li>
    <li class="breadcrumb-item active">@lang('delegates.nav.edit')</li>
  </ol>
@endsection

@section('content')
  <form class="form-horizontal" role="form" method="POST" action="{{ route('admin::delegates.update', $delegate) }}">
    <div class="row">
      <div class="col-12 col-lg-6">
        {{ csrf_field() }}
        {{ method_field('PUT') }}

        <div class="mb-2"><h3>@lang('delegates.user-details')</h3></div>

        <div class="row">
          <div class="col-6">
            <div class="md-form form-group{{ $errors->has('first_name') ? ' has-danger' : '' }} ">
              <input id="delegate-first_name" type="text"
                     class="form-control{{ $errors->has('first_name') ? ' form-control-danger' : '' }}"
                     name="first_name" value="{{ old('first_name', $delegate->user->first_name) }}" required maxlength="255">
              <label for="delegate-first_name">@lang('users.lbl.first-name')</label>
              @if ($errors->has('first_name'))
                <div class="form-control-feedback">{{ $errors->first('first_name') }}</div>
              @endif
            </div>
          </div>
          <div class="col-6">
            <div class="md-form form-group{{ $errors->has('last_name') ? ' has-danger' : '' }} ">
              <input id="delegate-last_name" type="text"
                     class="form-control{{ $errors->has('last_name') ? ' form-control-danger' : '' }}"
                     name="last_name" value="{{ old('last_name', $delegate->user->last_name) }}" required maxlength="255">
              <label for="delegate-last_name">@lang('users.lbl.last-name')</label>
              @if ($errors->has('last_name'))
                <div class="form-control-feedback">{{ $errors->first('last_name') }}</div>
              @endif
            </div>
          </div>
        </div>

        <div class="md-form form-group{{ $errors->has('email') ? ' has-danger' : '' }} ">
          <input id="delegate-email" type="text"
                 class="form-control{{ $errors->has('email') ? ' form-control-danger' : '' }}"
                 name="email" value="{{ old('email', $delegate->user->email) }}" required maxlength="255">
          <label for="delegate-email">@lang('users.lbl.email')</label>
          @if ($errors->has('email'))
            <div class="form-control-feedback">{{ $errors->first('email') }}</div>
          @endif
        </div>

        <div class="row">
          <div class="col-6">
            <div class="md-form form-group{{ $errors->has('password') ? ' has-danger' : '' }} ">
              <input id="delegate-password" type="password"
                     class="form-control{{ $errors->has('password') ? ' form-control-danger' : '' }}"
                     name="password" maxlength="255">
              <label for="delegate-password">@lang('users.lbl.password-wth-hint')</label>
              @if ($errors->has('password'))
                <div class="form-control-feedback">{{ $errors->first('password') }}</div>
              @endif
            </div>
          </div>
          <div class="col-6">
            <div class="md-form form-group{{ $errors->has('password_confirmation') ? ' has-danger' : '' }} ">
              <input id="delegate-password-confirmation" type="password"
                     class="form-control{{ $errors->has('password_confirmation') ? ' form-control-danger' : '' }}"
                     name="password_confirmation" maxlength="255">
              <label for="delegate-password-confirmation">@lang('users.lbl.password-confirmation')</label>
              @if ($errors->has('password_confirmation'))
                <div class="form-control-feedback">{{ $errors->first('password_confirmation') }}</div>
              @endif
            </div>
          </div>
        </div>
        <div class="md-form form-group{{ $errors->has('user_token') ? ' has-danger' : '' }}">
          <input id="delegate-user_token" type="text"
                 class="form-control"{{ $errors->has('user_token') ? ' form-control-danger' : '' }}
                 name="user_token" value="{{ old('user_token', $delegate->user->api_token) }}" maxlength="255">
          <label for="delegate-user_token">@lang('users.lbl.user-token')</label>
          @if ($errors->has('user_token'))
            <div class="form-control-feedback">{{ $errors->first('user_token') }}</div>
          @endif
        </div>
      </div>

      <div class="col-12 col-lg-6">
        <div class="mb-2"><h3>@lang('delegates.user-union')</h3></div>
        <div class="row">
          <div class="col-6">
            <div class="md-form form-group{{ $errors->has('union') ? ' has-danger' : '' }} ">
              <input id="delegate-union" type="text"
                     class="form-control{{ $errors->has('union') ? ' form-control-danger' : '' }}"
                     name="union" value="{{ old('union', $delegate->user->union) }}" required maxlength="255">
              <label for="delegate-union">@lang('users.lbl.union')</label>
              @if ($errors->has('union'))
                <div class="form-control-feedback">{{ $errors->first('union') }}</div>
              @endif
            </div>
          </div>
          <div class="col-6">
            <div class="md-form form-group{{ $errors->has('union_sort_key') ? ' has-danger' : '' }} ">
              <input id="delegate-union_sort_key" type="text"
                     class="form-control{{ $errors->has('union_sort_key') ? ' form-control-danger' : '' }}"
                     name="union_sort_key" value="{{ old('union_sort_key', $delegate->user->union_sort_key) }}" required maxlength="255">
              <label for="delegate-union_sort_key">@lang('users.lbl.union-sort-key')</label>
              @if ($errors->has('union_sort_key'))
                <div class="form-control-feedback">{{ $errors->first('union_sort_key') }}</div>
              @endif
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-6">
            <div class="md-form form-group{{ $errors->has('branch') ? ' has-danger' : '' }} ">
              <input id="delegate-branch" type="text"
                     class="form-control{{ $errors->has('branch') ? ' form-control-danger' : '' }}"
                     name="branch" value="{{ old('branch', $delegate->user->branch) }}" maxlength="255">
              <label for="delegate-branch">@lang('users.lbl.branch')</label>
              @if ($errors->has('branch'))
                <div class="form-control-feedback">{{ $errors->first('branch') }}</div>
              @endif
            </div>
          </div>
          <div class="col-6">
            <div class="md-form form-group{{ $errors->has('branch_sort_key') ? ' has-danger' : '' }} ">
              <input id="delegate-branch_sort_key" type="text"
                     class="form-control{{ $errors->has('branch_sort_key') ? ' form-control-danger' : '' }}"
                     name="branch_sort_key" value="{{ old('branch_sort_key', $delegate->user->branch_sort_key) }}" maxlength="255">
              <label for="delegate-branch_sort_key">@lang('users.lbl.branch-sort-key')</label>
              @if ($errors->has('branch_sort_key'))
                <div class="form-control-feedback">{{ $errors->first('branch_sort_key') }}</div>
              @endif
            </div>
          </div>
        </div>

        <div class="md-form form-group{{ $errors->has('delegate_id') ? ' has-danger' : '' }} ">
          <input id="delegate-delegate_id" type="text"
                 class="form-control{{ $errors->has('delegate_id') ? ' form-control-danger' : '' }}"
                 name="delegate_id" value="{{ old('delegate_id', $delegate->user->delegate_id) }}" maxlength="255">
          <label for="delegate-delegate_id">@lang('users.lbl.delegate-id')</label>
          @if ($errors->has('delegate_id'))
            <div class="form-control-feedback">{{ $errors->first('delegate_id') }}</div>
          @endif
        </div>

      </div>

      <div class="col-12">
        <div class="mb-2 mt-2"><h3>@lang('delegates.user-event')</h3></div>

        <div class="row">
          <div class="col-12 col-sm-6 col-lg-3">
            <div class="md-form form-group{{ $errors->has('participant_id') ? ' has-danger' : '' }} ">
              <input id="delegate-participant_id" type="text"
                     class="form-control{{ $errors->has('participant_id') ? ' form-control-danger' : '' }}"
                     name="participant_id" value="{{ old('participant_id', $delegate->participant_id) }}" maxlength="10">
              <label for="delegate-participant_id">@lang('delegates.lbl.participant_id')</label>
              @if ($errors->has('participant_id'))
                <div class="form-control-feedback">{{ $errors->first('participant_id') }}</div>
              @endif
            </div>
          </div>

          <div class="col-12 col-sm-6 col-lg-3">
            <div class="md-form form-group{{ $errors->has('role') ? ' has-danger' : '' }} text-size-md">
              <select class="mdb-select" name="role"
                      id="delegate-role">
                @foreach(\App\User::allEventRoles() as $role)
                  <option value="{{ $role }}"
                          @if ($role == old('role', $delegate->role)) selected @endif>@lang('delegates.role.' . $role)</option>
                @endforeach
              </select>
              <label for="delegate-role" class="active">@lang('delegates.lbl.role')</label>
              @if ($errors->has('role'))
                <div class="form-control-feedback">{{ $errors->first('role') }}</div>
              @endif
            </div>
          </div>

          <div class="col-12 col-sm-6 ">
            <div class="form-inline md-form">
              <label class="active">@lang('delegates.lbl.rights'):</label>
              <fieldset class="form-group {{ $errors->has('right_submit') ? ' has-danger' : '' }} ">
                <input type="checkbox" id="delegate-right_submit" name="right_submit" value="1"
                       @if(old('right_submit', $delegate->right_submit)) checked @endif>
                <label for="delegate-right_submit">@lang('delegates.lbl.right_submit')</label>
                @if ($errors->has('right_submit'))
                  <div class="form-control-feedback">{{ $errors->first('right_submit') }}</div>
                @endif
              </fieldset>

              <fieldset class="form-group {{ $errors->has('right_speak') ? ' has-danger' : '' }} ">
                <input type="checkbox" id="delegate-right_speak" name="right_speak" value="1"
                       @if(old('right_speak', $delegate->right_speak)) checked @endif>
                <label for="delegate-right_speak">@lang('delegates.lbl.right_speak')</label>
                @if ($errors->has('right_speak'))
                  <div class="form-control-feedback">{{ $errors->first('right_speak') }}</div>
                @endif
              </fieldset>

              <fieldset class="form-group {{ $errors->has('right_vote') ? ' has-danger' : '' }} ">
                <input type="checkbox" id="delegate-right_vote" name="right_vote" value="1"
                       @if(old('right_vote', $delegate->right_vote)) checked @endif>
                <label for="delegate-right_vote">@lang('delegates.lbl.right_vote')</label>
                @if ($errors->has('right_vote'))
                  <div class="form-control-feedback">{{ $errors->first('right_vote') }}</div>
                @endif
              </fieldset>
            </div>
          </div>
        </div>
      </div>

      <div class="col-12 mt-2">
        <div class="text-center">
          <input type="hidden" name="client_id" value="{{ $event->client_id }}">
          <button class="btn btn-default" type="reset">@lang('app.btn.reset')</button>
          <button class="btn btn-amber" type="submit">@lang('app.btn.save')</button>
        </div>
      </div>

    </div>
  </form>
@endsection

@section('scripts')
  <script>
    $().ready(function() {
      $('.mdb-select').on('change', function (){
        if ($('option[value=guest]', this).is(':selected') || $('option[value=official]', this).is(':selected')) {
          $('#delegate-right_speak, #delegate-right_submit, #delegate-right_vote').prop('checked', false);
        }
      });
    });
  </script>
@endsection
