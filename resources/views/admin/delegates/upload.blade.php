@extends('layouts.app')

@section('breadcrumbs')
  <ol class="breadcrumb transparent">

    <li class="breadcrumb-item"><a href="{{ route('admin::events.index') }}">@lang('events.index')</a></li>
    <li class="breadcrumb-item">{{ $event->name }} @if($event->trashed())(deleted)@endif</li>
    <li class="breadcrumb-item"><a href="{{ route('admin::delegates.index', $event) }}">@lang('delegates.index')</a></li>
    <li class="breadcrumb-item active">@lang('delegates.nav.upload')</li>
  </ol>
@endsection

@section('content')
  <div class="row">
    <div class="col-12">

      @foreach($error as $error_item)
        <div class="text-danger">
          <p>{{ $error_item }}</p>
        </div>
      @endforeach

      @if($exists)
        <div class="text-danger">
          <p>@lang('delegates.msg.user-exists'):</p>
          <ul>
            @foreach($exists as $email)
              <li>{{ $email }}</li>
            @endforeach
          </ul>
        </div>
      @endif

      @if(count($created) || count($updated))
        <div>
          {{ trans_choice('delegates.msg.user-ok', count($created) + count($updated), ['count' => count($created) + count($updated)]) }}
        </div>
      @endif

      <div class="mt-2">
        <div class="text-center">
          <a href="{{ route('admin::delegates.index', $event) }}" class="btn btn-outline-success waves-effect"><i class="fa fa-undo left"></i> @lang('delegates.btn.back')</a>
        </div>
      </div>
    </div>
  </div>
@endsection
