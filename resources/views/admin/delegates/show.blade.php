@extends('layouts.app')

@section('breadcrumbs')
  <ol class="breadcrumb transparent">

    <li class="breadcrumb-item"><a href="{{ route('admin::events.index') }}">@lang('events.index')</a></li>
    <li class="breadcrumb-item">{{ $event->name }} @if($event->trashed())(deleted)@endif</li>
    <li class="breadcrumb-item"><a href="{{ route('admin::delegates.index', $event) }}">@lang('delegates.index')</a></li>
    <li class="breadcrumb-item active">{{ $delegate->user->name }}</li>
  </ol>
@endsection

@section('content')
  <div class="form-horizontal">
    <div class="row view-only-form">
      <div class="col-12 col-lg-6">
        <div class="mb-2"><h3>@lang('delegates.user-details')</h3></div>

        <div class="md-form form-group">
          <input id="delegate-first_name" type="text" class="form-control" readonly
                 value="{{ $delegate->user->name }}">
          <label for="delegate-first_name" class="active">@lang('users.lbl.name')</label>
        </div>

        <div class="md-form form-group">
          <input id="delegate-email" type="text" class="form-control" readonly
                 value="{{ $delegate->user->email }}">
          <label for="delegate-email" class="active">@lang('users.lbl.email')</label>
        </div>

        <div class="md-form form-group">
          <input id="delegate-user_token" type="text" class="form-control" readonly
                 value="{{ $delegate->user->api_token }}">
          <label for="delegate-user_token" class="active">@lang('users.lbl.user-token')</label>
        </div>
      </div>

      <div class="col-12 col-lg-6">
        <div class="mb-2"><h3>@lang('delegates.user-union')</h3></div>
        <div class="row">
          <div class="col-6">
            <div class="md-form form-group">
              <input id="delegate-union" type="text" class="form-control" readonly
                     value="{{ $delegate->user->union }}">
              <label for="delegate-union" class="active">@lang('users.lbl.union')</label>
            </div>
          </div>
          <div class="col-6">
            <div class="md-form form-group">
              <input id="delegate-branch" type="text" class="form-control" readonly
                     value="{{ $delegate->user->branch }}">
              <label for="delegate-branch" class="active">@lang('users.lbl.branch')</label>
            </div>
          </div>
        </div>

        <div class="md-form form-group">
          <input id="delegate-delegate_id" type="text" class="form-control" readonly
                 value="{{ $delegate->user->delegate_id }}">
          <label for="delegate-delegate_id" class="active">@lang('users.lbl.delegate-id')</label>
        </div>
      </div>

      <div class="col-12">
        <div class="mb-2 mt-2"><h3>@lang('delegates.user-event')</h3></div>

        <div class="row">
          <div class="col-12 col-sm-6 col-lg-3">
            <div class="md-form form-group">
              <input id="delegate-participant_id" type="text" class="form-control" readonly
                     value="{{ $delegate->participant_id }}">
              <label for="delegate-participant_id" class="active">@lang('delegates.lbl.participant_id')</label>
            </div>
          </div>

          <div class="col-12 col-sm-6 col-lg-3">
            <div class="md-form form-group text-size-md">
              <input id="delegate-role" type="text" class="form-control" readonly
                     value="@lang('delegates.role.' . $delegate->role)">
              <label for="delegate-role" class="active">@lang('delegates.lbl.role')</label>
            </div>
          </div>

          <div class="col-12 col-sm-6">
            <div class="form-inline md-form">
              <label class="active">@lang('delegates.lbl.rights'):</label>
              <fieldset class="form-group">
                <input type="checkbox" id="delegate-right_submit" @if($delegate->right_submit) checked @endif disabled>
                <label for="delegate-right_submit">@lang('delegates.lbl.right_submit')</label>
              </fieldset>

              <fieldset class="form-group">
                <input type="checkbox" id="delegate-right_speak" @if($delegate->right_speak) checked @endif disabled>
                <label for="delegate-right_speak">@lang('delegates.lbl.right_speak')</label>
              </fieldset>

              <fieldset class="form-group">
                <input type="checkbox" id="delegate-right_vote" @if($delegate->right_vote) checked @endif disabled>
                <label for="delegate-right_vote">@lang('delegates.lbl.right_vote')</label>
              </fieldset>
            </div>
          </div>
        </div>
      </div>

      <div class="col-12 mt-2">
        <div class="text-center">
          <a href="{{ route('admin::delegates.index', $event) }}" class="btn btn-outline-success waves-effect"><i class="fa fa-undo left"></i> @lang('delegates.btn.back')</a>
        </div>
      </div>

    </div>
  </div>
@endsection
