@extends('layouts.app')

@section('breadcrumbs')
  <ol class="breadcrumb transparent">

    <li class="breadcrumb-item"><a href="{{ route('admin::events.index') }}">@lang('events.index')</a></li>
    <li class="breadcrumb-item">{{ $event->name }} @if($event->trashed())(deleted)@endif</li>
    <li class="breadcrumb-item active">@lang('delegates.index')</li>
  </ol>
@endsection

@section('content')
  <div class="row">
    <div class="col-12">
      <table class="table table-hover" id="data-table" style="table-layout: fixed">
        <thead>
        <tr>
          <th style="width:78px">@lang('delegates.lbl.id')</th>
          <th>@lang('delegates.lbl.name')</th>
          <th style="width:22%">@lang('delegates.lbl.union')</th>
          <th style="width:18%;">@lang('delegates.lbl.branch')</th>
          <th style="width:18%;">@lang('delegates.lbl.role')</th>
          <th style="width:10%"></th>
        </tr>
        </thead>
        <tbody>
        @foreach($delegates as $delegate)
          <tr @if($loop->iteration >= $pageItems) class="datatable-hidden-tr" @endif >
            <th scope="row">{{ $delegate->participant_id }}</th>
            <td data-order="{{ $delegate->user->last_name}} {{ $delegate->user->first_name}}">{{ $delegate->user->name }}</td>
            <td data-order="{{ $delegate->user->union_sort_key }}">{{ $delegate->user->union }}</td>
            <td data-order="{{ $delegate->user->branch_sort_key }}">{{ $delegate->user->branch }}</td>
            <td>{{ $delegate->role }}</td>
            <td class="dropnone text-center">
              <a href="#" id="dropdownMenuDelegate{{ $delegate->id }}"
                 data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-ellipsis-v"></i>
              </a>
              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuDelegate{{ $delegate->id }}"
                   data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">

                <a href="{{ route('admin::delegates.show', $delegate) }}" class="dropdown-item">@lang('delegates.nav.view')</a>

                @can('manage-delegates', $event)
                  <div class="dropdown-divider"></div>

                  <a href="{{ route('admin::delegates.edit', $delegate) }}" class="dropdown-item">@lang('delegates.nav.edit')</a>

                  <a href="#" class="dropdown-item" v-on:click="showModal" data-modal-target="modal"
                     data-modal-title="@lang('delegates.modal.ttl-delete')"
                     data-modal-body="@lang('delegates.modal.txt-delete', ['name' => $delegate->user->name,  'email' => $delegate->user->email])"
                     data-ok-func="objTrash" data-ok-args='{"url": "{{ route('admin::delegates.destroy', $delegate) }}", "method": "delete"}'
                     data-checkbox="@lang('delegates.modal.chkbx-delete')">
                    @lang('delegates.nav.delete')
                  </a>
                @endcan
              </div>
            </td>
          </tr>
        @endforeach
        </tbody>
      </table>
    </div>
  </div>

  @can('manage-delegates', $event)
    <div class="float-right dropnone">
      <a class="mdl-button mdl-button--fab mdl-button--colored amber darken-4" href="#"
         id="dropdownMenuDelegatesMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="fa fa-plus material-icons"></i>
      </a>
      <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuDelegatesMenu"
           data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">

        <a href="{{ route('admin::delegates.create', $event) }}" class="dropdown-item">@lang('delegates.nav.add')</a>

        <a href="#" class="dropdown-item" v-on:click="showModal" data-modal-target="modalDelegatesUpload"
           data-url="{{ route('admin::delegates.upload', $event) }}">@lang('delegates.nav.upload')</a>
      </div>
    </div>

    <div class="float-left mt-1">
      <a href="#" class="btn btn-outline-warning waves-effect"
         v-on:click="showModal" data-modal-target="modal" data-modal-title="@lang('delegates.modal.ttl-bulk-delete')"
         data-modal-body="@lang('delegates.modal.txt-bulk-delete')"
         data-ok-func="objTrash" data-ok-args='{"url": "{{ route('admin::delegates.bulk-destroy', [$event]) }}", "method": "delete"}'
      >@lang('delegates.nav.delete-all')</a>
    </div>

    <div is="modalDelegatesUpload" v-bind:data="modalDelegatesUpload" v-on:modal-shown="shownModal"></div>
  @endcan
@endsection

@section('scripts')
  <script>
    $().ready(function() {
      $('#data-table').DataTable({
        columnDefs: [{sortable: false, targets: [-1]}, {searchable: false, targets: [-1]}],
        paging: {{ (count($delegates) > $pageItems) ? 'true' : 'false' }},
      });
    });
  </script>
@endsection
