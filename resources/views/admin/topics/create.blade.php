@extends('layouts.app')

@section('breadcrumbs')
  <ol class="breadcrumb transparent">

    <li class="breadcrumb-item"><a href="{{ route('admin::events.index') }}">@lang('events.index')</a></li>
    <li class="breadcrumb-item">{{ $event->name }}</li>
    <li class="breadcrumb-item"><a href="{{ route('admin::topics.index', $event) }}">@lang('topics.index')</a></li>
    <li class="breadcrumb-item active">@lang('topics.create')</li>
  </ol>
@endsection

@section('content')
  <form class="form-horizontal" role="form" method="POST" action="{{ route('admin::topics.store', $headline) }}">
    <div class="row">
      {{ csrf_field() }}

      <div class="col-12 col-lg-6">
        <div class="mb-2"><h3>@lang('topics.details')</h3></div>

        <div class="md-form form-group{{ $errors->has('name') ? ' has-danger' : '' }} ">
          <input id="topic-name" type="text" class="form-control{{ $errors->has('name') ? ' form-control-danger' : '' }}"
                 name="name" value="{{ old('name') }}" required autofocus maxlength="255">
          <label for="topic-name">@lang('topics.lbl.name')</label>
          @if ($errors->has('name'))
            <div class="form-control-feedback">{{ $errors->first('name') }}</div>
          @endif
        </div>

        <div class="md-form form-group{{ $errors->has('description') ? ' has-danger' : '' }}">
          <textarea class="md-textarea form-control{{ $errors->has('description') ? ' form-control-danger' : '' }}"
                    type="text" id="topic-description" name="description" style="height: 4.2rem"
                    maxlength="255" length="255">{{ old('description') }}</textarea>
          <label for="topic-description">@lang('topics.lbl.description')</label>
          @if ($errors->has('description'))
            <div class="form-control-feedback">{{ $errors->first('description') }}</div>
          @endif
        </div>
      </div>

      <div class="col-12 col-lg-6">
        <div class="mb-2"><h3>@lang('topics.settings')</h3></div>

        <div class="md-form form-group{{ $errors->has('speech_times') ? ' has-danger' : '' }} ">
          <input id="topic-speech_times" type="number" min="{{ $speech_min }}" max="{{ $speech_max }}"
                 class="form-control{{ $errors->has('speech_times') ? ' form-control-danger' : '' }}"
                 name="speech_times" value="{{ old('speech_times', $speech_max) }}" required maxlength="3">
          <label for="topic-speech_times">@lang('topics.lbl.speech_times')</label>
          @if ($errors->has('speech_times'))
            <div class="form-control-feedback">{{ $errors->first('speech_times') }}</div>
          @endif
        </div>

        <div class="row">
          <div class="col-12">
            <h6 class="mb-2">@lang('topics.lbl.speech_length'):</h6>
          </div>
          @for($i = $speech_min; $i <= $speech_max; $i++)
            <div class="col md-form js-topic-speech-length" @if($i > old('speech_times', $speech_max)) style="display: none" @endif >
              <div class="form-group{{ $errors->has('speech_length.' . $i) ? ' has-danger' : '' }}">
                <input id="topic-speech_length_{{ $i }}" type="number" min="1" max="60"
                       class="form-control{{ $errors->has('speech_length.' . $i) ? ' form-control-danger' : '' }}"
                       name="speech_length[{{ $i }}]" value="{{ old('speech_length.' . $i, $speech_default) }}"
                       required maxlength="2">
                <label for="topic-speech_length_{{ $i }}" class="pl-3">{{ trans_choice('topics.lbl.speech_length_i', $i) }}</label>
                @if ($errors->has('speech_length.' . $i))
                  <div class="form-control-feedback">{{ $errors->first('speech_length.' . $i) }}</div>
                @endif
              </div>
            </div>
          @endfor
        </div>

        <div class="md-form">
          <fieldset class="form-group {{ $errors->has('speech_opened') ? ' has-danger' : '' }} ">
            <input type="checkbox" id="topic-speech_opened" name="speech_opened" value="1"
                   checked>
            <label for="topic-speech_opened">@lang('topics.lbl.speech_opened')</label>
            @if ($errors->has('speech_opened'))
              <div class="form-control-feedback">{{ $errors->first('speech_opened') }}</div>
            @endif
          </fieldset>
        </div>
      </div>

      <div class="col-12 mt-1">
        <div class="text-center">
          <button class="btn btn-default" type="reset">@lang('app.btn.reset')</button>
          <button class="btn btn-amber" type="submit">@lang('app.btn.save')</button>
        </div>
      </div>

    </div>
  </form>
@endsection

@section('scripts')
  <script>
    $().ready(function() {
      $('#topic-speech_times').on('change', function (e)  {
        $('.js-topic-speech-length').hide();
        $('.js-topic-speech-length input').prop('required', false);
        for(i = 1; i <= $(this).val(); i++) {
          $('#topic-speech_length_' + i).prop('required', true);
          $('#topic-speech_length_' + i).parents('.js-topic-speech-length').show();
        }
      });
    });
  </script>
@endsection

