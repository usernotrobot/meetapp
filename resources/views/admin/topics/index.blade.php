@extends('layouts.app')

@section('breadcrumbs')
  <ol class="breadcrumb transparent">

    <li class="breadcrumb-item"><a href="{{ route('admin::events.index') }}">@lang('events.index')</a></li>
    <li class="breadcrumb-item">{{ $event->name }} @if($event->trashed())(deleted)@endif</li>
    <li class="breadcrumb-item active">@lang('topics.index')</li>
  </ol>
@endsection

@php ($openStyles = "")
@php ($discussionStyles = "amber lighten-4")
@php ($votingStyles = "red lighten-4")
@php ($closedStyles = "green lighten-4")
@php ($deletedStyles = "grey lighten-2")

@section('content')
  <div class="row">
    @forelse($topics as $topic)
      <div class="col-6 col-md-4">
        <div class="card @if($topic->trashed()) {{ $deletedStyles }} @else {{ ${$topic->status . 'Styles'} }} @endif"
             data-obj="{{ $topic->id }}" data-change-status-url="{{ route('admin::topics.status', $topic) }}">
          <div class="card-block">
            {{-- Name --}}
            <h4 class="card-title text-ellipsis">{{ $topic->name }}</h4>
            {{-- Description --}}
            <p class="card-text text-muted text-size-sm">{{ $topic->description }}</p>
            {{-- Suggestions --}}
            <p class="card-text text-size-sm"><strong>@php($s = $topic->suggestionsSummary())
                {{ trans_choice('topics.lbl.suggestions', $s[0], ['suggestions' => $s[0]]) }}
                {{ trans_choice('topics.lbl.from-delegates', $s[1], ['delegates' => $s[1]]) }}</strong></p>
            {{-- Footer --}}
            <div class="card-footer transparent pr-0 pl-0 text-center dropnone text-size-md">
              {{-- Links --}}
              @can('manage-topics', $event)
                @if($topic->trashed())
                  <a href="#" class="card-link" v-on:click="showModal" data-modal-target="modal" data-modal-title="@lang('topics.modal.ttl-restore')"
                     data-modal-body="@lang('topics.modal.txt-restore', ['name' => $topic->name])" data-ok-func="objTrash"
                     data-ok-args='{"url": "{{ route('admin::topics.restore', $topic) }}", "method": "post"}'>
                    @lang('topics.nav.restore')
                  </a>
                @else
                  <a href="{{ route('admin::topics.edit', $topic) }}" class="card-link">@lang('topics.nav.edit')</a>
                @endif
              @endcan
              <a href="{{ route('admin::suggestions.index', $topic) }}" class="card-link">@lang('topics.nav.suggestion')</a>
              <a href="{{ route('admin::topics.files', $topic) }}" class="card-link">@lang('topics.nav.files')</a>

              @can('manage-topics', $event)
                @if(!$topic->trashed())
                  {{-- Dropdown --}}
                  <a href="#" class="card-link" id="dropdownMenuTopic{{ $topic->id }}"
                     data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-ellipsis-v"></i>
                  </a>
                  <div class="dropdown-menu dropdown-menu-right dropdown-menu-up" aria-labelledby="dropdownMenuTopic{{ $topic->id }}"
                       data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">
                    @if($topic->open())
                      <a href="#" class="dropdown-item" v-on:click="showModal" data-modal-target="modal" data-modal-title="@lang('topics.modal.ttl-change')"
                         data-modal-body="@lang('topics.modal.txt-change', ['name' => $topic->name, 'status' => trans('topics.status.discussion')])"
                         data-ok-func="objMarkAs" data-ok-args='{"obj": {{ $topic->id }}, "status": "discussion"}'>
                        @lang('topics.nav.discussion')
                      </a>
                    {{--@elseif($topic->discussion())
                      <a href="#" class="dropdown-item" v-on:click="showModal" data-modal-target="modal" data-modal-title="@lang('topics.modal.ttl-change')"
                         data-modal-body="@lang('topics.modal.txt-change', ['name' => $topic->name, 'status' => trans('topics.status.voting')])"
                         data-ok-func="objMarkAs" data-ok-args='{"obj": {{ $topic->id }}, "status": "voting"}'>
                        @lang('topics.nav.voting')
                      </a>--}}
                    @elseif($topic->voting())
                      <a href="#" class="dropdown-item" v-on:click="showModal" data-modal-target="modal" data-modal-title="@lang('topics.modal.ttl-change')"
                         data-modal-body="@lang('topics.modal.txt-change', ['name' => $topic->name, 'status' => trans('topics.status.closed')])"
                         data-ok-func="objMarkAs" data-ok-args='{"obj": {{ $topic->id }}, "status": "closed"}'>
                        @lang('topics.nav.closed')
                      </a>
                    @endif
                    <a href="#" class="dropdown-item" v-on:click="showModal" data-modal-target="modal" data-modal-title="@lang('topics.modal.ttl-delete')"
                       data-modal-body="@lang('topics.modal.txt-delete', ['name' => $topic->name])"
                       data-ok-func="objTrash" data-ok-args='{"url": "{{ route('admin::topics.destroy', $topic) }}", "method": "delete"}'>
                      @lang('topics.nav.delete')
                    </a>
                  </div>
                @endif
              @endcan
            </div>
          </div>
        </div>
        <br/>
      </div>
    @empty
      <div class="col-12">@lang('topics.empty')</div>
    @endforelse
  </div>

  @can('manage-topics', $event)
    <a class="float-right mdl-button mdl-button--fab mdl-button--colored amber darken-4" href="{{ route('admin::topics.create', $event) }}"><i class="fa fa-plus material-icons"></i></a>
  @endcan

  {{ $topics->links() }}
@endsection
