@extends('layouts.app')

@section('breadcrumbs')
  <ol class="breadcrumb transparent">

    <li class="breadcrumb-item"><a href="{{ route('admin::events.index') }}">@lang('events.index')</a></li>
    <li class="breadcrumb-item">{{ $event->name }} @if($event->trashed())(deleted)@endif</li>
    <li class="breadcrumb-item"><a href="{{ route('admin::topics.index', $event) }}">@lang('topics.index')</a></li>
    <li class="breadcrumb-item">{{ $topic->name }} @if($topic->trashed())(deleted)@endif</li>
    <li class="breadcrumb-item active">@lang('suggestions.index')</li>
  </ol>
@endsection

@section('content')
  <div class="row">
    <div class="col-12">
      <table class="table table-hover" id="data-table" style="table-layout: fixed">
        <thead>
        <tr>
          <th style="width:58px">@lang('suggestions.lbl.id')</th>
          <th>@lang('suggestions.lbl.suggestion')</th>
          <th style="width:22%">@lang('suggestions.lbl.author')</th>
          <th style="width:18%;">@lang('suggestions.lbl.when')</th>
          <th style="width:10%"></th>
        </tr>
        </thead>
        <tbody>
        @foreach($suggestions as $suggestion)
          <tr class="@if($loop->iteration >= $pageItems) datatable-hidden-tr @endif">
            <th scope="row" width="30">{{ $loop->iteration }}</th>
            <td class="text-ellipsis" title="{{ $suggestion->summary }}">{{ $suggestion->summary }}</td>
            <td>{{ $suggestion->user->name }}</td>
            <td class="text-nowrap" data-order="{{ $suggestion->created_at->format('U') }}">{{ $suggestion->created_at->format('H:i, d M Y') }}</td>
            <td class="dropnone text-center">
              <a href="#" id="dropdownMenuSuggestion{{ $suggestion->id }}"
                 data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-ellipsis-v"></i>
              </a>
              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuSuggestion{{ $suggestion->id }}"
                   data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">
                <a href="{{ route('admin::suggestions.show', [$topic, $suggestion]) }}" class="dropdown-item">@lang('suggestions.nav.view')</a>
              </div>
            </td>

          </tr>
        @endforeach
        </tbody>
      </table>
    </div>
  </div>
@endsection

@section('scripts')
  <script>
    $().ready(function() {
      $('#data-table').DataTable({
        paging: {{ (count($suggestions) >= $pageItems) ? 'true' : 'false' }},
      });
    });
  </script>
@endsection

