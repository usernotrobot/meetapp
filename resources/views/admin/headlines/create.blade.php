@extends('layouts.app')

@section('breadcrumbs')
  <ol class="breadcrumb transparent">

    <li class="breadcrumb-item"><a href="{{ route('admin::events.index') }}">@lang('events.index')</a></li>
    <li class="breadcrumb-item">{{ $event->name }}</li>
    <li class="breadcrumb-item"><a href="{{ route('admin::headlines.index', $event) }}">@lang('headlines.index')</a></li>
    <li class="breadcrumb-item active">@lang('headlines.create')</li>
  </ol>
@endsection

@section('content')
  <form class="form-horizontal" role="form" method="POST" action="{{ route('admin::headlines.store', $event) }}">
    <div class="row">
      {{ csrf_field() }}

      <div class="col-12 col-lg-6">
        <div class="mb-2"><h3>@lang('headlines.details')</h3></div>

        <div class="md-form form-group{{ $errors->has('name') ? ' has-danger' : '' }} ">
          <input id="headline-name" type="text" class="form-control{{ $errors->has('name') ? ' form-control-danger' : '' }}"
                 name="name" value="{{ old('name') }}" required autofocus maxlength="255">
          <label for="headline-name">@lang('headlines.lbl.name')</label>
          @if ($errors->has('name'))
            <div class="form-control-feedback">{{ $errors->first('name') }}</div>
          @endif
        </div>

        <div class="md-form form-group{{ $errors->has('description') ? ' has-danger' : '' }}">
          <textarea class="md-textarea form-control{{ $errors->has('description') ? ' form-control-danger' : '' }}"
                    type="text" id="headline-description" name="description" style="height: 4.2rem"
                    maxlength="255" length="255">{{ old('description') }}</textarea>
          <label for="headline-description">@lang('headlines.lbl.description')</label>
          @if ($errors->has('description'))
            <div class="form-control-feedback">{{ $errors->first('description') }}</div>
          @endif
        </div>
      </div>

      <div class="col-12 mt-1">
        <div class="text-center">
          <button class="btn btn-default" type="reset">@lang('app.btn.reset')</button>
          <button class="btn btn-amber" type="submit">@lang('app.btn.save')</button>
        </div>
      </div>

    </div>
  </form>
@endsection

@section('scripts')
  <script>
    $().ready(function() {
      $('#headline-speech_times').on('change', function (e)  {
        $('.js-headline-speech-length').hide();
        $('.js-headline-speech-length input').prop('required', false);
        for(i = 1; i <= $(this).val(); i++) {
          $('#headline-speech_length_' + i).prop('required', true);
          $('#headline-speech_length_' + i).parents('.js-headline-speech-length').show();
        }
      });
    });
  </script>
@endsection

