@extends('layouts.app')

@push('styles')
<link href="{{ mix('css/admin.css') }}" rel="stylesheet">
@endpush

@section('breadcrumbs')
  <ol class="breadcrumb transparent">
    <li class="breadcrumb-item"><a href="{{ route('admin::events.index') }}">@lang('events.index')</a></li>
    <li class="breadcrumb-item">{{ $event->name }} @if($event->trashed())(deleted)@endif</li>
    <li class="breadcrumb-item active">Headline topics</li>
  </ol>
@endsection

@php ($openStyles = "")
@php ($discussionStyles = "amber lighten-4")
@php ($votingStyles = "red lighten-4")
@php ($closedStyles = "green lighten-4")
@php ($deletedStyles = "grey lighten-2")

@section('content')
<div class="h-topics" v-cloak>
  <div class="h-topics_table">
    <div class="h-topics_table-row thead-inverse">
      <div class="h-topics_table-td w60 text-center"><i class="fa fa-arrows-v" aria-hidden="true"></i></div>
      <div class="h-topics_table-td">@lang('headlines.tbl.first')</div>
      <div class="h-topics_table-td">@lang('headlines.tbl.second')</div>
      <div class="h-topics_table-td">@lang('headlines.tbl.third')</div>
      <div class="h-topics_table-td w130">&nbsp;</div>
    </div>
    <draggable :list="headlineTopics" :options="{handle: '.fa-bars', ghostClass: 'sortable-ghost', chosenClass: 'sortable-chosen', draggable:'.draggable'}" class="h-topics_table-body" @end="onMoveEnd">
      <div class="h-topics_table-row draggable"
           v-for="(item, index) in headlineTopics"
           v-on:click="navigateToTopics(item, $event)"
           :key="item.position"
           track-by="position">
        <div class="h-topics_table-td w60 has-buttons">
          <span class="h-topics_table-handle"><i class="fa fa-bars" aria-hidden="true"></i></span>
        </div>
        <div class="h-topics_table-td">@{{item.name}}</div>
        <div class="h-topics_table-td"><nobr>@{{item.topics}}</nobr></div>
        <div class="h-topics_table-td"><nobr>@{{item.last_updated}} @{{item.updated_at}}</nobr></div>
        <div class="h-topics_table-td text-right w130 has-buttons">
          <a href="#" v-if="item.canDelete" class="h-topics_table-btn"
            title="@lang('headlines.modal.ttl-delete')"
            v-on:click="showDeleteModal(item, index)">
            <i class="fa fa-trash" aria-hidden="true"></i>
          </a>
          <a href="#" class="h-topics_table-btn"
             v-on:click="showNewTopicModal(item, index)"
             title="@lang('headlines.nav.new-topic')">
             <i class="fa fa-plus-square" aria-hidden="true"></i>
          </a>
          <a class="h-topics_table-btn"
             title="@lang('headlines.nav.edit-headline')"
             :href="getHref('{{route('admin::headlines.edit','#')}}', item.id)">
             <i class="fa fa-pencil" aria-hidden="true"></i>
          </a>
        </div>
      </div>
    </draggable>
  </div>
  <div class="h-topics_table-empty" v-if="headlineTopics.length === 0" >
    @lang('headlines.empty')
  </div>
</div>

<div class="fixed-btn">
  <a href="{{ route('admin::headlines.create', $event) }}" class="btn-floating btn-large amber darken-4 waves-effect waves-light"><i class="fa fa-plus material-icons"></i></a>
</div>

<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <!--Content-->
    <div class="modal-content">
      <!--Header-->
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h5 class="modal-title"> @lang('headlines.modal.ttl-delete') </h5>
      </div>
      <!--Body-->
      <div class="modal-body">@{{modalText}}</div>
      <!--Footer-->
      <div class="modal-footer">
        <button type="button" class="btn-flat btn-sm dark-amber-text waves-effect" data-dismiss="modal">@lang('app.btn.cancel')</button>
        <button type="submit" class="btn-flat btn-sm waves-effect" @click="confirmDelete">@lang('app.btn.accept')</button>
      </div>
   </div>
   <!--/.Content-->
 </div>
</div>

<div class="modal fade" id="modalTopicNew" tabindex="-1" role="dialog" aria-labelledby="modalTopicNew" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title w-100" id="modalTopicNew">@lang('topics.modal.ttl-create')</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="newTopicForm">
          <div class="md-form">
            <input type="text" id="topicname" class="form-control" required minlength="1"
                   name="name" v-model="topicName">
            <label for="topicname">@lang('topics.lbl.name')</label>
          </div>
          <div class="md-form">
            <textarea type="text" id="topicdescription" class="md-textarea" name="description" v-model="topicDescription" length="255" maxlength="255"></textarea>
            <label for="topicdescription">@lang('topics.lbl.description')</label>
          </div>
          <!-- Defaults -->
          <input type="hidden" name="speech_times" value="3">
          <input type="hidden" name="speech_length_default" value="300">
          <input type="hidden" name="speech_opened" value="1">
          <input type="hidden" name="speech_length[1]" value="5">
          <input type="hidden" name="speech_length[2]" value="5">
          <input type="hidden" name="speech_length[3]" value="5">

          <div class="text-right">
            <button type="button" class="btn-flat btn-sm dark-amber-text waves-effect" data-dismiss="modal">@lang('app.btn.cancel')</button>
            <button type="button" :disabled="topicName.length<1 || topicAjaxProgress" @click="submitNewTopic" class="btn-flat btn-sm waves-effect">@lang('messages.mdl.confirm-btn-ok')</button>
          </div>
        </form>

      </div>
    </div>
  </div>
</div>
@endsection

@section('vuejs')
  <script>
    let deleteRoute = "{!! route('admin::headlines.destroy','') !!}";
    let moveRoute = "{!! route('admin::headlines.position', $event) !!}";
    let topicCreateRoute = "{!! route('admin::topics.store', '#') !!}";
    let topicsShowRoute = "{!! route('admin::topics.headline', '#') !!}";
    let headlineTopics = {!! $headlines !!};
    // console.log(headlineTopics);
  </script>
  <script src="{{ mix('js/headline-topics.js') }}"></script>
@endsection