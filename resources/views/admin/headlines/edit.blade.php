@extends('layouts.app')

@push('styles')
<link href="{{ mix('css/admin.css') }}" rel="stylesheet">
@endpush

@section('breadcrumbs')
  <ol class="breadcrumb transparent">
    <li class="breadcrumb-item"><a href="{{ route('admin::events.index') }}">@lang('events.index')</a></li>
    <li class="breadcrumb-item">{{ $event->name }} @if($event->trashed())(deleted)@endif</li>
    <li class="breadcrumb-item"><a href="{{ route('admin::headlines.index', $event) }}">@lang('headlines.index')</a></li>
    <li class="breadcrumb-item">{{ $headline->name }} @if($headline->trashed())(deleted)@endif</li>
    <li class="breadcrumb-item active">@lang('headlines.edit')</li>
  </ol>
@endsection

@section('content')
  <div class="h-edit">

    <div class="h-edit-head">
      <a href="{{ route('admin::headlines.index', $event) }}" class="h-edit-btn is-small"><< @lang('headlines.nav.back')</a>
      <button class="h-edit-btn is-grey" type="submit" form="h-edit-form">@lang('app.btn.save')</button>
    </div>

    <div class="h-edit-body">

      <!-- CONTENT -->
      <form class="h-edit-body_content js-h-edit-form" role="form" method="POST" id="h-edit-form"
            action="{{ route('admin::headlines.update', $headline) }}">
        {{ csrf_field() }}
        {{ method_field('PUT') }}
        <div class="md-form{{ $errors->has('name') ? ' has-danger' : '' }}">
          <input type="text" id="name" name="name" class="form-control{{ $errors->has('name') ? ' form-control-danger' : '' }}" value="{{ old('name', $headline->name) }}" maxlength="255">
          <label for="name">@lang('headlines.lbl.name')</label>
          @if ($errors->has('name'))
            <div class="form-control-feedback">{{ $errors->first('name') }}</div>
          @endif
        </div>
        <div class="md-form{{ $errors->has('description') ? ' has-danger' : '' }}">
          <textarea type="text" name="description" id="description" class="md-textarea" length="255" maxlength="255">{{ old('description', $headline->description) }}</textarea>
          <label for="description">@lang('headlines.lbl.description')</label>
          @if ($errors->has('description'))
            <div class="form-control-feedback">{{ $errors->first('description') }}</div>
          @endif
        </div>
        <div class="md-form">
          <label class="active">@lang('headlines.lbl.subtopic')</label>
          <span class="h-edit-new-topic" v-on:click="showNewTopicModal">
            @lang('headlines.nav.new-topic')
            <i class="fa fa-plus" aria-hidden="true"></i>
          </span>
          <!-- CARDS -->
          <div class="h-edit-cards">
            @forelse($headline->topics as $topic)
              <!-- TODO: make this Vue -->
              <!-- <div class="h-edit-cards_box js-topic-card" v-for="topicsArr"> -->
              <div class="h-edit-cards_box js-topic-card">
                <div class="h-edit-card">
                  <div class="h-edit-card_head">
                    <div class="form-group">
                      <input value="null" type="checkbox" name="subtopic[{{ $topic->id }}]" class="js-checkbox" id="topic-{{ $topic->id }}">
                      <label for="topic-{{ $topic->id }}">{{ $topic->name }}</label>
                    </div>
                    {{ $topic->description }}
                  </div>
                  <div class="h-edit-card_body">
                    @php($s = $topic->suggestionsSummary())
                    {{ trans_choice('topics.lbl.suggestions', $s[0], ['suggestions' => $s[0]]) }}
                  </div>
                  <div class="h-edit-card_footer">
                    <a href="#" class="h-edit-btn">@lang('headlines.nav.delete')</a>
                    <a href="#" class="h-edit-btn is-orange">@lang('headlines.nav.edit')</a>
                  </div>
                </div>
              </div>
            @empty
              <div>
                <div class="h-edit-cards_box">
                    <h4>@lang('topics.empty')</h4>
                </div>
              </div>
            @endforelse
          </div>

        </div>
      </form>

      <!-- ASIDE -->
      <form role="form" method="POST" enctype="multipart/form-data" class="h-edit-body_aside" novalidate>
        {{ csrf_field() }}
        <div class="h-edit-aside_head">
          @lang('headlines.lbl.files')
        </div>
        <label class="h-edit-aside_drop js-drop-zone" v-cloak>
          <input type="file" id="file" :name="uploadFieldName"
            data-rule-extension="" accept=".pdf,.doc,.docx,.xls,.xlsx" multiple
            @change="filesChange($event)">
            <span v-if="isInitial">@lang('headlines.lbl.droppable')</span>
            <span v-if="isSaving">@lang('files.msg.upload-progress')</span>
        </label>
        <transition-group name="files-list" tag="ul" class="h-edit-aside_list" v-cloak>
          <li :data-id="index" v-for="(file, index) in filesArr" v-bind:key="file.id">
            <a :href="deleteFileRoute+'/'+file.id" target="_blank">@{{file.name}}</a>
            <i @click="showRemoveModal(file, index)" aria-hidden="true" class="fa fa-times"></i>
          </li>
        </transition-group>
      </form>
    </div>
  </div>

  <div class="h-edit-dialog is-hidden js-move-modal">
    <div class="h-edit-dialog_body">
      <div class="h-edit-dialog_select">
        @lang('headlines.lbl.move-topics')
        <select class="js-headline-select">
          <option value="" selected>-- Choose a destination Headline --</option>
          @foreach($event->headlines as $event_headlines)
            @if( $event_headlines->id != $headline->id )
              <option value="{{ $event_headlines->id }}">{{ $event_headlines->name }}</option>
            @endif
          @endforeach
        </select>
      </div>
      <div>
        <button class="h-edit-btn js-mm-cancel">@lang('app.btn.cancel')</button>
        <button disabled class="h-edit-btn js-mm-submit">@lang('app.btn.accept')</button>
      </div>
    </div>
  </div>

  <div class="modal fade" id="confirmDelete" tabindex="-1" role="dialog" aria-labelledby="confirmDeleteModal" aria-hidden="true">
      <div class="modal-dialog" role="document">
          <div class="modal-content">
              <div class="modal-header">
                  <h4 class="modal-title w-100" id="confirmDeleteModal">@lang('files.modal.ttl-delete')</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>
              <div class="modal-body">
                @{{modalText}}
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn-flat btn-sm dark-amber-text waves-effect" data-dismiss="modal">@lang('app.btn.cancel')</button>
                  <button type="button" @click="okBtn" class="btn-flat btn-sm waves-effect">@lang('messages.mdl.confirm-btn-ok')</button>
              </div>
          </div>
      </div>
  </div>

  <div class="modal fade" id="modalTopicNew" tabindex="-1" role="dialog" aria-labelledby="modalTopicNew" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title w-100" id="modalTopicNew">@lang('topics.modal.ttl-create')</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form id="newTopicForm">
            <div class="md-form">
              <input type="text" id="topicname" class="form-control" required minlength="1"
                     name="name" v-model="topicName">
              <label for="topicname">@lang('topics.lbl.name')</label>
            </div>
            <div class="md-form">
              <textarea type="text" id="topicdescription" class="md-textarea" name="description" v-model="topicDescription" length="255" maxlength="255"></textarea>
              <label for="topicdescription">@lang('topics.lbl.description')</label>
            </div>
            <!-- Defaults -->
            <input type="hidden" name="speech_times" value="3">
            <input type="hidden" name="speech_length_default" value="300">
            <input type="hidden" name="speech_opened" value="1">
            <input type="hidden" name="speech_length[1]" value="5">
            <input type="hidden" name="speech_length[2]" value="5">
            <input type="hidden" name="speech_length[3]" value="5">

            <div class="text-right">
              <button type="button" class="btn-flat btn-sm dark-amber-text waves-effect" data-dismiss="modal">@lang('app.btn.cancel')</button>
              <button type="button" :disabled="topicName.length<1 || topicAjaxProgress" @click="submitNewTopic" class="btn-flat btn-sm waves-effect">@lang('messages.mdl.confirm-btn-ok')</button>
            </div>
          </form>

        </div>
      </div>
    </div>
  </div>


@endsection

@section('scripts')
@endsection
@section('vuejs')
  <script>
      let filesArr = {!! $headline->files !!};
      let topicsArr = {!! $headline->topics !!};
      let deleteFileRoute = "{!! route('files.destroy', '') !!}";
      let uploadRoute = "{!! route('admin::headlines.massupload', $headline) !!}";
      let topicCreateRoute = "{!! route('admin::topics.store', $headline) !!}";
      let topicDeleteRoute = "{!! route('admin::topics.destroy', '#') !!}";
      let filesExt = "pdf|doc|docx|xls|xlsx";
  </script>
  <script src="{{ mix('js/headline-edit.js') }}"></script>
@endsection