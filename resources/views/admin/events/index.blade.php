@extends('layouts.app')

@push('styles')
<link href="{{ mix('css/admin.css') }}" rel="stylesheet">
@endpush

@section('breadcrumbs')
  <ol class="breadcrumb transparent">

    <li class="breadcrumb-item active">@lang('events.index')</li>
  </ol>
@endsection

@php ($plannedStyles = "is-yellow")
@php ($activeStyles = "is-green")
@php ($completedStyles = "")
@php ($deletedStyles = "grey lighten-2")

@section('content')
  <div class="event-cards">
    @forelse($events as $event)

      <div class="ecard @if($event->trashed()) {{ $deletedStyles }} @else {{ ${$event->status . 'Styles'} }} @endif"
          data-obj="{{ $event->id }}" data-change-status-url="{{ route('admin::events.status', $event) }}">
          <h4 class="ecard-title">{{ $event->name }}</h4>
          <!--/.eCard content-->
          <div class="ecard-headlines">
            <div class="ecard-headlines_header">
              @lang('events.headlines-header')
              <a href="{{ route('admin::headlines.index', $event) }}" class="ecard-btn">@lang('events.headlines-details')</a>
            </div>
            <ul class="ecard-headlines_list">
              @forelse($event->headlines as $headline)
                <li>
                  <span class="ecard-headlines_list-counter">{{ $headline->id }}</span>{{ $headline->name }} <nobr>({{ trans_choice('events.lbl.topics-count', $headline->topics->count(), ['count' => $headline->topics->count()]) }})</nobr>
                </li>
              @empty
                <li></li>
              @endforelse
            </ul>
          </div>
          <div class="ecard-details">
            <div class="ecard-details_header">
            @can('index', App\Models\Client::class)
              @if($event->client)
                <small class="text-muted">@lang('events.lbl.client'): <strong>{{ $event->client->name }}</strong></small>
              @endif
            @endcan
            <small class="text-muted">@lang('events.lbl.union'): <strong>{{ $event->union }}</strong></small>
            </div>
            <p class="card-text text-muted text-size-sm">{{ $event->description }}</p>
          </div>
          <div class="ecard-footer dropnone">
            @if(!$event->trashed() && Auth::user()->can('manage', $event))
              <a href="{{ route('admin::events.show', $event) }}" class="ecard-footer_btn">@lang('events.nav.view')</a>
            @endif

            @can('view-delegates', $event)
              <a href="{{ route('admin::delegates.index', $event) }}" class="ecard-footer_btn">@lang('events.nav.delegates')</a>
            @endcan

            <a href="{{ route('admin::events.files', $event) }}"  class="ecard-footer_btn">@lang('events.nav.files')</a>

            @can('manage', $event)
              <a href="#" class="ecard-footer_btn is-dropdown" id="dropdownMenuEvent{{ $event->id }}"
                 data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-ellipsis-v"></i>
              </a>
              <div class="dropdown-menu dropdown-menu-right dropdown-menu-up" aria-labelledby="dropdownMenuEvent{{ $event->id }}" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">
              @if($event->trashed())
                <a href="#" class="ecard-footer_btn" v-on:click="showModal" data-modal-target="modal"
                   data-modal-title="@lang('events.modal.ttl-restore')"
                   data-modal-body="@lang('events.modal.txt-restore', ['name' => $event->name])"
                   data-ok-func="objTrash"
                   data-ok-args='{"url": "{{ route('admin::events.restore', $event) }}", "method": "post"}'>
                  @lang('events.nav.restore')
                </a>
              @else
                @if(!$event->active())
                  <a href="#" class="ecard-footer_btn" v-on:click="showModal" data-modal-target="modal"
                     data-modal-title="@lang('events.modal.ttl-change')"
                     data-modal-body="@lang('events.modal.txt-change', ['name' => $event->name, 'status' => trans('events.status.active')])"
                     data-ok-func="objMarkAs" data-ok-args='{"obj": {{ $event->id }}, "status": "active"}'>
                    @lang('events.nav.active')
                  </a>
                @elseif(!$event->planned())
                  <a href="#" class="ecard-footer_btn" v-on:click="showModal" data-modal-target="modal"
                     data-modal-title="@lang('events.modal.ttl-change')"
                     data-modal-body="@lang('events.modal.txt-change', ['name' => $event->name, 'status' => trans('events.status.planned')])"
                     data-ok-func="objMarkAs" data-ok-args='{"obj": {{ $event->id }}, "status": "planned"}'>
                    @lang('events.nav.planned')
                  </a>
                @endif

                @if(!$event->completed())
                  <a href="#" class="ecard-footer_btn" v-on:click="showModal" data-modal-target="modal"
                     data-modal-title="@lang('events.modal.ttl-change')"
                     data-modal-body="@lang('events.modal.txt-change', ['name' => $event->name, 'status' => trans('events.status.completed')])"
                     data-ok-func="objMarkAs" data-ok-args='{"obj": {{ $event->id }}, "status": "completed"}'>
                    @lang('events.nav.completed')
                  </a>
                @endif

                <a href="#" class="ecard-footer_btn" v-on:click="showModal" data-modal-target="modal"
                   data-modal-title="@lang('events.modal.ttl-delete')"
                   data-modal-body="@lang('events.modal.txt-delete', ['name' => $event->name])"
                   data-ok-func="objTrash"
                   data-ok-args='{"url": "{{ route('admin::events.destroy', $event) }}", "method": "delete"}'>
                  @lang('events.nav.delete')
                </a>
              @endif
              </div>
            @endcan
          </div>
      </div>
    @empty
      <div class="col-12">@lang('events.empty')</div>
    @endforelse
  </div>

  @can('manage', \App\Models\Event::class)
    <div class="fixed-btn">
      <a class="btn-floating btn-large amber darken-4"
       href="{{ route('admin::events.create') }}"><i class="fa fa-plus material-icons"></i></a>
    </div>


  @endcan

  {{ $events->links() }}
@endsection
