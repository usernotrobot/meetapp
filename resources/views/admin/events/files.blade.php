@extends('layouts.app')

@section('breadcrumbs')
  <ol class="breadcrumb transparent">

    <li class="breadcrumb-item"><a href="{{ route('admin::events.index') }}">@lang('events.index')</a></li>
    <li class="breadcrumb-item">{{ $event->name }} @if($event->trashed())(deleted)@endif</li>
    <li class="breadcrumb-item active">@lang('events.files')</li>
  </ol>
@endsection

@section('content')
  <div class="row">
    <div class="col-12">
      <table class="table table-hover" id="data-table" style="table-layout: fixed">
        <thead>
        <tr>
          <th style="width:58px">@lang('files.lbl.id')</th>
          <th>@lang('files.lbl.name')</th>
          <th style="width:22%">@lang('files.lbl.date')</th>
          @can('manage', $event) <th style="width:18%;">@lang('files.lbl.user')</th> @endcan
          <th style="width:10%"></th>
        </tr>
        </thead>
        <tbody>
        @foreach($files as $file)
          <tr @if($loop->iteration >= $pageItems) class="datatable-hidden-tr" @endif >
            <th scope="row">{{ $loop->iteration }}</th>
            <td class="text-ellipsis" title="{{ $file->name }}">{{ $file->name }}</td>
            <td class="text-nowrap" data-order="{{ $file->created_at->format('U') }}">{{ $file->created_at->format('d F Y') }}</td>
            @can('manage', $event) <td>{{ $file->owner->name }}</td> @endcan
            <td class="dropnone text-center">
              <a href="#" id="dropdownMenuFile{{ $file->id }}"
                 data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-ellipsis-v"></i>
              </a>
              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuFile{{ $file->id }}"
                   data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">

                <a href="{{ route('files.download', $file) }}" class="dropdown-item">@lang('files.nav.download')</a>

                @can('manage', $event)
                  <a href="#" class="dropdown-item" v-on:click="showModal" data-modal-target="modal"
                     data-modal-title="@lang('files.modal.ttl-delete')"
                     data-modal-body="@lang('files.modal.txt-delete', ['name' => $file->name])"
                     data-ok-func="objTrash" data-ok-args='{"url": "{{ route('files.destroy', $file) }}", "method": "delete"}'>
                    @lang('files.nav.delete')
                  </a>
                @endcan
              </div>
            </td>
          </tr>
        @endforeach
        </tbody>
      </table>
    </div>
  </div>

  @can('manage', $event)

    <a class="float-right mdl-button mdl-button--fab mdl-button--colored amber darken-4" href="#"
       v-on:click="showModal" data-modal-target="modalUpload" data-url="{{ route('admin::events.upload', $event) }}">
      <i class="fa fa-plus material-icons"></i>
    </a>

    <div is="modalUpload" v-bind:data="modalUpload" v-on:modal-shown="shownModal"></div>
  @endcan
@endsection

@section('scripts')
  <script>
    $().ready(function() {
      $('#data-table').DataTable({
        paging: {{ (count($files) >= $pageItems) ? 'true' : 'false' }},
      });
    });
  </script>
@endsection
