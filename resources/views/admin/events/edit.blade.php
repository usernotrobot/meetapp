@extends('layouts.app')

@section('breadcrumbs')
  <ol class="breadcrumb transparent">

    <li class="breadcrumb-item"><a href="{{ route('admin::events.index') }}">@lang('events.index')</a></li>
    <li class="breadcrumb-item">{{ $event->name }} @if($event->trashed())(deleted)@endif</li>
    <li class="breadcrumb-item active">@lang('events.edit')</li>
  </ol>
@endsection

@section('content')

  <form class="form-horizontal row" role="form" method="POST" action="{{ route('admin::events.update', $event) }}">
    <div class="col-12 col-lg-6">
      {{ csrf_field() }}
      {{ method_field('PUT') }}

      <div class="md-form form-group{{ $errors->has('name') ? ' has-danger' : '' }} ">
        <input id="event-name" type="text" class="form-control{{ $errors->has('name') ? ' form-control-danger' : '' }}"
               name="name" value="{{ old('name', $event->name )}}" required autofocus maxlength="255">
        <label for="event-name" class="active">@lang('events.lbl.name')</label>
        @if ($errors->has('name'))
          <div class="form-control-feedback">{{ $errors->first('name') }}</div>
        @endif
      </div>

      @can('manage', App\Models\Client::class)
        <div class="md-form form-group{{ $errors->has('client_id') ? ' has-danger' : '' }} text-size-md">
          <select class="mdb-select" name="client_id" id="event-client" data-check-empty>
            <option value="" disabled @if(!$event->client) selected @endif>@lang('events.lbl.choose-client')</option>
            @foreach($clients as $client)
              <option value="{{ $client->id }}"
                      @if($event->client && $client->id == $event->client->id) selected @endif>{{ $client->name }}
              </option>
            @endforeach
          </select>
          <label for="event-client" class="active">@lang('events.lbl.client')</label>
          @if ($errors->has('client_id'))
            <div class="form-control-feedback">{{ $errors->first('client_id') }}</div>
          @endif
        </div>
      @else
        <input type="hidden" name="client_id" value="{{ $event->client_id }}" />
      @endcan

      <div class="md-form form-group{{ $errors->has('union') ? ' has-danger' : '' }}">
        <input id="event-union" type="text" class="form-control{{ $errors->has('union') ? ' form-control-danger' : '' }}"
               name="union" value="{{ old('union', $event->union) }}" required maxlength="255">
        <label for="event-union" class="active">@lang('events.lbl.union')</label>
        @if ($errors->has('union'))
          <div class="form-control-feedback">{{ $errors->first('union') }}</div>
        @endif
      </div>

      <div class="md-form form-group{{ $errors->has('description') ? ' has-danger' : '' }}">
        <textarea class="md-textarea form-control{{ $errors->has('description') ? ' form-control-danger' : '' }}"
                  type="text" id="event-description" name="description"
                  maxlength="255" length="255">{{ old('description', $event->description) }}</textarea>
        <label for="event-description" class="active">@lang('events.lbl.description')</label>
        @if ($errors->has('description'))
          <div class="form-control-feedback">{{ $errors->first('description') }}</div>
        @endif
      </div>
      <div class="row">
        <div class="col-12 col-sm-6">
          <div class="md-form form-group{{ $errors->has('start') ? ' has-danger' : '' }}">
            <input type="date" id="event-start" value="{{ old('start', $event->start->format('Y-m-d')) }}" name="start" required
                   class="form-control datepicker{{ $errors->has('start') ? ' form-control-danger' : '' }}">
            <label for="event-start" class="active">@lang('events.lbl.start')</label>
            @if ($errors->has('start'))
              <div class="form-control-feedback">{{ $errors->first('start') }}</div>
            @endif
          </div>
        </div>

        <div class="col-12 col-sm-6">
          <div class="md-form form-group{{ $errors->has('end') ? ' has-danger' : '' }}">
            <input type="date" id="event-end" value="{{ old('end', $event->end->format('Y-m-d')) }}" name="end" required
                   class="form-control datepicker{{ $errors->has('end') ? ' form-control-danger' : '' }}">
            <label for="event-end" class="active">@lang('events.lbl.end')</label>
            @if ($errors->has('end'))
              <div class="form-control-feedback">{{ $errors->first('end') }}</div>
            @endif
          </div>
        </div>
      </div>

      <div class="md-form form-group{{ $errors->has('status') ? ' has-danger' : '' }} text-size-md">
        <select class="mdb-select" name="status"
                id="event-status">
          @foreach(\App\Models\Event::allStatuses() as $status)
            <option value="{{ $status }}" @if($event->status == $status) selected @endif>@lang('events.status.' . $status)</option>
          @endforeach
        </select>
        <label for="event-status" class="active">@lang('events.lbl.status')</label>
        @if ($errors->has('status'))
          <div class="form-control-feedback">{{ $errors->first('status') }}</div>
        @endif
      </div>

      <div class="text-center">
        <button class="btn btn-default" type="reset">@lang('app.btn.reset')</button>
        <button class="btn btn-amber" type="submit">@lang('app.btn.save')</button>
      </div>
    </div>
  </form>


@endsection