@extends('layouts.auth')

<!-- Main Content -->
@section('content')
  <div class="card card-block col-md-7 col-lg-5 col-xl-3">
    <div class="flex-center mt-1 mb-1">
      <img src="/images/logo.png" alt="{{ config('app.name') }}"/>
    </div>
  
    <p class="card-text text-center">@lang('auth.lost-pwd-text')</p>
    
    @if (session('status'))
      <div class="alert alert-success text-center mt-1" role="alert">
        {{ session('status') }}
      </div>
    @endif
            
    <form class="form-horizontal" role="form" method="POST" action="{{ route('pwd-email') }}">
      {{ csrf_field() }}
              
      <div class="md-form form-group{{ $errors->has('email') ? ' has-danger' : '' }}">
        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' form-control-danger' : '' }}"
               name="email" value="{{ old('email') }}" placeholder="@lang('auth.email')" required autofocus>
        @if ($errors->has('email'))
          <div class="form-control-feedback">{{ $errors->first('email') }}</div>
        @endif
      </div>
      
      <div class="text-right">
        <button type="submit" class="btn btn-amber">@lang('auth.send-pwd-btn')</button>
      </div>
      
      <a href="{{ route('login') }}" class="card-link">@lang('auth.login')</a>
    </form>
  </div>
@endsection
