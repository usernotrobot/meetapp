@extends('layouts.auth')

@section('content')
  <div class="card card-block col-md-7 col-lg-5 col-xl-3">
    <div class="flex-center mt-1 mb-1">
      <img src="/images/logo.png" alt="{{ config('app.name') }}"/>
    </div>
  
    <form class="form-horizontal" role="form" method="POST" action="{{ route('login') }}">
      {{ csrf_field() }}
      
      <div class="md-form form-group{{ $errors->has('email') ? ' has-danger' : '' }}">
        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' form-control-danger' : '' }}"
               name="email" value="{{ old('email') }}" placeholder="@lang('auth.email')" required autofocus>
        @if ($errors->has('email'))
          <div class="form-control-feedback">{{ $errors->first('email') }}</div>
        @endif
      </div>
    
      <div class="md-form form-group{{ $errors->has('password') ? ' has-error' : '' }}">
        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' form-control-danger' : '' }}"
               name="password" placeholder="@lang('auth.password')" required>
        @if ($errors->has('password'))
          <div class="form-control-feedback">{{ $errors->first('password') }}</div>
        @endif
      </div>
     
      <div class="text-right">
        <button type="submit" class="btn btn-amber">@lang('auth.login')</button>
      </div>
      
      <a href="{{ route('pwd-forgot') }}" class="card-link">@lang('auth.forgot-password')</a>
  
      <input type="hidden" name="remember" value="on">
    </form>
  </div>
@endsection
