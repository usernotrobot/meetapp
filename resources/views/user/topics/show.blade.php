@extends('layouts.app')

@section('breadcrumbs')
  <ol class="breadcrumb transparent">

    <li class="breadcrumb-item"><a href="{{ route('user::agenda', $event) }}">{{ $event->name }}</a></li>
    <li class="breadcrumb-item">{{ $topic->name }}</li>
    <li class="breadcrumb-item active">@lang('topics.nav.view')</li>
  </ol>
@endsection

@section('content')
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-block">
          <h4 class="card-title">{{ $topic->name }}</h4>
          <hr/>
          <p class="card-text text-size-sm">{{ $topic->description }}</p>

          @can('manage-speechlist', $event)
            <a href="{{ route('user::topics.settings', [$event, $topic]) }}"
             class="card-link link-text">
              <i class="fa fa-cog"></i>
              @lang('topics.nav.settings')
            </a>
          @endcan

          <a href="{{ route('user::topics.files', [$event, $topic]) }}"
             class="link-text card-link pull-right">
            {{ trans_choice('topics.lbl.files', count($topic->files), ['count' => count($topic->files)]) }}
            <i class="fa fa-chevron-right"></i>

          </a>
        </div>
      </div>
      <br/>
    </div>
    <div class="col-12 col-sm-6">
      <div class="card">
        <div class="card-block">
          <h5 class="card-title">
            @lang('topics.lbl.last-suggestions')
          </h5>
          <hr/>
          {{-- Suggestions counter--}}
          <ul>
            @foreach($suggestions as $suggestion)
              <li><i class="fa fa-info fa-fw text-size-sm"></i> <a href="{{ route('user::suggestions.show', [$event, $topic, $suggestion]) }}">{{ $suggestion->summary }}</a> ({{ $suggestion->user->name }})</li>
            @endforeach
          </ul>
          <a href="{{ route('user::topics.suggestions', [$event, $topic]) }}" class="text-size-sm link-text">
            <h5>{{ trans_choice('topics.lbl.suggestions', $suggestionsCount, ['suggestions' => $suggestionsCount]) }}
              {{ trans_choice('topics.lbl.from-delegates', $suggestionsDelegatesCount, ['delegates' => $suggestionsDelegatesCount]) }} <i class="fa fa-chevron-right"></i></h5>
          </a>
        </div>
      </div>
      <br/>
    </div>
    <div class="col-12 col-sm-6">
      <div class="card">
        <div class="card-block">
          <h5 class="card-title">
            {{ trans_choice('topics.lbl.speechlist', count($speechlist), ['count' => count($speechlist)]) }}
          </h5>
          <hr/>
          <ul>
            @for ($i = 0; $i < (count($speechlist) > 10 ? 10 : count($speechlist)); $i++)
              <li @if($speechlist[$i]->active()) class="text-success" @endif >
                @if($speechlist[$i]->active())
                  <i class="fa fa-commenting-o fa-fw text-size-sm"></i>
                @else
                  <i class="fa fa-comment-o fa-fw text-size-sm"></i>
                @endif

                {{$speechlist[$i]->delegate->user->name}}
              </li>
            @endfor
          @if (count($speechlist) > 10)
            <li>@lang('topics.lbl.speechlist-more', ['count' => count($speechlist) - 10])</li>
          @endif
          </ul>
          <a href="{{ route('user::topics.speechlist', [$event, $topic]) }}" class="text-size-sm link-text">
            <h5>@lang('topics.lbl.all-speechlist') <i class="fa fa-chevron-right"></i></h5></a>
        </div>
      </div>
      <br/>
    </div>
  </div>
@endsection
