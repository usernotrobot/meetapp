@extends('layouts.app')

@push('main-class', 'view moderation-page moderation-page--discussion')
@push('styles')
<link href="{{ mix('css/moderation.css') }}" rel="stylesheet">
@endpush

@section('breadcrumbs')
  <ol class="breadcrumb transparent">

    <li class="breadcrumb-item"><a href="{{ route('user::agenda', $event) }}">{{ $event->name }}</a></li>
    <li class="breadcrumb-item">{{ $topic->name }}</li>
    <li class="breadcrumb-item active">@lang('topics.nav.view')</li>
  </ol>
@endsection

@section('content')

  <div id="app-props" class="now-block d-md-flex flex-md-column align-content-md-stretch no-gutters">
    <div class="card topic-header">
      <form class="card topic-header" id="moderationForm" name="moderationForm">
        <div class="card-header grey lighten-4 flex-column flex-sm-row justify-content-start">
          <label>@lang('topics.moderation.topic.label')</label><br>
          <input placeholder="@lang('topics.moderation.topic.name-holder')" name="name" type="text" value=""><i class="fa fa-pencil fa-2x"></i></input>
          <div class="btn-group ml-sm-auto">
            <button type="button" id="save_moderation" class="btn btn-save-topic">@lang('topics.moderation.topic.btn-save-topic')</button>
          </div>
        </div>
        <div class="card-block-speaker row align-items-center not-active">
          <div class="speaker h-text col-12 text-center text-sm-left">
            <label>
              <span>@lang('topics.moderation.topic.settings')</span>
            </label>
          </div>
          <div class="col-6">
            <div class="row">
              <div class="col-6">
                <div class="switch">
                  <label>
                    @lang('topics.moderation.topic.pickup-pdf')
                    <input type="checkbox" name="pickup_pdf">
                    <span class="lever"></span>
                  </label>
                </div>
              </div>
              <div class="col-6">
                <div class="switch">
                  <label>
                    @lang('topics.moderation.topic.preserve-list')
                    <input type="checkbox" name="preserve_list">
                    <span class="lever"></span>
                  </label>
                </div>
              </div>
            </div>
          </div>
        </div>
      </form>
    </div>
    <div class="col row topic-blocks">
      <div class="col-12 col-md-7 col-lg-8">
        <div class="card z-depth-0">
          <div class="card-header grey lighten-4"><h6>{{ trans_choice('topics.lbl.suggestions', $suggestionsCount, ['suggestions' => $suggestionsCount ]) }}</h6></div>
          <!-- DRAG LIST -->
          <draggable class="card-block suggestions-block" id="draggable" v-model="dragList" v-cloak :options="{group:'suggestions'}">
            <!-- title -->
            <div v-for="suggestion in dragList" class="suggestion clearfix" :data-id="suggestion.id">
              <div class="row">
                <div class="col-11">
                  <span class="suggestion-author pr-1">@{{suggestion.username}}:</span>
                  <span class="suggestion-header">@{{suggestion.summary}}</span>
                </div>
                <div class="col-1 text-right">
                  <div class="btn-group dropnone">
                    <!-- move -->
                    <div class="to-new-suggestion" data-toggle="tooltip" data-animation="false" data-placement="top" title="@lang('topics.moderation.topic.add-suggestion')" v-on:click="addToList(suggestion, $event)">
                      <i class="fa fa-share"></i>
                    </div>
                  </div>
                </div>
              </div>
              <!-- content -->
              <div class="suggestion-body">
                @{{ suggestion.details }}
              </div>
              <!-- more -->
              <span
              v-if="!suggestion.isShort"
              v-on:click="showModal"
              data-modal-target="modalSuggestion"
              :data-id="suggestion.id"
              data-view="true"
              class="card-more">@lang('topics.moderation.suggestion.more')</span>
            </div>
          </draggable>
        </div>
      </div>
      <div class="col-12 col-md-5 col-lg-4">
        <div id="block-speechlist" class="card z-depth-0 grey lighten-4">
          <div class="card-header"><h6>@lang('topics.moderation.suggestion.title')</h6></div>
          <div class="card-block speech-block">

            <!-- DROP LIST -->
            <draggable id="sortable" class="droplist" v-cloak v-model="dropList" :options="{group:'suggestions'}">
              <!-- title -->
              <div v-for="suggestion in dropList" class="suggestion clearfix" :data-id="suggestion.id">
                <div class="row">
                  <div class="col-10">
                    <span class="suggestion-author pr-1">@{{suggestion.username}}:</span>
                    <span class="suggestion-header">@{{suggestion.summary}}</span>
                  </div>
                  <!-- buttons -->
                  <div class="col-2 text-right">
                    <div class="btn-group dropnone">
                      <!-- edit -->
                      <a href="#" v-on:click="showModal"
                         data-modal-target="modalSuggestion"
                         :data-id="suggestion.id"
                         data-toggle="tooltip" data-animation="false" data-placement="top" title="@lang('edit')">
                         <i class="fa fa-pencil"></i>
                      </a>
                      <!-- move back -->
                      <a href="#" v-on:click="removeFromList(suggestion, $event)"
                      data-toggle="tooltip" data-animation="false" data-placement="top" title="@lang('topics.moderation.topic.remove-suggestion')">
                        <i class="fa fa-times"></i>
                      </a>

                    </div>
                  </div>
                </div>
                <!-- content -->
                <div class="suggestion-body">
                  @{{ suggestion.details }}
                </div>
                <!-- more -->
                <span
                    v-if="!suggestion.isShort"
                    v-on:click="showModal"
                    data-modal-target="modalSuggestion"
                    :data-id="suggestion.id"
                    data-view="true"
                    class="card-more">@lang('topics.moderation.suggestion.more')</span>

              </div>
            </draggable>

            <div class="drop-down-box text-center" id="drop-box">
              <span>@lang('topics.moderation.suggestion.drop-box')</span>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>

  <div is="modalSuggestion" v-bind:data="modalSuggestion" v-on:modal-shown="shownModal"></div>

  <div id="confirmSaveTopic" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" class="modal fade">
      <div role="document" class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                  <h5 class="modal-title"></h5>
              </div>
                <div class="modal-body">
                    @lang('topics.moderation.topic.confirm-body')
                </div>
              <div class="modal-footer">
                  <button type="button" data-dismiss="modal" class="btn-flat btn-sm dark-amber-text waves-effect">
                    @lang('topics.moderation.topic.btn-cancel-topic')
                  </button>
                  <button type="submit" class="btn-flat btn-sm waves-effect" data-dismiss="modal" onclick="saveTopic()">@lang('topics.moderation.topic.btn-save-topic')</button>
              </div>
          </div>
      </div>
  </div>


@endsection

@section('vuejs')
    <script>
        const moderation = window.moderation = {
            routes: {!! $routes_json !!},
            url: '{!! $updateUrl !!}',
            suggestions: {!! $suggestions !!}
        }
    </script>
    <script src="{{ mix('js/moderation.js') }}"></script>
@endsection