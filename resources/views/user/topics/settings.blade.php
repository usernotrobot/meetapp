@extends('layouts.app')

@section('breadcrumbs')
  <ol class="breadcrumb transparent">
    <li class="breadcrumb-item"><a href="{{ route('user::agenda', $event) }}">{{ $event->name }}</a></li>
    <li class="breadcrumb-item"><a href="{{ route('user::topics.show', [$event, $topic]) }}">{{ $topic->name }}</a></li>
    <li class="breadcrumb-item active">@lang('topics.nav.settings')</li>
  </ol>
@endsection

@section('content')
  <form class="form-horizontal" role="form" method="POST" action="{{ route('user::topics.settings-update', [$event, $topic]) }}">
    <div class="row">
      <div class="col-12 col-sm-6">
        {{ csrf_field() }}
        {{ method_field('PUT') }}

        <div class="mb-2"><h3>@lang('topics.settings')</h3></div>

        <div class="md-form form-group{{ $errors->has('speech_times') ? ' has-danger' : '' }} ">
          <input id="topic-speech_times" type="number" min="{{ $speech_min }}" max="{{ $speech_max }}"
                 class="form-control{{ $errors->has('speech_times') ? ' form-control-danger' : '' }}"
                 name="speech_times" value="{{ old('speech_times', $topic->speech_times) }}" required maxlength="3">
          <label for="topic-speech_times">@lang('topics.lbl.speech_times')</label>
          @if ($errors->has('speech_times'))
            <div class="form-control-feedback">{{ $errors->first('speech_times') }}</div>
          @endif
        </div>

        <div class="row">
          <div class="col-12">
            <h6 class="mb-2">@lang('topics.lbl.speech_length'):</h6>
          </div>
          @for($i = $speech_min; $i <= $speech_max; $i++)
            <div class="col md-form js-topic-speech-length" @if($i > old('speech_times', $topic->speech_times)) style="display: none" @endif >
              <div class="form-group{{ $errors->has('speech_length.' . $i) ? ' has-danger' : '' }}">
                <input id="topic-speech_length_{{ $i }}" type="number" min="1" max="60"
                       class="form-control{{ $errors->has('speech_length.' . $i) ? ' form-control-danger' : '' }}"
                       name="speech_length[{{ $i }}]" value="{{ old('speech_length.' . $i, $topic->speechMinutes($i)) }}"
                       required maxlength="2">
                <label for="topic-speech_length_{{ $i }}" class="pl-3">{{ trans_choice('topics.lbl.speech_length_i', $i) }}</label>
                @if ($errors->has('speech_length.' . $i))
                  <div class="form-control-feedback">{{ $errors->first('speech_length.' . $i) }}</div>
                @endif
              </div>
            </div>
          @endfor
        </div>

        <div class="md-form">
          <fieldset class="form-group {{ $errors->has('speech_opened') ? ' has-danger' : '' }} ">
            <input type="checkbox" id="topic-speech_opened" name="speech_opened" value="1"
                   @if(old('speech_opened', $topic->speech_opened)) checked @endif>
            <label for="topic-speech_opened">@lang('topics.lbl.speech_opened')</label>
            @if ($errors->has('speech_opened'))
              <div class="form-control-feedback">{{ $errors->first('speech_opened') }}</div>
            @endif
          </fieldset>
        </div>

        <div class="text-center mt-4">
          <input type="hidden" name="client_id" value="{{ $event->client_id }}">
          <button class="btn btn-default" type="reset">@lang('app.btn.reset')</button>
          <button class="btn btn-amber" type="submit">@lang('app.btn.save')</button>
        </div>
      </div>

    </div>
  </form>
@endsection

@section('scripts')
  <script>
    $().ready(function() {
      $('#topic-speech_times').on('change', function (e)  {
        $('.js-topic-speech-length').hide();
        for(i = 1; i <= $(this).val(); i++) {
          $('#topic-speech_length_' + i).parents('.js-topic-speech-length').show();
        }
      });
    });
  </script>
@endsection
