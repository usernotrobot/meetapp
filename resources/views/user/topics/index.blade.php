@extends('layouts.app')

@push('styles')
<link href="{{ mix('css/delegate.css') }}" rel="stylesheet">
@endpush

@section('breadcrumbs')
  <ol class="breadcrumb transparent">
    <li class="breadcrumb-item">{{ $event->name }}</li>
    <li class="breadcrumb-item active">@lang('app.nav.user.agenda')</li>
  </ol>
@endsection

@php ($openStyles = "")
@php ($discussionStyles = "amber lighten-4")
@php ($votingStyles = "red lighten-4")
@php ($closedStyles = "green lighten-4")
@php ($deletedStyles = "grey lighten-2")

@section('event-name')
{{ $event->name }}
@endsection

@section('content')
<table class="table agenda-table">
  <tbody>
  @forelse($headlines as $headline)
        <tr class="tr-headline">
          <td colspan="4">{{ $loop->iteration }}. {{ $headline->name }} </td>
          <td class="text-right">
            <small>({{ trans_choice('events.lbl.topics-count', $headline->topics->count(), ['count' => $headline->topics->count()]) }})</small>
            <a href=""><i class="fa fa-file" aria-hidden="true"></i> @lang('topics.nav.files')</a>
          </td>
        </tr>
        @foreach($headline->topics as $topic)
        <tr class="tr-topic">
          <td>&nbsp;</td>
          <td>{{ $loop->iteration }}. {{ $topic->name }}</td>
          <td>{{ $topic->description }}</td>
          <td>
            @php($s = $topic->suggestionsSummary())
                {{ trans_choice('topics.lbl.suggestions', $s[0], ['suggestions' => $s[0]]) }}
                {{ trans_choice('topics.lbl.from-delegates', $s[1], ['delegates' => $s[1]]) }}
          </td>
          <td class="dropnone text-right">

            <a class="tr-topic_dd-toggle" id="dropdownMenuTopic{{ $topic->id }}"
               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <i class="fa fa-ellipsis-v"></i>
            </a>

            <div class="dropdown-menu dropdown-menu-right dropdown-menu-down" aria-labelledby="dropdownMenuTopic{{ $topic->id }}" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">

              <a href="{{ route('user::topics.show', [$event, $topic]) }}" class="dropdown-item">@lang('topics.nav.view')</a>

              <a href="{{ route('user::topics.suggestions', [$event, $topic]) }}" class="dropdown-item">@lang('topics.nav.suggestion')</a>

              <a href="{{ route('user::topics.files', [$event, $topic]) }}"  class="dropdown-item" >@lang('topics.nav.files')</a>

              <a href="{{ route('user::topics.speechlist', [$event, $topic]) }}" class="dropdown-item">@lang('topics.nav.speechlist')</a>
              @can('secretary', $event)
                <a href="{{ route( 'user::suggestions.moderation', [$event, $topic]) }}" class="dropdown-item">@lang('topics.nav.new_create_topic')</a>
              @endcan
            </div>
          </td>
        </tr>
        @endforeach

  @empty
    <tr class="tr-topic">
      <td colspan="5">@lang('topics.empty')</td>
    </tr>
  @endforelse
  </tbody>
</table>
<div class="agenda-table_spacer"><!-- space added for last row's dropdown visibility --></div>

  {{ $headlines->links() }}
@endsection
