@php ($events[App\Models\Event::STATUS_ACTIVE] = Auth::user()->listEvents(App\Models\Event::STATUS_ACTIVE))
@php ($events[App\Models\Event::STATUS_PLANNED] = Auth::user()->listEvents(App\Models\Event::STATUS_PLANNED))
@php ($events[App\Models\Event::STATUS_COMPLETED] = Auth::user()->listEvents(App\Models\Event::STATUS_COMPLETED))
@php ($current = Request::route('event') ? app(\App\Repositories\EventRepository::class)->get(Request::route('event')) : null)

@foreach($events as $status => $items)
  @if($items->count())
  <div class="panel panel-default">
    
    <div class="panel-heading" role="tab" id="heading{{ $status }}">
      <h5 class="panel-title"><a class="arrow-r" data-toggle="collapse" data-parent="#accordion" href="#collapse{{ $status }}">
          @lang('app.nav.user.' . $status . '-event')
          <i class="fa fa-angle-down rotate-icon rotate-element"></i>
        </a></h5>
    </div>
    <div id="collapse{{ $status }}" class="panel-collapse collapse @if($current && $current->status == $status)in show @endif" role="tabpanel" aria-labelledby="heading{{ $status }}">
      @foreach($items as $event)
        <a href="{{ route('user::now', $event) }}" class="current-event {{ (Request::fullUrlIs(route('user::now', $event)) ? 'active' : '') }}">{{ $event->name }}</a>
        @if (Request::fullUrlIs(route('user::event', $event) . '*'))
        <ul>
          <li>
            <small>@lang('app.nav.user.documents')</small>
          </li>
          <li>
            <a href="{{ route('user::agenda', $event) }}" class="collapsible-header waves-effect {{ (Request::fullUrlIs(route('user::agenda', $event) . '*') && !Request::is('*suggestions*')) ? 'active' : '' }}" ><i class="fa fa-book"></i> @lang('app.nav.user.agenda')</a>
          </li>
          <li>
            <a href="{{ route('user::files', $event) }}" class="waves-effect {{ (Request::fullUrlIs(route('user::files', $event)) ? 'active' : '') }}"><i class="fa fa-download"></i> @lang('app.nav.user.files')</a>
          </li>
        </ul>
        <ul>
          <li>
            <small>@lang('app.nav.user.suggestions')</small>
          </li>
          <li>
            <a href="{{ route('user::suggestions', $event) }}" class="waves-effect {{ Request::is('*/suggestions*') ? 'active' : '' }}"><i class="fa fa-list"></i>@lang('app.nav.user.suggestions')</a>
          </li>
          <li>
            <a href="{{ route('user::my-suggestions', $event) }}" class="waves-effect {{ (Request::fullUrlIs(route('user::my-suggestions', $event)) ? 'active' : '') }}"><i class="fa fa-id-card-o"></i>@lang('app.nav.user.my-suggestions')</a>
          </li>
        </ul>
        <ul>
          <li>
            <small>@lang('app.nav.user.speechlist')</small>
          </li>
          <li>
            <a href="{{ route('user::my-speechlist', $event) }}" class="waves-effect {{ (Request::fullUrlIs(route('user::my-speechlist', $event)) ? 'active' : '') }}"><i class="fa fa-comments-o"></i>@lang('app.nav.user.my-speechlist')</a>
          </li>
        </ul>
        @endif
      @endforeach
    </div>
  </div>
  @endif
@endforeach

{{--
@can('index', App\Models\Event::class)
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingTwo">
      <h5 class="panel-title"><a class="arrow-r" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">@lang('app.nav.user.events-upcoming')<i class="fa fa-angle-down rotate-icon"></i></a></h5>
    </div>
    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
      <ul>
        <li>
          <a href="{{ route('user::events.all', ['status' => 'upcoming']) }}" class="waves-effect {{ (Request::fullUrlIs(route('user::events.all', ['upcoming'])) ? 'active' : '') }}">Event 1</a>
        </li>
        <li>
          <a href="{{ route('user::events.all', ['status' => 'upcoming']) }}" class="waves-effect {{ (Request::fullUrlIs(route('user::events.all', ['pasupcomingt'])) ? 'active' : '') }}">Event 2</a>
        </li>
      </ul>
    </div>
  </div>

  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingThree">
      <h5 class="panel-title"><a class="arrow-r" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">@lang('app.nav.user.events-past')<i class="fa fa-angle-down rotate-icon"></i></a></h5>
    </div>
    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
      <ul>
        <li>
          <a href="{{ route('user::events.all', ['status' => 'past']) }}" class="waves-effect {{ (Request::fullUrlIs(route('user::events.all', ['past'])) ? 'active' : '') }}">Event 2</a>
        </li>
        <li>
          <a href="{{ route('user::events.all', ['status' => 'past']) }}" class="waves-effect {{ (Request::fullUrlIs(route('user::events.all', ['past'])) ? 'active' : '') }}">Event 3</a>
        </li>
      </ul>
    </div>
  </div>
@endcan
--}}

  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingFour">
      <h5 class="panel-title"><a class="arrow-r" data-toggle="collapse" data-parent="#accordion" href="#collapseFour">@lang('app.mail.header')<i class="fa fa-angle-down rotate-icon"></i></a></h5>
    </div>
    <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
      <ul>
        <li>
          <a href="#" class="waves-effect">@lang('app.mail.inbox')</a>
        </li>
        <li>
          <a href="#" class="waves-effect">@lang('app.mail.compose')</a>
        </li>
      </ul>
    </div>
  </div>

{{--


<li>
  <a href="{{ route('profile') }}" class="collapsible-header waves-effect {{ (Request::is('profile*') ? 'active' : '') }}">@lang('app.nav.profile')</a>
</li>
--}}
{{--

<li class="divider"></li>

<li>
  <a href="{{ route('logout') }}" class="collapsible-header waves-effect"
     onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
    @lang('app.status.logout')
  </a>
</li>
--}}
