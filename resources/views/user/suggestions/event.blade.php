@extends('layouts.app')

@section('breadcrumbs')
  <ol class="breadcrumb transparent">

    <li class="breadcrumb-item"><a href="{{ route('user::agenda', $event) }}">{{ $event->name }}</a></li>
    <li class="breadcrumb-item active">@lang('suggestions.index')</li>
  </ol>
@endsection

@php ($openStyles = "")
@php ($discussionStyles = "table-danger")
@php ($votingStyles = $closedStyles = "table-active")

@section('content')
  <div class="row">
    <div class="col-12">
      <table class="table table-hover" id="data-table" style="table-layout: fixed">
        <thead>
        <tr>
          <th style="width:58px">@lang('suggestions.lbl.id')</th>
          <th>@lang('suggestions.lbl.topic')</th>
          <th style="width:22%">@lang('suggestions.lbl.count')</th>
          <th style="width:18%;">@lang('suggestions.lbl.last-author')</th>
          <th style="width:10%"></th>
        </tr>
        </thead>
        <tbody>
        @foreach($topics as $topic)
          <tr class="{{ ${$topic->status . 'Styles'} }} @if($loop->iteration >= $pageItems) datatable-hidden-tr @endif"  >
            <th scope="row" width="30">{{ $loop->iteration }}</th>
            <td class="text-ellipsis" title="{{ $topic->name }}">{{ $topic->name }}</td>
            <td>{{ trans_choice('suggestions.lbl.suggestion-count', count($topic->suggestions), ['count' => count($topic->suggestions)]) }}</td>
            @if(count($topic->suggestions))
              <td class="text-nowrap" data-order="{{ $topic->lastSuggestion()->created_at->format('U') }}">
                {{ $topic->lastSuggestion()->user->name }}
                {{ $topic->lastSuggestion()->created_at->format('d F Y') }}
              </td>
            @else
              <td class="text-nowrap"></td>
            @endif
            <td class="text-center">
              <a href="{{ route('user::topics.suggestions', [$event, $topic]) }}">
                <i class="fa fa-chevron-right" aria-hidden="true"></i>
              </a>
            </td>
          </tr>
        @endforeach
        </tbody>
      </table>
    </div>
  </div>
@endsection

@section('scripts')
  <script>
    $().ready(function() {
      $('#data-table').DataTable({
        paging: {{ (count($topics) >= $pageItems) ? 'true' : 'false' }},
      });
    });
  </script>
@endsection
