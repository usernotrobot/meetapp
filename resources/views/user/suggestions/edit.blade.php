@extends('layouts.app')

@section('breadcrumbs')
  <ol class="breadcrumb transparent">

    <li class="breadcrumb-item"><a href="{{ route('user::agenda', $event) }}">{{ $event->name }}</a></li>
    <li class="breadcrumb-item"><a href="{{ route('user::topics.show', [$event, $topic]) }}">{{ $topic->name }}</a></li>
    <li class="breadcrumb-item active">{{ $suggestion->summary }}</li>
  </ol>
@endsection

@section('content')
  <form class="form-horizontal" role="form" method="POST"
        action="{{ route('user::suggestions.update', [$event, $topic, $suggestion]) }}">
    {{ csrf_field() }}
    {{ method_field('PUT') }}
    <div class="row">
      <div class="col-12">
        <div class="mb-2"><h3>@lang('suggestions.show')</h3></div>

        <div class="md-form form-group view-only-form">
          <input id="suggestion-user" type="text" class="form-control"
                 value="{{ $suggestion->user->name }}" readonly>
          <label for="suggestion-user">@lang('suggestions.lbl.author')</label>
        </div>

        <div class="md-form form-group{{ $errors->has('summary') ? ' has-danger' : '' }} ">
          <input id="suggestion-summary" type="text"
                 class="form-control{{ $errors->has('summary') ? ' form-control-danger' : '' }}"
                 name="summary" value="{{ old('summary', $suggestion->summary) }}" required
                 maxlength="255" length="255" autofocus>
          <label for="suggestion-summary">@lang('suggestions.lbl.summary')</label>
          @if ($errors->has('summary'))
            <div class="form-control-feedback">{{ $errors->first('summary') }}</div>
          @endif
        </div>

        <div class="md-form form-group{{ $errors->has('details') ? ' has-danger' : '' }} ">
          <textarea class="md-textarea form-control{{ $errors->has('details') ? ' form-control-danger' : '' }}"
                    type="text" id="suggestion-details" name="details" style="height: 12rem;overflow-y: scroll;"
                    maxlength="10000" length="10000">{{ old('details', $suggestion->details) }}</textarea>
          <label for="suggestion-details">@lang('suggestions.lbl.details')</label>
          @if ($errors->has('details'))
            <div class="form-control-feedback">{{ $errors->first('details') }}</div>
          @endif
        </div>

        <div class="text-center">
          <button class="btn btn-default" type="reset">@lang('app.btn.reset')</button>
          <button class="btn btn-amber" type="submit">@lang('app.btn.save')</button>
        </div>
      </div>
    </div>
  </form>
@endsection

