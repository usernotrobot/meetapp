@extends('layouts.app')

@section('breadcrumbs')
  <ol class="breadcrumb transparent">

    <li class="breadcrumb-item"><a href="{{ route('user::agenda', $event) }}">{{ $event->name }}</a></li>
    <li class="breadcrumb-item"><a href="{{ route('user::topics.show', [$event, $topic]) }}">{{ $topic->name }}</a></li>
    <li class="breadcrumb-item active">{{ $suggestion->summary }}</li>
  </ol>
@endsection

@section('content')
  <form class="form-horizontal view-only-form" role="form" method="POST"
        action="{{ route('user::suggestions.store', [$event, $topic]) }}">
    {{ csrf_field() }}

    <div class="row">
      <div class="col-12 ">
        <div class="mb-2"><h3>@lang('suggestions.show')</h3></div>

        <div class="md-form form-group">
          <input id="suggestion-user" type="text" class="form-control"
                 value="{{ $suggestion->user->name }}" readonly>
          <label for="suggestion-user">@lang('suggestions.lbl.author')</label>
        </div>

        <div class="md-form form-group">
          <input id="suggestion-when" type="text" class="form-control"
                 value="{{ $suggestion->created_at->format('d F Y, H:i') }}" readonly>
          <label for="suggestion-when">@lang('suggestions.lbl.added')</label>
        </div>

        <div class="md-form form-group">
          <input id="suggestion-summary" type="text"
                 class="form-control"
                 name="summary" value="{{ $suggestion->summary }}" required maxlength="255"
                 readonly>
          <label for="suggestion-summary">@lang('suggestions.lbl.summary')</label>
        </div>

        <div class="md-form form-group">
          <textarea class="md-textarea form-control"
                    type="text" id="suggestion-details" name="details" style="height: 12rem;overflow-y: scroll;"
                    readonly>{{ $suggestion->details }}</textarea>
          <label for="suggestion-details">@lang('suggestions.lbl.details')</label>
        </div>

        <div class="text-center">
          <a href="{{ route('user::topics.suggestions', [$event, $topic]) }}"
             class="btn btn-outline-success waves-effect"><i class="fa fa-undo left"></i> @lang('suggestions.btn.back')</a>
        </div>
      </div>

    </div>
  </form>
@endsection

