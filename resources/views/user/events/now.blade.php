@extends('layouts.app')

@push('main-class', 'view now-page now-page--discussion')
@push('styles')
<link href="{{ mix('css/now.css') }}" rel="stylesheet">
@endpush

@section('breadcrumbs')
  <ol class="breadcrumb transparent">
    <li class="breadcrumb-item">{{ $event->name }}</li>
    <li class="breadcrumb-item active">@lang('app.nav.user.now')</li>
  </ol>
@endsection

@section('content')
  <component :is="blockVote"></component>
  <div class="now-block d-md-flex flex-md-column align-content-md-stretch no-gutters"
       id="app-props">

  <div is="blockTopic" ref="blockTopic" v-on:show-modal-child="showModalChild"></div>

    <div class="row topic-blocks">
      {{-- Suggestions block --}}
      <div class="col-12 col-md-7 col-lg-8">
        <div is="blockSuggestion" ref="blockSuggestion" v-on:show-modal-child="showModalChild"></div>
      </div>

      {{-- Speechlist block --}}
      <div class="col-12 col-md-5 col-lg-4">
        <div is="blockAddSpeaker" {{--:show="blockAddSpeaker"--}}
             v-bind:data="blockAddSpeaker" v-on:modal-shown="shownModal" v-if="delegate.canManageSpeechlist"></div>

        <div is="blockSpeechlist" ref="blockSpeechlist" v-on:show-modal-child="showModalChild"></div>
      </div>
    </div>
  </div>

  <div is="modalSuggestion" v-bind:data="modalSuggestion" v-on:modal-shown="shownModal"></div>
  @can('manage-speechlist', $event)
    <div is="modalSpeakerLength" v-bind:data="modalSpeakerLength" v-on:modal-shown="shownModal"></div>
    <div is="modalSpeakerPriority" v-bind:data="modalSpeakerPriority" v-on:modal-shown="shownModal"></div>
  @endcan
  @can('moderate', $event)
    <div is="modalVotingStart" v-bind:data="modalVotingStart" v-on:modal-shown="shownModal"></div>
    <div is="modalTopicSettings" v-bind:data="modalTopicSettings" v-on:modal-shown="shownModal"></div>
  @endcan
@endsection

@section('data')
<script>
const now_page = window.now_page = {
  delegate: {!! $delegate_json !!},
  topic: {!! $topic_json !!},
  routes: {!! $routes_json !!},
  speechlist: {!! $speechlist_json !!},
  url: '{!! $updateUrl !!}'
}
</script>
@endsection
