@extends('layouts.app')

@section('breadcrumbs')
  <ol class="breadcrumb transparent">

    @if($status)
      <li class="breadcrumb-item"><a href="{{ route('user::events.all') }}">@lang('app.nav.user.events')</a></li>
      <li class="breadcrumb-item active">@lang('app.nav.user.events-' . $status)</li>
    @else
      <li class="breadcrumb-item active">@lang('app.nav.user.events')</li>
    @endif
  </ol>
@endsection

@php ($plannedStyles = "")
@php ($activeStyles = "amber lighten-4")
@php ($completedStyles = "green lighten-4")
@php ($deletedStyles = "grey lighten-2")

@section('content')
  <div class="row">
    @forelse($events as $event)
      <div class="col-6 col-md-4">
        <div class="card {{ ${$event->status . 'Styles'} }}">
          <div class="card-block">

            {{-- Name and Client Name--}}
            <h4 class="card-title text-ellipsis">{{ $event->name }}</h4>

            {{-- Union name --}}
            <h5>{{ $event->union }}</h5>
            {{-- Description --}}
            <p class="card-text text-muted text-size-sm">{{ $event->description }}</p>
            {{-- Links --}}
            <div class="card-footer transparent pr-0 pl-0 text-center dropnone text-size-md">
              @can('view-topics', $event)
                <a href="{{ $route('user::agenda', $event) }}" class="card-link">@lang('events.nav.topics')</a>
              @endcan

              <a href="{{ route('user::files', $event) }}" class="card-link">@lang('events.nav.files')</a>
            </div>
          </div>
        </div>
        <br/>
      </div>
    @empty
      <div class="col-12">@lang('events.empty')</div>
    @endforelse
  </div>

  {{ $events->links() }}
@endsection
