@extends('layouts.app')

@section('breadcrumbs')
  <ol class="breadcrumb transparent">

    <li class="breadcrumb-item"><a href="{{ route('user::agenda', $event) }}">{{ $event->name }}</a></li>
    <li class="breadcrumb-item active">@lang('speechlist.my')</li>
  </ol>
@endsection

@php ($waitingStyles = "")
@php ($activeStyles = "table-success")
@php ($finishedStyles = "table-active")

@section('content')
  <div class="row">
    <div class="col-12">
      <table class="table table-hover" id="data-table" style="table-layout: fixed">
        <thead>
        <tr>
          <th style="width:58px">@lang('speechlist.lbl.id')</th>
          <th>@lang('speechlist.lbl.topic')</th>
          <th style="width:22%">@lang('speechlist.lbl.added')</th>
          <th style="width:10%"></th>
        </tr>
        </thead>
        <tbody>
        @foreach($speechlist as $speech)
          <tr class="{{ ${$speech->status . 'Styles'} }} @if($loop->iteration >= $pageItems) datatable-hidden-tr @endif">
            <th scope="row" width="30">{{ $loop->iteration }}</th>
            <td class="text-ellipsis" title="$speech->topic->name ">{{ $speech->topic->name }}</td>
            <td class="text-nowrap" data-order="{{ $speech->created_at->format('U') }}">{{ $speech->created_at->format('H:i, d M Y') }}</td>
            @can('delete', $speech)
            <td class="dropnone text-center">
              <a href="#" id="dropdownMenuSuggestion{{ $speech->id }}"
                 data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-ellipsis-v"></i>
              </a>
              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuSuggestion{{ $speech->id }}"
                   data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">

                  <a href="#" class="dropdown-item" v-on:click="showModal" data-modal-target="modal"
                     data-modal-title="@lang('speechlist.modal.ttl-delete')"
                     data-modal-body="@lang('speechlist.modal.txt-delete',
                     ['user' => $speech->delegate->user->name])"
                     data-ok-func="objTrash"
                     data-ok-args='{"url": "{{ route('user::speechlist.destroy', [$event, $speech->topic, $speech]) }}", "method": "delete"}'>
                    @lang('speechlist.nav.delete')
                  </a>
              </div>
            </td>
            @else
              <td></td>
            @endcan

          </tr>
        @endforeach
        </tbody>
      </table>
    </div>
  </div>
@endsection

@section('scripts')
  <script>
    $().ready(function() {
      $('#data-table').DataTable({
        paging: {{ ($speechlist->count() >= $pageItems) ? 'true' : 'false' }},
      });
    });
  </script>
@endsection

