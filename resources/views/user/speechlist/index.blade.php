@extends('layouts.app')

@section('breadcrumbs')
  <ol class="breadcrumb transparent">

    <li class="breadcrumb-item"><a href="{{ route('user::agenda', $event) }}">{{ $event->name }}</a></li>
    <li class="breadcrumb-item"><a href="{{ route('user::topics.show', [$event, $topic]) }}">{{ $topic->name }}</a></li>
    <li class="breadcrumb-item active">@lang('speechlist.index')</li>
  </ol>
@endsection

@php ($waitingStyles = "")
@php ($activeStyles = "table-success")
@php ($finishedStyles = "table-active")

@section('content')
  <div class="row">
    <div class="col-12">
      <table class="table table-hover" id="data-table" style="table-layout: fixed">
        <thead>
        <tr>
          <th style="width:58px">@lang('speechlist.lbl.id')</th>
          <th>@lang('speechlist.lbl.user')</th>
          <th>@lang('speechlist.lbl.union')</th>
          <th style="width:10%"></th>
        </tr>
        </thead>
        <tbody>
        @foreach($speechlist as $speech)
          <tr class="{{ ${$speech->status . 'Styles'} }} @if($loop->iteration >= $pageItems) datatable-hidden-tr @endif">
            <th scope="row" width="30">{{ $loop->iteration }}</th>
            <td>{{ $speech->delegate->user->name }}</td>
            <td>{{ $speech->delegate->user->union}}</td>

            @can('delete', $speech)
            <td class="dropnone text-center">
              <a href="#" id="dropdownMenuSpeech{{ $speech->id }}"
                 data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-ellipsis-v"></i>
              </a>
              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuSpeech{{ $speech->id }}"
                   data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">

                @can('delete', $speech)
                  <a href="#" class="dropdown-item" v-on:click="showModal" data-modal-target="modal"
                     data-modal-title="@lang('speechlist.modal.ttl-delete')"
                     data-modal-body="@lang('speechlist.modal.txt-delete',
                     ['user' => $speech->delegate->user->name])"
                     data-ok-func="objTrash"
                     data-ok-args='{"url": "{{ route('user::speechlist.destroy', [$event, $speech->topic, $speech]) }}", "method": "delete"}'>
                    @lang('speechlist.nav.delete')
                  </a>
                @endcan

                @can('manage-speechlist', $event)
                    <div class="dropdown-divider"></div>

                    <a href="#" class="dropdown-item" v-on:click="showModal" data-modal-target="modal"
                       data-modal-title="@lang('speechlist.modal.ttl-mark-as-last')"
                       data-modal-body="@lang('speechlist.modal.txt-mark-as-last',
                     ['user' => $speech->delegate->user->name, 'topicName' => $topic->name,
                     'number' => $loop->iteration])"
                       data-ok-func="objAction"
                       data-ok-args='{"url": "{{ route('user::speechlist.mark-last', [$event, $speech->topic, $speech]) }}", "method": "post"}'>
                      @lang('speechlist.nav.markLast')
                    </a>
                @endcan
              </div>
            </td>
            @else
              <td></td>
            @endcan
          </tr>
        @endforeach
        </tbody>
      </table>
    </div>
  </div>
  @can('add-speechlist', $topic)
    @can('manage-speechlist', $event)
      <a class="float-right mdl-button mdl-button--fab mdl-button--colored amber darken-4" href="#"
         v-on:click="showModal" data-modal-target="modalSpeechlist"
         data-modal-title="@lang('speechlist.modal.ttl-add-moder')"
         data-modal-body="@lang('speechlist.modal.txt-add-moder', ['TopicName' => $topic->name])"
         data-ok-func="objAction"
         data-ok-args='{"url": "{{ route('user::speechlist.store', [$event, $topic]) }}", "method": "post", "data": {"delegate_id": {{ $delegate->id }} }}'>
        <i class="fa fa-plus material-icons"></i></a>
      <div is="modalSpeechlist" v-bind:data="modalSpeechlist" v-on:modal-shown="shownModal">
        <select class="mdb-select" slot="delegates" name="delegate_id" required>
          <option value="" disabled selected>@lang('speechlist.modal.txt-add-moder-label')</option>
          @foreach($speech_delegates as $delegate_to_speak)
            <option value="{{ $delegate_to_speak->id }}">{{ $delegate_to_speak->user->name }} ({{ $delegate_to_speak->user->union }})</option>
          @endforeach
        </select>

      </div>
    @else
      <a class="float-right mdl-button mdl-button--fab mdl-button--colored amber darken-4" href="#"
         v-on:click="showModal" data-modal-target="modal"
         data-modal-title="@lang('speechlist.modal.ttl-add-myself')"
         data-modal-body="@lang('speechlist.modal.txt-add-myself', ['TopicName' => $topic->name])"
         data-ok-func="objAction"
         data-ok-args='{"url": "{{ route('user::speechlist.store', [$event, $topic]) }}", "method": "post", "data": {"delegate_id": {{ $delegate->id }} }}'>
        <i class="fa fa-plus material-icons"></i></a>
    @endcan
  @endcan
@endsection

@section('scripts')
  <script>
    $().ready(function() {
      $('#data-table').DataTable({
        paging: {{ ($speechlist->count() >= $pageItems) ? 'true' : 'false' }},
      });
    });
  </script>
@endsection

