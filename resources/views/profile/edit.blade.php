@extends('layouts.app')

@section('breadcrumbs')
  <ol class="breadcrumb transparent">

    <li class="breadcrumb-item">@lang('users.profile', ['name' => $user->name])</li>
    <li class="breadcrumb-item active">@lang('users.edit')</li>
  </ol>
@endsection

@section('content')
  <form class="form-horizontal" role="form" method="POST" action="{{ route('profile.update') }}">
    <div class="row">
      <div class="col-12 col-md-6">
        {{ csrf_field() }}
        {{ method_field('PUT') }}

        <div class="mb-2"><h3>@lang('users.user-details')</h3></div>

        <div class="row">
          <div class="col-12 col-sm-6">
            <div class="md-form form-group{{ $errors->has('first_name') ? ' has-danger' : '' }} ">
              <input id="user-first_name" type="text"
                     class="form-control{{ $errors->has('first_name') ? ' form-control-danger' : '' }}"
                     name="first_name" value="{{ old('first_name', $user->first_name) }}" required maxlength="255">
              <label for="user-first_name">@lang('users.lbl.first-name')</label>
              @if ($errors->has('first_name'))
                <div class="form-control-feedback">{{ $errors->first('first_name') }}</div>
              @endif
            </div>
          </div>
          <div class="col-12 col-sm-6">
            <div class="md-form form-group{{ $errors->has('last_name') ? ' has-danger' : '' }} ">
              <input id="user-last_name" type="text"
                     class="form-control{{ $errors->has('last_name') ? ' form-control-danger' : '' }}"
                     name="last_name" value="{{ old('last_name', $user->last_name) }}"
                     @if(!$user->isGlobalAdmin()) required @endif maxlength="255">
              <label for="user-last_name">@lang('users.lbl.last-name')</label>
              @if ($errors->has('last_name'))
                <div class="form-control-feedback">{{ $errors->first('last_name') }}</div>
              @endif
            </div>
          </div>
        </div>

        <div class="md-form form-group{{ $errors->has('email') ? ' has-danger' : '' }} ">
          <input id="user-email" type="email"
                 class="form-control{{ $errors->has('email') ? ' form-control-danger' : '' }}"
                 name="email" value="{{ old('email', $user->email) }}" required maxlength="255">
          <label for="user-email">@lang('users.lbl.email')</label>
          @if ($errors->has('email'))
            <div class="form-control-feedback">{{ $errors->first('email') }}</div>
          @endif
        </div>

      </div>

      <div class="col-12 col-md-6">
        <div class="mb-2"><h3>@lang('users.user-union')</h3></div>

        <div class="md-form form-group{{ $errors->has('union') ? ' has-danger' : '' }} ">
          <input id="user-union" type="text"
                 class="form-control{{ $errors->has('union') ? ' form-control-danger' : '' }}"
                 name="union" value="{{ old('union', $user->union) }}"
                 @if(!$user->isGlobalAdmin()) required @endif maxlength="255">
          <label for="user-union">@lang('users.lbl.union')</label>
          @if ($errors->has('union'))
            <div class="form-control-feedback">{{ $errors->first('union') }}</div>
          @endif
        </div>

        <div class="row">
          <div class="col-12 col-sm-6">
            <div class="md-form form-group{{ $errors->has('branch') ? ' has-danger' : '' }} ">
              <input id="user-branch" type="text"
                     class="form-control{{ $errors->has('branch') ? ' form-control-danger' : '' }}"
                     name="branch" value="{{ old('branch', $user->branch) }}" maxlength="255">
              <label for="user-branch">@lang('users.lbl.branch')</label>
              @if ($errors->has('branch'))
                <div class="form-control-feedback">{{ $errors->first('branch') }}</div>
              @endif
            </div>
          </div>
          <div class="col-12 col-sm-6">
            <div class="md-form form-group{{ $errors->has('delegate_id') ? ' has-danger' : '' }} ">
              <input id="user-delegate_id" type="text"
                     class="form-control{{ $errors->has('delegate_id') ? ' form-control-danger' : '' }}"
                     name="delegate_id" value="{{ old('delegate_id', $user->delegate_id) }}" maxlength="255">
              <label for="user-delegate_id">@lang('users.lbl.delegate-id')</label>
              @if ($errors->has('delegate_id'))
                <div class="form-control-feedback">{{ $errors->first('delegate_id') }}</div>
              @endif
            </div>
          </div>
        </div>
      </div>

      <div class="col-12 col-lg-6">
        <div class="mt-2 mb-1"><h3>@lang('users.password')</h3></div>
        <div class="row">
          <div class="col-12 col-sm-6">
            <div class="md-form form-group{{ $errors->has('password') ? ' has-danger' : '' }} ">
              <input id="user-password" type="password"
                     class="form-control{{ $errors->has('password') ? ' form-control-danger' : '' }}"
                     name="password" maxlength="255">
              <label for="user-password">@lang('users.lbl.password-wth-hint')</label>
              @if ($errors->has('password'))
                <div class="form-control-feedback">{{ $errors->first('password') }}</div>
              @endif
            </div>
          </div>
          <div class="col-12 col-sm-6">
            <div class="md-form form-group{{ $errors->has('password_confirmation') ? ' has-danger' : '' }} ">
              <input id="user-password-confirmation" type="password"
                     class="form-control{{ $errors->has('password_confirmation') ? ' form-control-danger' : '' }}"
                     name="password_confirmation" maxlength="255">
              <label for="user-password-confirmation">@lang('users.lbl.password-confirmation')</label>
              @if ($errors->has('password_confirmation'))
                <div class="form-control-feedback">{{ $errors->first('password_confirmation') }}</div>
              @endif
            </div>
          </div>
        </div>
      </div>

      <div class="col-12 mt-1">
        <div class="text-center">
          <input type="hidden" name="client_id" value="{{ $user->client_id }}">
          <button class="btn btn-default" type="reset">@lang('app.btn.reset')</button>
          <button class="btn btn-amber" type="submit">@lang('app.btn.save')</button>
        </div>
      </div>

    </div>
  </form>
@endsection

