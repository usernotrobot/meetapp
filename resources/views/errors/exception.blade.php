@if (config('app.debug') && $exception)
  @php($e = \App\Exceptions\FlattenExceptionOnSteroids::create($exception))

  <div class="container">
    <div class="mx-auto text-center">
      <a style="cursor: pointer;"
         onclick="document.getElementById('exception-details').style.display='block'; this.style.display='none'">@lang('app.err.details')</a></div>

    <div id="exception-details" style="display: none" class="mb-3">
      <p>
        <b>{{ $e->getClassName() }} ({{ $e->getStatusCode() }}):</b>
        in {{ $exception->getFile() }} line {{ $exception->getLine() }}:
        <br/>
        {{ $exception->getMessage() }}
      </p>

      @foreach($e->getTraceAsArray() as $id => $trace)
        <p>
          <small>
            <b>#{{ $id }}</b>
            @if (isset($trace['file']))
              {{ $trace['file'] }} ({{ $trace['line'] }}): <br/>
            @endif
            @if (isset($trace['function']))
              at {!! $trace['function'] !!}
            @endif
          </small>
        </p>
      @endforeach
    </div>
  </div>
@endif
