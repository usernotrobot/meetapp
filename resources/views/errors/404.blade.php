@extends('layouts.fatal')

@section('content')
  <div class="flex-column mt-5">
    <div class="flex-row flex-center">
      <img src="/images/404.png" alt="Error 404" class="mt-5">
    </div>
    <div class="flex-row mt-3 mb-r text-center">
      <h1 class="h1-responsive">@lang('app.err.404.title')</h1>
      <h5 class="h5-responsive mt-2">@lang('app.err.404.desc')</h5>
      <h5 class="h5-responsive mt-2" onclick="window.history.back();" style="cursor: pointer;">@lang('app.err.action')</h5>
    </div>
  </div>
  @include('errors.exception')
@endsection
