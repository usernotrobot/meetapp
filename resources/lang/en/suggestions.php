<?php

return [
  'index' => 'Suggestions',
  'add' => 'Add',
  'create' => 'Add new Suggestion',
  'edit' => 'Edit Suggestion',
  'show' => 'Suggestion details',
  'my' => 'My Suggestions',

  'lbl' => [
    'id' => '',
    'topic' => 'Topic',
    'count' => 'Suggestions',
    'last-author' => 'Last Suggestion',

    'suggestion-count' => '{0} No suggestions|{1} One suggestion|[2,*] :count suggestions',

    'suggestion' => 'Suggestion',
    'author' => 'From',
    'when' => 'When',
    'added' => 'Added',

    'summary' => 'Summary',
    'details' => 'Details',
  ],

  'nav' => [
    'edit' => 'Edit',
    'delete' => 'Delete',
    'view' => 'Details',
    'add' => 'Add'
  ],

  'modal' => [
    'ttl-delete' => 'Delete Suggestion',
    'txt-delete' => 'Are you sure you want to delete suggestion &laquo;:Summary&raquo; from :User?',
  ],

  'msg' => [
    'create-success' => 'Suggestion was successfully created',
    'update-success' => 'Suggestion was successfully updated',
    'delete-success' => 'Suggestion was successfully deleted',
  ],

  'btn' => [
    'back' => 'Back to topics suggestion list'
  ],
];
