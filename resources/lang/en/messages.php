<?php

return [
  'action-fail' => 'Oops! Something went wrong. Unable to complete action',
  'status-change-fail' => 'Oops! Something went wrong. Unable to update status',

  'datatable' => [
    'search-placeholder' => 'Search..',
    'info' => 'Showing _START_ to _END_ of _TOTAL_ entries',
    'info-empty' => "No entries to show",
    'info-filtered' => '',
  ],

  /* Modals */
  'mdl' => [
    'confirm-title' => 'Confirm',
    'confirm-body' => 'Are you sure you want to proceed?',
    'confirm-btn-ok' => 'Ok',
    'confirm-btn-cancel' => 'Cancel',

    'upload-title' => 'Upload file ',
    'upload-body' => 'Select file to upload',
    'upload-btn-ok' => 'Upload',
    'upload-btn-cancel' => 'Cancel',
    'upload-btn-select' => 'Select',
    'upload-btn-placeholder' => 'Choose your file',
    'upload-name-label' => 'Enter a name for your file (optional)',

    'delegates-upload-title' => 'Upload delegates file ',
    'delegates-upload-body' => 'Select file to upload',
    'delegates-upload-btn-ok' => 'Upload',
    'delegates-upload-btn-cancel' => 'Cancel',
    'delegates-upload-btn-select' => 'Select',
    'delegates-upload-btn-placeholder' => 'Choose file with delegates list',

    'speechlist-title' => 'Add to Speech List',
    'speechlist-body' => 'Choose user to add to this Speech List:',
    'speechlist-btn-ok' => 'Add',
    'speechlist-btn-cancel' => 'Cancel',

    'speaker-add' => [
      'ttl' => 'Add speaker',
      'btn-ok' => 'Accept',
      'btn-cancel' => 'Decline',
      'success' => 'User was successfully added to speechlist',
    ],

    'suggestions' => [
      'ttl-add' => 'Add new suggestion',
      'ttl-view' => 'View more',
      'ttl-edit' => 'Edit suggestion',

      'lbl-details' => 'Details',
      'lbl-summary' => 'Summary',

      'btn-save' => 'Save',
      'btn-decline' => 'Decline',

      'action' => [
        'success-update' => 'Suggestion was successfully updated',
        'success-store' => 'Suggestion was successfully created',
      ],
    ],
    'files' => [
      'ttl-delete' => 'Delete File',
      'txt-delete' => 'Are you sure you want to delete file «:name»? This action cannot be undone. '
    ]
  ],

  'validation' => [
    'type' => 'Please only submit files of type: :types',
    'size' => 'File size should be less than :size',
  ],

  'moderation' => [
	  'msg'=> [
		  'create-success' => 'Topic was successfully created',
	  ],
	  'topic' => [
		  'add-suggestion'=> 'Add to new list',
		  'remove-suggestion' => 'Remove from list',
	  ],
  ],

  'headlines' => [
    'ttl-delete' => 'Delete Headline Topic',
    'txt-delete' => 'Are you sure you want to delete headline topic «:Name»?',

    'ttl-restore' => 'Restore Headline Topic',
    'txt-restore' => 'Are you sure you want to restore headline topic «:Name»?',

    'move-success' => 'New position saved'
  ],

  'now' => [
    'suggestions' => [
      'ttl-count-0' => 'No suggestions',
      'ttl-count' => 'One suggestion|:suggestions suggestions',
      'ttl-delegates' => ' from 1 delegate| from :delegates delegates',

      'empty' => 'No suggestions yet',
      'empty-can-add' => 'Be first?',

      'btn-more' => 'More',
      'btn-edit' => 'Edit',
      'btn-delete' => 'Delete',

      'mdl-ttl-delete' => 'Delete Suggestion',
      'mdl-txt-delete' => 'Are you sure you want to delete suggestion «:summary» from :user?',
      'mdl-txt-delete-own' => 'Are you sure you want to delete your suggestion «:summary»?',
    ],

    'topic' => [
      'btn-to-discussion' => 'Discussion',
      'btn-to-voting' => 'Voting',
      'btn-to-closed' => 'Close',

      'btn-change-topic' => 'Change Topic',
      'btn-new-topic' => 'Create Topic',
      'btn-settings' => 'Topic Settings',

      'status-change-success' => 'Topic status was successfully changed',
      'status-change-fail' => 'Oops! Something went wrong. Unable to change topic status',

      'settings-update-success' => 'Topic settings were successfully changed',
      'settings-update-fail' => 'Oops! Something went wrong. Unable to change topic settings',

      'files-count' => 'Materials (1 file)|Materials (:count files)',

      'lbl-speech-opened' => 'Allow to add a delegate to speech list',
      'lbl-speech-times' => 'The number of times that delegate can speak',
      'lbl-speech-length' => 'The maximum length of delegate\'s speech (in minutes)',

      'lbl-speech-time-first' => 'First',
      'lbl-speech-time-second' => 'Second',
      'lbl-speech-time-third' => 'Third',
      'lbl-speech-time-other' => 'th',

      'err-speech-length' => 'Speech length cannot be empty',

      'settings-title' => 'Topic Settings',
      'btn-ok' => 'Save',
      'btn-cancel' => 'Discard'
    ],

    'speechlist' => [
      'ttl-count-0' => 'No next speakers',
      'ttl-count' => 'The last speaker|Next Speakers (:count delegates)',

      'btn-start' => 'Start to speak',
      'btn-stop' => 'Stop speaker',

      'lbl-current-speaker' => 'Now speaking',
      'lbl-next-speaker' => 'Next speaker',
      'lbl-no-speaker' => '< Idle >',

      'mdl-ttl-add-myself' => 'Add to Speech List',
      'mdl-txt-add-myself' => 'Are you sure you want to be added to speechlist for topic «:topicName»?',

      'mdl-ttl-delete' => 'Delete Speaker',
      'mdl-txt-delete' => 'Are you sure you want to remove user «:user» from Speech List?',

      'mdl-ttl-delete-myself' => 'Delete Speaker',
      'mdl-txt-delete-myself' => 'Are you sure you want to be removed from Speech List?',

      'mdl-ttl-add-moder' => 'Add Speaker',
      'mdl-txt-add-moder' => 'Select user you want to add to «:topicName» topic\'s Speech List' ,
      'mdl-txt-add-moder-label' => 'Choose delegate',

      'mdl-ttl-mark-as-last' => 'Mark as Last Speaker',
      'mdl-txt-mark-as-last' => 'Are you sure you want to mark :user (#:number) as the last speaker in the Speech List for topic «:topicName»?',

      'mdl-ttl-length' => 'Edit Speech Length',
      'mdl-txt-length' => 'Choose speech length for :user speech:',

      'mdl-ttl-priority' => 'Update Priority',
      'mdl-txt-priority' => 'Enter number position for :user speech',

      'start-success' => 'Speech was successfully started',
      'start-fail' => 'Unable to start this speech',

      'stop-success' => 'Speech was successfully stopped',
      'stop-fail' => 'Unable to stop this speech',

      'empty' => 'No speakers yet',
      'empty-can-add' => 'Can you be next?',

      'delete' => 'Delete',
      'mark-last' => 'Mark As Last Speaker',
      'edit-time' => 'Edit time',
      'priority' => 'Change priority',
    ],

    'voting-start' => [
      'title' => 'Put to the vote',

      'lbl-length' => 'Voting length',

      'lbl-type-header' => 'Voting type',
      'lbl-type-auto' => 'Electronic',
      'lbl-type-manual' => 'Manual',

      'lbl-privacy-header' => 'Voting method',
      'lbl-privacy-open' => 'Open',
      'lbl-privacy-secret' => 'Secret',

      'lbl-answers' => 'Answers',
      'btn-answers-add' => 'Add',

      'tag-answers-yes' => 'Yes',
      'tag-answers-no' => 'No',
      'tag-answers-abstained' => 'Abstained',

      'err-tags-empty' => 'Please, enter at least two voting options',

      'btn-cancel' => 'Decline',
      'btn-ok' => 'Start',

      'success-start' => 'The voting has been successfully started',
      'fail-start' => 'Oops! Something went wrong while starting voting for this topic',
    ],

    'vote' => [
      'title' => 'Vote',

      'btn-cancel' => 'Cancel and set topic to opened',
      'btn-finish' => 'Finish',
      'btn-close' => 'Close Topic',

      'lbl-results' => 'Voted :votes of :total delegates',
      'lbl-manual-voting-moderator' => 'Please, set voting results:',
      'lbl-manual-voting-delegate' => 'Please, show your opinion to moderator. Possible voting results:',

      'lbl-no-rights' => 'Unfortunately you have no rights to vote now',

      'cancel-fail' => 'Oops! Something went wrong while canceling voting',
      'cancel-success' => 'The voting was successfully canceled. Topic status changed to opened',

      'finish-fail' => 'Oops! Something went wrong while finishing voting',
      'finish-success' => 'The voting was successfully finished. Results will appear in a few moments.',

      'vote-fail' => 'Oops! Something went wrong while saving your vote',
    ],
  ],
];
