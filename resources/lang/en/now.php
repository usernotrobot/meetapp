<?php

return [
  'lbl' => [
    'speaking' => 'Now speaking',
    'next-speaker' => 'Next speaker',
    'no-speaker' => '< Idle >',

    'materials' => '{0}|{1}Materials (1 file)|[2,*]Materials (:count&nbsp;files)',

    'speechlist' => '{0} No next speakers|{1} The last speaker|[2,*] Next Speakers (:count&nbsp;delegates)',

    'empty-speechlist' => 'No speakers yet',
    'empty-speechlist-add' => 'No&nbsp;speakers&nbsp;yet<br/>Can&nbsp;you&nbsp;be&nbsp;next?',
    'empty-suggestions' => 'No suggestions yet',
    'empty-suggestions-add' => 'No&nbsp;suggestions&nbsp;yet<br/>Be&nbsp;first?',

  ],

  'btn' => [
    'change' => 'Change Topic',
    'voting' => 'Voting',
    'settings' => 'Settings',

    'more' => 'More',
    'edit' => 'Edit',
    'delete' => 'Delete',
  ],

  'modal' => [

  ]
];
