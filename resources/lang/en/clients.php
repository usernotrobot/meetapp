<?php
return [
  'index' => 'Manage Clients',
  'create' => 'Add new',
  'edit' => 'Edit',

  'empty' => 'There are no clients added yet',

  'client-details' => 'Client Details',
  'admin-details' => 'Admin Details',

  'nav' => [
    'edit' => 'Edit',
    'delete' => 'Delete',
    'restore' => 'Restore',

    'add-event' => 'Add new Event',
  ],

  'lbl' => [
    'id' => '#',
    'name' => 'Name',
    'description' => 'Description',
    'users' => 'Users',
    'events' => 'Events',
  ],

  'modal' => [
    'ttl-delete' => 'Delete Client',
    'txt-delete' => 'Are you sure you want to delete client &laquo;:Name&raquo;?',

    'ttl-restore' => 'Restore Client',
    'txt-restore' => 'Are you sure you want to restore client &laquo;:Name&raquo;?'
  ],

  'msg' => [
    'create-success' => 'Client was successfully created',
    'create-fail' => 'Oops! Something went wrong. Unable to create client',

    'update-success' => 'Client was successfully updated',

    'delete-fail' => 'Oops! Something went wrong. Unable to delete client',
    'delete-success' => 'Client was successfully deleted',

    'restore-fail' => 'Oops! Something went wrong. Unable to restore client',
    'restore-success' => 'Client was successfully restored',
  ],
];
