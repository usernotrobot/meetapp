<?php

return [
  'nav' => [
    'toggle' => 'Toggle Navigation',
    'profile' => 'My profile',
    'search' => 'Search All..',

    'events' => 'Events List',
    'clients' => 'Manage Clients',

    'user' => [
      'active-event' => 'ACTIVE EVENTS',
      'completed-event' => 'PAST',
      'planned-event' => 'UPCOMING',

      'now' => 'NOW',
      'agenda' => 'Agenda',
      'topic' => 'Topic List',
      'speech' => 'Speech List',
      'suggestions' => 'Suggestions',
      'my-suggestions' => 'My suggestions',
      'files' => 'Event PDF',

      'documents' => 'DOCUMENTS',
      'speechlist' => 'SPEECHLIST',

      'my-speechlist' => 'My speechlist',

      'events' => 'All Events',
      'events-upcoming' => 'Upcoming',
      'events-past' => 'Past',
    ],
  ],

  'status' => [
    'online' => 'Online',
    'away' => 'Away',
    'logout' => 'Logout',
  ],

  'mail' => [
    'header' => 'Messages',
    'inbox' => 'Inbox',
    'compose' => 'Compose'
  ],

  'btn' => [
    'save' => 'Save',
    'reset' => 'Reset',
    'cancel' => 'Cancel',
    'accept' => 'Accept',
    'change' => 'Change'
  ],

  'msg' => [
    'file-upload-success' => 'File uploaded successfully.',
    'file-upload-fail' => 'Unable to upload file. Something went wrong.',
    'post-fail' => 'Oops! Unable to process request. Max upload size exceeded (:size).',
  ],

  'err' => [
    'details' => 'Show details',
    'action' => 'Click here to go back.',

    '404' => [
      'title' => 'Page not found',
      'desc' => 'The page you are looking for is not found.',
    ],

    '403' => [
      'title' => 'Permission denied',
      'desc' => 'You are not authorized to view this page.',
      //'desc' => 'Maybe you\'re trying to access a resource that does not belong to you.',
    ],

    '500' => [
      'title' => 'Server Error',
      'desc' => 'Whoops, looks like something went wrong.',
    ],
  ],
];
