<?php

return [

  /*
  |--------------------------------------------------------------------------
  | Authentication Language Lines
  |--------------------------------------------------------------------------
  |
  | The following language lines are used during authentication for various
  | messages that we need to display to the user. You are free to modify
  | these language lines according to your application's requirements.
  |
  */

  'failed' => 'These credentials do not match our records.',
  'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',

  'password' => 'Your password',
  'password-confirm' => 'Confirm Password',
  'email' => 'Your email',
  'login' => 'Login',

  'forgot-password' => 'Forgot Your Password?',
  'remember' => 'Remember Me',

  'lost-pwd-text' => 'Lost your password? Please enter your email address.<br/>'
    . 'You will receive a link to create a new password via email.',
  'reset-pwd-btn' => 'Reset Password',
  'send-pwd-btn' => 'Send Password Reset Link',

];
