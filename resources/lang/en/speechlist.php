<?php

return [
  'index' => 'Speech List',
  'my' => 'My Speech List',

  'lbl' => [
    'id' => '',
    'topic' => 'Topic',
    'added' => 'Added',
    'user' => 'Delegate Name',
    'union' => 'Union',
    'length' => 'Length',
    'priority' => 'Priority',

    'action' => 'Action',
  ],

  'nav' => [
    'edit' => 'Edit',
    'delete' => 'Delete',
    'markLast' => 'Mark As Last Speaker',
    'add' => 'Add me',

    'edit-time' => 'Edit time',
    'priority' => 'Change priority',
  ],

  'modal' => [
    'ttl-delete' => 'Delete Speaker',
    'txt-delete' => 'Are you sure you want to remove user &laquo;:User&raquo; from Speech List?',

    'ttl-delete-myself' => 'Delete Speaker',
    'txt-delete-myself' => 'Are you sure you want to be removed from Speech List?',

    'ttl-add-myself' => 'Add to Speech List',
    'txt-add-myself' => 'Are you sure you want to be added to speechlist for topic &laquo;:TopicName&raquo;?',

    'ttl-add-moder' => 'Add Speaker',
    'txt-add-moder' => 'Select user you want to add to &laquo;:TopicName&raquo; topic\'s Speech List' ,
    'txt-add-moder-label' => 'Choose delegate',

    'ttl-mark-as-last' => 'Mark as Last Speaker',
    'txt-mark-as-last' => 'Are you sure you want to mark :User (#:number) as the last speaker in the Speech List for topic &laquo;:TopicName&raquo;?',

    'ttl-length' => 'Edit Speech Length',
    'txt-length' => 'Choose speech length for :User speech:',

    'ttl-priority' => 'Update Priority',
    'txt-priority' => 'Enter number position for :User speech',
  ],

  'msg' => [
    'create-success' => 'The user was successfully added to speechlist',
    'delete-success' => 'The user was successfully removed from speechlist',

    'speech-times-exceed' => 'Unable to add user to speechlist. Maximum speech times (:max) has been exceeded.',

    'mark-last-error' => 'Oops! Something went wrong. Unable to mark as last speaker',
    'mark-last-success' => 'The user :Name successfully marked as last in current Speech List',

    'update-length-success' => 'Speech Length was successfully updated',
    'update-length-fail' => 'Oops! Something went wrong. Unable to update Speech Length for this speaker',

    'update-priority-success' => 'Speech Priority was successfully updated',
    'update-priority-fail' => 'Oops! Something went wrong. Unable to update Speech Priority for this speaker',

    'topic-discussion-closed' => 'Upable to add user to the speechlist. Topic discussion is closed'
  ],
];
