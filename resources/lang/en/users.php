<?php

return [
  'user-details' => 'User Details',
  'user-union' => 'Union/Company Details',
  'password' => 'Change password',

  'profile' => ':Name profile',
  'edit' => 'Edit',

  'lbl' => [
    'first-name' => 'First Name',
    'last-name' => 'Last Name',
    'name' => 'Full Name',
    'email' => 'Email',

    'password' => 'Password',
    'password-confirmation' => 'Confirm Password',

    'password-wth-hint' => 'Password (type to set new)',

    'union' => 'Union/Company',
    'union-sort-key' => 'Union/Company Sort Key',
    'branch' => 'Branch/Local',
    'branch-sort-key' => 'Branch/Local Sort Key',
    'delegate-id' => 'Delegate ID',
    'user-token' => 'API Token',
  ],

  'msg' => [
    'user-exists' => 'Unable to create user with such email',

    'profile-update-success' => 'Your profile was successfully updated',
    'profile-update-fail' => 'Something went wrong while updating your profile',
  ]
];
