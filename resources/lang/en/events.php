<?php
return [
  'index' => 'Events',
  'create' => 'Add new',
  'edit' => 'Edit',
  'files' => 'Files',

  'empty' => 'There are no events added yet',
  'headlines-header' => 'Agenda:',
  'headlines-details' => 'Details',

  'nav' => [
    'view' => 'View',
    'topics' => 'Topics',
    'headlines' => 'Hedline Topics',
    'delegates' => 'Delegates',
    'files' => 'Files',

    'delete' => 'Delete',
    'restore' => 'Restore',

    'completed' => 'Mark as completed',
    'active' => 'Mark as active',
    'planned' => 'Mark as planned',

    'summary' => 'Summary',

  ],

  'lbl' => [
    'name' => 'Event name',
    'union' => 'Union',
    'start' => 'Start date',
    'end' => 'End date',
    'description' => 'Description',
    'status' => 'Status',

    'client' => 'Client',
    'client_id' => 'Client',
    'choose-client' => '-- Choose client --',
    'topics-count' => '{0} No topics|{1} One topic|[2,*] :count topics',
  ],

  'modal' => [
    'ttl-change' => 'Change Event',
    'txt-change' => 'Are you sure you want to mark event &laquo;:Name&raquo; as :status?',

    'ttl-delete' => 'Delete Event',
    'txt-delete' => 'Are you sure you want to delete event &laquo;:Name&raquo;?',

    'ttl-restore' => 'Restore Event',
    'txt-restore' => 'Are you sure you want to restore event &laquo;:Name&raquo;?'
  ],

  'status' => [
    'planned' => 'planned',
    'active' => 'active',
    'completed' => 'completed'
  ],

  'msg' => [
    'create-success' => 'Event was successfully created',
    'update-success' => 'Event was successfully updated',

    'status-change-success' => 'Event status was successfully updated',

    'delete-fail' => 'Oops! Something went wrong. Unable to delete this event',
    'delete-success' => 'Event was successfully deleted',

    'restore-fail' => 'Oops! Something went wrong. Unable to restore this event',
    'restore-success' => 'Event was successfully restored',
  ],
];
