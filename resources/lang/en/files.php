<?php

return [
  'nav' => [
    'download' => 'Download',
    'delete' => 'Delete'
  ],

  'lbl' => [
    'id' => '#',
    'name' => 'Filename',
    'date' => 'Modified',
    'user' => 'User',
  ],

  'modal' => [
    'ttl-delete' => 'Delete File',
    'txt-delete' => 'Are you sure you want to delete file &laquo;:name&raquo;? This action cannot be undone. '
  ],

  'msg' => [
    'upload-progress' => 'Uploading files...',
    'delete-fail' => 'Oops! Something went wrong. Unable to delete this file',
    'delete-success' => 'File was successfully deleted',
  ]
];
