<?php

return [
  'index' => 'Delegates',

  'user-details' => 'User Details',
  'user-union' => 'Union/Company Details',
  'user-event' => 'User Events Details & Rights',

  'lbl' => [
    'id' => '#',
    'name' => 'Name',
    'union' => 'Union/Company',
    'branch' => 'Branch/Local',

    'participant_id' => 'Participant ID',
    'participant-id' => 'Participant ID',

    'role' => 'Participant Type',
    'choose_role' => 'Choose Participant Type',

    'rights' => 'Special Rights',

    'right_submit' => 'Submit proposals',
    'right_speak' => 'Speak',
    'right_vote' => 'Voting rights',

    'notify' => 'Notify delegate about event'
  ],

  'role' => [
    'admin' => 'Admin (Manage Event)',
    'moderator' => 'Moderator',
    'secretary' => 'Secretary',
    'delegate' => 'Delegate',
    'official' => 'Official',
    'guest' => 'Guest',
    'projector' => 'Projector',
  ],

  'nav' => [
    'view' => 'Details',
    'edit' => 'Edit \ Update permissions',
    'delete' => 'Delete',

    'delete-all' => 'Bulk Delete',

    'upload' => 'Upload from file',
    'add' => 'Add new',
  ],

  'msg' => [
    'file-upload-success' => 'File processed successfully. Event delegates list is updated.',
    'file-upload-result' => 'File uploaded successfully. Processed :count record.| File uploaded successfully. Processed :count records.',

    'file-upload-fail' => 'Unable to process file. Something went wrong.',
    'file-upload-fail-no-entries' => 'Delegates upload failed. No entries were processed.',

    'user-exists' => 'Unable to create some delegates. These emails already belong to another delegates',
    'user-parse-fail' => 'Something went wrong. Unable to create\update user with email :email (:error)',
    'user-ok' => 'Delegates list was successfully updated with :count record|Delegates list was successfully updated with :count records',

    'create-success' => 'Delegate was successfully added',
    'create-fail' => 'Unable to create delegate',

    'update-success' => 'Delegate info was successfully updated',
    'delete-success' => 'Delegate &laquo;:Name&raquo; (:email) was successfully deleted from event',

    'bulk-delete-success' => 'ALL delegates were successfully removed from event',
  ],

  'modal' => [
    'ttl-delete' => 'Delete delegate',
    'txt-delete' => 'Are you sure you want to delete delegate &laquo;:Name&raquo; (:email) from this event?',
    'chkbx-delete' => 'Notify delegate',

    'ttl-bulk-delete' => 'Remove delegates',
    'txt-bulk-delete' => 'Are you sure you want to delete ALL delegates from this event? This action cannot be undone!',
    'chkbx-bulk-delete' => 'Notify delegates',
  ],

  'btn' => [
    'back' => 'Back to delegates list'
  ]
];
