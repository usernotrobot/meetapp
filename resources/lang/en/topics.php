<?php

return [
  'index' => 'Topics',

  'create' => 'Add new',
  'edit' => 'Edit',

  'details' => 'Topic Details',
  'settings' => 'Speech List Settings',

  'empty' => 'There are no topics added yet',

  'nav' => [
    'view' => 'View',
    'edit' => 'Edit',
    'upload' => 'Upload',
    'suggestion' => 'Suggestions',
    'speechlist' => 'Speechlist',
    'files' => 'Files',

    'delete' => 'Delete',
    'restore' => 'Restore',

    'new_create_topic' => 'Create a new topic from this...',

    'discussion' => 'Mark as Discussion',
    'voting' => 'Mark as Voting',
    'closed' => 'Mark as Closed',

    'settings' => 'Settings',
  ],

  'lbl' => [
    'name' => 'Topic name',
    'description' => 'Description',
    'status' => 'Status',

    'suggestion-summary' => ':suggestions&nbsp;suggestions from :delegates&nbsp;delegates',
    'suggestions' => '{0}No suggestions|{1}One suggestion|[2,*]:suggestions&nbsp;suggestions',
    'from-delegates' => '{0}|{1} from one delegate|[2,*] from&nbsp;:delegates&nbsp;delegates',
    'all-suggestions' => 'All suggestions',
    'last-suggestions' => 'Last suggestions',

    'files' => '{0}No files attached to this topic|{1}One file attached to this topic |[2,*] :count&nbsp;files attached to this topic',
    'materials' => '{0}|{1}Materials (1 file)|[2,*]Materials (:count&nbsp;files)',

    'speechlist' => '{0} No next speakers|{1} The last speaker|[2, *] Next Speakers (:count&nbsp;delegates)',
    'all-speechlist' => 'Full Speech List',
    'speechlist-more' => '.. and :count more',
    'speechlist-active' => '(speaking)',

    'speech_times' => 'The number of times that delegate can speak',
    'speech_length' => 'The maximum length of delegate\'s speech (in minutes)',
    'speech_length_i' => '{1} First time|{2} Second time |{3} Third time',

    'speech_opened' => 'Allow to add a delegate to speech list',

    'speech_length_item' => 'Max speech length',
  ],

  'modal' => [
    'ttl-create' => 'Create Topic',

    'ttl-change' => 'Change Topic',
    'txt-change' => 'Are you sure you want to mark topic &laquo;:Name&raquo; as :status?',

    'ttl-delete' => 'Delete Topic',
    'txt-delete' => 'Are you sure you want to delete topic &laquo;:Name&raquo;?',

    'ttl-restore' => 'Restore Topic',
    'txt-restore' => 'Are you sure you want to restore topic &laquo;:Name&raquo;?'
  ],

  'status' => [
    'open' => 'open',
    'discussion' => 'discussion',
    'voting' => 'voting',
    'closed' => 'closed'
  ],

  'msg' => [
    'create-success' => 'Topic was successfully created',
    'update-success' => 'Topic was successfully updated',

    'status-change-success' => 'Topic status was successfully changed',
    'status-change-fail' => 'Oops! Something went wrong. Unable to change status of topic «:Topic» to :status',

    'delete-success' => 'Topic &laquo;:Name&raquo; was successfully deleted',
    'restore-success' => 'Topic &laquo;:Name&raquo; was successfully restored',

    'settings-update-success' => 'Topic settings were successfully updated',
  ],

	'moderation' => [
		'topic' => [
			'add-suggestion'=> 'Add to new list',
			'remove-suggestion' => 'Remove from list',
	    'btn-save-topic' => 'Save Topic',
			'btn-cancel-topic'=> 'Cancel',
	    'label' => 'Name Topic',
	    'placeholder' => 'Create New Topic',
	    'settings' => 'Settings Topic',
	    'name-holder' => 'Create Name Topic',
	    'pickup-pdf' => 'Pickup PDF materials',
	    'preserve-list' => 'Preserve speechlist',
			'confirm-body'=>'Do you really want to save this topic without any suggestions ?'
		],
		'suggestion' => [
	    'title' => 'New suggestion list',
	    'drop-box' => 'DRAG AND DROP SUGGESTION BLOCK',
	    'more' => 'More'
		]
  ]
];
