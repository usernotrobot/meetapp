<?php
return [
  'msg' => [
    'unable-to-start' => 'Unable to start voting. It has been already started or topic is closed',
    'starting-fail' => 'Oops! Something went wrong while starting new voting.',
    'vote-finished' => 'Unable to save your vote. Voting is over'
  ],

  'lbl' => [
    'length' => 'Length',
    'type' => 'Type',
    'privacy' => 'Method',
    'answers' => 'Answers',
  ],
];
