<?php
return [
  'index' => 'Headline Topics',
  'create' => 'Create new',
  'edit' => 'Edit',
  'details' => 'Details',
  'empty' => 'There are no Headline Topics to display.',

  'lbl' => [
    'name' => 'Name',
    'description' => 'Description',
    'files' => 'Uploaded materials',
	  'subtopic' => 'Subtopic',
	  'droppable' => 'Drag and Drop Materials',
	  'move-topics' => 'Move selected topics to',
  ],

  'tbl' => [
    'first' => 'Headline Topic',
    'second' => 'Subtopics',
    'third' => 'Last changes',
  ],

  'nav' => [
    'view' => 'View',
    'edit' => 'Edit',
    'back' => 'Back',
    'upload' => 'Upload',
    'files' => 'Files',

    'new-topic' => 'Add new topic',
    'edit-headline' => 'Edit Headline Topic',

    'delete' => 'Delete',
    'restore' => 'Restore',
  ],

  'modal' => [
    'ttl-delete' => 'Delete Headline Topic',
    'txt-delete' => 'Are you sure you want to delete headline topic &laquo;:Name&raquo;?',

    'ttl-restore' => 'Restore Headline Topic',
    'txt-restore' => 'Are you sure you want to restore headline topic &laquo;:Name&raquo;?'
  ],

  'msg' => [
    'create-success' => 'Headline topic was successfully created',
    'update-success' => 'Headline topic was successfully updated',
    'delete-error' => 'Oops! Something went wrong. Headline topic &laquo;:name&raquo; have subtopics',

    'status-change-success' => 'Headline topic status was successfully changed',
    'status-change-fail' => 'Oops! Something went wrong. Unable to change status of headline topic «:topic» to :status',

    'delete-success' => 'Headline topic &laquo;:name&raquo; was successfully deleted',
    'restore-success' => 'Headline topic &laquo;:name&raquo; was successfully restored',
  ],

];
