<?php
return [
  'account-created' => [
    'subject' => 'Your new MyMeet account',
    'header' => 'Welcome to the ' . config('app.name') . ' application!',
    'body' => 'Your account was successfully created. Your temporary password is :password',
    'action' => 'Login',
    'footer' => 'Thank you for using our application!',
  ],

  'delegate-created' => [
    'subject' => 'You were added to an event',
    'header' => 'You were listed as participant in the event ":Name"',
    'body' => '',
    'action' => 'View your events',
    'footer' => 'Thank you for using our application!',
  ],

  'delegate-deleted' => [
    'subject' => 'You were removed from an event',
    'header' => 'You were removed as participant in the event ":Name"',
    'body' => '',
    'action' => 'View your events',
    'footer' => 'Thank you for using our application!',
  ],

];
