# MeetApp  

## System Requirements

To run the project you have to install following dependencies:

* PHP7 (OpenSSL, PDO, Mbstring, Tokenizer, XML, Zip PHP extensions)
* PHP-FPM (recommended) or Apache with mod_php
* Nginx web server (optional)
* Postgresql
* Redis 3+
* Node 5.0+
* Laravel Echo Server (socket.io)
* Zip\unzip as Composer requirements
* Memcached (optional)
* Supervisor (optional)

## Dev Prerequisites

This stuff is necessary to successfully compile project's front-end part in dev environment:

* [NPM](https://nodejs.org/en/download/package-manager/#debian-and-ubuntu-based-linux-distributions) (v6.x)

## Installation

1. Clone\pull project files into the necessary directory.
2. Configure your web server's document / web root to be the project's `public` directory.
3. Install necessary php dependencies: `php composer.phar install`.
4. Configure directories access rights.
    - directories within the `storage` and the `bootstrap/cache` directories should be writable by your web server.
    - create a symbolic link at `public/storage` which points to the `storage/app/public` directory. 
    You may create the link using the `php artisan storage:link` command.
5. Configure your database, create user and db for this project.   
6. Create `.env` with all necessary environment variables.
    - use `.env.example` for reference to fill all the settings including db, mail, memcached, etc.
    - set your application key to a random string `php artisan key:generate`.
7. Run all db migrations `php artisan migrate`.
8. Rebuild front-end (see below)

xx. To give your application a speed boost, you should cache all of your configuration files 
into a single file using the `php artisan config:cache` command (optional -- good for production)

xx. Create a route cache file for faster route registration using the `php artisan route:cache` command (optional -- good for production) 

8. Try to open your application in the web browser ^^

## Updating 
1. Run `php artisan down` to put the application into maintenance mode.
2. Update (pull\copy) project files from the vcs.
3. Install\update php dependencies `php composer.phar install`.
4. Update your `.env` if necessary (look for changes in `.env.example`)
5. Run new db migrations `php artisan migrate`
6. Rebuild front-end (see below)
7. Clear config and route caches if your cached them before `php artisan config:clear` and `php artisan route:clear`
8. Clear views cache by `php artisan view:clear` (optional)
9. Flush the application cache if necessary `php artisan cache:clear` 
10. Run `php artisan up` to bring the application out of maintenance mode
11. Restart your queue workers `php artisan queue:restart`

##### Upgrade to Laravel 5.4

* `php composer.phar install`
* `php artisan view:clear`
* `php artisan route:clear`

##### Rebuilding the front-end
1. Install all necessary npm packages and their dependencies according to package.json
`npm install -f`
2. Build front-end: 
    - for production `npm run production`
    - for dev `npm run dev`

## PHP/Web server configuration

Since the application allows to upload files up to 50Mb, 
php and web server settings should be tuned to permit uploading files of such size.

For instance, php.ini should be changed in such way:
* upload_max_filesize >= 50Mb
* post_max_size >= upload_max_size

## Queueing mails
It's better to use use queues to improve delegates upload speed and mail notifications sending.

Make sure that your `.env` contains `QUEUE_DRIVER=redis` value. 

To start processing queue just run `php artisan queue:work --tries=3`

## Broadcasting Installation and Configuration
Redis should be installed.
Make sure that your `.env` contains `BROADCAST_DRIVER=redis`

We use Laravel Echo Server (socket.io based server) as broadcasting server for websockets.

Unfortunately, Laravel Echo Server should be installed globally. To do this - just run `npm install -g laravel-echo-server`.
After that server should be initialized and it's config created. Run the init command in your project directory:
`laravel-echo-server init`

The cli tool will help you setup a `laravel-echo-server.json` file in the root directory of your project. This file will be loaded by the server during start up. You may edit this file later on to manage the configuration of your server.

Also, config example is available here: `laravel-echo-server.example.json` 

Currently, we use hardcoded clients' app_id and key that can be found in the example file. So please, update your `laravel-echo-server.json` file with those settings from example. 

To run the server, run in your project root directory `laravel-echo-server start`

More info about configuration is available by this [link](https://github.com/tlaverdure/laravel-echo-server).

It's a good idea to run Echo Server with the Supervisor.
  
### Supervisor Configuration
Supervisor is a process monitor for the Linux operating system, and will automatically restart your `queue:work` process if it fails.

Supervisor configuration files are typically stored in the 
`/etc/supervisor/conf.d` directory. Within this directory, 
you may create any number of configuration files that instruct supervisor 
how your processes should be monitored. For example, let's create a 
`mymeet-worker.conf` file that starts and monitors a `queue:work` process:

```
[program:mymeet-worker]
process_name=%(program_name)s_%(process_num)02d
command=php /home/www/mymeet.com/artisan queue:work --sleep=3 --tries=3
autostart=true
autorestart=true
user=www
numprocs=8
redirect_stderr=true
stdout_logfile=/home/www/mymeet.com/worker.log
```
In this example, the numprocs directive will instruct Supervisor to run 8 
queue:work processes and monitor all of them, automatically restarting them 
if they fail. Of course, you should change the `queue:work` portion of the 
command directive to reflect your desired queue connection.

For more information on Supervisor, consult the [Supervisor documentation](http://supervisord.org/index.html).


### Useful Docker commands (DEV instance)

```docker volume create -d local-persist --name=mymeet-dev -o mountpoint=/home/www/meetapp.gnolltech.com```
```docker run --rm -it -v mymeet-dev:/home/app docker.gnoll.tech/mymeet-dev:0.9.0rc3 /bin/bash```
```docker create --name mymeet-dev -p 127.0.0.1:6002:6001 -v mymeet-dev:/home/app docker.gnoll.tech/mymeet-dev:0.9.0```
```docker start mymeet-dev```

