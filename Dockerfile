FROM debian:jessie-backports

MAINTAINER Gleb Golubitsky <sectoid@gnolltech.com>

# Non-interactive frontend for debian stuff to reduce error noise
ENV DEBIAN_FRONTEND noninteractive

# Update the base image
RUN apt-get update

# Install basic essentials
RUN apt-get install -y apt-utils curl wget locales mc sudo

# Setup the locale
RUN echo "en_US.UTF-8 UTF-8" > /etc/locale.gen && \
    locale-gen en_US.UTF-8 && \
    dpkg-reconfigure locales && \
    /usr/sbin/update-locale LANG=en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8

# Setup PHP7 repository
RUN curl https://www.dotdeb.org/dotdeb.gpg | apt-key add -
RUN echo "deb http://packages.dotdeb.org jessie all" > /etc/apt/sources.list.d/dotdeb.list

# Setup PostgreSQL repository
# It should be the same key as https://www.postgresql.org/media/keys/ACCC4CF8.asc
RUN curl https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add -
RUN echo "deb http://apt.postgresql.org/pub/repos/apt/ jessie-pgdg main" > /etc/apt/sources.list.d/pgdg.list

# Setup Nodejs Repository
RUN curl -sL https://deb.nodesource.com/setup_6.x | bash -

# Update the base image
RUN apt-get update
RUN apt-get upgrade -y

# Install PHP7 FPM + deps
RUN apt-get install -y php7.0-fpm php7.0-cli php-pdo php-mbstring php-tokenizer php-xml php-pgsql php7.0-memcached php-zip

# Install Memcached
RUN apt-get install -y memcached

# Install PostgreSQL 9.6
RUN apt-get install -y postgresql-9.6

# Install Supervisor daemon
RUN apt-get install -y supervisor

# Install Redis server & tools
RUN apt-get install -y redis-server redis-tools

# Install NPM & Nodejs
RUN apt-get install -y nodejs

# Install Laravel echo server
RUN npm install -g laravel-echo-server

# Make application user root directory
RUN useradd -M -c 'MyMeet Application' -s /bin/bash app
RUN mkdir /home/app && chown app.app /home/app

# Cleanup & configure PHP FPM
RUN rm /etc/php/7.0/fpm/pool.d/www.conf
COPY deploy/fpm/fpm.conf /etc/php/7.0/fpm/pool.d/app.conf

# Cleanup & configure PostgreSQL
RUN pg_dropcluster --stop 9.6 main && \
    mkdir -p /home/app/db/9.6 && \
    mkdir -p /home/app/log && \
    pg_createcluster \
        -u app -g app \
        -d /home/app/db/9.6 \
        -l /home/app/log/postgresql-9.6.log \
        --locale en_US.UTF-8 \
        -p 5432 \
        --start-conf manual 9.6 app && \
    rm -rf /home/app/db /home/app/log

# Configure Memcached
RUN sed -i 's/-m 64/-m 256/g' /etc/memcached.conf && \
    sed -i 's:/var/log/memcached.log:/home/app/memcached.log:g' /etc/memcached.conf

# Configure Redis
RUN sed -i 's:daemonize yes:daemonize no:g' /etc/redis/redis.conf

# Configure suprtvisor to start everything correctly
COPY deploy/supervisor/*.conf /etc/supervisor/conf.d/

# Run supervisor to manage all the components
CMD ["supervisord", "-n", "-c", "/etc/supervisor/supervisord.conf"]
