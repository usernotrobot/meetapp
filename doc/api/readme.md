## Example of code

    const socket = io('{WEBSOCKET}', {
        'secure': true,
        'reconnect': true,
        'reconnection delay': 500,
        'max reconnection attempts': 10,
    });
    socket.emit('subscribe', {
        channel: '{CHANNEL}',
        auth: {
            "headers": {"Authorization": 'Bearer {USER_TOKEN}'}
        }
    });
    socket.on('{EVENT}', function (channel, data) {
        console.log(channel);
        console.log(data);
    });

## Options
> **{WEBSOCKET}** - address of websocket ( Example: ws://meetapp.loc:6001 )
> **{CHANNEL}** - what channel we have to join ( Example: private-topics.20 , where 20 - id of topic )
> **{USER_TOKEN}** - api token from users table ( See image: [Link](https://lh4.googleusercontent.com/I1Z4eCHITQe_TdH5WYjRyAvjmqSiWABVdXR7jZ5t4ywloDOnDhzc05im3rQMRk1bdab3Ea1EUmzrCog=w1920-h974-rw) )
> **{EVENT}** - what event we have to listen ( Example: App\\Events\\SuggestionEvent )

## Testing code



##### SuggestionEvent

    const socket = io('ws://meetapp.loc:6001', {
        'secure': true,
        'reconnect': true,
        'reconnection delay': 500,
        'max reconnection attempts': 10,
    });
    socket.emit('subscribe', {
        channel: 'private-topics.20',
        auth: {
            "headers": {"Authorization": 'Bearer qwerty123'}
        }
    });

    socket.on('App\\Events\\SuggestionEvent', function (channel, data) {
        console.log(channel);
        console.log(data);
    });

By using this code we will listen "SuggestionEvent" on "private-topics.20" channel, result of our listening will be:

    {
        action: "SuggestionCreated"
        socket: null
        suggestion: {
            details: "Asda"
            id: 24
            isShort: true
            owner_id: 3
            summary: "Asd"
            username: "Secretar secretar"
        }
    }

For _SuggestionEvent_ we have 3 types of action: _SuggestionDeleted, SuggestionUpdated, SuggestionCreated_.
`suggestion` Object is the same for all actions.


##### SpeechEvent

    const socket = io('ws://meetapp.loc:6001', {
        'secure': true,
        'reconnect': true,
        'reconnection delay': 500,
        'max reconnection attempts': 10,
    });
    socket.emit('subscribe', {
        channel: 'private-topics.20',
        auth: {
            "headers": {"Authorization": 'Bearer qwerty123'}
        }
    });

    socket.on('App\\Events\\SpeechEvent', function (channel, data) {
        console.log(channel);
        console.log(data);
    });


By using this code we will listen "SpeechEvent" on "private-topics.20" channel, result of our listening will be:

    {
        action: "SpeechDeleted",
        speechlist: [
            {
                active: false
                created_at: "2017-07-31 15:06:24"
                delegate_id: 1
                end: null
                id: 5
                length: 300
                name: "secretar secretar"
                order_key: 1
                start: null
                status: "waiting"
                time_left: "5:00"
                topic_id: 20
                updated_at: "2017-07-31 15:06:24"
            },
            {
                active: false
                created_at: "2017-07-31 15:06:24"
                delegate_id: 1
                end: null
                id: 5
                length: 300
                name: "secretar secretar"
                order_key: 1
                start: null
                status: "waiting"
                time_left: "5:00"
                topic_id: 20
                updated_at: "2017-07-31 15:06:24"
            }
        ]
        socket: null
    }

For _SpeechEvent_ we have 3 types of action: _SpeechCreated, SpeechUpdated, SpeechDeleted_.
`speechlist` Array it is the list of speech objects.


##### TopicEvent

    const socket = io('ws://meetapp.loc:6001', {
        'secure': true,
        'reconnect': true,
        'reconnection delay': 500,
        'max reconnection attempts': 10,
    });
    socket.emit('subscribe', {
        channel: 'private-topics.20',
        auth: {
            "headers": {"Authorization": 'Bearer qwerty123'}
        }
    });

    socket.on('App\\Events\\TopicEvent', function (channel, data) {
        console.log(channel);
        console.log(data);
    });


By using this code we will listen "TopicEvent" on "private-topics.20" channel, result of our listening will be:

    {
        action: "TopicUpdated"
        socket: null
        topic: {
            created_at: "2017-07-27 14:07:22"
            default_speech_settings: Object
            count_max: 3
            count_min: 1
            length: 5
            deleted_at: "2017-08-01 13:22:43"
            description: "sdasdasdasdasd"
            event: Object
            event_id: 1
            filesCount: 1
            id: 20
            name: "asdasdasdasdas"
            speechAvailable: true
            speech_length: Object
            speech_length_default: 300
            speech_opened: true
            speech_times: 3
            status: "discussion"
            updated_at: "2017-08-01 13:22:43"
            voting_available: false
            voting_end: null
            voting_results: Array(0)
            voting_settings: Object
            voting_start: null
            voting_time_left: "10:00"
            voting_type: "auto"
        }
    }

For _TopicEvent_ we have one type action: _TopicUpdated_.
`topic` Object of our topic.
