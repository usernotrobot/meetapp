<?php
use App\User;
use App\Repositories\EventRepository;
use App\Repositories\TopicRepository;

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/

/*Broadcast::channel('App.User.{id}', function ($user, $id) {
    return (int) $user->id === (int) $id;
});*/


Broadcast::routes(['middleware' => ['web']]);

Broadcast::channel('events.{eventId}', function (User $user, $event_id) {
  return $user->delegate(app(EventRepository::class)->get($event_id));
});
Broadcast::channel('topics.{topicId}', function (User $user, $topic_id) {
  return $user->delegate(app(TopicRepository::class)->get($topic_id)->event);
});
