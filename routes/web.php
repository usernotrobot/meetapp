<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', 'UserController@redirect');

// Authentication Routes...
$this->get('login', 'Auth\LoginController@showLoginForm')->name('login');
$this->post('login', 'Auth\LoginController@login');
$this->post('logout', 'Auth\LoginController@logout')->name('logout');

// Password Reset Routes...
$this->get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('pwd-forgot');
$this->post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('pwd-email');
$this->get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('pwd-reset');
$this->post('password/reset', 'Auth\ResetPasswordController@reset')->name('pwd-change');

Route::group(['middleware' => ['auth:web']], function () {
  /** User's stuff */
  Route::get('profile', 'UserController@edit')->name('profile');
  Route::put('profile', 'UserController@update')->name('profile.update');

  /** Files */
  Route::get('file/{file}', 'Admin\FileController@download')->name('files.download');
  Route::delete('file/{file}', 'Admin\FileController@destroy')->name('files.destroy');

  /** Admin stuff*/
  Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'as' => 'admin::', 'middleware' => ['role:admin']],
    function() {
      /** Clients */
      Route::resource('clients', 'ClientController');
      Route::post('clients/{client}/restore', 'ClientController@restore')->name('clients.restore');

      /** Events */
      Route::resource('events', 'EventController');
      Route::post('events/{event}/restore', 'EventController@restore')->name('events.restore');
      Route::post('events/{event}/status', 'EventController@status')->name('events.status');

      /* Event files */
      Route::get('events/{event}/files', 'EventController@files')->name('events.files');
      Route::post('events/{event}/upload', 'EventController@upload')->name('events.upload');

      /* Event delegates */
      Route::resource('events/{event}/delegates', 'DelegateController', ['only' => ['index', 'create', 'store']]);
      Route::post('events/{event}/delegates/upload', 'DelegateController@upload')->name('delegates.upload');
      Route::resource('delegates', 'DelegateController', ['except' => ['index', 'create', 'store']]);
      Route::delete('events/{event}/delegates', 'DelegateController@bulkDelete')->name('delegates.bulk-destroy');

      /** Headline topics */
      Route::resource('headlines/{headline}/topics', 'TopicController', ['only' => ['create', 'store']]);
      Route::get('headlines/{event}/topics', 'TopicController@indexHeadline')->name('topics.headline');
      Route::get('events/{event}/topics', 'TopicController@index')->name('topics.index');
      Route::resource('topics', 'TopicController', ['except' => ['index', 'create', 'store']]);
      Route::post('topics/{topic}/restore', 'TopicController@restore')->name('topics.restore');
      Route::post('topics/{topic}/status', 'TopicController@status')->name('topics.status');

      /** Event headlines */
      Route::resource('events/{event}/headlines', 'HeadlineController', ['only' => ['index', 'create', 'store']]);
      Route::resource('headlines', 'HeadlineController', ['except' => ['index', 'create', 'store']]);
      Route::post('headlines/{headline}/restore', 'HeadlineController@restore')->name('headlines.restore');
      Route::post('headlines/{headline}/status', 'HeadlineController@status')->name('headlines.status');
      Route::get('headlines/{headline}/files', 'HeadlineController@files')->name('headlines.files');
      Route::post('headlines/{headline}/upload', 'HeadlineController@upload')->name('headlines.upload');
      Route::post('headlines/{headline}/massupload', 'HeadlineController@massUpload')->name('headlines.massupload');
      Route::post('events/{event}/headlinesPosition', 'HeadlineController@updatePosition')->name('headlines.position');

      /* Topic files */
      Route::get('topics/{topic}/files', 'TopicController@files')->name('topics.files');
      Route::post('topics/{topic}/upload', 'TopicController@upload')->name('topics.upload');

      /* Topic Suggestions */
      Route::get('topics/{topic}/suggestions', 'SuggestionController@index')->name('suggestions.index');
      Route::get('topics/{topic}/suggestions/{suggestion}', 'SuggestionController@show')->name('suggestions.show');
    }
  );

  Route::group(['namespace' => 'User', 'as' => 'user::', 'middleware' => ['role:delegate']], function() {

    /* Current event -- redirect */
    Route::get('events/current/{params?}', 'EventController@current')->where('params', '.*');

    /* Other events */
    Route::get('events/{status?}', 'EventController@other')->name('events.all')
      ->where(['status' => '(past|upcoming|)']);

    /* Event Files */
    Route::get('events/{event}/files', 'EventController@files')->name('files');

    /* Event NOW page */
    Route::get('events/{event}/now', 'EventController@now')->name('now');
    Route::get('events/{event}/now-json', 'EventController@nowJson')->name('now-json');

    /** Topics */

    /* Event Agenda == Topics Index */
    Route::get('events/{event}', 'EventController@event')->name('event');
    Route::get('events/{event}/topics', 'TopicController@index')->name('agenda');

    /* Topic page */
    Route::get('events/{event}/topics/{topic}', 'TopicController@show')->name('topics.show');
    Route::get('events/{event}/topic/{topic}', 'TopicController@createNewBasedOnCurrent')->name('suggestions.moderation');
		Route::post('events/{event}/topic/{topic}/save', 'TopicController@moderationSave')->name('suggestions.moderation.save');


    /* Topic files*/
    Route::get('events/{event}/topics/{topic}/files', 'TopicController@files')->name('topics.files');
    Route::post('events/{event}/topics/{topic}/upload', 'TopicController@upload')->name('topics.upload');

    /* Topic settings */
    Route::get('events/{event}/topics/{topic}/settings', 'TopicController@settings')->name('topics.settings');

    Route::put('events/{event}/topics/{topic}/settings', 'TopicController@settingsUpdate')->name('topics.settings-update');

    /* Topic - update status */
    Route::post('events/{event}/topics/{topic}/status', 'TopicController@status')->name('topics.status');

    /** Voting */

    /* Start voting*/
    Route::post('events/{event}/topics/{topic}/voting/start', 'TopicController@startVoting')->name('topics.voting');

    /* Accepts vote from a delegate */
    Route::post('events/{event}/topics/{topic}/vote', 'TopicController@vote')->name('topics.vote');

    /* Auto Voting results */
    Route::post('events/{event}/topics/{topic}/voting/auto', 'TopicController@autoVoting')->name('topics.voting-auto');

    /* Manual Voting results */
    Route::post('events/{event}/topics/{topic}/voting/manual', 'TopicController@manualVoting')->name('topics.voting-manual');

    /* Cancel Voting */
    Route::post('events/{event}/topics/{topic}/voting/cancel', 'TopicController@cancelVoting')->name('topics.voting-cancel');

    /** Suggestions */

    /* Event suggestions (grouped by topic) */
    Route::get('events/{event}/suggestions', 'SuggestionController@event')->name('suggestions');

    /* Topic suggestions */
    Route::get('events/{event}/topics/{topic}/suggestions', 'SuggestionController@index')->name('topics.suggestions');

    /* My suggestions */
    Route::get('events/{event}/my-suggestions', 'SuggestionController@my')->name('my-suggestions');

    /** Suggestion management (delegate and moderator) */

    /* Create suggestion page */
    Route::get('events/{event}/topics/{topic}/suggestions/create', 'SuggestionController@create')->name('suggestions.create');

    /* Store */
    Route::post('events/{event}/topics/{topic}/suggestions', 'SuggestionController@store')->name('suggestions.store');

    /* Show */
    Route::get('events/{event}/topics/{topic}/suggestions/{suggestion}', 'SuggestionController@show')->name('suggestions.show');

    /* Edit page */
    Route::get('events/{event}/topics/{topic}/suggestions/{suggestion}/edit', 'SuggestionController@edit')->name('suggestions.edit');

    /* Update */
    Route::put('events/{event}/topics/{topic}/suggestions/{suggestion}', 'SuggestionController@update')->name('suggestions.update');

    /* Delete */
    Route::delete('events/{event}/topics/{topic}/suggestions/{suggestion}', 'SuggestionController@destroy')->name('suggestions.destroy');

    /** Speechlist */

    /* Topic speechlist */
    Route::get('events/{event}/topics/{topic}/speechlist', 'SpeechController@index')->name('topics.speechlist');

    /* Next speakers*/
    Route::get('events/{event}/topics/{topic}/next', 'SpeechController@next')->name('speechlist.next');

    /* My speechlist */
    Route::get('events/{event}/my-speechlist', 'SpeechController@my')->name('my-speechlist');

    /** Speech management */

    /* Get speakers */
    Route::get('events/{event}/topics/{topic}/delegates', 'SpeechController@delegates')->name('speechlist.delegates');

    /* Add to speechlist */
    Route::post('events/{event}/topics/{topic}/speechlist', 'SpeechController@store')->name('speechlist.store');

    /* Delete from speechlist */
    Route::delete('events/{event}/topics/{topic}/speechlist/{speech}', 'SpeechController@destroy')->name('speechlist.destroy');

    /* Mark as Last */
    Route::post('events/{event}/topics/{topic}/speechlist/{speech}/last', 'SpeechController@markLast')->name('speechlist.mark-last');

    /* Edit Time */
    Route::post('events/{event}/topics/{topic}/speechlist/{speech}/length', 'SpeechController@editLength')->name('speechlist.edit-length');

    /* Update Priority */
    Route::post('events/{event}/topics/{topic}/speechlist/{speech}/priority', 'SpeechController@updatePriority')->name('speechlist.update-priority');

    /* Status update */
    Route::put('events/{event}/topics/{topic}/speechlist/{speech}', 'SpeechController@update')->name('speechlist.update');

    /* Speech Start */
    Route::post('events/{event}/topics/{topic}/speechlist/{speech}/start', 'SpeechController@start')->name('speechlist.start');

    /* Speech Stop */
    Route::post('events/{event}/topics/{topic}/speechlist/{speech}/stop', 'SpeechController@stop')->name('speechlist.stop');

  });
});
