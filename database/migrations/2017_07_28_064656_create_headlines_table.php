<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHeadlinesTable extends Migration {

  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::create('headlines', function (Blueprint $table) {
      $table->increments('id');
      $table->integer('event_id');
      $table->string('name');
      $table->string('description');
      $table->softDeletes();
      $table->timestamps();

      $table->foreign('event_id')->references('id')->on('events');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::table('headlines', function ($table) {
      $table->dropForeign(['event_id']);
    });

    Schema::drop('headlines');
  }

}
