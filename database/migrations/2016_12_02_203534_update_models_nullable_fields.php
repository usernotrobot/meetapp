<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateModelsNullableFields extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::table('topics', function (Blueprint $table) {
      $table->string('description')->nullable()->change();
    });
    Schema::table('events', function (Blueprint $table) {
      $table->string('description')->nullable()->change();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::table('topics', function (Blueprint $table) {
      $table->string('description')->change();
    });
    Schema::table('events', function (Blueprint $table) {
      $table->string('description')->change();
    });
  }
}
