<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class VotingRefactoring extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::table('topics', function (Blueprint $table) {
      $table->jsonb('voting_settings')->default(
        json_encode(['type' => 'auto', 'length' => 600, 'privacy' => 'open', 'answers' => []])
      );
      $table->dateTime('voting_start')->nullable();
      $table->dateTime('voting_end')->nullable();
      $table->jsonb('voting_results')->default(json_encode([]));
    });

    Schema::table('votes', function (Blueprint $table) {
      $table->dropForeign('votes_topic_id_foreign');
      $table->foreign('topic_id')->references('id')->on('topics');
    });

    Schema::dropIfExists('votings');
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::table('topics', function (Blueprint $table) {
      $table->dropColumn(['voting_settings', 'voting_start', 'voting_end', 'voting_results']);
    });

    Schema::create('votings', function (Blueprint $table) {
      $table->integer('topic_id');
      $table->string('type')->default('auto');
      $table->unsignedSmallInteger('length')->default(600);
      $table->string('status')->default('planned');
      $table->string('privacy')->default('open');
      $table->dateTime('start')->nullable();
      $table->dateTime('end')->nullable();
      $table->jsonb('answers')->default('{}');
      $table->jsonb('results')->default('{}');

      $table->primary('topic_id');
      $table->foreign('topic_id')->references('id')->on('topics');

      $table->timestamps();
    });

    Schema::table('votes', function (Blueprint $table) {
      $table->dropForeign('votes_topic_id_foreign');
      $table->foreign('topic_id')->references('topic_id')->on('votings');
    });
  }
}
