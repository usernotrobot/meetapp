<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\User;

class AddRoles extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::table('users', function (Blueprint $table) {
      $table->enum('role', ['admin', 'moderator', 'secretary', 'delegate'])->default('delegate');
    });

    User::where('email', 'admin@mymeet.com')->update(['role' => 'admin']);
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::table('users', function ($table) {
      $table->dropColumn('role');
    });
  }
}
