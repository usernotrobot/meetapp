<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUniqKeyToDelegates extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::table('delegates', function (Blueprint $table) {
      $table->unique(['event_id', 'user_id'], 'user_per_event_idx');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::table('delegates', function (Blueprint $table) {
      $table->dropUnique('user_per_event_idx');
    });
  }
}
