<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSpeechLengthToSpeaches extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::table('speeches', function (Blueprint $table) {
      $table->unsignedSmallInteger('length')->default(300);
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::table('speeches', function (Blueprint $table) {
      $table->dropColumn('length');
    });
  }
}
