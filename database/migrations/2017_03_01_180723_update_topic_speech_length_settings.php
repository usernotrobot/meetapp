<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTopicSpeechLengthSettings extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::table('topics', function (Blueprint $table) {
      $table->renameColumn('speech_length', 'speech_length_default');
    });

    Schema::table('topics', function (Blueprint $table) {
      $table->json('speech_length')->default(json_encode([1 => 300, 2 => 300, 3 => 300]));
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::table('topics', function (Blueprint $table) {
      $table->dropColumn('speech_length');
    });

    Schema::table('topics', function (Blueprint $table) {
      $table->renameColumn('speech_length_default', 'speech_length');
    });

  }
}
