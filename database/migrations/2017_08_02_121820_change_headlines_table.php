<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeHeadlinesTable extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::table('headlines', function (Blueprint $table) {
      $table->integer('position')->default(0);
      $table->integer('last_updated_user_id')->nullable();
      $table->foreign('last_updated_user_id')->references('id')->on('users');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::table('headlines', function (Blueprint $table) {
      $table->dropColumn(['position', 'last_updated_user_id']);
    });
  }
}
