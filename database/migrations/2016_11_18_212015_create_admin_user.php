<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\User;

class CreateAdminUser extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    $user = new User();
    $user->name = 'Admin';
    $user->email = 'admin@mymeet.com';
    $user->password = bcrypt('admin');
    $user->save();
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    User::where('email', 'admin@mymeet.com')->delete();
  }
}
