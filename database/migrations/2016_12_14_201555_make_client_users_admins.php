<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use App\User;

class MakeClientUsersAdmins extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    User::where('client_id', '<>', null)->update(['role' => 'admin']);
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    User::where('client_id', '<>', null)->update(['role' => 'delegate']);
  }
}
