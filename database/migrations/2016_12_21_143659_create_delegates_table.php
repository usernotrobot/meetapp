<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateDelegatesTable extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::table('users', function (Blueprint $table) {
      $table->renameColumn('role', 'old_role');
    });

    Schema::table('users', function (Blueprint $table) {
      $table->string('role')->default('delegate');
    });

    DB::table('users')
      ->update(['role' => DB::raw('old_role')]);

    Schema::table('users', function ($table) {
      $table->dropColumn('old_role');
    });

    Schema::create('delegates', function (Blueprint $table) {
      $table->increments('id');
      $table->integer('event_id');
      $table->integer('user_id');
      $table->integer('participant_id')->nullable();
      $table->string('role')->default('delegate');

      $table->boolean('right_submit')->default(false);
      $table->boolean('right_speak')->default(false);
      $table->boolean('right_vote')->default(false);

      $table->integer('created_by');
      $table->timestamps();
    });

  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::drop('delegates');

    Schema::table('users', function (Blueprint $table) {
      $table->renameColumn('role', 'old_role');
    });

    Schema::table('users', function (Blueprint $table) {
      $table->enum('role', ['admin', 'moderator', 'secretary', 'delegate'])->default('delegate');
    });

    DB::table('users')
      ->update(['role' => DB::raw('old_role')]);

    Schema::table('users', function ($table) {
      $table->dropColumn('old_role');
    });

  }
}
