<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FixColumnNameForSpeeches extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::table('speeches', function (Blueprint $table) {
      $table->dropUnique(['topic_id', 'order']);

      $table->renameColumn('order', 'order_key');
    });

    /* Alert: PG-specific stuff! */
    DB::statement('alter table "speeches" add constraint "speeches_topic_id_order_key_unique" unique ("topic_id", "order_key") DEFERRABLE INITIALLY DEFERRED');
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::table('speeches', function (Blueprint $table) {
      $table->dropUnique(['topic_id', 'order_key']);

      $table->renameColumn('order_key', 'order');

      $table->unique(['topic_id', 'order']);
    });
  }
}
