<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTopicSpeechTimesSettings extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    DB::table('topics')
      ->where('speech_times', '>', 3)
      ->update(['speech_times' => 3]);
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    // There is nothing to do here
  }
}
