<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSuggestionsTable extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::create('suggestions', function (Blueprint $table) {
      $table->increments('id');

      $table->integer('topic_id');
      $table->integer('user_id');
      $table->string('summary');
      $table->text('details');

      $table->timestamps();
      $table->softDeletes();

      $table->foreign('topic_id')->references('id')->on('topics');
      $table->foreign('user_id')->references('id')->on('users');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::table('suggestions', function ($table) {
      $table->dropForeign(['topic_id']);
      $table->dropForeign(['user_id']);
    });

    Schema::dropIfExists('suggestions');
  }
}
