<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class AddUserDetails extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::table('users', function (Blueprint $table) {
      $table->string('first_name')->nullable();
      $table->string('last_name')->nullable();
      $table->string('image')->nullable();
      $table->string('union')->nullable();
      $table->string('union_sort_key')->nullable();
      $table->string('branch')->nullable();
      $table->string('branch_sort_key')->nullable();
      $table->string('delegate_id')->nullable();
    });

    DB::table('users')
      ->update(['first_name' => DB::raw('name')]);
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::table('users', function (Blueprint $table) {
      $table->dropColumn('first_name');
      $table->dropColumn('last_name');
      $table->dropColumn('union');
      $table->dropColumn('union_sort_key');
      $table->dropColumn('branch');
      $table->dropColumn('branch_sort_key');
      $table->dropColumn('delegate_id');
      $table->dropColumn('image');
    });
  }
}
