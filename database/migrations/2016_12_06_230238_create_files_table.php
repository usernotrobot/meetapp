<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFilesTable extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::create('files', function (Blueprint $table) {
      $table->increments('id');
      $table->integer('owner_id');
      $table->integer('entity_id');
      $table->string('entity_type', 30);
      $table->string('name');
      $table->string('original_name');
      $table->string('path');
      $table->timestamps();

      $table->index('path', 'path_idx');
      $table->foreign('owner_id')->references('id')->on('users');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::table('files', function ($table) {
      $table->dropIndex(['path_idx']);
      $table->dropForeign(['owner_id']);
    });

    Schema::drop('files');
  }
}
