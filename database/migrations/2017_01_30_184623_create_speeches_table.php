<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpeechesTable extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::create('speeches', function (Blueprint $table) {
      $table->increments('id');

      $table->integer('delegate_id');
      $table->integer('topic_id');
      $table->string('status', 30)->default('waiting');
      $table->unsignedSmallInteger('order')->default(0);
      $table->dateTime('start')->nullable();
      $table->dateTime('end')->nullable();

      $table->timestamps();

      $table->unique(['topic_id', 'order']);

      $table->foreign('delegate_id')->references('id')->on('delegates');
      $table->foreign('topic_id')->references('id')->on('topics');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::table('speeches', function ($table) {
      $table->dropForeign(['topic_id']);
      $table->dropForeign(['delegate_id']);
    });

    Schema::dropIfExists('speeches');
  }}
