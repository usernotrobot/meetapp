<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveOutdatedColumnsFromUsers extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::table('users', function ($table) {
      $table->dropColumn('name');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::table('users', function ($table) {
      $table->string('name')->nullable();
    });

    // PG-specific sorting
    if (env('DB_CONNECTION') == 'pgsql') {
      DB::table('users')
        ->update(['name' => DB::raw('COALESCE(first_name, \'\')|| COALESCE(last_name, \'\')')]);
    } else {
      DB::table('users')
        ->update(['name' => DB::raw('first_name')]);
    }

  }
}
