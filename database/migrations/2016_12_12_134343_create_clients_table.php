<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsTable extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::create('clients', function (Blueprint $table) {
      $table->increments('id');
      $table->string('name');
      $table->string('description')->nullable();

      $table->softDeletes();
      $table->timestamps();
    });

    Schema::table('users', function (Blueprint $table) {
      $table->integer('client_id')->nullable();

      $table->foreign('client_id')->references('id')->on('clients');
    });

    Schema::table('events', function (Blueprint $table) {
      $table->integer('client_id')->nullable();

      $table->foreign('client_id')->references('id')->on('clients');
    });

  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::table('users', function ($table) {
      $table->dropForeign(['client_id']);

      $table->dropColumn('client_id');
    });

    Schema::table('events', function ($table) {
      $table->dropForeign(['client_id']);

      $table->dropColumn('client_id');
    });

    Schema::drop('clients');
  }
}
