<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddVotesUniqness extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::table('votes', function (Blueprint $table) {
      $table->unique(['topic_id', 'delegate_id']);
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::table('votes', function (Blueprint $table) {
      $table->dropUnique(['topic_id_delegate_id']);
    });
  }
}
