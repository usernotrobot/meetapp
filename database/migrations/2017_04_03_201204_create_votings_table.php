<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVotingsTable extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::create('votings', function (Blueprint $table) {
      $table->integer('topic_id');
      $table->string('type')->default('auto');
      $table->unsignedSmallInteger('length')->default(600);
      $table->string('status')->default('planned');
      $table->string('privacy')->default('open');
      $table->dateTime('start')->nullable();
      $table->dateTime('end')->nullable();
      $table->jsonb('answers')->default('{}');
      $table->jsonb('results')->default('{}');

      $table->primary('topic_id');
      $table->foreign('topic_id')->references('id')->on('topics');

      $table->timestamps();
    });

    Schema::create('votes', function (Blueprint $table) {
      $table->increments('id');
      $table->unsignedInteger('topic_id');
      $table->unsignedInteger('delegate_id');
      $table->string('vote');

      $table->foreign('delegate_id')->references('id')->on('delegates');
      $table->foreign('topic_id')->references('topic_id')->on('votings');
    });

  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::dropIfExists('votes');

    Schema::dropIfExists('votings');
  }
}
