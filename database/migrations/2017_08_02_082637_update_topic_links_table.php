<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTopicLinksTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('topics', function (Blueprint $table) {
      $table->integer('position')->default(0);
      $table->integer('headline_id')->default(0);
    });

    /* Now we need to create helper headlines to reparent existing
     * topics to it. So first of all we get all the events in the DB. */
    $events = DB::table('events')
            ->select('id', 'name')
            ->get();

    /* Then make a helper headline with the same name as the event
     * was and with corresponding description */
    $headlines = $events->each(function ($row) {
      DB::insert("INSERT INTO headlines (event_id,name,description)
                  VALUES (:event_id,:name,:description)", [
        ':event_id' => $row->id,
        ':name' => $row->name,
        ':description' => 'Created automatically on migraion. Please sort this out',
      ]);
    });

    /* Then reparent all the existing topics to the helper headlines
     * we just created. I have to write native query here since
     * there's no way to write UPDATE + JOIN in the artisan migration
     * syntax. */
    DB::update("UPDATE topics SET headline_id = headlines.id
                FROM headlines WHERE headlines.event_id = topics.event_id");


    /* Due to limitation of Doctrine have to use RAW query here */
    DB::unprepared("ALTER TABLE topics ALTER COLUMN headline_id DROP DEFAULT");

    /* And now we can drop away old event_id link and switch to
     * headline_id.  */
    Schema::table('topics', function (Blueprint $table) {
      $table->dropColumn(['event_id']);

      $table->foreign('headline_id')->references('id')->on('headlines');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('topics', function (Blueprint $table) {
      $table->integer('event_id');
      $table->foreign('event_id')->references('id')->on('events');
    });

    DB::update("UPDATE topics SET event_id = headlines.event_id
                FROM headlines WHERE headlines.id = headline_id");

    Schema::table('topics', function (Blueprint $table) {
      $table->dropColumn(['headline_id', 'position']);
    });
  }
}
