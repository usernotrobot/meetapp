<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use Illuminate\Support\Facades\DB;

class AddTopicSettings extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::table('topics', function (Blueprint $table) {
      $table->unsignedSmallInteger('speech_times')->default(3);
      $table->unsignedSmallInteger('speech_length')->default(300);
      $table->boolean('speech_opened')->default(true);
    });

    DB::table('topics')
      ->update(['speech_opened' => true]);
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::table('topics', function (Blueprint $table) {
      $table->dropColumn('speech_times');
      $table->dropColumn('speech_length');
      $table->dropColumn('speech_opened');
    });
  }
}
